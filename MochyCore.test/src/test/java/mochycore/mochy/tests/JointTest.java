package mochycore.mochy.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.jupiter.api.Test;

import fr.inria.mochy.core.RPN.RPNConfig;
import fr.inria.mochy.core.RPN.RegulNet;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.TimeTable;

public class JointTest {
	@Test
	/**test the joint simulation*/
	void jointSim() {
		ClassLoader classLoader = getClass().getClassLoader();
		
		//load the net
		File file = new File(classLoader.getResource("JointNet3.net").getFile());
		Sim simu = null;
		try {
			simu = new Sim(0,file.getAbsolutePath(), "test.txt", false, "Net");
		} catch (FileNotFoundException | ClassNotFoundException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		simu.getN().loadFile();
		
		//load the tt
		file = new File(classLoader.getResource("JointTable3.tt").getFile());
		TimeTable timeTable = new TimeTable(file.getAbsolutePath());
		timeTable.fileLoading();
		TTConfig ttConfig = new TTConfig(timeTable);
		ttConfig.init("", false);
		
		//load the correspondance file and set up the regulnet
		file = new File(classLoader.getResource("Corr3.cor").getFile());
		RegulNet rn = new RegulNet(timeTable, (Net) simu.getN());
		rn.LoadCorrespondance(file.getAbsolutePath());
		rn.buildCorrespondance();
		RPNConfig rpnc = new RPNConfig((Net) simu.getN(), ttConfig, "", false);
		
		//test the load of the correspondance file
		assertEquals("1 ", timeTable.getEvent(1).getTransitions());
		
		//test the advance of time : the transition t1 must be firable
		float remTime = rpnc.maxTimedMove();
		rpnc.advanceTime(remTime);
		//remTime = rpnc.maxTimedMove();
		//rpnc.advanceTime(remTime);
		//assertEquals("fireable", simu.getN().findTransition(1).getDistribution());
		assertEquals("t1", rn.getNet().getFirable().get(1).getName());
		
		//test a move : the transition t2 must be enabled and event 1 must be realized 
		Transition t = (Transition) rn.getNet().getFirable().get(1);
		TableEvent cte = null;
		for (TableEvent te : ttConfig.getMinevents()) {
			if (te.realizedBy(t.getNumber())) { // event realized by transition t
				// remember this event
				cte = te;
				break;
			}
		}
		Boolean tcefired = false;
		if (cte != null) {
			if (cte.getDate() <= rpnc.getCurrentTime())
				tcefired = ttConfig.discreteMoveTTPN(cte.getNumber());
		}
		if (tcefired)
			rn.getNet().discreteMove(t, "", rpnc.getCurrentTime(), false);
		assertEquals("t2", rn.getNet().getEnabled().get(2).getName());
		assertEquals(true, timeTable.getEvent(1).getRealized());
	}
}
