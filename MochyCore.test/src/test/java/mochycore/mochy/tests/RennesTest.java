package mochycore.mochy.tests;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.jupiter.api.Test;

import fr.inria.mochy.core.RPN.RPNConfig;
import fr.inria.mochy.core.RPN.RegulNet;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.timetable.TTChecker;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TimeTable;
//import fr.inria.mochy.ui.MultipleRunsController;
//import fr.inria.mochy.ui.StartController;

public class RennesTest {
	@Test
	/**test the joint simulation*/
	void jointSim() {
		ClassLoader classLoader = getClass().getClassLoader();
		
		//load the net
		File file = new File(classLoader.getResource("rennesTotal.net").getFile());
		Sim simu = null;
		try {
			simu = new Sim(0,file.getAbsolutePath(), "test.txt", false, "Net");
		} catch (FileNotFoundException | ClassNotFoundException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		simu.getN().loadFile();
		
		//load the tt
		file = new File(classLoader.getResource("rennesTotal.tt").getFile());
		TimeTable timeTable = new TimeTable(file.getAbsolutePath());
		timeTable.fileLoading();
		TTChecker ttChecker = new TTChecker(timeTable);
		//ttChecker.hasNoCycle();//+ 39 seconds
		ttChecker.verifiedConstraints();
		TTConfig ttConfig = new TTConfig(timeTable);
		ttConfig.init("", false);
		
		//load the correspondance file and set up the regulnet
		file = new File(classLoader.getResource("rennesTotal.cor").getFile());
		RegulNet rn = new RegulNet(timeTable, (Net) simu.getN());
		rn.LoadCorrespondance(file.getAbsolutePath());
		rn.buildCorrespondance();
		RPNConfig rpnc = new RPNConfig((Net) simu.getN(), ttConfig, "", false);
		
		/*StartController.setSimu(simu);
		StartController.setTtConfig(ttConfig);
		StartController.setRn(rn);
		StartController.setRpnc(rpnc);
		StartController.setConnectTT(true);
		/*for (int i=0; i<1000; i++)
			StartController.globalMove();*/
		/*MultipleRunsController multipleRuns = new MultipleRunsController();
		multipleRuns.run();//run fails to be achieved in a normal time*/
	}		
}
