package mochycore.mochy.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.inria.mochy.core.RPN.RPNConfig;
import fr.inria.mochy.core.RPN.RegulNet;
import fr.inria.mochy.core.dom.Dom;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.timetable.TTChecker;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.TimeTable;

public class SimTest {

	@Test
	void oneStepReseau() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("reseau.net").getFile());
		String netFileUrl = file.getAbsolutePath();
		Sim simu = null;
		try {
			simu = new Sim(0, netFileUrl, "test.txt", false, "Net");
		} catch (FileNotFoundException | ClassNotFoundException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		simu.getN().loadFile();
		ArrayList<Dom> domList = new ArrayList<>();
		
		domList = simu.oneStep();
		assertEquals(6, domList.size());
	}

	@Test
	void intervalClock() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("reseau5.net").getFile());
		String netFileUrl = file.getAbsolutePath();
		Sim simu = null;
		try {
			simu = new Sim(0,netFileUrl, "test.txt", false, "Net");
		} catch (FileNotFoundException | ClassNotFoundException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<Dom> domList = new ArrayList<>();
		
		simu.getN().loadFile();
		//the first step make the t1 fireable
		domList = simu.oneStep();
		//the second step fire the t1
		domList = simu.oneStep();

		for (Dom d: domList) {
			//p2 is now marked
			if (d.getName().equals("p2")) {
				assertEquals("marked",d.getContent());
			}
			//the transition t2 with p2 as preset is now enable
			//and a clock between the bounds is set randomly
			if (d.getName().equals("t2")) {
				assertEquals(true, Float.valueOf(d.getClock())>=d.getLowerBound() && Float.valueOf(d.getClock())<=d.getUpperBound());
			}
		}
	}
}
