package mochycore.mochy.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

import fr.inria.mochy.core.timetable.TTChecker;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.TimeTable;

public class TimeTableTest {
	@Test
	/**load the timetable file and check that an event and a dependency has been add*/
	void timeTableLoad() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("timetable.tt").getFile());
		String fileUrl = file.getAbsolutePath();
		TimeTable timeTable = new TimeTable(fileUrl);
		timeTable.fileLoading();
		
		TableEvent e1 = timeTable.getEvents().get(1);
		assertEquals("e1", e1.getLabel());
		TableDependency d = timeTable.getDependencies().get(0);
		assertEquals(150, d.getDuration());
		assertEquals("e2", e1.getSuccessors().get(0).getLabel());
		TableEvent e2 = timeTable.getEvents().get(2);
		assertEquals("e1", e2.getPredecessors().get(0).getLabel());
	
		assertEquals(2, timeTable.nextEvents(1).get(0));
		assertEquals(1, timeTable.PredecessorEvents(2).get(0));
		
		assertEquals(150, timeTable.DependenciesFromEvent(1).get(0).getDuration());
		
		assertEquals(1, timeTable.minList().size());
		assertEquals("e1", timeTable.minList().get(0).getLabel());
		
		timeTable.getEvent(1).setRealized(true);
		assertEquals(1, timeTable.minListUnexecuted().size());
		assertEquals("e2", timeTable.minListUnexecuted().get(0).getLabel());
	
		timeTable.PropagateDelay(2, 150);
		assertEquals(250, timeTable.getEvent(2).getDate());
		
		assertEquals(1, timeTable.minList().size());
		assertEquals("e1", timeTable.minList().get(0).getLabel());
		
	}
	
	@Test
	/**test the TTConfig*/
	void ttConfig() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("timetable3.tt").getFile());
		String fileUrl = file.getAbsolutePath();
		TimeTable timeTable = new TimeTable(fileUrl);
		timeTable.fileLoading();
		TTConfig ttConfig = new TTConfig(timeTable);
		ttConfig.init("", false);
		
		int nbEvent = ttConfig.getMinevents().get(0).getNumber();
		ttConfig.timeMove(100);
		ttConfig.discreteMove(nbEvent);
		assertEquals(true, timeTable.getEvent(nbEvent).getRealized());
		
	}
	
	@Test
	void tagsEvents() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("timetable4-Tags.tt").getFile());
		String fileUrl = file.getAbsolutePath();
		TimeTable timeTable = new TimeTable(fileUrl);
		timeTable.fileLoading();
		
		assertEquals("a",timeTable.getEvent(1).getTags().get(0));
		assertEquals("b",timeTable.getEvent(1).getTags().get(1));
	}
	
	@Test
	/**test the propagation of a delay from a first event to a second*/
	void propagateDelay() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("ProblemTable3.tt").getFile());
		String fileUrl = file.getAbsolutePath();
		TimeTable timeTable = new TimeTable(fileUrl);
		timeTable.fileLoading();
		TTConfig ttConfig = new TTConfig(timeTable);
		ttConfig.init("", false);
		
		ttConfig.timeMove(150);
		ttConfig.discreteMove(1);
		
		assertEquals(310, timeTable.getEvents().get(2).getDate());
	}
	
	@Test
	/**check the constraints and cycle detection*/
	void ttChecker (){
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("timetable3.tt").getFile());
		String fileUrl = file.getAbsolutePath();
		TimeTable timeTable = new TimeTable(fileUrl);
		timeTable.fileLoading();
		TTChecker ttChecker = new TTChecker (timeTable);
		
		file = new File(classLoader.getResource("timetable2.tt").getFile());
		fileUrl = file.getAbsolutePath();
		TimeTable timeTable2 = new TimeTable(fileUrl);
		timeTable2.fileLoading();
		TTChecker ttChecker2 = new TTChecker (timeTable2);
		
		assertEquals(true, ttChecker.verifiedConstraints());
		assertEquals(true, ttChecker.hasNoCycle());
		assertEquals(false, ttChecker2.verifiedConstraints());
		assertEquals(false, ttChecker2.hasNoCycle());
	}
	
}
