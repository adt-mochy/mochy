package mochycore.mochy.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.dom.Dom;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.sampler.Sampler;
import fr.inria.mochy.core.trajectory.Point;
import fr.inria.mochy.core.trajectory.Segment;
import fr.inria.mochy.core.trajectory.TrajPlace;
import fr.inria.mochy.core.trajectory.TrajTransition;
import fr.inria.mochy.core.trajectory.Trajectory;
import fr.inria.mochy.core.trajectory.TrajectoryNet;

public class TrajectoryTest {

	Sampler sampler = new Sampler();
	
	@Test
	/**test the cross of two trajectory with one segment each*/
	void trajectoryCross(){
		Trajectory trajectory1 = new Trajectory();
		Segment segment1 = new Segment(new Point(0, 1000), new Point(90, 0));
		trajectory1.addSegment(segment1);
		trajectory1.setHeadway(200);
		
		//trajectory 2 start after trajectory1 but arrive before, so there is a cross
		Trajectory trajectory2 = new Trajectory();
		Segment segment2 = new Segment(new Point(0, 1600), new Point(80, 0));
		trajectory2.addSegment(segment2);
		
		//check that the segments cross
		Float x = segment1.cross(segment2);
		assertTrue(x > 0 && x < 80);
		
		//check that trajectory2 is blocked by trajectory1 at x in the interval set
		x = trajectory2.isBlockedBy(trajectory1);
		assertTrue(x > 0 && x < 80);
		
		//adapt the trajectory 2 to the trajectory 1 and its headway
		trajectory2.truncateFrom(x);
		trajectory2.followFrom(x, trajectory1);
		
		//recover its traject from x = 100
		trajectory2.leftShift(90);
		trajectory2.recover();
		
		System.out.println("value : "+trajectory2.ordinate(0));
		for (Segment segment : trajectory2.getSegments())
			segment.drop();
		
		//check that when trajectory1 is to y = 0, the trajectory2 is near (<0.01) to y = trajectory1.headway
		assertEquals(trajectory1.getHeadway(), trajectory2.ordinate(0), 0.01);
		
		//check that trajectory2 recover from its traject from x=100 to a 0 value
		assertTrue(trajectory2.ordinate(5) < trajectory1.getHeadway());
	}
	
	@Test
	void loadFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("reseauTraj.net").getFile());
		String netFileUrl = file.getAbsolutePath();
		Sim simu = null;
		try {
			simu = new Sim(0, netFileUrl, "test.txt", false, "TrajectoryNet");
		} catch (FileNotFoundException | ClassNotFoundException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		simu.getN().loadFile();
		ArrayList<Dom> domList = new ArrayList<>();
		TrajectoryNet n = (TrajectoryNet) simu.getN();
		
		domList = simu.oneStep();
		assertEquals(6, domList.size());
	}
	
	@Test
	void placeAvailable() {
		TrajPlace place = new TrajPlace("p", 1, 1000f, 200f);
		TrajTransition transition = new TrajTransition("t", 1, 200f, 200f);
		place.setPost(transition);
		place.createTrajectory(sampler);
		
		//as a trajectory has been created, there should not be available place for
		//a new trajectory
		assertEquals(false, place.availableForNewPlace());
		
		for (Trajectory trajectory : place.trajectories) {
			trajectory.leftShift(100);
		}
		
		//as the first trajectory has been shifted, there should be place available
		//for a new trajectory
		assertEquals(true, place.availableForNewPlace());
	}
	
	@Test
	void placeUsable() {
		TrajPlace place = new TrajPlace("p", 1, 1000f, 200f);
		TrajTransition transition = new TrajTransition("t", 1, 200f, 200f);
		place.setPost(transition);
		transition.setPre(place);
		place.createTrajectory(sampler);
		TrajectoryNet n = new TrajectoryNet("");
		n.places.put(1, place);
		n.transitions.put(1, transition);
		
		//as the new trajectory ends at (200,0), it is usable from 200 only
		
		n.progressTime(100f);
		assertEquals(true, place.usableTrajectory() == null);
		assertEquals(false, place.trajectories.get(0).isSinglePoint());
		
		n.progressTime(100f);
		assertEquals(true, place.usableTrajectory() != null);
		assertEquals(true, place.usableTrajectory().isSinglePoint());
		
		n.progressTime(100f);
		assertEquals(true, place.usableTrajectory() != null);
		assertEquals(true, place.usableTrajectory().isSinglePoint());
		
	}
	
	@Test
	//test that a trajectory created in a place with a (0,0)-(0,0) single point trajectory
	//respect the headway
	void placeWithSinglePoint() {
		float headway = 100f;
		
		//prepare the net
		TrajPlace place = new TrajPlace("p", 1, 1000f, headway);
		TrajTransition transition = new TrajTransition("t", 1, 50f, 50f);
		place.setPost(transition);
		transition.setPre(place);
		TrajectoryNet n = new TrajectoryNet("");
		n.places.put(1, place);
		n.transitions.put(1, transition);
		
		//add a (0,0)-(0,0) traj to the place
		ArrayList<Segment> segments = new ArrayList<>();
		segments.add(new Segment(0,0,0,0));
		Trajectory singlePointTraj = new Trajectory(segments);
		singlePointTraj.setHeadway(headway);
		place.trajectories.add(singlePointTraj);
		
		//create a new trajectory in the place
		place.createTrajectory(sampler);
		
		//as there is a single point in the place, the new trajectory must
		//be at y = headway even when its time is completed
		Trajectory upwardShiftTraj = singlePointTraj.upwardShift();
		System.out.print("*****");
		upwardShiftTraj.getSegments().get(0).drop();
		assertTrue(upwardShiftTraj.getSegments().get(0).getP1().getY() == headway);
		assertTrue(place.trajectories.get(1).isBlockedBy(singlePointTraj) != null);
		n.progressTime(50f);
		assertEquals(0, place.trajectories.get(0).ordinate(0));
		assertEquals(headway, place.trajectories.get(1).ordinate(0));
	}
}
