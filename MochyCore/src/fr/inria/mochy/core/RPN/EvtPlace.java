package fr.inria.mochy.core.RPN;


import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.timetable.TableEvent;


/**
 * this pair makes links between timetable events and places
 * to allow transitions of nets at dates predicted by timetables
 * <p>
 * <img src="EvtPlace.svg" alt="EvtPlace class diagram" style="float: right;">
 */
public class EvtPlace{
	/**An event in the table*/
	TableEvent te; 
	
	/**a place p that is filled as soon as the current date is te's date*/
	PlaceAbstract p; 
	
	/**instantiate a pair of an event and a place*/
	EvtPlace(TableEvent nte,PlaceAbstract npl){
		
		this.te = nte;
		this.p = npl;
		
	}

	
	
}