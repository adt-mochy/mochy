package fr.inria.mochy.core.RPN;


import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.timetable.TableEvent;


/**
 * this pair makes links between timetable events and transitions of the net model
 * it allows to wait to realize an event as the transition linked is not fired.
 * <p>
 * <img src="EvtTrans.svg" alt="EvtTrans class diagram" style="float: right;">
 */
public class EvtTrans{
	/**an event*/
	TableEvent te;
	
	/**a transition*/
	TransitionAbstract t;
	
	/**instantiate a pair of an event and a transition*/
	EvtTrans(TableEvent nte, TransitionAbstract nt){
		
		this.te = nte;
		this.t = nt;
		
	}
	
}