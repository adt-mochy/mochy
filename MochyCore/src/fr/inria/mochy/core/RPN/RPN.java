package fr.inria.mochy.core.RPN;

import java.util.ArrayList;
import java.util.HashMap;

import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.mochysim.Net;

import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.TimeTable;



// Class for regulated Petri nets, i.e. Petri nets with 

public class RPN{
	
	TimeTable rptt; // The timetable used to control the regulated Petri net.
	Net rpnet; // The Petri net givint the physicla descripton of the trnasport system. 
	
	//ArrayList<EvtTrans> Awaits; // list of pairs of  the form <TableEvent,Transition> 
// this correspondance table works as follows : if an entry of the form (te,t) appears in the table 
	// then table event te is the an event that may correpsond to a physical event represented by transition t
	// (for trains an arrival). 
	// hence,  the event has to be executed at the same date as SOME transition t such that (t,te) is in the table
	// Note : there can be several transitions "implementing an event"
	
	//ArrayList<EvtPlace> Allows; // list of pairs of  the form <TableEvent,Place> 
   // This correspondance works as follows : if a pair (te,p) appears in the list, then, as soon as the table event te 
	// is feasible, place p receives a token
	// Note that p may appear in several pairs, i.e. if the same transition is repeated k times, it is allowed by k events 
	// A priori, in the model considered for trains, the place is a control place
	
	
	
	/**
	 * Constructor for an RPN
	 */
	RPN(TimeTable t, Net n){
		
		this.rptt = t;
		this.rpnet = n;
		//Awaits = new ArrayList<EvtTrans>();
		//Allows = new ArrayList<EvtPlace>();
		
	}
	
	/**
	 * Creates the correpsondance between 
	 */
	/*void buildCorrespondance() {
		// visit every timetable event
		// for every te, look at the list of trnasition names t1,...tk, and 
		// add pairs (te,t1)...(te,tk)
		
		HashMap<Integer, TableEvent> evts=this.rptt.getEvents();
		
		for ( Integer key : evts.keySet()) {
			TableEvent te =evts.get(key);
			for (Integer tname : te.getTransNames() ) {
				// for each tname
				// find the transition 
				Transition t=rpnet.findTransition(tname);
				Awaits.add(new EvtTrans(te,t));				
			}
		}		
	}*/
	
}