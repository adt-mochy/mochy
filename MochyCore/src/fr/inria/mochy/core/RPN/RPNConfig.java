package fr.inria.mochy.core.RPN;

import java.util.ArrayList;

import fr.inria.mochy.core.RPN.*;
import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.mochysim.Delay;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.mochysim.Place;

import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.timetable.TTConfig;

import fr.inria.mochy.core.timetable.TimeTable;
import fr.inria.mochy.core.timetable.TableEvent;

/**
 * Used to control the RegulNet model : the link between a timetable and a net
 * <p>
 * <img src="RPNConfig.svg" alt="RPNConfig class diagram" style="float: right;">
 */
public class RPNConfig {

	RegulNet NetAndTT; // The regulated net that is simulated

	TTConfig currentTTConfig; // The current configuration of the timetable
	PhysicalModel currentNetConfig; // The current configuration of the Net
	private float currentTime; // the current time elapsed

	String pathLogs;
	boolean enableLogs;
	
	ArrayList<Delay> delays = new ArrayList<>(); // the delays to add to specific transition(s)
												//at a specific time

	public RPNConfig(PhysicalModel rp, TTConfig ttc, String pathLogs, boolean enableLogs) {
		NetAndTT = new RegulNet(ttc.getTable(), rp);

		currentTTConfig = ttc; // create a configuration (current date equals 0)
		currentTTConfig.init(pathLogs, enableLogs); // initializes it (minimal events are computed)

		this.pathLogs = pathLogs;
		this.enableLogs = enableLogs;

		currentNetConfig = rp;
		setCurrentTime(0);
	}

	/**
	 * Rule to fire a transition and jointly a table event transition t is supposed
	 * firable
	 * 
	 * @param t
	 * @return the infos about the discreteMove of the net
	 */
	public String fireTransition(Transition t) {

		// fire the transition in the net
		String infos = currentNetConfig.discreteMove(t, pathLogs, getCurrentTime(), enableLogs);

		// find the node to execute at current date (if any)
		for (TableEvent te : currentTTConfig.getMinList()) {

			// if the transition is in the list of te then execute te at current time
			for (Integer n : te.getTransnames()) {
				if (t.getNumber() == n) {
					currentTTConfig.discreteMove(n);
					// NB: the current time of the timetable is supposed up to date
				}

			}
		}

		return infos;
	}

	/**
	 * 
	 * @param te the (minimal event in the table that has reached its date
	 * @return true if eveything worked
	 */
	public Boolean fillPlace(TableEvent te) {

		PhysicalModel rp = NetAndTT.getNet();

		for (Integer p : te.getPlaceNames()) {

			rp.addToken(p);// add one token in place p

		}
		return true;// no error management so far
	}

	// Advance Time
	/**
	 * Advance time for the net and the timetable by some duration val
	 * 
	 * @param val : the considered duration
	 * @return : true if val is a legal time move, false otherwise
	 */
	public float advanceTime(float duration) {
		manageDelays();
		System.out.println("Current Time: " + getCurrentTime());

		Float deltanet = currentNetConfig.maxAllowedTimedMove();
		System.out.println("Net Max Time: " + deltanet);

		float deltaTT = currentTTConfig.maxAllowedTime(getCurrentTime());
		System.out.println("TT Max Time: " + deltaTT);

		/*boolean match = false;
		float minDelay = -1;
		for (Transition t : currentNetConfig.getFirable()) {
			for (TableEvent te : currentTTConfig.getMinList()) {
				System.out.println("event:"+te.getLabel());
				if (te.realizedBy(t.getNumber()) && te.getDate() > getCurrentTime()) {
					match = true;
					
					if(minDelay < 0)
						minDelay = te.getDate() - getCurrentTime();
					else
						minDelay = Math.min(minDelay, te.getDate() - getCurrentTime());
				}
			}
		}
		System.out.println("MATCH : "+match);
		System.out.println("duration : "+minDelay);
		if (match) {//if events in the min list linked to a transition is found, we advance to the minimum delay
			currentNetConfig.progressTime(minDelay);
			currentTTConfig.timeMove(minDelay, currentNetConfig);
			setCurrentTime(getCurrentTime() + minDelay);
			return true;
		}*/

		// min clock of match enabled trans/event dates
		float minDuration = -1;
		for (TransitionAbstract t : currentNetConfig.fireableTransition()) {
			for (TableEvent te : t.getLinkedEvents()) {
				if (currentTTConfig.getMinList().contains(te) && te.getDate() > getCurrentTime()){
					if (minDuration == -1)
						minDuration = te.getDate() - getCurrentTime();
					else
						minDuration = Math.min(te.getDate() - getCurrentTime(), minDuration);
					/*currentNetConfig.progressTime(duration);
					currentTTConfig.timeMove(duration, currentNetConfig);
					setCurrentTime(getCurrentTime() + duration);
					return true;*/
				}/*else if (te.realizedBy(t.getNumber()) && t.getClock() > 0) {
					if (minDuration == -1)
						minDuration = t.getClock();
					else
						minDuration = Math.min(minDuration, t.getClock());
				}*/
			}
		}
		/*if (minDuration != -1) {
			currentNetConfig.progressTime(minDuration);
			currentTTConfig.timeMove(minDuration, currentNetConfig);
			setCurrentTime(getCurrentTime() + minDuration);
			return true;
		}*/
		/*for (Transition t : currentNetConfig.getEnabled()) {
			for (TableEvent te : currentTTConfig.getMinList()) {
				if (te.realizedBy(t.getNumber()) && te.getDate() > getCurrentTime()
						&& (te.getDate() - getCurrentTime()) > t.getClock()) {
					System.out.println("===1===");
					duration = te.getDate() - getCurrentTime();
					currentNetConfig.progressTime(duration);
					currentTTConfig.timeMove(duration, currentNetConfig);
					setCurrentTime(getCurrentTime() + duration);
					return true;
				} else if (te.realizedBy(t.getNumber()) && te.getDate() > getCurrentTime()) {
					System.out.println("===2===");
					duration = t.getClock();
					currentNetConfig.progressTime(duration);
					currentTTConfig.timeMove(duration, currentNetConfig);
					setCurrentTime(getCurrentTime() + duration);
					return true;
				}
			}
		}*/
		//minDuration = -1;
		for (TransitionAbstract t : currentNetConfig.getEnabled().values()) {
			for (TableEvent te : t.getLinkedEvents()) {
				if (currentTTConfig.getMinList().contains(te) && te.getDate() > getCurrentTime()
						&& (te.getDate() - getCurrentTime()) > t.getClock()) {
					if (minDuration == -1)
						minDuration = te.getDate() - getCurrentTime();
					else 
						minDuration = Math.min(minDuration, te.getDate() - getCurrentTime());
				} else if (currentTTConfig.getMinList().contains(te)) {
					if (minDuration == -1)
						minDuration = t.getClock();
					else
						minDuration = Math.min(minDuration, t.getClock());
				}
			}
		}
		if (minDuration != -1) {
			currentNetConfig.progressTime(minDuration);
			currentTTConfig.timeMove(minDuration, currentNetConfig);
			setCurrentTime(getCurrentTime() + minDuration);
			manageDelays();
			System.out.println("minDuration : "+minDuration);
			return minDuration;
		}

		if (duration > deltanet) { // if the proposed duration is more that the time
			// allowed by the net
			return -1;
		}

		if (deltaTT < 0) { // If the timetable can let time progress as much as possible
			// then we take the duration allowed by the net
			currentNetConfig.progressTime(duration);
			currentTTConfig.timeMove(duration, currentNetConfig);
			setCurrentTime(getCurrentTime() + duration);
			manageDelays();
			return duration;
		}

		if (duration > deltaTT) { // if the proposed duration is more that the time
			// allowed by the timetable
			return -1;
		}
		currentNetConfig.progressTime( duration);
		currentTTConfig.timeMove(duration);
		setCurrentTime(getCurrentTime() + duration);
		manageDelays();
		return duration;

	}

	/**
	 * Progress time for a duration that is guaranteed to contain no discrete 
	 * event .
	 * @param duration
	 */
	public void advanceVerifiedTime(float duration){
		currentNetConfig.progressTime( duration);
		currentTTConfig.timeMove(duration);
		setCurrentTime(getCurrentTime() + duration);
		manageDelays();

	}
	
	
	public float maxTimedMove() {

		System.out.println("Current Time: " + getCurrentTime());

		float deltanet = currentNetConfig.maxAllowedTimedMove();
		System.out.println("Net Max Time: " + deltanet);

		float deltaTT = currentTTConfig.maxAllowedTime(getCurrentTime());
		System.out.println("TT Max Time: " + deltaTT);

		// min clock of match firable trans/event dates
		for (int tnum : currentNetConfig.getFirable().keySet()) {
			for (TableEvent te : currentTTConfig.getMinList()) {
				if (te.realizedBy(tnum) && te.getDate() > getCurrentTime()) {
					return te.getDate() - getCurrentTime();
				}
			}
		}

		// min clock of match enabled trans/event dates
		for (TransitionAbstract t : currentNetConfig.getEnabled().values()) {
			for (TableEvent te : currentTTConfig.getMinList()) {
				if (te.realizedBy(t.getNumber()) && te.getDate() > getCurrentTime()
						&& (te.getDate() - getCurrentTime()) > t.getClock()) {
					return te.getDate() - getCurrentTime();
				} else if (te.realizedBy(t.getNumber()) && te.getDate() > getCurrentTime()) {
					return t.getClock();
				}
			}
		}

		if (deltaTT < 0) { // If the timetable can let time progress as much as possible
			// then we take the duration allowed by the net
			return deltanet;
		}
		if (deltaTT < deltanet) { // If the timetable can let time progress as much as possible
			// then we take the duration allowed by the net
			return deltaTT;
		} else {
			return deltanet;
		}

	}

	public String showMaxTimedMove() {

		String log = new String("");
		long deltanet = (long) currentNetConfig.maxAllowedTimedMove();
		float deltaTT = currentTTConfig.maxAllowedTime(getCurrentTime());

		log = log + "TT Max Time: " + deltaTT;

		log = log + " Net Max Time: " + deltanet;

		return log;

	}

	/**
	 * returns a string depicting net and TT configuration
	 */
	/*public String DropConfig() {

		String s1 = this.currentNetConfig.dropConfig();
		String s2 = this.currentTTConfig.dropConfig();

		return "Date : " + getCurrentTime() + "\n" + s1 + s2;
	}*/
	
	/**add the delays to specific transition(s) at a specific time as indicated in the scenario menu*/
	public void manageDelays() {
		for (Delay delay : delays) {
			if (delay.isValid() && currentTime > delay.getEndDate()) {
				TransitionAbstract transition = currentNetConfig.findTransition(delay.getTransitionNb());
				transition.setLowerBound(transition.getLowerBound() - delay.getDelay());
				transition.setUpperBound(transition.getUpperBound() - delay.getDelay());
				delay.setValid(false);
			}
			else if (!delay.isValid() && currentTime >= delay.getStartDate() && currentTime <= delay.getEndDate()) {
				TransitionAbstract transition = currentNetConfig.findTransition(delay.getTransitionNb());
				transition.setLowerBound(transition.getLowerBound() + delay.getDelay());
				transition.setUpperBound(transition.getUpperBound() + delay.getDelay());
				delay.setValid(true);
			}
		}
	}
	
	public void setDelays(ArrayList<Delay> delays) {
		this.delays = delays;
	}
	
	public void resetDelays() {
		for (Delay delay : delays) {
			if (delay.isValid()) {
				TransitionAbstract transition = currentNetConfig.findTransition(delay.getTransitionNb());
				transition.setLowerBound(transition.getLowerBound() - delay.getDelay());
				transition.setUpperBound(transition.getUpperBound() - delay.getDelay());
				delay.setValid(false);
			}
		}
		delays = new ArrayList<>();
	}

	public float getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(float currentTime) {
		this.currentTime = currentTime;
	}

	public void reset() {
		TimeTable tt = NetAndTT.getTable();
		PhysicalModel n = NetAndTT.getNet();

		currentTTConfig.reset();
		n.reset(true);

		String cfname = NetAndTT.cfname;
		NetAndTT = new RegulNet(currentTTConfig.getTable(), n);
		
		ArrayList<Delay> delaysCopy = delays;
		resetDelays();
		delays = delaysCopy;
		manageDelays();

		//currentNetConfig = n;
		//currentTTConfig = new TTConfig(tt);
		//currentTTConfig.init();
		
		//NetAndTT.LoadCorrespondance(cfname);
		//NetAndTT.buildCorrespondance();
		setCurrentTime(0);
	}

	public RegulNet getNetAndTT() {
		return NetAndTT;
	}

	public String getPathLogs() {
		return pathLogs;
	}
	
	
}