package fr.inria.mochy.core.RPN;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.mochysim.Net;

import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.TimeTable;



// Class for regulated Petri nets, i.e. Petri nets with 

/**
 * Model to link a timetable and a net
 * 
 *	place are linked to events with tableEvent.addPlaceName(placeNumber);
 *	table event are linked to transitions with transition.addLinkedEvent(tableEvent);
 *	transition are linked to table event with te.addTransName(transitionNumber);
 *
 * <p>
 * <img src="RegulNet.svg" alt=RegulNet class diagram" style="float: right;">
 */
public class RegulNet{
	
	/**The timetable used to control the regulated Petri net.*/
	TimeTable rptt;
	/**The Petri net giving the physical description of the transport system.*/
	PhysicalModel rpnet; 
	/**the name of the correspondance file which make the link between the timetable and the physical model*/
	String cfname;
	
	/** list of pairs of  the form <TableEvent,Transition> 
	* this correspondance table works as follows : if an entry of the form (te,t) appears in the table 
	* then table event te is the an event that may correpsond to a physical event represented by transition t
	* (for trains an arrival). 
	* hence,  the event has to be executed at the same date as SOME transition t such that (t,te) is in the table
	* Note : there can be several transitions "implementing an event"*/
	//ArrayList<EvtTrans> Awaits; 
	
	/** list of pairs of  the form <TableEvent,Place> 
	* This correspondance works as follows : if a pair (te,p) appears in the list, then, as soon as the table event te 
	* is feasible, place p receives a token
	* Note that p may appear in several pairs, i.e. if the same transition is repeated k times, it is allowed by k events*/ 
	//ArrayList<EvtPlace> Allows; 
	
	/**
	 * Constructor for an Regulated Petri Net
	 * @param t the timetable
	 * @param n the physical model
	 */
	public RegulNet(TimeTable t, PhysicalModel n){
		
		this.rptt = t;
		this.rpnet = n;
		//Awaits = new ArrayList<EvtTrans>();
		//Allows = new ArrayList<EvtPlace>();
		
	}
	
	/**
	 * --unused-- Creates the correspondance/link between the timetable events and the places and transitions of the physical model 
	 */
	public void buildCorrespondance() {
		// visit every timetable event
		// for every te, look at the list of trasitionn names t1,...tk, and 
		// add pairs (te,t1)...(te,tk)
		
		HashMap<Integer, TableEvent> evts=this.rptt.getEvents();
		
		for ( Integer key : evts.keySet()) {
			TableEvent te =evts.get(key);
			for (Integer tname : te.getTransNames() ) {
				// for each tname
				// find the transition 
				TransitionAbstract t=rpnet.findTransition(tname);
				//Awaits.add(new EvtTrans(te,t));
				
			}
			
			for (Integer pname : te.getPlaceNames() ) {
				// for each pname
				// find the transition 
				PlaceAbstract p=rpnet.findPlace(pname);
				//Allows.add(new EvtPlace(te,p));
				
			}	
		}
						
		}

	

	/**
	 * load the correspondance file, call handleLineCorr method for each each line
	 * @param fname the name of the file containing a correspondance table
	 */
	public String LoadCorrespondance(String fname) {
		this.cfname = fname;
		//String logs=new String("");
		StringBuffer logs = new StringBuffer();
		BufferedReader br = null;
		int lineNb = 1;
		try {
			br = new BufferedReader(new FileReader(fname));

			while (br.ready()) { // until end of file
				String line = br.readLine();
				logs.append(handleLineCorr(line, lineNb));
				lineNb++;
			}
			br.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logs.insert(0, "Warning : "+e.getMessage());//if file can't be find
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logs.append("error while closing the BufferedReader in class Regulnet");
				}
			}
		}
		return logs.toString();

	}
	
	/**load a line from the input file
	 * @param lineString a line of the input file
	 * @param lineNb the number of the line in the input file*/
	public String handleLineCorr(String lineString,int lineNb) {

		String logline=new String("");
		int tnum;
		int evtnum;
		int pnum; 
				
		// Split the line in different fields
		String[] segments = lineString.split(" ");
		
		//line of the form "transition i implements event j"
		if (segments[0].equals("transition")) {
			tnum = Integer.parseInt(segments[1]);
			evtnum = Integer.parseInt(segments[4]);
			TableEvent te= this.rptt.getEvent(evtnum);
			TransitionAbstract t = this.rpnet.findTransition(tnum);
			t.addLinkedEvent(te);
			te.addTransName(tnum);			
		}
		
		//line of the form "event i allows place j"
		if (segments[0].equals("event")) {
			evtnum = Integer.parseInt(segments[1]);
			pnum = Integer.parseInt(segments[4]);
			TableEvent te= this.rptt.getEvent(evtnum);
			
			te.addPlaceName(pnum);
			//System.out.println(te.drop());

		}
		
		if (segments[0].equals("%")) {
			//comments
		}
		return logline;
		
	}
	
	
	/**********************************************************/
	/*             GETTERS                                    */
	/**********************************************************/
	
	public PhysicalModel getNet() { return  rpnet;}
	public TimeTable getTable() { return rptt;}
	
	/**********************************************************/
	/*             SETTERS                                    */
	/**********************************************************/
	
	public void setNet(PhysicalModel n) { rpnet = n;}
	public void setTable(TimeTable tt) { rptt = tt;}
	
	
	/***********************************************************/
	/*          DISPLAY                 */
	/************************************************************/
	
	public String drop() {
		String s=new String("");
		s.concat(rpnet.dropConfig());
		s.concat("-----------------\n");
		s.concat(this.rptt.dropConfig());
		
		return s;
		
	}
	
}// End of class
	
	



	