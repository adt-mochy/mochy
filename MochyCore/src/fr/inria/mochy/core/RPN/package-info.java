/**
 * It models the simulation of a timetable linked to a net.
 * <p>
 *
 * <img src="package.svg" alt="Package class diagram" style="float: right;">
 */
package fr.inria.mochy.core.RPN;
