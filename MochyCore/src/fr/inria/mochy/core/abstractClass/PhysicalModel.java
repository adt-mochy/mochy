package fr.inria.mochy.core.abstractClass;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;

import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.mochysim.Marking;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.timetable.Tag;

/**
 * it is the abstraction of mochysim.Net, trajectory.TrajectoryNet, equalization.EquNet... and models the transport networks via petri-nets
 * <p>
 * <img src="PhysicalModel.svg" alt="PhysicalModel class diagram" style="float: right;">
 */
public abstract class PhysicalModel {
	/**the name of the input model file loaded*/
	public String fname;
	/**duration elapsed so far during a simulation*/
	public float timeElapsed = 0;
	/**the number of steps performed (timed move, discrete move...)*/
	public int stepsNb = 0;
	/**number of discrete steps that occurred during simulation*/
	public int nbDiscreteSteps = 0;
	/**set to true when the last step is a discrete step (in contrary to to a timed move)*/
	public boolean discreteStep = false;
	/**set to true once the first line of the logs of the out.txt file have be written*/
	public boolean startLogs = false;// 
	//public HashMap<Integer, ? extends TransitionAbstract> transitions;
	/**the number of tokens in the network*/
	public int nbTokens = 0;
	/**the list of the tokens which represents train, metro... in the network*/
	public LinkedHashMap<Integer, Token> tokens = new LinkedHashMap<>();
	
	/**called to load the input model file*/
	public abstract String loadFile();
	
	/**list the transitions which can be recovered with their number*/
	public abstract HashMap<Integer, ? extends TransitionAbstract> getTransitions();
	
	/**list the places which can be recovered with their number*/
	public abstract HashMap<Integer, ? extends PlaceAbstract> getPlaces();
	
	/**return the allowed timed move to be elapsed to perform a step*/
	public abstract float maxAllowedTimedMove();
	
	/**return the number of fireable transitions*/
	public abstract int numberFireable();
	
	/**return the number of blocked transitions*/
	public abstract int numberBlocked();
	
	/**return true if the transition specified in parameter by its number is blocked*/
	public abstract boolean isBlocked(int tnum);
	
	/**advance the time of the delta parameter value and update the status of the transitions (blocked, fireable...)*/
	public abstract void progressTime(Float delta);
	
	/**perform a discrete move e.g. a transition will be fired and a token will change of place*/
	public abstract void discreteMove();
	
	/**perform the number of steps specified in parameter (timed move, discrete move, ...)*/
	public abstract void multipleSteps(int steps);
	
	/**reset the network model to its initial status*/
	public void reset (boolean init) {
		startLogs = false;
	}
	/**perform a discrete move e.g. a transition will be fired and a token will change of place*/
	public abstract boolean discreteMove(String pathLogs, long elapsedTime, boolean enableLogs);
	
	/**display informations of the network model*/
	public abstract void drop();
	
	/**display informations of the configuration of the network model*/
	public abstract String dropConfig();
	
	/**return the transition as specified by its number in parameter*/
	public abstract TransitionAbstract findTransition(Integer tname);
	
	/**return the transition as specified by its name in parameter*/
	public abstract TransitionAbstract findTransition(String string);
	
	/**return the place as specified by its number in parameter*/
	public abstract PlaceAbstract findPlace(int pname);
	
	/**add a token in the place specified in parameter by its number*/
	public abstract Boolean addToken(int p);
	
	/**return a control place as specified by its number in parameter*/
	public abstract PlaceAbstract getControlPlace(int aplace);
	
	/**return the list of the control places which can be recovered by their id number*/
	public abstract HashMap<Integer, ? extends PlaceAbstract> getControlPlaces();

	/**return the minimum time to advance in order to update the status of tokens or transitions*/
	public abstract float minimumClock();

	/**return the list of fireable transitions*/
	public abstract ArrayList<TransitionAbstract> fireableTransition();

	/**perform a discrete move e.g. a transition will be fired and a token will change of place*/
	public abstract String discreteMove(TransitionAbstract onet, String logFile, float currentTime, boolean enableLogs);

	/**get the list of the enabled transitions which can be retrieved by their id number*/
	public abstract HashMap<Integer, ? extends TransitionAbstract> getEnabled();
	
	/**get the list of the fireable transitions which can be retrieved by their id number*/
	public abstract HashMap<Integer, ? extends TransitionAbstract> getFirable();
	
	/**return true if the prob. distribution is gaussian. used for the sampling of the transitions*/
	public abstract boolean isGaussian();
	
	/**return true if the prob. distribution is weibull. used for the sampling of the transitions*/
	public abstract boolean isWeibull();
	
	/**get the coefficient used for the weibull prob. distribution. used for the sampling of the transitions*/
	public abstract int getWeibullCoef();
	
	/**get the time elapsed*/
	public float getTimeElapsed() {
		return timeElapsed;
	}
	
	/**get the steps (timed moves, discrete moves...) performed*/
	public int getStepsNb() {
		return stepsNb;
	}
	
	/**get the number of discrete steps performed*/
	public int getNbDiscreteSteps() {
		return nbDiscreteSteps ;
	}
	
	/**return true if the last step is a discrete step*/
	public boolean isDiscreteStep() {
		return discreteStep;
	}

	public abstract LinkedHashMap<Integer, Token> getTokens();
}
