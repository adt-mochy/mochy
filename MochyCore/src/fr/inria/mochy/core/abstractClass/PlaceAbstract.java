package fr.inria.mochy.core.abstractClass;

import java.util.ArrayList;

import fr.inria.mochy.core.equalization.EquPlace;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.trajectory.TrajPlace;

/**defines a place from the petri net in the physicalModel, it is the the abstraction of mochysim.Place, trajectory.TrajPlace, equalization.EquPlace...
 * <p>
 * <img src="PlaceAbstract.svg" alt="PlaceAbstract class diagram" style="float: right;"> 
 * */
public abstract class PlaceAbstract {
	String name;
	Integer number;
	
	/**get the name of the place*/
	public abstract String getName();
	
	/**get the number of the place*/
	public abstract Integer getNumber();
	
	/**add a token in the place*/
	public abstract void addToken();
	

}
