package fr.inria.mochy.core.abstractClass;

import java.util.ArrayList;
import java.util.HashMap;

import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TableEvent;

/**
 * It is the abstraction of the TimeTable class
 * <p>
 * <img src="Schedular.svg" alt="Schedular class diagram" style="float: right;">
 */
public abstract class Schedular {
	/**the list of the events which should occur at a specific date. can be retrieved by their id number*/
	HashMap<Integer, TableEvent> events;
	
	/**list the dependencies between the events*/
	ArrayList<TableDependency> dependencies;
	
	/**the name of the input file*/
	String filename;
	
	/**load the input file*/
	public abstract String fileLoading();
	
	/**handle a line (of the input file) specified in parameter*/
	protected abstract String handleLine(String ln, int lineNb);
	
	/**list the events which can be retrieved by their id number*/
	public abstract HashMap<Integer, TableEvent> getEvents();
	
	/**return the list of the dependencies between the events*/
	public abstract ArrayList<TableDependency> getDependencies();
	
	/**retrieve an evenet by its id*/
	public abstract TableEvent getEvent(Integer evtid);
	
	/**returns the list of minimal events for a timetable e.g. they have no predecessors or executed predecessors*/
	public abstract ArrayList<TableEvent> minList(); 
	
	/**returns the list of minimal events that are not yet executed. in a timetable we search every event that has only executed predecessors or none*/
	public abstract ArrayList<TableEvent> minListUnexecuted();
	
	/**propagate a delay from an event nb on all its sucessors */
	public abstract void PropagateDelay(Integer nb, float Delay);
	//timeMove + discretemove ?
}
