package fr.inria.mochy.core.abstractClass;

import java.util.ArrayList;

import fr.inria.mochy.core.equalization.EquPlace;
import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.trajectory.TrajPlace;
import fr.inria.mochy.core.trajectory.TrajTransition;
import fr.inria.mochy.core.trajectory.Trajectory;

/**
 * it defines the abstraction of the transitions of the petri nets models
 * defined by PhysicalModel
 * <p>
 * <img src="TransitionAbstract.svg" alt="TransitionAbstract class diagram"
 * style="float: right;">
 */
public abstract class TransitionAbstract {
	/** the name of the transition */
	public String name;

	/** the id number of the transition */
	public Integer number;

	/** the remaning time for a transition to be at the end (when its value is 0) */
	protected Float clock;

	/** the lowerbound of the interval in which the clock will start */
	public Float lowerBound;

	/** the upperbound of the interval in which the clock will start */
	public Float upperBound;

	/** the upperbound of the interval in which the clock will start as a string */
	public String max;

	/** the list of the events linked to this transition */
	ArrayList<TableEvent> linkedEvents;

	/** the list of the events linked to this transition */
	public ArrayList<TableEvent> getLinkedEvents() {
		return linkedEvents;
	}

	/** add an event to the list of the linked events to this transition */
	public void addLinkedEvent(TableEvent te) {
		this.linkedEvents.add(te);
	}

	/** the id number of the transition */
	public int getNumber() {
		return number;
	}

	/** the name of the transition */
	public String getName() {
		return name;
	}

	/** the remaning time for a transition to be at the end (when its value is 0) */
	public Float getClock() {
		return clock;
	}

	/**
	 * set the remaning time for a transition to be at the end (when its value is 0)
	 */
	public void setClock(Float value) {
		this.clock = value;
	}

	/**
	 * set the remaning time for a transition to be at the end (when its value is 0)
	 */
	public void setClock(float value) {
		this.clock = new Float(value);
	}

	/** the lower bound of the interval in which the clock will start */
	public Float getLowerBound() {
		return lowerBound;
	}

	/** set the lower bound of the interval in which the clock will start */
	public void setLowerBound(Float lowerbound) {
		this.lowerBound = lowerbound;
	}

	/** the upper bound of the interval in which the clock will start */
	public Float getUpperBound() {
		return upperBound;
	}

	/** set the upper bound of the interval in which the clock will start */
	public void setUpperBound(Float upperbound) {
		this.upperBound = upperbound;
	}

	/** the upper bound of the interval in which the clock will start as a string */
	public String getMax() {
		return max;
	}

	/**
	 * return the list of the previous places immediately linked to this transition
	 */
	public abstract ArrayList<? extends PlaceAbstract> getPre();

	/** return the list of the next places immediately linked to this transition */
	public abstract ArrayList<? extends PlaceAbstract> getPost();

	/**
	 * return the list of the previous control places immediately linked to this
	 * transition
	 */
	public abstract ArrayList<? extends PlaceAbstract> getControlPre();

	/**
	 * return the list of the values to get to the end of the place for each token
	 * of the place
	 */
	public ArrayList<Float> tokensTimeToFire() {
		ArrayList<Float> ttf = new ArrayList<>();
		for (PlaceAbstract p : this.getPre()) {
			if (p instanceof Place) {
				Place place = (Place) p;
				if (place.getContents() != null)
					ttf.add(this.clock);
			} else if (p instanceof TrajPlace) {
				TrajPlace place = (TrajPlace) p;
				TrajTransition transition = ((TrajPlace) p).getPost();
				for (Trajectory traj : place.trajectories) {
					if (place.trajectories.get(0).isSinglePoint())
						ttf.add(0f);
					else
						ttf.add(place.trajectories.get(0).absciss(0));
				}
			} else if (p instanceof EquPlace) {
				EquPlace place = (EquPlace) p;
				for (Token token : place.getTokens()) {
					ttf.add(token.getTimeToBrowse());
				}
			}
		}
		return ttf;
	}
}
