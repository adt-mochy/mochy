/**
 * abstractClass package : used to model the components of the petri-net transport networks (PhysicalModel, PlaceAbstract, TransitionAbstract) and their time constraints (Schedular)
 * <p>
 *
 * <img src="package.svg" alt="Package class diagram" style="float: right;">
 */
package fr.inria.mochy.core.abstractClass;
