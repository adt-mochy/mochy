package fr.inria.mochy.core.dom;

import java.util.ArrayList;

import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.trajectory.Trajectory;

/**
 * Object used to contain a transition or a place depending of the type "t" or "p".
 * Used to send the feedback of the state of the net to the user interface
 * <p>
 * <img src="Dom.svg" alt="Dom class diagram" style="float: right;">
 */
public class Dom {
	/**the type of the dom, p for places, t for transitions*/
	String type;
	/**the name*/
	String name;
	/**the id number*/
	String number;
	/**the content for the places*/
	String content;
	/**the name of the previous places for transtions separated by , */
	String pre;
	/**the name of the next places for transitions separated by , */
	String post;
	/**the lowerbound of the starting clock for transitions*/
	Float lowerBound;
	/**the upperbound of the starting clock for transitions*/
	Float upperBound;
	/**the clock value as a String*/
	String clock;
	/**the distribution law of the starting clock, weibull:coef, gaussian...*/
	String sampler;
	/**set to true if the place is a control place*/
	Boolean control = false;
	/**list of the tokens'trajectories for trajectory.TrajectoryNet in the places*/
	ArrayList<Trajectory> trajectories;
	/**list of the tokens in the places for equalization.EquNet*/
	ArrayList<Token> tokens;
	
	public Dom(String name, String pre, String post, String content, String type, Float min, Float max, String clock) {
		super();
		this.name = name;
		this.content = content;
		this.pre = pre;
		this.post = post;
		this.type=type;
		this.lowerBound = min;
		this.upperBound = max;
		this.clock = clock;
	}
	
	/**return the name*/
	public String getName() {
		return name;
	}
	
	/**set the name*/
	public void setName(String name) {
		this.name = name;
	}
	
	/**get the place content*/
	public String getContent() {
		return content;
	}
	
	/**get the previous places names of transitions separated by , */
	public String getPre() {
		return pre;
	}
	
	/**set the previous places names of transitions separated by , */
	public void setPre(String pre) {
		this.pre = pre;
	}
	
	/**the next places names of the transitions separated by , */
	public String getPost() {
		return post;
	}
	
	/**set the next places names of the transitions separated by , */
	public void setPost(String post) {
		this.post = post;
		
		
	}
	
	/**get the type of the Dom : p for place, t for transition*/
	public String getType() {
		return type;
	}
	
	/**set the type of the Dom : p for place, t for transition*/
	public void setType(String type) {
		this.type = type;
	}
	
	/**set the content of the places*/
	public void setContent(String content) {
		this.content = content;
	}
	
	/**get the lowerbound of the transitions'clock*/
	public Float getLowerBound() {
		return lowerBound;
	}
	
	/**set the lowerbound of the transitions'clock*/
	public void setLowerBound(Float lowerBound) {
		this.lowerBound = lowerBound;
	}
	
	/**get the upperbound of the transitions'clock*/
	public Float getUpperBound() {
		return upperBound;
	}
	
	/**set the upperbound of the transitions'clock*/
	public void setUpperBound(Float upperBound) {
		this.upperBound = upperBound;
	}
	
	/**get a transition'clock value*/
	public String getClock() {
		return clock;
	}
	
	/**set a transition'clock value*/
	public void setClock(String clock) {
		this.clock = clock;
	}
	
	/**get the id number*/
	public String getNumber() {
		return number;
	}
	
	/**set the id number*/
	public void setNumber(String number) {
		this.number = number;
	}
	
	/**get the distribution probability law of a transition*/
	public String getSampler() {
		return sampler;
	}
	
	/**set the distribution probability law of a transition*/
	public void setSampler(String sampler) {
		this.sampler = sampler;
	}
	
	/**true if the place is a control place*/
	public Boolean getControl() {
		return control;
	}
	
	/**set the control value to true or false depending of if the place is a control place*/
	public void setControl(Boolean control) {
		this.control = control;
	}
	
	/**get the list of the trajectories in the place in trajectory.TrajectoryNet*/
	public ArrayList<Trajectory> getTrajectories() {
		return trajectories;
	}
	
	/**set the list of the trajectories in the place in trajectory.TrajectoryNet*/
	public void setTrajectories(ArrayList<Trajectory> trajectories) {
		this.trajectories = trajectories;
	}
	
	/**get the list of the place'tokens in equalization.EquNet*/
	public ArrayList<Token> getTokens() {
		return tokens;
	}
	
	/**set the list of the place'tokens in equalization.EquNet*/
	public void setTokens(ArrayList<Token> tokens) {
		this.tokens = tokens;
	}
	
}
