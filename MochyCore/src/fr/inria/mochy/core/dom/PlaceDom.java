package fr.inria.mochy.core.dom;

import java.util.ArrayList;

import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.trajectory.Trajectory;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * Used to contain informations of a Place object and display it to the user interface.
 * <p>
 * <img src="PlaceDom.svg" alt="PlaceDom class diagram" style="float: right;">
 */
public class PlaceDom {
	String name;
	String content;
	String number;
	BooleanProperty control;
	ArrayList<Trajectory> trajectories;
	ArrayList<Token> tokens;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public PlaceDom(String name, String content) {
		super();
		this.name = name;
		this.content = content;
		control = new SimpleBooleanProperty();
		control.addListener((observable, oldValue, newValue) -> {
			if (newValue)
				setControl(true);
			else
				setControl(false);
	    });
	}
	public PlaceDom() {
		super();
		this.name = "temp";
		this.content = "";
		control = new SimpleBooleanProperty();
		control.addListener((observable, oldValue, newValue) -> {
			if (newValue)
				setControl(true);
			else
				setControl(false);
	    });
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	public BooleanProperty controlProperty() { return control; }

    public boolean isControl() { return this.control.get(); }

    public void setControl(boolean value) { this.control.set(value); }
    
	public ArrayList<Trajectory> getTrajectories() {
		return trajectories;
	}
	public void setTrajectories(ArrayList<Trajectory> trajectories) {
		this.trajectories = trajectories;
	}
	public ArrayList<Token> getTokens() {
		return tokens;
	}
	public void setTokens(ArrayList<Token> tokens) {
		this.tokens = tokens;
	}
	
}

