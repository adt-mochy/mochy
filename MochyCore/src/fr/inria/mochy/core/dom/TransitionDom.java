package fr.inria.mochy.core.dom;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * Used to contain informations of a Transition object and display it to the user interface.
 * <p>
 * <img src="TransitionDom.svg" alt="TransitionDom class diagram" style="float: right;">
 */
public class TransitionDom {
	String name;
	String number;
	String distribution;//enabled, fireable, blocked or none
	String pre;
	String post;
	Float lowerBound;
	Float upperBound;
	String min;
	String max;
	String clock;
	String sampler;
	//String gaussian;
	BooleanProperty weibull;
	Integer weibullCoef;
	BooleanProperty gaussian;
	boolean control = false;
	
	public TransitionDom(String name, String pre, String post, String distribution, Float min, Float max, String clock) {
		super();
		this.name = name;
		this.distribution = distribution;
		this.pre = pre;
		this.post = post;
		this.lowerBound = min;
		this.upperBound = max;
		this.clock = clock;
		this.min = String.valueOf(min);
		this.max = String.valueOf(max);
		gaussian = new SimpleBooleanProperty();
		gaussian.addListener((observable, oldValue, newValue) -> {
			if (newValue)
				weibull.set(false);
	    });
		weibull = new SimpleBooleanProperty();
		weibull.addListener((observable, oldValue, newValue) -> {
			if (newValue)
				gaussian.set(false);
	    });
		weibullCoef = 0;
	}
	public TransitionDom() {
		this.name="temp";
		gaussian = new SimpleBooleanProperty();
		gaussian.addListener((observable, oldValue, newValue) -> {
			if (newValue)
				weibull.set(false);
	    });
		weibull = new SimpleBooleanProperty();
		weibull.addListener((observable, oldValue, newValue) -> {
			if (newValue)
				gaussian.set(false);
	    });
		weibullCoef = 0;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDistribution() {
		return distribution;
	}
	public void setDistribution(String distribution) {
		this.distribution = distribution;
	}
	public String getPre() {
		return pre;
	}
	public void setPre(String pre) {
		this.pre = pre;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	public Float getLowerBound() {
		return lowerBound;
	}
	public void setLowerBound(Float lowerBound) {
		this.lowerBound = lowerBound;
	}
	public Float getUpperBound() {
		return upperBound;
	}
	public void setUpperBound(Float upperBound) {
		this.upperBound = upperBound;
	}
	public String getClock() {
		return clock;
	}
	public void setClock(String clock) {
		this.clock = clock;
	}
	public String getMin() {
		return min;
	}
	public void setMin(String min) {
		this.min = min;
	}
	public String getMax() {
		return max;
	}
	public void setMax(String max) {
		this.max = max;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getSampler() {
		return sampler;
	}
	public void setSampler(String sampler) {
		this.sampler = sampler;
	}
	
	public BooleanProperty gaussianProperty() { return gaussian; }

    public boolean isGaussian() { return this.gaussian.get(); }

    public void setGaussian(boolean value) { this.gaussian.set(value); }
	
    public BooleanProperty weibullProperty() { return weibull; }

    public boolean isWeibull() { return this.weibull.get(); }

    public void setWeibull(boolean value) { this.weibull.set(value); }
    
	public int getWeibullCoef() {
		return weibullCoef;
	}
	public void setWeibullCoef(Number number) {
		this.weibullCoef = number.intValue();
	}
	public boolean control() {
		
		return control;
	}
	public void setControl(boolean value) {
		control = value;
	}

}

