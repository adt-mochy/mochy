/**
 * Data Object Model package : used to transfer information from the net to the user information
 * <p>
 *
 * <img src="package.svg" alt="Package class diagram" style="float: right;">
 */
package fr.inria.mochy.core.dom;
