package fr.inria.mochy.core.equalization;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.abstractClass.LTL;
import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.exceptions.NullGarageException;
import fr.inria.mochy.core.exceptions.TransitionNotFoundException;
import fr.inria.mochy.core.sampler.Sampler;

/**An EquNet is a Petri-Net which have tokens on a loop with successives transitions and places.
 * The goal of its regulation is to seek to stick to an equal distance between each pair of successive tokens*/
public abstract class EquNet extends PhysicalModel {
	// String fname; //the path of the file to load
	/**the list of the transitions which manage the time in the petri nets*/
	public HashMap<Integer, EquTransition> transitions = new HashMap<>();
	/**the list of the places which manage the location of a token in the petri nets*/
	public HashMap<Integer, EquPlace> places = new HashMap<>();
	/**the list of enabled transitions e.g. their clock is not 0 but there is tokens in their previous places*/
	public HashMap<Integer, EquTransition> enabled = new HashMap<>();
	/**the list of fireable transitions e.g. their clock is 0 and there is tokens in their previous places*/
	public HashMap<Integer, EquTransition> fireable = new HashMap<>();
	/**the list of the blocked transitions e.g. there is tokens at the end of the previous places but they are prevent from entering the next place due to minimum safety distance*/
	public HashMap<Integer, EquTransition> blocked = new HashMap<>();
	
	/**a place which is a garage from where new tokens can be inserted by calling the insertToken method*/
	public EquPlace garage;
	/** the pair of place number and the time to browse set to its token at the
	* initial state*/
	public HashMap<Integer, Float> initialState = new HashMap<>();
	int tokenIndex = 0;
	/**set to true if the clocks of the transitions follow a gaussian probability law*/
	boolean gaussian = false;
	/**set to true if the clocks of the transitions follow a weibull probability law*/
	boolean weibull = false;
	/**set the coefficient of a weibull probability law if EquNet transition clocks follow such a law*/
	int weibullCoef = 5;
	/**the total distance of the EquNet loop*/
	float totalDistance;
	/** average of the headway distance between the tokens, used in
	* getStandardDeviation method*/
	float average = 0;
	boolean startPlace = false;// set to true if the start place has been set
	Sampler s = new Sampler();
	Logger logger = Logger.getLogger("logger");
	/**the minimum headway distance between the tokens in a place. can be set in the input file as a line : MIN_HEADWAY_DISTANCE:value*/
	static float MIN_HEADWAY_DISTANCE = 30;
	/**the maximum speed of the tokens in a moving place. can be set in the input file as a line : MAX_SPEED:value*/
	static float MAX_SPEED = 600;// 1km/h = 17m/min // 600m/min = 35 km/h
	/**the minimum speed of the tokens in a moving place. can be set in the input file as a line : MIN_SPEED:value*/
	static float MIN_SPEED = 0;
	/** A constant for the whole simulation describing adherence to target speed
	* This constant is used when computing an advice for a train
	* can be set in the input file as a line : ALPHA:value*/
	float ALPHA = 10; 
	/**the range for the noise in the calculTime method. can be set in the input file as a line : RANGE_NOISE:value*/
	static float RANGE_NOISE = 1;
	
	
	boolean logs = true;// used to log in the console
	/** used to get the average speed in MutlipleRuns.EqualizationFullStats */
	float lastTokenSpeed = 0;
	float lastTokenTtb = 0;
	//float totalSpeedSum = 0;
	//int speedSteps = 0;
	float elapsedDistanceAvg = 0;//used to get the average speed
	float elapsedTimeToGetAvgSpeed = 0;//used to get the average speed

	// public abstract void progressTime(Float delta);
	// public abstract String discreteMove(TransitionAbstract onet, String logFile,
	// float currentTime, boolean enableLogs);
	abstract float calculTime(Token token, EquTransition t);
	// public abstract float minimumClock();

	/**instanciate the EquNet and set the file name as specified in parameters*/
	public EquNet(String fname) {
		this.fname = fname;
	}

	@Override
	public String loadFile() {
		StringBuffer logs = new StringBuffer();
		int lineNb = 1;
		// Open a file
		// reads places, transitions, flows
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fname));
			System.out.println("*******************");
			System.out.println("Opening File : " + fname);

			while (br.ready()) { // Tant qu'on peut encore lire des choses
				logs.append(handleLine(br.readLine(), lineNb));
				lineNb++;
			}

			// At this place all lines are read
			// i.e. all places, transitions and the current marking
			// We can hence sample duration for every enabled transition
			// set the gaussian value to true to all the transitions of the net
			// if the file read the Gaussian value and the same if this is weibull
			/** TODO **/
			br.close();
			System.out.println("*******************");

			// from initial marking, build initial configuration

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class Net");
				}
			}
		}
		// set the order of the tokens and get the total distance
		totalDistance = 0;
		for (EquPlace p : places.values()) {
			if (!p.equals(garage)) {
				totalDistance = getTotalDistance() + p.getDistance();
				for (Token token : p.tokens) {
					boolean preTokenFound = false;
					EquPlace prePlace = p.pre.pre.get(0);// the previous place
					while (!preTokenFound) {
						if (prePlace.tokens.size() > 0) {
							preTokenFound = true;
							prePlace.tokens.get(0).postToken = token;
							token.previousToken = prePlace.tokens.get(0);
						}
						prePlace = prePlace.pre.pre.get(0);// the previous place
					}
				}
			}
		}

		// set weibull or gaussian distribution to the transition
		for (EquTransition t : transitions.values()) {
			if (gaussian)
				t.gaussian = true;
			else if (weibull) {
				t.weibull = true;
				t.weibullCoef = weibullCoef;
			}
		}

		return logs.toString();
	}

	/**load a line of the input file
	 * @param readLine the line of the input file
	 * @param lineNb the number of the line in the input file
	 * @return logs information as a String*/
	protected String handleLine(String readLine, int lineNb) {
		String logs = "";
		String[] segments = readLine.split(":");
		ArrayList<EquPlace> placesToCreate = new ArrayList<>();
		try {
			if (segments[0].equals("place")) {// place:nb:name
				int nb = Integer.parseInt(segments[1]);
				int distance = Integer.parseInt(segments[3]);
				EquPlace place = null;

				if (!startPlace) {// if no starting place was defined yet
					place = new EquPlace(this, nb, segments[2], distance, true);
					startPlace = true;
				} else
					place = new EquPlace(this, nb, segments[2], distance, false);
				places.put(nb, place);
			} else if (segments[0].equals("garage")) {
				int nb = Integer.parseInt(segments[1]);
				int distance = Integer.parseInt(segments[3]);
				EquPlace place = new EquPlace(this, nb, segments[2], distance, false);
				places.put(nb, place);
				garage = place;
			} else if (segments[0].equals("transition")) {// transition:nb:name:[min,max]:distance
				int nb = Integer.parseInt(segments[1]);
				EquTransition transition;
				String[] interval = segments[3].substring(1, segments[3].length() - 1).split(",");
				// if the upperBound is infinite
				if (interval[1].equals("inf")) {
					float min = Float.parseFloat(interval[0]);
					transition = new EquTransition(nb, segments[2], min, null);
				} else {
					float min = Float.parseFloat(interval[0]);
					float max = Float.parseFloat(interval[1]);
					transition = new EquTransition(nb, segments[2], min, max);
				}
				transitions.put(nb, transition);
			} else if (segments[0].equals("inflow")) {
				int trans = Integer.parseInt(segments[1]);
				int place = Integer.parseInt(segments[2]);
				this.addInFlow(trans, place);
			} else if (segments[0].equals("outflow")) {
				int trans = Integer.parseInt(segments[1]);
				int place = Integer.parseInt(segments[2]);
				this.addOutFlow(trans, place);
			} else if (segments[0].equals("initial")) {
				for (int i = 1; i < segments.length; i++) {
					int placeNb = Integer.parseInt(segments[i]);
					EquPlace place = this.places.get(placeNb);
					// place.createTrajectory();
					// placesToCreate.add(place);
					addToken(place);
					nbTokens ++;
				}
			} else if (segments[0].equals("token")) {// token:placeNb:speed:position:timeToBrowse
				EquPlace p = places.get(Integer.parseInt(segments[1]));
				addToken(p);
				Token token = p.tokens.get(p.tokens.size() - 1);
				token.speed = Integer.parseInt(segments[2]);
				int x = Integer.parseInt(segments[3]);
				token.xPlace += x;
				token.setXTotal(token.getXTotal() + x);
				token.timeToBrowse = Float.parseFloat(segments[4]);
				p.post.setClock(p.tokens.get(p.tokens.size() - 1).timeToBrowse);
			} else if (segments[0].equals("Gaussian")) {
				gaussian = true;
				logs += "Law of Gauss";
			} else if (segments[0].equals("Weibull")) {
				weibull = true;
				weibullCoef = Integer.valueOf(segments[1]);
				logs += "Law of Weibull";
			} else if (segments[0].equals("ALPHA")) {
				this.ALPHA = Float.parseFloat(segments[1]);
			} else if (segments[0].equals("MIN_HEADWAY_DISTANCE")) {
				this.MIN_HEADWAY_DISTANCE = Integer.parseInt(segments[1]);
			} else if (segments[0].equals("MAX_SPEED")) {
				this.MAX_SPEED = Integer.parseInt(segments[1]);
			} else if (segments[0].equals("MIN_SPEED")) {
				this.MIN_SPEED = Integer.parseInt(segments[1]);
			} else if (segments[0].equals("RANGE_NOISE")) {
				this.RANGE_NOISE = Float.parseFloat(segments[1]);
			}
		} catch (NumberFormatException nfe) {
			logs += "error net line " + lineNb + " : a value is not a number\n";
			nfe.printStackTrace();
		} catch (Exception e) {
			logs += "error in the net file at line " + lineNb + "\n";
			e.printStackTrace();
		}
		if (garage == null)
			garage = new EquPlace(this, 0, "", 0, false);
		return logs;
	}

	/**adds a place in the preset of a transition
	 * 
	 * @param tnum the id number of the transition
 	 * @param pnum the id number of the place
	 */
	protected void addInFlow(int tnum, int pnum) {
		EquPlace p = this.places.get(pnum);
		EquTransition t = this.transitions.get(tnum);
		t.addPre(p);
		p.post = t;
	}

	/** adds a place in the postset of a transition
	 * 
	 * @param tnum the transition id number
	 * @param pnum the place id number
	 */
	protected void addOutFlow(int tnum, int pnum) {
		EquPlace p = this.places.get(pnum);
		EquTransition t = this.transitions.get(tnum);
		t.addPost(p);
		if (t != null && !t.pre.get(0).equals(garage))
			p.pre = t;
	}

	private void addToken(EquPlace p) {// used only at start/load of the input file
		float timeToBrowse = p.post.sample(s);
		Token token = new Token(p, timeToBrowse);
		p.addToken(token);
		tokens.put(tokenIndex, token);
		tokenIndex++;
		initialState.put(p.getNumber(), timeToBrowse);
	}

	@Override
	public HashMap<Integer, ? extends TransitionAbstract> getTransitions() {
		return transitions;
	}

	@Override
	public HashMap<Integer, EquPlace> getPlaces() {
		return places;
	}

	@Override
	public float maxAllowedTimedMove() {
		return minimumClock();
	}

	@Override
	@LTL
	/**
	 * returns the number of transitions that are firable in current configuration
	 * of the net
	 */
	public int numberFireable() {
		return fireable.size();
	}

	@Override
	@LTL
	/**
	 * return the Number of transitions that are blocked in current configuration.
	 * reminder : a transition is blocked it tokens cannot be produced in its
	 * postset without violating palce occupacy constraint : here a sufficient
	 * headway allows to enter next track segment
	 */
	public int numberBlocked() {
		return blocked.size();
	}

	@Override
	/**
	 * Return a booelan indicating if a transition is currently blocked. Waring :
	 * the list of blocked transition has to be computed before
	 */
	public boolean isBlocked(int tnum) {
		return blocked.containsKey(tnum);
	}

	@Override
	public void multipleSteps(int steps) {
		// TODO Auto-generated method stub

	}

	@Override
	public void reset(boolean init) {
		super.reset(init);
		enabled = new HashMap<>();
		blocked = new HashMap<>();
		fireable = new HashMap<>();
		tokens = new LinkedHashMap<>();
		int tokenIndex = 0;
		for (EquTransition t : transitions.values()) {
			t.setClock(0f);
		}
		for (EquPlace place : places.values()) {
			place.tokens = new ArrayList<>();
			if (initialState.containsKey(place.getNumber())) {
				Token token = new Token(place, place.post.sample(s));
				place.addToken(token);
				tokens.put(tokenIndex, token);
				tokenIndex++;
			}
		}
		// rearrange the order of tokens
		for (EquPlace p : places.values()) {
			for (Token token : p.tokens) {
				boolean preTokenFound = false;
				EquPlace prePlace = p.pre.pre.get(0);// the previous place
				while (!preTokenFound) {
					if (prePlace.tokens.size() > 0) {
						preTokenFound = true;
						prePlace.tokens.get(0).postToken = token;
						token.previousToken = prePlace.tokens.get(0);
					}
					prePlace = prePlace.pre.pre.get(0);// the previous place
				}
			}
		}
		timeElapsed = 0;
		nbDiscreteSteps = 0;
		//totalSpeedSum = 0;
		//speedSteps = 0;
		elapsedDistanceAvg = 0;
		elapsedTimeToGetAvgSpeed = 0;
		stepsNb = 0;
	}
	
	/**set the net in a bunching state where tokens are grouped in successives places*/
	public void setBunchingState (int nbTokens) {
		this.nbTokens = nbTokens;
		super.reset(true);
		enabled = new HashMap<>();
		blocked = new HashMap<>();
		fireable = new HashMap<>();
		tokens = new LinkedHashMap<>();
		tokenIndex = 0;
		for (EquTransition t : transitions.values()) {
			t.setClock(0f);
		}
		for (EquPlace place : places.values()) {
			place.tokens = new ArrayList<>();
		}
		for (int i = 1; i <= nbTokens; i++) {
			EquPlace place = places.get(i);
			Token token = new Token(place, place.post.sample(s));
			place.addToken(token);
			tokens.put(tokenIndex, token);
			tokenIndex++;
		}
		// rearrange the order of tokens
		for (EquPlace p : places.values()) {
			for (Token token : p.tokens) {
				boolean preTokenFound = false;
				EquPlace prePlace = p.pre.pre.get(0);// the previous place
				while (!preTokenFound) {
					if (prePlace.tokens.size() > 0) {
						preTokenFound = true;
						prePlace.tokens.get(0).postToken = token;
						token.previousToken = prePlace.tokens.get(0);
					}
					prePlace = prePlace.pre.pre.get(0);// the previous place
				}
			}
		}
		timeElapsed = 0;
		nbDiscreteSteps = 0;
		//totalSpeedSum = 0;
		//speedSteps = 0;
		elapsedDistanceAvg = 0;
		elapsedTimeToGetAvgSpeed = 0;
		stepsNb = 0;
	}

	@Override
	public void discreteMove() {
		discreteMove("", 0, false);
	}

	@Override
	public boolean discreteMove(String pathLogs, long elapsedTime, boolean enableLogs) {
		Random rand = new Random();
		int randomNum = rand.nextInt(fireable.size());
		Integer[] keys = (Integer[]) fireable.keySet().toArray(new Integer[0]);
		EquTransition t = fireable.get(keys[randomNum]);
		discreteMove(t, pathLogs, elapsedTime, enableLogs);
		return true;
	}

	@Override
	public void drop() {
		if (logs) {
			System.out.println("places:" + places.size());
			for (EquPlace p : this.places.values()) {
				if (p.isStartPlace()) {
					System.out.print("*" + p.getName());
				} else {
					System.out.print(p.getName());
				}
				ArrayList<Token> lt = p.getTokens();
				for (Token tok : lt) {
					// for every token give speed and remaining time to arrival
					System.out.print("<" + tok.getSpeed() + "," + tok.getTimeToBrowse() + ">");

				}
				System.out.print("-");
			}

			System.out.println("\ntransitions:" + transitions.size());
			for (EquTransition t : this.transitions.values()) {

				System.out.print(t.name);
			}
		}

	}

	@Override
	public String dropConfig() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public TransitionAbstract findTransition(Integer tname) {
		return transitions.get(tname);
	}

	@Override
	public TransitionAbstract findTransition(String string) {
		for (EquTransition t : transitions.values()) {
			if (t.getName().equals(string))
				return t;
		}
		return null;
	}

	@Override
	public PlaceAbstract findPlace(int pname) {
		return places.get(pname);
	}

	@Override
	public Boolean addToken(int p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PlaceAbstract getControlPlace(int aplace) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, ? extends PlaceAbstract> getControlPlaces() {
		// TODO Auto-generated method stub
		return new HashMap<>();
	}

	/**return the standard deviation of the distances between the pairs of successive tokens*/
	@LTL
	public float getStandardDeviation() {
		return getStandardDeviation("");
	}
	
	/**return the standard deviation of the distances between the pairs of successive tokens*/
	public float getStandardDeviation(String pathLogs) {
		// get the positions of the tokens of the net
		ArrayList<Float> tokensPosition = new ArrayList<>();
		EquNet n = this;
		for (EquPlace p : n.places.values()) {
			if (!p.equals(garage)) {
				for (Token token : p.getTokens()) {
					tokensPosition.add(token.getXTotal());
				}
			}
		}

		// get the headways in distance between the tokens of the net
		ArrayList<Float> headways = new ArrayList<>();
		for (int i = 1; i < tokensPosition.size(); i++) {
			headways.add(tokensPosition.get(i) - tokensPosition.get(i - 1));
		}
		headways.add(tokensPosition.get(0) + n.getTotalDistance() - tokensPosition.get(tokensPosition.size() - 1));

		// calcul the standard deviation
		float standardDeviation = 0;
		average = 0;
		for (Float headway : headways) {
			average += headway;
		}
		average = average / headways.size();
		for (Float headway : headways) {
			standardDeviation += Math.pow(headway - average, 2);
		}
		standardDeviation = (float) Math.sqrt(standardDeviation / headways.size());

		return standardDeviation;
	}

	@Override
	public ArrayList<TransitionAbstract> fireableTransition() {
		Collection<EquTransition> values = fireable.values();
		return new ArrayList<>(values);
	}
	
	//return the avg speed, can be reset with resetSpeedData()
	public float getAvgSpeed () {
		if (elapsedTimeToGetAvgSpeed == 0)
			return 0;
		return elapsedDistanceAvg/elapsedTimeToGetAvgSpeed;
		/*if (this.speedSteps != 0)
			return this.totalSpeedSum/this.speedSteps;
		return 0;*/
	}

	@Override
	public HashMap<Integer, ? extends TransitionAbstract> getEnabled() {
		return enabled;
	}

	@Override
	public HashMap<Integer, ? extends TransitionAbstract> getFirable() {
		return fireable;
	}

	@Override
	public boolean isGaussian() {
		return gaussian;
	}

	@Override
	public boolean isWeibull() {
		return weibull;
	}

	@Override
	public int getWeibullCoef() {
		return weibullCoef;
	}

	/**get the total distance of the loop of the network*/
	public float getTotalDistance() {
		return totalDistance;
	}

	/** average of the headway distance between the tokens, used in
	* getStandardDeviation method*/
	public float getAverage() {// get the average headway distance
		getStandardDeviation("");
		return average;
	}

	/**get the time elapsed since the start of the simulation*/
	public float getTimeElapsed() {
		return timeElapsed;
	}

	public int getNbDiscreteSteps() {
		return nbDiscreteSteps;
	}

	/** A constant for the whole simulation describing adherence to target speed
	* This constant is used when computing an advice of time to browse a moving place for a train
	* can be set in the input file as a line : ALPHA:value*/
	public float getALPHA() {
		return ALPHA;
	}

	/** Set Alpha, A constant for the whole simulation describing adherence to target speed
	* This constant is used when computing an advice of time to browse a moving place for a train
	* can be set in the input file as a line : ALPHA:value*/
	public void setALPHA(float aLPHA) {
		ALPHA = aLPHA;
	}

	/**the range for the noise in the calculTime method. can be set in the input file as a line : RANGE_NOISE:value*/
	public static float getRANGE_NOISE() {
		return RANGE_NOISE;
	}

	/**set the range for the noise in the calculTime method. can be set in the input file as a line : RANGE_NOISE:value*/
	public static void setRANGE_NOISE(float rANGE_NOISE) {
		RANGE_NOISE = rANGE_NOISE;
	}

	/**get the speed of a token which has been inserted into a new place while a discrete step was performed*/
	public float getLastTokenSpeed() {
		return lastTokenSpeed;
	}

	/**get the time to browse/clock of a token which has been inserted into a new place while a discrete step was performed*/
	public float getLastTokenTtb() {
		return lastTokenTtb;
	}

	/**return true if the last step is a discrete move*/
	public boolean isDiscreteMove() {
		return discreteStep;
	}

	/**get the current average speed of the moving tokens in the EquNet loop*/
	@LTL
	public float getCurrentAvgSpeed() {
		float sum = 0;
		int nb = 0;
		for (EquPlace p : places.values()) {
			for (Token token : p.tokens) {
				if (token.speed > 0) {
					sum += token.speed;
					nb++;
				}
			}
		}
		if (nb == 0)
			return 0;
		return sum / nb;
	}
	
	/**the min speed at the current state of the moving tokens*/
	@LTL
	public float getCurrentMinSpeed() {
		float minSpeed = Float.POSITIVE_INFINITY;
		for (EquPlace p : places.values()) {
			for (Token token : p.tokens) {
				if (token.speed > 0) {
					minSpeed = Math.min(minSpeed, token.speed);
				}
			}
		}
		if (minSpeed == Float.POSITIVE_INFINITY)
			return 0;
		return minSpeed;
	}
	
	/**the max speed at the current state*/
	@LTL
	public float getCurrentMaxSpeed() {//the min speed at the current state
		float maxSpeed = 0;
		for (EquPlace p : places.values()) {
			for (Token token : p.tokens) {
				maxSpeed = Math.max(maxSpeed, token.speed);
			}
		}
		return maxSpeed;
	}
	
	/**insert a new token in the net at the garage place*/
	public void insertToken() throws NullGarageException {
		Token token = new Token();
		try { 
			token.timeToBrowse = garage.post.sample(s);
			token.speed = garage.distance / token.timeToBrowse;
			if (garage.tokens.size() > 0)
				garage.tokens.get(garage.tokens.size() - 1).previousToken = token;
			garage.addToken(token);
			tokens.put(tokenIndex, token);
			tokenIndex++;
		}catch(NullPointerException e) {
			if (garage == null || garage.post == null) {
				throw new NullGarageException(e);
			}
			e.printStackTrace();
		}
	}

	boolean allowInsertion() {// allow the insertion of a new token in the next place of the garage
		if (garage.tokens.size() > 0 && garage.post.post.get(0).tokens.size() == 0
				&& garage.post.post.get(0).pre.pre.get(0).tokens.size() == 0)
			return true;
		return false;
	}

	void rearrangeTokens() {// we rearrange the orders of the tokens (pre and post)
		Token token = garage.tokens.get(0);
		EquPlace newPlace = garage.post.post.get(0);

		EquPlace nextPlace = newPlace.post.post.get(0);
		while (nextPlace.tokens.size() == 0) {
			nextPlace = nextPlace.post.post.get(0);
		}
		token.postToken = nextPlace.tokens.get(nextPlace.tokens.size() - 1);
		nextPlace.tokens.get(nextPlace.tokens.size() - 1).previousToken = token;

		EquPlace previousPlace = newPlace.pre.pre.get(0);
		while (previousPlace.tokens.size() == 0) {
			previousPlace = previousPlace.pre.pre.get(0);
		}
		token.previousToken = previousPlace.tokens.get(0);
		previousPlace.tokens.get(0).postToken = token;
	}

	/** update the grarage transition, it is expected that a token is at the end of
	* the garage place*/
	void updateGarageTransition() {
		Token token = garage.tokens.get(0);
		EquPlace p = garage;
		if (token.isAtTheEnd()) {
			enabled.remove(p.post.getNumber());
			EquPlace nextPlace = p.post.post.get(0);
			if (nextPlace.tokens.size() > 0 || nextPlace.pre.pre.get(0).tokens.size() > 0) {
				if (!blocked.containsKey(p.post.getNumber())) {
					blocked.put(p.post.getNumber(), p.post);
					p.tokens.get(0).blockToken();
				}
			} else {
				if (blocked.containsKey(p.post.getNumber())) {
					blocked.remove(p.post.getNumber());
					p.tokens.get(0).unblockToken();
				}
				fireable.put(p.post.getNumber(), p.post);
				// System.out.println("make "+p.post.getName()+" fireable");
			}
		}
	}	
	
	/**used to reset the speed data information used to get average speed on a part of a simulation*/
	public void resetSpeedData () {
		elapsedDistanceAvg = 0;
		elapsedTimeToGetAvgSpeed = 0;
	}
	
	/**get the current speed of the token number set in parameter
	 * @return the current speed*/
	@LTL
	public float getSpeed(Integer tokenNumber) {
		return tokens.get(tokenNumber).getSpeed(); 
	} 
	
	/**get the time to browse a place or the time to be elapsed to allow the token to be ready to get to a next place
	 * @param tokenNumber the id of the token from 0 to n-1, n is the number of tokens in the EquNet 
	 * @return the time to browse a place*/
	@LTL
	public float getTimeToBrowse(Integer tokenNumber) {
		ArrayList<Token> tok = refreshTokensList();
		return tokens.get(tokenNumber).getTimeToBrowse(); 
	} 
	
	ArrayList<Token> refreshTokensList(){
		ArrayList<Token> tok = new ArrayList<>();
		/*for (EquPlace p : places.values()) {
			for (Token t : p.tokens) {
				tok.add(t);
			}
		}*/
		return tok;
	}
	
	/**get the position of a token in a place
	 * @param tokenNumber the id of the token from 0 to n - 1, n is the number of tokens*/
	@LTL
	public float getDistanceInPlace(Integer tokenNumber) {
		ArrayList<Token> tok = refreshTokensList();
		return tokens.get(tokenNumber).getxPlace(); 
	} 
	
	/**get the position of a token in the EquNet
	 * @param tokenNumber the id of the token from 0 to n - 1, n is the number of tokens*/
	@LTL
	public float getDistanceInNetwork(Integer tokenNumber) {
		ArrayList<Token> tok = refreshTokensList();
		return tokens.get(tokenNumber).getXTotal();
	}
	
	/**get the clock of a transition
	 * @param transitionNumber the id of the transition as described in the .net inpute file
	 * i.e. transition:3:ta2:[0.5,1] -> id = 3
	 * @throws TransitionNotFoundException */
	@LTL
	public float getClock(Integer transitionNumber) throws TransitionNotFoundException {
		try {
			if (transitions.get(transitionNumber).getClock() == null)
				return 0;
			return transitions.get(transitionNumber).getClock();
		} catch (Exception e) {
			throw new TransitionNotFoundException();
		}
	}
	
	/**get the clock of a transition
	 * @param name the name of the transition
	 * @throws InvocationTargetException 
	 */
	@LTL
	public float getTransitionClock(String name) throws Exception {
		for (EquTransition transition : transitions.values()) {
			if (transition.getName().equals(name)) {
				if (transition.getClock() == null)
					return 0;
				return transition.getClock();
			}	
		}
		throw new TransitionNotFoundException();
	}

	@Override 
	public LinkedHashMap<Integer, Token> getTokens(){
		return tokens;
	}
	
}
