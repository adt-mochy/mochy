package fr.inria.mochy.core.equalization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;

import org.neuroph.core.Connection;
import org.neuroph.core.Layer;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.Neuron;
import org.neuroph.core.Weight;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.CompetitiveLearning;
import org.neuroph.util.TransferFunctionType;
import org.neuroph.util.random.WeightsRandomizer;

import fr.inria.mochy.core.abstractClass.TransitionAbstract;

/**
 * An EqualizationNet EquNetNeural model with transitions, places and their
 * state. It is loaded from a net file. The time to browse is calculated with a
 * neural network/IA. It is an abstract class which extends EquNet, its subclass
 * are EquNetNeuralFix and EquNetNeuralMov.
 * <p>
 * <img src="EquNetNeural.svg" alt="Neural Net class diagram" style="float:
 * right;">
 */
public abstract class EquNetNeural extends EquNet {

	public NeuralNetwork nnet = new MultiLayerPerceptron(TransferFunctionType.TANH, 2, 10, 10, 10, 1);
	CompetitiveLearning l;
	String nnetPath = null;
	// targetSpeed and beta are used in statsAndTask.OptimizeNeuralNetTask in the
	// MochyUi project
	int targetSpeed = 500;
	float beta = 0;
	NeuralNetwork mainNnet = null;
	HashMap<Integer, String> specificNnets = new HashMap<>();
	boolean normalized = false;

	/**
	 * instantiate EquNetNeural, set the input file path and load the neural network
	 * used as specified in the input file as neuralNet:pathInTheNeuralFolder
	 */
	public EquNetNeural(String fname) throws FileNotFoundException {
		super(fname);
		try {
			FileReader file = new FileReader(fname);
			BufferedReader br = new BufferedReader(file);
			String line;
			while ((line = br.readLine()) != null) {
				if (line.startsWith("neuralNet")) {
					String[] segments = line.split(":");
					String path = "";
					if (!isInt(segments[1])) {
						path = "neural/" + segments[1];
						nnet = NeuralNetwork.createFromFile(path);
						nnetPath = path;
						mainNnet = nnet;
					} else {// allow the possibility to add a network around -2 and +2 tokens to the token
						// specified in the input file with neuralNet:tokensNumber:neuralNetFilePath
						int tokensNb = Integer.parseInt(segments[1]);
						if (tokensNb >= 7)
							specificNnets.put(tokensNb - 2, segments[2]);
						if (tokensNb >= 6)
							specificNnets.put(tokensNb - 1, segments[2]);
						specificNnets.put(tokensNb, segments[2]);
						specificNnets.put(tokensNb + 1, segments[2]);
						specificNnets.put(tokensNb + 2, segments[2]);
					}
				} else if (line.startsWith("targetSpeed")) {
					String[] segments = line.split(":");
					targetSpeed = Integer.parseInt(segments[1]);
				} else if (line.startsWith("beta")) {
					String[] segments = line.split(":");
					beta = Float.parseFloat(segments[1]);
				} else if (line.startsWith("normalized")) {
					String[] segments = line.split(":");
					if (segments[1].equals("true"))
						normalized = true;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String loadFile() {
		String infos = super.loadFile();

		System.out.println("nb of tokens : " + nbTokens);
		if (specificNnets.containsKey(this.nbTokens)) {
			System.out.println("true");
			String path = "neural/" + specificNnets.get(this.nbTokens);
			nnet = NeuralNetwork.createFromFile(path);
			nnetPath = path;
		}

		return infos;
	}

	/**
	 * compute the duration for the trip in the next segment that minimizes the
	 * headway difference, and the distance to a target speed Warning : we assume
	 * the considered net is a ring.
	 */
	float calculTime(Token token, EquTransition t) {
		if (t.pre.get(0).distance == 0)// if we are at a stop
			return t.sample(s);

		float DnPlusUn = token.postToken.getXTotal() - token.getXTotal();
		// Distance to the next token in the net
		// computed as the distance of next token wrt origin of the ring net
		// minus the distance of current token to origin
		if (DnPlusUn < 0)
			DnPlusUn = token.postToken.getXTotal() + getTotalDistance() - token.getXTotal();
		// if the difference is negative, then the next token has already passed the
		// origin
		// Make a round of XTotal to prevent the distance to be lightly superior to the
		// distance of the place

		float DnMoinsUn = token.getXTotal() - token.previousToken.roundXTotal();
		
		// distance to previous token in the ring net
		if (DnMoinsUn < 0) {
			DnMoinsUn = token.getXTotal() + getTotalDistance() - token.previousToken.roundXTotal();
		}
		float VnMoinsUn = token.previousToken.speed;
		if (VnMoinsUn == 0) {// get the speed at the next place
			EquPlace p = token.previousToken.p.post.post.get(0);
			VnMoinsUn = p.distance / p.post.getNormalTime();
		}
		float VnPlusUn = token.postToken.speed;
		if (VnPlusUn == 0) {
			EquPlace p = token.postToken.p.post.post.get(0);
			VnPlusUn = p.distance / p.post.getNormalTime();
		}
		double[] input = new double[4];

		if (nnet.getInputNeurons().length == 2) {
			input = new double[] { (double) DnMoinsUn, (double) DnPlusUn };
		} else if (nbTokens < 5 && nnet.getInputNeurons().length == 4) {
			// avec les distances :
			input = new double[] { (double) DnMoinsUn, (double) DnPlusUn, (double) 0, (double) 0 };
			// avec les vitesses :
			input = new double[] { (double) DnMoinsUn, (double) DnPlusUn, (double) VnMoinsUn, (double) VnPlusUn };
		} else if (nbTokens >= 5 && nnet.getInputNeurons().length == 4) {
			float DnMoinsDeux = token.getXTotal() - token.previousToken.previousToken.roundXTotal();
			if (DnMoinsDeux < 0)
				DnMoinsDeux = token.getXTotal() + getTotalDistance() - token.previousToken.previousToken.roundXTotal();
			float DnPlusDeux = token.postToken.postToken.getXTotal() - token.getXTotal();
			if (DnPlusDeux < 0)
				DnPlusDeux = token.postToken.postToken.getXTotal() + getTotalDistance() - token.getXTotal();
			// avec les distances :
			input = new double[] { (double) DnMoinsUn, (double) DnPlusUn, (double) DnMoinsDeux, (double) DnPlusDeux };
			// avec les vitesses :
			// input = new double[]{(double) DnMoinsUn, (double) DnPlusUn , (double)
			// VnMoinsUn, (double) VnPlusUn};
		} else if (nbTokens < 5 && nnet.getInputNeurons().length == 6) {
			input = new double[] { (double) DnMoinsUn, (double) DnPlusUn, (double) 0, (double) 0, 0, 0 };
		} else if (nnet.getInputNeurons().length == 6) {
			// avec les distances n-2, n+2 et les vitesses
			float DnMoinsDeux = token.getXTotal() - token.previousToken.previousToken.roundXTotal();
			if (DnMoinsDeux < 0)
				DnMoinsDeux = token.getXTotal() + getTotalDistance() - token.previousToken.previousToken.roundXTotal();
			float DnPlusDeux = token.postToken.postToken.getXTotal() - token.getXTotal();
			if (DnPlusDeux < 0)
				DnPlusDeux = token.postToken.postToken.getXTotal() + getTotalDistance() - token.getXTotal();
			if (normalized) {
				DnMoinsUn = (DnMoinsUn * (float) Math.sqrt(nbTokens)) / totalDistance;
				DnPlusUn = (DnPlusUn * (float) Math.sqrt(nbTokens)) / totalDistance;
				DnMoinsDeux = (DnMoinsDeux * (float) Math.sqrt(nbTokens)) / totalDistance;
				DnPlusDeux = (DnPlusDeux * (float) Math.sqrt(nbTokens)) / totalDistance;
				VnMoinsUn = (VnMoinsUn - MIN_SPEED) / (MAX_SPEED - MIN_SPEED);
				VnPlusUn = (VnPlusUn - MIN_SPEED) / (MAX_SPEED - MIN_SPEED);
			}
			input = new double[] { (double) DnMoinsUn, (double) DnPlusUn, (double) DnMoinsDeux, (double) DnPlusDeux,
					(double) VnMoinsUn, (double) VnPlusUn };
		} else if (nnet.getInputNeurons().length == 7) {
			// avec les distances n-2, n+2 et les vitesses et le nb de jetons
			float DnMoinsDeux = token.getXTotal() - token.previousToken.previousToken.roundXTotal();
			if (DnMoinsDeux < 0)
				DnMoinsDeux = token.getXTotal() + getTotalDistance() - token.previousToken.previousToken.roundXTotal();
			float DnPlusDeux = token.postToken.postToken.getXTotal() - token.getXTotal();
			if (DnPlusDeux < 0)
				DnPlusDeux = token.postToken.postToken.getXTotal() + getTotalDistance() - token.getXTotal();
			DnMoinsUn = (DnMoinsUn * (float) Math.sqrt(nbTokens)) / totalDistance;
			DnPlusUn = (DnPlusUn * (float) Math.sqrt(nbTokens)) / totalDistance;
			DnMoinsDeux = (DnMoinsDeux * (float) Math.sqrt(nbTokens)) / totalDistance;
			DnPlusDeux = (DnPlusDeux * (float) Math.sqrt(nbTokens)) / totalDistance;
			VnMoinsUn = (VnMoinsUn - MIN_SPEED) / (MAX_SPEED - MIN_SPEED);
			VnPlusUn = (VnPlusUn - MIN_SPEED) / (MAX_SPEED - MIN_SPEED);
			input = new double[] { (double) DnMoinsUn, (double) DnPlusUn, (double) DnMoinsDeux, (double) DnPlusDeux,
					(double) VnMoinsUn, (double) VnPlusUn, (nbTokens - 17) / 17 };
		}

		nnet.setInput(input);
		nnet.calculate();
		double[] networkOutput = nnet.getOutput();
		// System.out.println(" Input: " + Arrays.toString(input));
		// displayWeights();
		float output = (float) (t.getNormalTime() * (1 + networkOutput[0]));
		/*************/
		/*if (nbTokens > 15)
			output = (float) (t.getNormalTime() * (1 + networkOutput[1]));
		else if (nbTokens > 35)
			output = (float) (t.getNormalTime() * (1 + networkOutput[2]));*/
		/*************/
		float speed = t.pre.get(0).distance / output;
		if (speed > MAX_SPEED)
			speed = MAX_SPEED;
		// System.out.println(" Output: " + output + " speed : " + speed);
		return output;
	}

	/**
	 * display the weights of the connections of the neural network loaded in the
	 * console
	 */
	public void displayWeights() {
		int i = 0;
		for (Layer layer : nnet.getLayers()) {
			System.out.println("layer " + i);
			int j = 0;
			for (Neuron neuron : layer.getNeurons()) {
				System.out.println("neuron " + j);
				for (Connection c : neuron.getInputConnections()) {
					System.out.println(c.getWeight());
				}
				/*
				 * for (Weight w : neuron.getWeights()) System.out.println(w.value);
				 */
				j++;
			}
			i++;
		}
	}

	/** set the neural network to use as the regulation during the simulation */
	public void setNeuralNetwork(NeuralNetwork neuralNetwork) {
		this.nnet = neuralNetwork;
	}

	/** get the neural network used as the regulation during the simulation */
	public NeuralNetwork getNnet() {
		return nnet;
	}

	/**
	 * get the path of the neural network used as the regulation during the
	 * simulation
	 */
	public String getNnetPath() {
		return nnetPath;
	}

	/**
	 * save the current neural network and stats in pathToData they are in the
	 * neural folder
	 * 
	 * @param outOfTargetNb
	 * @param averageSpeed
	 * @param averageTimeElapsed
	 * @param value              : timeElapsed + beta * |targetSpeed - averageSpeed|
	 */
	public void saveNeuralNetwork(String pathToData, int outOfTargetNb, float value, float averageSpeed, float time,
			boolean setNnet) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String path = "neural/" + timestamp.toString().replace(":", "");

		File file = new File(path + ".nnet");
		FileWriter writer = null;
		try {
			file.createNewFile();
			writer = new FileWriter(file, false);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.nnet.save(file.getAbsolutePath());
		if (setNnet) {
			this.nnetPath = file.getAbsolutePath();
			this.setNeuralNetwork(NeuralNetwork.createFromFile(file.getAbsolutePath()));
		}
		file = new File(pathToData);
		writer = null;
		try {
			writer = new FileWriter(file, true);
			writer.write(timestamp.toString().replace(":", "") + ";" + String.valueOf(time).replace(".", ",") + ";"
					+ String.valueOf(averageSpeed).replace(".", ",") + ";" + String.valueOf(value).replace(".", ",")
					+ ";" + outOfTargetNb + ";" + String.valueOf(getAvgSpeed()).replace(".", ",") + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * @Override public float getAvgSpeed () { //if (this.speedSteps != 0) //return
	 * this.totalSpeedSum/this.speedSteps; return 0; }
	 */

	@Override
	public void reset(boolean value) {
		super.reset(value);
		// this.totalSpeedSum = 0;
		// this.speedSteps = 0;
	}

	/**
	 * get the target speed as specified in the input file as a line :
	 * targetSpeed:value it is used in statsAndTask.OptimizeNeuralNetTask in the
	 * MochyUi project where it is seeked to minimize (timeElapsed + beta *
	 * |targetSpeed - averageSpeed|)
	 */
	public int getTargetSpeed() {
		return targetSpeed;
	}

	/**
	 * get the beta coefficient as specified in the input file as a line :
	 * beta:value it is used in statsAndTask.OptimizeNeuralNetTask in the MochyUi
	 * project where it is seeked to minimize (timeElapsed + beta * |targetSpeed -
	 * averageSpeed|)
	 */
	public float getBeta() {
		return beta;
	}

	/** check if a String contains only numbers */
	static boolean isInt(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (!Character.isDigit(s.charAt(i)))
				return false;
		}
		return true;
	}

}
