package fr.inria.mochy.core.equalization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;

import org.neuroph.core.Layer;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.Neuron;
import org.neuroph.core.Weight;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.util.TransferFunctionType;
import org.neuroph.util.random.WeightsRandomizer;

import fr.inria.mochy.core.abstractClass.TransitionAbstract;

/**
 * An EqualizationNet EquNetNeural model with transitions, places and their state. It is loaded from a net
 * file. The time to browse is calculated with a neural network/IA. It uses the fixed blocks method.
 * <p>
 * <img src="EquNetNeural.svg" alt="Neural Net class diagram" style="float: right;">
 */
public class EquNetNeuralFix extends EquNetNeural {

	/**instantiate EquNetNeuralFix, set the input file path and the neural network used as the regulation*/
	public EquNetNeuralFix(String fname) throws FileNotFoundException {
		super(fname);
	}

	@Override
	/**
	 * Advance time by a posiitv evalue Delta The effect is that tokens in places
	 * move toward the en of the physical support that the place represents. This
	 * time progress updates the lisy of blocked transition
	 */
	public void progressTime(Float delta) {
		stepsNb++;
		discreteStep = false;
		timeElapsed += delta;

		// advance the time for the tokens not at the end of the place
		// and not linked to a blocked transition
		for (EquPlace p : places.values()) {
			for (Token token : p.getTokens()) {
				// if (!token.isAtTheEnd() && !blocked.containsKey(p.post.getNumber()))
				if (!token.isAtTheEnd() && !token.blocked)
					token.advanceTime(delta);
			}
		}
		// update the enabled, blocked and fireable hashmaps
		for (EquPlace p : places.values()) {
			for (Token token : p.getTokens()) {
				if (token.isAtTheEnd() && !p.equals(garage)) {
					enabled.remove(p.post.getNumber());
					EquPlace nextPlace = p.post.post.get(0);
					if (token.postToken.p.equals(nextPlace)) {
						if (!blocked.containsKey(p.post.getNumber())) {
							blocked.put(p.post.getNumber(), p.post);
							p.tokens.get(0).blockToken();
						}
					} else {
						if (blocked.containsKey(p.post.getNumber())) {
							blocked.remove(p.post.getNumber());
							p.tokens.get(0).unblockToken();
						}
						fireable.put(p.post.getNumber(), p.post);
						// System.out.println("make "+p.post.getName()+" fireable");
					}
				}
			}
		}
		// manage the insertion of a new token
		if (!allowInsertion() && garage.tokens.size() > 0) {
			updateGarageTransition();
		} else if (allowInsertion()) {// if we are currently ready to insert a new token
			// we rearrange the orders of the tokens (pre and post)
			rearrangeTokens();
			updateGarageTransition();
		}
	}

	@Override
	public String discreteMove(TransitionAbstract onet, String logFile, float currentTime, boolean enableLogs) {
		stepsNb++;
		discreteStep = true;
		EquTransition t = (EquTransition) onet;
		if (!fireable.containsKey(t.getNumber())) {
			System.out.println("transition " + onet.getNumber() + " is not fireable");
			return "transition " + t.getNumber() + " is not fireable";
		}
		fireable.remove(t.getNumber());
		Token token = t.getPre().get(0).getReadyToken();
		t.getPre().get(0).getTokens().remove(token);

		EquTransition preTransition = token.previousToken.p.post;
		if (blocked.containsKey(preTransition.getNumber())) {
			blocked.remove(preTransition.getNumber());
			preTransition.pre.get(0).tokens.get(0).unblockToken();
			fireable.put(preTransition.getNumber(), preTransition);
		}

		/****************************************************************/
		/************************** add the J calculus ********************/
		float timeToBrowse = calculTime(token, t.getPost().get(0).getPost());

		// move the considered token in the next place in the ring, with
		// a trip duration equal to timetobrowse
		token.changePlace(token.p.getPost().getPost().get(0), timeToBrowse);

		lastTokenSpeed = token.speed;
		lastTokenTtb = token.timeToBrowse;
		if (token.speed != 0) {
			//totalSpeedSum += token.speed;
			//speedSteps ++;
		}

		// manage the insertion of a new token
		if (!allowInsertion() && garage.tokens.size() > 0) {
			updateGarageTransition();
		} else if (allowInsertion()) {// if we are currently ready to insert a new token
			// we rearrange the orders of the tokens (pre and post)
			rearrangeTokens();
			updateGarageTransition();
		}

		// t.getPost().get(0).getPost().getPost().get(0)
		// log the new Token information
		if (enableLogs) {
			FileWriter fileLogs = null;
			try {
				fileLogs = new FileWriter(logFile, true);
				fileLogs.write("Transition " + t.getName() + " fired: new Token in place " + token.p.getName()
						+ " : Distance " + token.p.getDistance() + " : Time " + token.timeToBrowse + " : Speed "
						+ token.speed + "\n");
				// System.out.println("Transition " + t.getName() + " fired: new Token in place
				// " + token.p.getName()
				// + " : Distance " + token.p.getDistance() + " : Time " + token.timeToBrowse +
				// " : Speed "
				// + token.speed + "\n");
			} catch (IOException e) {
				logger.log(Level.WARNING, "error of writing in fileLogs in class EquNetV2Fix");
			} finally {
				try {
					fileLogs.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the fileLogs in class EquNetV2Fix");
				}
			}
		}
		nbDiscreteSteps++;
		return "transition " + t.getNumber() + " fired";
	}

	

	@Override
	public float minimumClock() {
		// browse the tokens and get the minimal clock
		// for a token to be at the end of the place
		float clock = Float.POSITIVE_INFINITY;
		for (EquPlace p : places.values()) {
			for (Token token : p.getTokens()) {
				if (!token.blocked) {
					clock = Math.min(clock, token.timeToBrowse);
					// System.out.println("clock 1 : "+token.timeToBrowse+", speed : "+token.speed);
				}
			}
		}
		return clock;
	}
}
