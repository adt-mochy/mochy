package fr.inria.mochy.core.equalization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;

import org.neuroph.core.Layer;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.Neuron;
import org.neuroph.core.Weight;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.util.TransferFunctionType;
import org.neuroph.util.random.WeightsRandomizer;

import fr.inria.mochy.core.abstractClass.TransitionAbstract;

/**
 * An EqualizationNet EquNetNeural model with transitions, places and their
 * state. It is loaded from a net file. The time to browse is calculated with a
 * neural network/IA. It uses the moving blocks method.
 * <p>
 * <img src="EquNetNeural.svg" alt="Neural Net class diagram" style="float:
 * right;">
 */
public class EquNetNeuralMov extends EquNetNeural {

	/**instantiate EquNetNeuralMov, set the path of the input file and set the neural network used for the regulation*/
	public EquNetNeuralMov(String fname) throws FileNotFoundException {
		super(fname);
	}

	@Override
	/**
	 * Advance time by a posiitv evalue Delta The effect is that tokens in places
	 * move toward the en of the physical support that the place represents. This
	 * time progress updates the lisy of blocked transition
	 */
	public void progressTime(Float delta) {
		stepsNb++;
		discreteStep = false;
		timeElapsed += delta;
		//used to get the elapsedDistanceAvg to get the average speed : 
		float elapsedDistanceSum = 0;
		this.elapsedTimeToGetAvgSpeed += delta;
		int moveTokens = 0;

		// advance the time for the tokens not at the end of the place
		// and not linked to a blocked transition
		for (EquPlace p : places.values()) {
			if (!p.equals(garage)) {
				for (Token token : p.getTokens()) {
					if (!token.isAtTheEnd() && !(token.p.distance == 0)) {
						elapsedDistanceSum += token.speed * delta;
						moveTokens++;
					}
					// block a token if its successor is in the same place, is blocked and it will
					// violates the safety distance rule
					// the distance between the token and the position where the headway must be
					// satisfied
					float distance = token.postToken.xPlace - token.xPlace - this.MIN_HEADWAY_DISTANCE - 0.1f;
					if (!token.blocked && token.postToken.blocked && token.p.tokens.contains(token.postToken)
							&& distance <= 0)
						token.blockToken();
					// adapt the speed of the token to the next if the safety distance is achieved
					// but
					// the next token in the same place is not blocked
					if (token.p.tokens.contains(token.postToken) && !token.blocked && !token.postToken.blocked
							&& distance <= 0f && !token.p.tokens.get(0).equals(token)) {
						token.setTimeToBrowse (token.postToken.getTimeToBrowse()
								+ MIN_HEADWAY_DISTANCE / token.postToken.speed);
						// token.speed = (token.p.distance - token.xPlace) / token.timeToBrowse;
						token.speed = token.postToken.speed;
						//System.out.println("adapt speed "+token.p.getName()+" "+token.speed + " "+token.timeToBrowse);
					}
					// if (!token.isAtTheEnd() && !blocked.containsKey(p.post.getNumber()))
					if (!token.isAtTheEnd() && !token.blocked)
						token.advanceTime(delta);
				}
			}
		}
		//calcul the avg of the elapsed distances
		if (moveTokens != 0)
			this.elapsedDistanceAvg += elapsedDistanceSum / moveTokens;
		// update the enabled, blocked and fireable hashmaps
		for (EquPlace p : places.values()) {
			for (Token token : p.getTokens()) {
				if (token.isAtTheEnd() && !p.equals(garage)) {
					enabled.remove(p.post.getNumber());
					EquPlace nextPlace = p.post.post.get(0);

					if ((token.postToken.xPlace < MIN_HEADWAY_DISTANCE || token.postToken.p.getDistance() == 0)
							&& token.postToken.p.equals(nextPlace)) {
						if (!blocked.containsKey(p.post.getNumber())) {
							blocked.put(p.post.getNumber(), p.post);
							p.tokens.get(0).blockToken();
						}
					} else {
						if (blocked.containsKey(p.post.getNumber())) {
							blocked.remove(p.post.getNumber());
							p.tokens.get(0).unblockToken();
						}
						fireable.put(p.post.getNumber(), p.post);
						//System.out.println(p.getName()+" ready to fire 1, posttoken in place "+token.postToken.p.getName()+" at x = "+token.postToken.xPlace);
						// System.out.println("make "+p.post.getName()+" fireable");
					}
				}
				//if (token.getXTotal()>token.postToken.getXTotal()) {
					//System.out.println("token "+p.getName()+" is over "+token.postToken.p.getName()+" xTotal : "+token.getXTotal()+"-"+token.postToken.getXTotal()+" x : "+token.getxPlace()+"-"+token.postToken.getxPlace());
				//}
			}
		}
		// manage the insertion of a new token
		if (garage.tokens.size() > 0 && !garage.tokens.get(0).isAtTheEnd()) {
			garage.tokens.get(0).advanceTime(delta);
		}
		if (!allowInsertion() && garage.tokens.size() > 0) {
			updateGarageTransition();
		} else if (allowInsertion()) {// if we are currently ready to insert a new token
			// we rearrange the orders of the tokens (pre and post)
			rearrangeTokens();
			updateGarageTransition();
		}
	}

	@Override
	public String discreteMove(TransitionAbstract onet, String logFile, float currentTime, boolean enableLogs) {
		stepsNb++;
		discreteStep = true;
		EquTransition t = (EquTransition) onet;
		if (!fireable.containsKey(t.getNumber())) {
			System.out.println("transition " + onet.getNumber() + " is not fireable");
			return "transition " + t.getNumber() + " is not fireable";
		}
		//System.out.println("fire from place "+onet.getPre().get(0).getName());
		fireable.remove(t.getNumber());
		Token token = t.getPre().get(0).getReadyToken();
		t.getPre().get(0).getTokens().remove(token);
		if (t.getPre().get(0).getTokens().size() > 0) {
			enabled.put(t.getNumber(), t);
			t.setClock(t.getPre().get(0).getTokens().get(0).getTimeToBrowse());// clock is equal to the first token time to
																			// browse
		}

		// if the previous transition was blocked by this one
		// unblock it as the next has been fired
		// (if there is no token < min headway in the next place)

		EquTransition preTransition = token.previousToken.p.post;
		boolean tokenAtStart = false;
		for (Token currentToken : token.p.tokens) {
			if (currentToken.xPlace < MIN_HEADWAY_DISTANCE)
				tokenAtStart = true;
		}
		if (blocked.containsKey(preTransition.getNumber()) && !tokenAtStart) {
			blocked.remove(preTransition.getNumber());
			preTransition.pre.get(0).tokens.get(0).unblockToken();
			fireable.put(preTransition.getNumber(), preTransition);
			//System.out.println(preTransition.pre.get(0).getName()+" ready to fire 2 ");
		}

		/****************************************************************/
		/************************** add the J calculus ********************/
		float timeToBrowse = calculTime(token, t.getPost().get(0).getPost());
		// move the considered token in the next place in the ring, with
		// a trip duration equal to timetobrowse
		token.changePlace(token.p.getPost().getPost().get(0), timeToBrowse);

		lastTokenSpeed = token.speed;
		lastTokenTtb = token.getTimeToBrowse();
		/*if (token.speed != 0) {
			totalSpeedSum += token.speed;
			speedSteps++;
		}*/

		// manage the insertion of a new token
		if (!allowInsertion() && garage.tokens.size() > 0) {
			updateGarageTransition();
		} else if (allowInsertion()) {// if we are currently ready to insert a new token
			// we rearrange the orders of the tokens (pre and post)
			rearrangeTokens();
			updateGarageTransition();
		}

		// t.getPost().get(0).getPost().getPost().get(0)
		// log the new Token information
		if (enableLogs) {
			FileWriter fileLogs = null;
			try {
				fileLogs = new FileWriter(logFile, true);
				fileLogs.write("Time : "+timeElapsed+"Transition " + t.getName() + " fired: new Token in place " + token.p.getName()
						+ " : Distance " + token.p.getDistance() + " : Time " + token.getTimeToBrowse() + " : Speed "
						+ token.speed + "\n");
				// System.out.println("Transition " + t.getName() + " fired: new Token in place
				// " + token.p.getName()
				// + " : Distance " + token.p.getDistance() + " : Time " + token.timeToBrowse +
				// " : Speed "
				// + token.speed + "\n");
			} catch (IOException e) {
				logger.log(Level.WARNING, "error of writing in fileLogs in class EquNetNeuralMov");
			} finally {
				try {
					fileLogs.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the fileLogs in class EquNetNeuralMov");
				}
			}
		}
		nbDiscreteSteps++;
		return "transition " + t.getNumber() + " fired";
	}

	@Override
	public float minimumClock() {
		// browse the tokens and get the minimal clock
		// for a token to be at the end of the place
		float clock = Float.POSITIVE_INFINITY;
		for (EquPlace p : places.values()) {
			for (Token token : p.getTokens()) {
				//System.out.println("token in place "+p.getName()+" ttb = "+token.timeToBrowse+" x = "+token.xPlace+" xTot = "+token.getXTotal()+" speed = "+token.speed+" saved speed : "+token.savedSpeed+" blocked:"+token.blocked);
				if (!token.blocked) {
					clock = Math.min(clock, token.getTimeToBrowse());
					// System.out.println("clock 1 : "+token.timeToBrowse+", speed : "+token.speed);
				}
				// get the minimum time between the clock and the time before a token will be
				// blocked :
				if (!token.p.equals(garage) && token.p.tokens.contains(token.postToken) && !token.blocked
						&& token.postToken.blocked) {
					// the distance between the token and the position where the headway must be
					// satisfied
					float distance = token.postToken.xPlace - token.xPlace - this.MIN_HEADWAY_DISTANCE;
					float time = distance / token.speed;
					clock = Math.min(clock, time);
					// System.out.println("clock 2 : "+time);
				}
				// get the minimum time between the clock and the time necessay for a token to
				// get to
				// the min headway distance of the next token in the same place
				// x2 + d2 - h - x1 - d1 = 0
				// x2 + v2.t - h - x1 - v1.t
				// t = (x1 - x2 + h)/(v2 - v1)
				if (!token.p.equals(garage) && token.p.tokens.contains(token.postToken) && !token.blocked
						&& !token.postToken.blocked && token.speed > token.postToken.speed
						&& !token.postToken.isAtTheEnd() && token.postToken.xPlace > token.xPlace) {
					float timeToBrowse = ( this.MIN_HEADWAY_DISTANCE + token.xPlace - token.postToken.xPlace)
							/ (token.postToken.speed - token.speed);
					clock = Math.min(clock, timeToBrowse);
					//System.out.println("clock 2.5 : "+timeToBrowse+" "+token.speed+" "
					//+token.postToken.speed+" "+token.xPlace+" "+token.postToken.xPlace+" "+token.p.getName());
					// System.out.println("tokens : "+token.p.tokens.size());
				}
			}
		}
		// get the minimum time to advance to make a transition unblocked
		for (EquTransition t : blocked.values()) {
			EquPlace linkedPlace = t.pre.get(0);
			if (!linkedPlace.equals(garage)) {
				EquPlace nextPlace = linkedPlace.post.post.get(0);
				Token tokenBlocking = nextPlace.tokens.get(nextPlace.tokens.size() - 1);
				float distanceToBrowseToUnblock = MIN_HEADWAY_DISTANCE - tokenBlocking.xPlace;
				float timeToElapseToUnblock;
				if (tokenBlocking.speed > 0) {
					timeToElapseToUnblock = distanceToBrowseToUnblock / tokenBlocking.speed;
					clock = Math.min(clock, timeToElapseToUnblock);
					//System.out.println("clock 3 : " + timeToElapseToUnblock);
				} else if (tokenBlocking.getTimeToBrowse() > 0) {
					timeToElapseToUnblock = tokenBlocking.getTimeToBrowse();
					clock = Math.min(clock, timeToElapseToUnblock);
					// System.out.println("clock 4 : "+timeToElapseToUnblock);
				}
			}
		}
		if (clock == Float.POSITIVE_INFINITY) {
			for (EquPlace p : places.values()) {
				for (Token token : p.tokens) {
					System.out.println("Token in place "+p.getName()+" at xPlace="+token.xPlace+" (length="+p.getDistance()+") with speed = "+token.speed+" ttb = "+token.getTimeToBrowse()+" posttoken in "+token.postToken.p.name+" blocked : "+token.blocked);
				}
			}
		}
		return clock;
	}

}
