package fr.inria.mochy.core.equalization;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.sampler.Sampler;


/**
 * A Net model with transitions, places and their state. It is loaded from a net
 * file.
 * <p>
 * <img src="Net.svg" alt="Net class diagram" style="float: right;">
* Net designed by Loic to test Headway regulation with stops
 */


public class EquNetStopsLoic extends EquNet{

	int initialPlaceNumber; // the number of the first place met
	float TARGETSPEED=583.0f; // target speed of trains (m/minute)
	float INITIAL_SPEED=583.0f; // Initial speed of trains (m/minute)
	
	// Default 35k/h, another value (optional) can be read from the loaded net file
	
	float TOTALLENGTH=0.0f; // Total length of a full round trip (in meters)
	// Originally set to 0, the lenght of the network must be computed immediately after loading 
	// the net
	
	

	// A net contains transitions (actions)
	// places (place holders for objects that are moved, consumed or produced)
	// control places : they are used to allow or conversely disable transitions.

	// the semantics is the following :
	// as soon as a transition has all its places in its preset filled, a value is
	// sampled.
	// This is the time to fire (TTF) of the transition.
	// The time to fire decreases over time
	// When the TTF reaches 0, the transition is firable. However, it can fire only
	// if
	// its control place is filled, and if the postset places of the net are free

	
	
	// This class inherits the following ingredients from EquNet
	//public HashMap<Integer, EquPlace> places;  list of places
	//public HashMap<Integer, EquTransition> transitions; list of transitions
	//public HashMap<Integer, EquTransition> enabled; // list of enabled transitions : previous places marked and clock != 0
	//public HashMap<Integer, EquTransition> blocked; // list of blocked transitions : previous places marked, and at least one next place is marked and clock = 0
	
	//public HashMap<Integer, EquTransitionStopsLoic> firable; // list of firable transitions : previous places marked, free next places, clock=0
	// a transition is firable iff its preset is filled, its clock has value 0,
	// and its postset is empty

	
	HashMap<Integer, EquPlace> controlPlaces; // Control Places of the net

	
	Random rand; // random number generator

	
	
	

	Marking initMarking;
	Marking currentMarking;

	HashMap<Integer, Float> Config;
	// Establishes relation between transition numbers
	// and the value of attached clock
	
	HashMap<Integer, Float> initConfig;

	
	
	
	int n = 1; // the number of line, the number of transitions fired

	Logger logger = Logger.getLogger("logger");

	Sampler s;

	// String fname;

	boolean gaussian = false;
	boolean weibull = false;
	int weibullCoef = 30;

	// ***********************************************
	// * Parameters of simulation *
	// ***********************************************
	boolean consume_control_tokens = true; // set to true if contents of control places is consumed
											// when firing transition

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// CONSTRUCTORS
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	public EquNetStopsLoic(String fname) {
        super(fname);
		this.fname = fname;

		rand = new Random();
		s = new Sampler();

		places = new HashMap<>(); // Creates an empty list of places
		controlPlaces = new HashMap<>(); // Creates an empty list of control places
		transitions = new HashMap<>(); // Creates an empty list of transitions
		currentMarking = new Marking(); // creates a marking where all places are empty

		Config = new HashMap<>();
		// Establishes relation between transition numbers
		// and the value of attached clock

		enabled = new HashMap<>();
		blocked = new HashMap<>();
		fireable = new HashMap<>(); 
		setFirable(new HashMap<>());
	}

	
	/**
	 * Computes the total length of a circular network 
	 * CAUTION: may cause an error if the network is not circular or
	 * transitions have more than one successor. Use with care. 
	 * @return
	 */
	public float ComputeTotalLength() {
		
		
		
		System.out.println("Computing Total Length");
		float dis=0.0f;
		
		EquPlace currentPlace=places.get(initialPlaceNumber); // take the first place in the net
		EquPlace startPlace=currentPlace;// remember the starting point;
		
		dis=dis+currentPlace.getDistance();
		currentPlace = currentPlace.getPost().getPost().get(0);
		
		
		while (currentPlace != startPlace) {
		// add the distance of places met, successors after successor and stop 
			// when the start place is met again. 
			
			dis=dis+currentPlace.getDistance();
			currentPlace = currentPlace.getPost().getPost().get(0);
			
		}
		
		totalDistance = dis;
		
		return dis;
		
	}
	
	
	
	
	/**
	 * 
	 */
	public void initializeSpeeds() {
		// find all enabled transitions
		// if it is a departure, set successor speed
		// if it is an arrival sets the clock value
			
		System.out.println("*** Speeds Initialization **** "); 
		Sampler s=new Sampler();	// create a new random number generator 	

		// set speed to 0 and clock to -2 for every transition
		for (EquTransition et : this.transitions.values()) {
			EquTransitionStopsLoic t = (EquTransitionStopsLoic) et; // typecast
			t.setSpeed(0);
			t.setGhostSpeed(INITIAL_SPEED);
			t.setClock(-2);
		}
		
		// visit the list of enabled transitions
		for (EquTransition et : this.enabled.values()) {
			
			EquTransitionStopsLoic t = (EquTransitionStopsLoic) et; // typecast
			
             if (t.getNumber() %2 != 0 ) { // if the transition is an arrival
				
            	 // sets the clock of the transition to the duration up to arrival
            	 float length = t.getPre().get(0).getDistance(); // length of preceding place in meters
            	 float spd = INITIAL_SPEED; // speed in m/min
             	 float clock = length/spd; // duration of trip at initial spped in mins
            	 
             	 t.setSpeed(spd); // set speed of transition
             	 t.setClock(clock); // set speed 
            	 
            	 System.out.println("Current Train(arrival): " +t.getNumber() 
                 + " " +t.getName() + " spd =  " 
	               + t.getSpeed() + "Clock="+t.getClock()
	               +" Distance = "+length);	
            	 
				
				// Sets the value of token in preset accordingly
				t.pre.get(0).tokens.get(0).speed = spd;
				t.pre.get(0).tokens.get(0).timeToBrowse = clock;
				
			} else { // departure

			      System.out.println("Current Train(departure): " +t.getNumber() + " " +t.getName() + "spd =  " + t.getSpeed());

			      t.setSpeed(0); // trains in stations have no speed. By convention take 0
			      t.setGhostSpeed(INITIAL_SPEED); // The ghost speed is not computed yet, we fix an arbitrary value, here INITIAL_SPEED
			      t.dwellSample(s,0.75f, 0.2f); // sample a dwell time around 45 seconds
			      
			   // Sets the value of token in preset accordingly
			      Token tok = t.pre.get(0).tokens.get(0);
					tok.speed = 0;
					tok.timeToBrowse = t.getClock();			      
			      									
			}
		}
	}

	
	
	

	
	
    
	
	
	

	
	

	
	/**
	 * Loads a net from a file
	 * 
	 * @return The method returns a strings to display success/failure in the log
	 *         area of the interface.
	 */
	public String loadFile() {
		// String logs = ""; // The string to return once the net is loaded
		StringBuffer logs = new StringBuffer();
		int lineNb = 1;
		// Open a file
		// reads places, transitions, flows
		// read distributions
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fname));
			System.out.println("*******************");
			System.out.println("Opening File : " + fname);

			while (br.ready()) { // As long as the file contains unread lines
				logs.append(handleLineStopsLoic(br.readLine(), lineNb));
				// += handleLine(br.readLine(), lineNb);
				lineNb++;
			}

			// At this place all lines are read
			// i.e. all places, transitions and the current marking
			// We can hence sample duration for every enabled transition
			// set the gaussian value to true to all the transitions of the net
			// if the file read the Gaussian value and the same if this is weibull
			if (this.gaussian) {
				for (EquTransition teq : this.transitions.values()) {
					
					EquTransitionStopsLoic t=(EquTransitionStopsLoic) teq;
					
					t.setGaussian(true);
					t.setWeibull(false);
					if (currentMarking.presetMarked(t)) {
						t.sample(s);
						enabled.put(t.number, t);
						this.Config.put(t.getNumber(), t.getClock());
					}
				}
			} else if (this.weibull) {
				for (EquTransition teq : this.transitions.values()) {
					EquTransitionStopsLoic t=(EquTransitionStopsLoic) teq;
					t.setGaussian(false);
					t.setWeibull(true);
					t.setWeibullCoef(weibullCoef);
					
					//if (t.presetMarked(currentMarking)) {
					if (currentMarking.presetMarked(t)) {

						t.sample(s);
						enabled.put(t.number, t);
						this.Config.put(t.getNumber(), t.getClock());
					}
				}
			} else {
				for (EquTransition teq : this.transitions.values()) {
					
					EquTransitionStopsLoic t=(EquTransitionStopsLoic) teq;
					
					//if (t.presetMarked(currentMarking)) {
					if (currentMarking.presetMarked(t)) {

						t.sample(s);
						enabled.put(t.number, t);
						this.Config.put(t.getNumber(), t.getClock());
					}
				}
			}

			br.close();
			System.out.println("*******************");
			System.out.println("Initialization");
			TOTALLENGTH=ComputeTotalLength();
			System.out.println("Total length of Network : "+ TOTALLENGTH);
			
			initializeSpeeds();
			
			arrangeTokensOrder();
			
		
			System.out.println("*******************");

			// from initial marking, build initial configuration

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class Net");
				}
			}
		}

		initMarking = currentMarking.copy();
		initConfig = Config;

		return logs.toString();
	}

	protected String handleLineStopsLoic(String line, int lineNb) {
		System.out.println(line);

		String logs = "";
		String[] segments = line.split(":");

		if (segments[0].equals("place")) {
			// System.out.println("A place");
			try {
				int nb = Integer.parseInt(segments[1]);
				int distance = Integer.parseInt(segments[3]);
				// TODO Warning, the original distance was not defined in the original files
				
				if (!startPlace) {// if no starting place was defined yet
					places.put (nb, new EquPlace(this, nb, segments[2], distance, true));
					startPlace = true;
					initialPlaceNumber=nb;
				} else {
				
				places.put(nb, new EquPlace(this,nb, segments[2], distance , false) );
				}
				
				
			} catch (NumberFormatException nfe) {
				logs += "Line " + lineNb + " : error segment 1 is not a number\n";
			}
		}

		else if (segments[0].equals("control")) {
			System.out.println("A control place");

			try {
				controlPlaces.put(Integer.parseInt(segments[1]), 
			       new EquPlace(this, Integer.parseInt(segments[1]), segments[2],0,false));
			} catch (NumberFormatException nfe) {
				System.out.println("Failed");

				logs += "Line " + lineNb + " : error segment 1 is not a number\n";
			}
		}

		else if (segments[0].equals("transition")) {
			// System.out.println("A transition");
			EquTransitionStopsLoic t;
			// if an interval has been set up in the parameters
			int transitionNb = 0;
			try {
				transitionNb = Integer.parseInt(segments[1]);
			} catch (NumberFormatException nfe) {
				logs += "Line " + lineNb + " : error segment 1 is not a number\n";
			}

			if (segments.length > 3) {
				String[] interval = segments[3].substring(1, segments[3].length() - 1).split(",");
				// if the upperBound is infinite

				if (interval[1].equals("inf")) {
					float min = 0;
					try {
						min = Float.parseFloat(interval[0]);
					} catch (NumberFormatException nfe) {
						logs += "Line " + lineNb + " : error the interval format is not [nb,nb] or [nb,inf]\\n";
					}
					t = new EquTransitionStopsLoic(transitionNb, segments[2], min, null);
				}
				// if both bounds are definite
				else {
					float min = 0, max = 0;
					try {
						min = Float.parseFloat(interval[0]);
						max = Float.parseFloat(interval[1]);
					} catch (NumberFormatException nfe) {
						logs += "Line " + lineNb + " : error the interval format is not [nb,nb] or [nb,inf]\n";
					}
					t = new EquTransitionStopsLoic(transitionNb, segments[2], min, max);
				}
				if (segments.length > 4) {
					if (segments[4].equals("Gaussian")) {
						t.setGaussian(true);
					} else if (segments[4].equals("Weibull") && segments.length > 5) {
						t.setWeibull(true);
						try {
							t.setWeibullCoef(Integer.parseInt(segments[5]));
						} catch (NumberFormatException nfe) {
							logs += "Line " + lineNb + " : error the coeficient of Weibull is not a number\n";
						}
					} else if (segments[4].equals("Weibull") && segments.length <= 5)
						logs += "Line " + lineNb + " : error the coeficient of Weibull is not defined\n";
				}
			}
			// if no bounds are definite
			else {
				t = new EquTransitionStopsLoic(transitionNb, segments[2], 0f, null);
			}
			transitions.put(t.getNumber(), t);
		}

		else if (segments[0].equals("inflow")) {
			// System.out.println("Inflow");
			int trans = 0, place = 0;
			try {
				trans = Integer.parseInt(segments[1]);
				place = Integer.parseInt(segments[2]);
				if (this.findTransition(trans) == null)
					logs += "Line " + lineNb + " : error the transition, segment 1, is not found\n";
				if (this.findPlace(place) == null)
					logs += "Line " + lineNb + " : error the place, segment 2, is not found\n";
				if (this.findTransition(trans) != null && this.findPlace(place) != null)
					this.addInFlow1(trans, place);
			} catch (NumberFormatException nfe) {
				logs += "Line " + lineNb + " : error the origin or destination is not a number\n";
			}
		}

		else if (segments[0].equals("outflow")) {
			// System.out.println("Outflow");
			int trans = 0, place = 0;
			try {
				trans = Integer.parseInt(segments[1]);
				place = Integer.parseInt(segments[2]);
				if (this.findTransition(trans) == null)
					logs += "Line " + lineNb + " : error the transition, segment 1, is not found\n";
				if (this.findPlace(place) == null)
					logs += "Line " + lineNb + " : error the place, segment 2, is not found\n";
				if (this.findTransition(trans) != null && this.findPlace(place) != null)
					this.addOutFlow(trans, place);
			} catch (NumberFormatException nfe) {
				logs += "Line " + lineNb + " : error the origin or destination is not a number\n";
			}
		}

		else if (segments[0].equals("initial")) {
			System.out.println("Initial Marking : " + (segments.length - 1) + " place(s) marked");

			for (int i = 1; i < segments.length; i++) {
				Integer pn = 0;
				try {
					pn = Integer.parseInt(segments[i]);
				} catch (NumberFormatException nfe) {
					logs += "Line " + lineNb + " : error the segment " + i + " is not a number\n";
				}
				System.out.println("Searching Place:" + pn +" Net has "+places.size() +" places");

				// add place to current marking
				EquPlace pl = (EquPlace) this.findPlace(pn);
				System.out.println("Place found" + pl);
				if (pl == null)
					logs += "Line " + lineNb + " : error the place, segment " + i + ", is not found\n";
				else {
					
					//pl.contents = new Float(0);
					//pl.addToken(new Token());
					Token token = new Token(pl, pl.pre.getClock());
					System.out.println("***create token in place "+pl.getName());
					pl.addToken(token);
					this.currentMarking.add(pl);
				}
			}
			System.out.println("Initial Marking Parsed");

		} // end of parsing of line for initial marking

		else if (segments[0].equals("Gaussian")) {
			gaussian = true;
			logs += "Law of Gauss";
		} else if (segments[0].equals("Weibull")) {
			weibull = true;
			weibullCoef = Integer.valueOf(segments[1]);
			logs += "Law of Weibull";
		} else if (segments[0].equals("ALPHA")) {
			
		//Handle value of ALPHA
			this.ALPHA = Float.parseFloat(segments[1]);
			logs +="ALPHA=" + ALPHA+"\n";
			
		} else if (segments[0].equals("MIN_HEADWAY_DISTANCE")) {
		
		// handle headways
			this.MIN_HEADWAY_DISTANCE = Integer.parseInt(segments[1]);
			logs +="MIN_HEADWAY_DISTANCE=" + MIN_HEADWAY_DISTANCE+"\n";
			
		} else if (segments[0].equals("MAX_SPEED")) {
			logs +="MAX_SPEED=" + MAX_SPEED+"\n";
			
			//Handle max speed
			this.MAX_SPEED = Integer.parseInt(segments[1]);
		
         } else if (segments[0].equals("TARGET_SPEED")) {
			
			//Handle target speed
			this.TARGETSPEED = Integer.parseInt(segments[1]);
			System.out.println("TARGET SPPED set to "+ TARGETSPEED);
			logs +="TARGET_SPEED=" + TARGETSPEED+"\n";
			
		} else if (segments[0].equals("RANGE_NOISE")) {
			
			//handle noise
			this.RANGE_NOISE = Float.parseFloat(segments[1]);
			logs +="NOISE RANGE=" + RANGE_NOISE+"\n";
			
				
		} else if (segments[0].equals("%")) {
			// This line is a comment and can be ignored
		}

		// System.out.println("Initial Marking parsed");

		return logs; // returns the line to log in the log window

	}// end of method Handleline

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// %% Net Construction functions %%
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	// adds a place in the preset of a transition
	protected void addInFlow1(int tnum, int pnum) {

		EquPlace p = this.findPlace(pnum);
		EquTransition t = this.findTransition(tnum);

		if (this.isAControlPlace(pnum)) {
			t.addControlPre(p);
		} else {
			t.addPre(p);
		}
		//		p.addPost(t);
		p.setPost(t);

	}

	// adds a place in the postset of a transition
	protected void addOutFlow(int tnum, int pnum) {

		EquPlace p = this.findPlace(pnum);
		EquTransition t = this.findTransition(tnum);
		t.addPost(p);
		
		//p.addPre(t);
		p.setPre(t);

	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// %% Useful functions %%
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	/**
	 * Find a place from its number This place can be a standard place or a control
	 * place
	 * 
	 * @param pnum
	 * @return the place found
	 */
	public EquPlace getPlace(int pnum) {

		if (places.containsKey(pnum))
			return places.get(pnum);

		if (controlPlaces.containsKey(pnum))
			return controlPlaces.get(pnum);

		return null;
	}

	public EquPlace getControlPlace(int pnum) {

		if (controlPlaces.containsKey(pnum))
			return controlPlaces.get(pnum);

		return null;
	}

	/**
	 * Add a token in a standard place
	 * 
	 * @param pnum
	 * @return
	 */
	public Boolean addToken(int pnum) {

		EquPlace p = getPlace(pnum);
		if (p != null) {
			p.addToken();
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Adds a token in a control place
	 * 
	 * @param pnum
	 * @return
	 */
	public Boolean addControlToken(int pnum) {

		EquPlace p = getControlPlace(pnum);
		if (p != null) {
			p.addToken();
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Checks is a place is a control places
	 * 
	 * @param pnum : the place number
	 * @return true if pnum is a control place, false otherwise
	 */
	public boolean isAControlPlace(int pnum) {
		if (controlPlaces.containsKey(pnum))
			return true;

		return false;
	}

	// Finds a transition from its number
	public EquTransitionStopsLoic findTransition(Integer tnum) {
		if (transitions.containsKey(tnum))
			return (EquTransitionStopsLoic) transitions.get(tnum);
		return null;
	}

	// Finds a transition from its name
	public EquTransitionStopsLoic findTransition(String name) {
		for (EquTransition teq : this.transitions.values()) {
			EquTransitionStopsLoic t=(EquTransitionStopsLoic) teq;
			
			if (t.getName().equals(name))
				return t;
		}
		return null;
	}

	// Checks if a transition is fireable, i.e belongs to the list of firable
	// transitions
	public boolean isFirable(EquTransition t) {
		if (fireable.containsKey(t.getNumber()))
			return true;

		return false;
	}

	/**
	 * Checks if the control places used as input to the current transition allow
	 * firing of t
	 * 
	 * @param t
	 * @return
	 */
	public boolean controlAllowsFiring(EquTransition t) {

		return t.controlAllowsFiring();

	}

	// Checks if a transition is enabled, i.e belongs to the list of enabled
	// transitions
	public boolean isEnabled(EquTransition t) {
		if (enabled.containsKey(t.number))
			return true;

		return false;
	}
	
	
	
	
//	public Transition findNextTransition(Transition t) {
//		
//		Place nextp=t.getPost().get(0); // find the next place
//		
//		Transition ntr = nextp.findNextTransition();
//		
//		return ntr;
//	}

	// Finds a place from its number
	public EquPlace findPlace(int pnum) {
		// System.out.println("searching place" + pnum);
		if (places.containsKey(pnum))
			return places.get(pnum);

		if (controlPlaces.containsKey(pnum))
			return controlPlaces.get(pnum);

		// System.out.println("Place not found");

		return null;
	}

	/**
	 * Returns true if pnum is the number of a control place
	 * 
	 * @param pnum
	 * @return
	 */
	public boolean isAControlPace(int pnum) {

		// if (places.containsKey(pnum))
		// return false;

		if (controlPlaces.containsKey(pnum))
			return true;

		return false;
	}

	// returns true if a transitions has a non-empty postset
	public boolean busyPostSet(Integer tnum) {

		EquTransition t = findTransition(tnum);
		ArrayList<EquPlace> post = t.post;

		for (EquPlace p : post) {
			// if one can find a place in psotset that
			// contains something then t is blocked
			if (p.isMarked()) {
				return true;
			}
		}

		return false;
	}

	// returns a list of newly enabled transition
	public ArrayList<EquTransition> newlyEnabled(Marking m1, Marking m2, EquTransition t) {

		ArrayList<EquTransition> result = new ArrayList<>();

		for (EquTransition tr : this.transitions.values()) {
			if (t.isNewlyEnabled(m1, m2, tr)) {
				result.add(tr);
			}
		}
		return result;

	}

	public void setMarking(ArrayList<EquPlace> l, ArrayList<Float> vallist) {

		// all places supposed empty at start
		// place list and contents list same size
		ListIterator<Float> it;

		it = vallist.listIterator();
		Float f;

		for (EquPlace p : l) {
			f = it.next();
			Token tok = new Token (p,f);
			p.addToken(tok);
		}
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// SEMANTICS
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	public void blockTransition(EquTransitionStopsLoic t) {
		// block a transition which clock reaches 0 and can
		// not be fired

		this.blocked.put(t.number, t);

	}

	public float maxAllowedTimedMove() {
		
		System.out.println("Max Allowed Timed Move");
		
		// returns the maximal allowed timed move, i.e.
		// the mimimal time to fire among enabled transitions
		if (!getFirable().isEmpty()) {
			System.out.println("One transition firable : min TTF=0");
			
			return new Float(0);
		}
		if (!enabled.isEmpty()) {
			
			System.out.println("Transition enabled : min TTF= minimal elapsing time");
			
			Float min = new Float(10000);
			// compare to all values in the table
			for (EquTransition t : this.enabled.values()) {
				System.out.println(t.name);
				if (t.getClock() < min) {
					min = t.getClock();
				}
			}
			return min;
		} else {
			System.out.println("ERROR : No transition enabled nor firable");
			
			return new Float(0);
		}
	}

	public void timedMove(Float f) {
		System.out.println("timed move : "+f);
		// this move implements timed move
		// assuming that this time elapsing is allowed
		Float clockval;
		Set<Integer> clk = this.Config.keySet();

		// update enabled transitions
		for (EquTransition t : this.enabled.values()) {
			t.setClock (t.getClock() - f);
			EquTransitionStopsLoic t2 = (EquTransitionStopsLoic) t;
			Token token = t.getPre().get(0).tokens.get(0);
			System.out.println("token ttb : "+token.timeToBrowse+" "+token.p.name);
			token.timeToBrowse -= f;
			System.out.println("token ttb : "+token.timeToBrowse+" "+token.p.name);
			if (t2.getSpeed() != 0) {
				token.xPlace += f * t2.getSpeed();
				token.setXTotal(token.getXTotal() + f * t2.getSpeed());
			}
		}

		for (Integer tnum : clk) {
			// for every entry in the hashtable
			clockval = Config.get(tnum);
			if (clockval > f) {
				// decrease by f if possible
				this.Config.put(tnum, new Float(clockval - f));
			} else { // the value is necessarily f

				// then compute blocked and fireable transitions

				// the transition must be removed from the clock configuration
				// and the corresponding transition becomes either
				// blocked or fireable
				this.Config.remove(tnum);

				if (busyPostSet(tnum)) {
					System.out.println("Transition " + tnum + " is blocked");
					blocked.put(tnum, findTransition(tnum));
				} else {
					System.out.println("Transition " + tnum + " is firable");

					getFirable().put(tnum, findTransition(tnum));
				}
			}
		} // end of for
	}

	public String toString() {
		String s = "";
		s += "places:" + places.size();
		s += "transitions:" + transitions.size();
		return s;

	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// % STPN MOVES %%
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	
	public void progressTime(Float delta) {
		System.out.println("progress time : "+delta);
		stepsNb ++; // Increase the number of steps
		discreteStep = false; // This is not a discrete step
		timeElapsed += delta; // The time elapsed is delta
		
		System.out.println("Progress time (EqNetStopLoic) by " + delta);
		
		
		HashMap<Integer, EquTransition> stillEnabled; // list of transitions still enabled
		stillEnabled = new HashMap<>();

		// remove delta to all enabled transitions that are not blocked
		for (EquTransition teq : enabled.values()) {
			
			EquTransitionStopsLoic t=(EquTransitionStopsLoic) teq;
			
			Token token = t.getPre().get(0).tokens.get(0);
			token.timeToBrowse -= delta;
			if (t.getSpeed() != 0) {
				token.xPlace += delta * t.getSpeed();
				token.setXTotal(token.getXTotal() + delta * t.getSpeed());
			}

			if (t.getClock().compareTo(delta) > 0) {
				// if the value of the clock attached to this transition allows a delta
				// time move
				t.setClock( new Float(t.getClock() - delta));
				stillEnabled.put(t.number, t);
				
				System.out.println("Transition " + t.name + "c="+t.getClock()+ " still enabled");
			} else { // clock is necessarily 0
				t.setClock( 0.0f);
				if (t.isBlocked(currentMarking)) {
					this.blocked.put(t.number, t);
					System.out.println("Transition " + t.name + "blocked");

				} else {
					getFirable().put(t.number, t);
					System.out.println("Transition " + t.name + "c="+ t.getClock()+  "firable");

				}
			}
		} // all enabled transitions have been visited

		// Refresh list of enabled transitions
		this.enabled = stillEnabled;

	}

	public void discreteMove() {
		discreteMove("", 0, false);
	}

	public boolean discreteMove(String pathLogs, long elapsedTime, boolean enableLogs) {

		// randomly choose a particular transition among allowed ones
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive

		System.out.print("Discrete Move : ");
		System.out.println("this.firable.size : " + this.getFirable().size());

		// if there is no firable transitions, the is no discrete
		// move possible on a random transition
		if (this.getFirable().size() == 0) {
			return false;
		}

		// choose randomly one of the firable transitions
		int randomNum = rand.nextInt(this.getFirable().size());
		System.out.print(randomNum + ":");

		Integer [] keys = (Integer[]) getFirable().keySet().toArray(new Integer[0]);
		EquTransition t = this.getFirable().get(keys[randomNum]);
		System.out.println(" : "+t.getNumber()+" : "+t.name);

		// Fire the selected transition 
		discreteMove(t, pathLogs, elapsedTime, enableLogs);
		return true;
	}

	
	
	public boolean discreteMove(String pathLogs, long elapsedTime, boolean enableLogs, float AdhToTargetSpeed) {

		// randomly choose a particular transition among allowed ones

		System.out.print("Discrete Move : ");
		System.out.println("this.firable.size : " + this.getFirable().size());

		// if there is no firable transitions, the is no discrete
		// move possible on a random transition
		if (this.getFirable().size() == 0) {
			return false;
		}

		// select randomly one of the 
		int randomNum = rand.nextInt(this.getFirable().size());
		System.out.print(randomNum + ":");

		Integer [] keys = (Integer[]) getFirable().keySet().toArray(new Integer[0]);
		EquTransition t = this.getFirable().get(keys[randomNum]);
		System.out.println(t.name);

		// Fire the selected transtion with headway equalization
		discreteMoveHeadway(t, pathLogs, elapsedTime, enableLogs, AdhToTargetSpeed);
		return true;
	}

	
	
	
	
	public boolean discreteMoveWithControl(String pathLogs, long elapsedTime, boolean enableLogs) {

		// randomly choose a particular transition among allowed ones
		// i.e. transitions with TTF to 0 and not blockd by an empty control place

		HashMap<Integer, EquTransition> firabletrans = this.getFirable();

		System.out.print("Discrete Move : ");
		System.out.println("this.firable.size : " + firabletrans.size());

		// if there is no firable transitions, there is no discrete
		// move possible on a random transition

		ArrayList<EquTransition> firecontrol = new ArrayList<EquTransition>();// list of firable and allowed transitions

		for (EquTransition t : firabletrans.values()) { // check every firable transition

			if (t.controlAllowsFiring()) {
				firecontrol.add(t);
			} // keep it if allowed by control

		}

		if (firecontrol.size() == 0) {
			return false;
		} // if the list is empty firing fails

		// otherwise pick one transition randomly
		int randomNum = rand.nextInt(firecontrol.size());
		System.out.print(randomNum + ":");

		EquTransition t = firecontrol.get(randomNum);
		System.out.println(t.name);

		discreteMove(t, pathLogs, elapsedTime, enableLogs);
		return true;
	}

	public String discreteMove2(TransitionAbstract trans, String pathLogs, float elapsedTime, boolean enableLogs) {
		// fires a transition t
		// IMPORTANT : t is supposed fireable
		EquTransition t = (EquTransition) trans;
		if (!isFirable(t)) {
			System.out.println("Trying to fire transition t" + t.name);
			System.out.println("not firable");
			return "Trying to fire transition t" + t.name + "not firable";
		}

		if (!controlAllowsFiring(t)) {
			System.out.println("Trying to fire transition t" + t.name);
			System.out.println("Control does not annow Firing");
			return "Trying to fire transition t" + t.name + "Control does not annow Firing";
		}

		/*** log the fired transition **********/
		/*** nline : date : fired transition ***/
		if (enableLogs) {
			FileWriter fileLogs = null;
			try {
				fileLogs = new FileWriter(pathLogs, true);
				fileLogs.write(n + ":" + elapsedTime + ":" + t.getName() + "\n");
			} catch (IOException e) {
				logger.log(Level.WARNING, "error of writing in fileLogs in class Net");
			} finally {
				try {
					fileLogs.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the fileLogs in class Net");
				}
			}
		}
		n++;

		// compute new marking

		Marking tempMarking = this.currentMarking.copy();

		for (EquPlace p : t.pre) {// for every place in the preset of t
			System.out.println("Removing" + p.getName() + "from marking");
			p.setEmpty();
			boolean b = tempMarking.remove(p);
		}

		// If semantics forces control token consumption
		if (consume_control_tokens) {
			for (EquPlace p : t.getControlPre() ) {// for every place in the control preset of t
				p.setEmpty();
			}

		}

		Marking newMarking = tempMarking.copy();

		System.out.println("Temporary marking");
		tempMarking.drop();

		for (EquPlace p : t.post) {// for every place in the preset of t
			System.out.println("Adding" + p.getName() + "to temp marking");
			boolean b = newMarking.add(p);
			
			//p.contents = new Float(0.0);
			p.addToken();
		}
		System.out.println("New marking");
		newMarking.drop();

		this.currentMarking = newMarking;

		// find all newly enabled transitions
		// and sample their clock value

		for (EquTransition et : this.transitions.values()) {
			if (et.isNewlyEnabled(tempMarking, newMarking, t)) {
				// if et was not enabled in temp marking
				// sample times for all newly enabled transitions
				et.sample(s);
				this.Config.put(et.number, et.getClock());
				System.out.println("Newly enabled" + et.name);
			}
		}

		this.enabled = new HashMap<>();
		this.blocked = new HashMap<>();
		this.setFirable(new HashMap<>());
		this.Config = new HashMap<>();

		// Rebuild list of enabled/blocked/firable transitions
		for (EquTransition etq : this.transitions.values()) {
			
			EquTransitionStopsLoic et=(EquTransitionStopsLoic) etq;
			
			if (et.presetMarked(newMarking)) {
				// transition enabled, look at clock value
				if (et.getClock().equals(new Float(0.0))) {
					// transition's clock has reached value 0
					if (et.busyPostSet()) { // if one place marked
						this.blocked.put(et.number, et);
						System.out.println(et.name + "blocked");

					} else {
						this.getFirable().put(et.number, et);
						System.out.println(et.name + "firable");
					}

				} else { // transition enabled
					this.enabled.put(et.number, et);
					Config.put(et.number, et.getClock());
				}

			}
		} // all transitions considered
		return "transition " + t.getName() + "fired";

	}

	
	
//	public String discreteMoveHeadway(TransitionAbstract trans, String pathLogs, float elapsedTime, boolean enableLogs,float AdhToTargetSpeed) {
	
	public String discreteMoveHeadway(TransitionAbstract trans, String pathLogs, float elapsedTime, boolean enableLogs,float AdhToTargetSpeed) {
		// fires a transition t
		// IMPORTANT : t is supposed fireable
		System.out.println("Discrete Move Headway entered");
		EquTransition t = (EquTransitionStopsLoic) trans;
		
		if (!isFirable(t)) {
			System.out.println("Trying to fire transition t (Headway Regulation)" + t.name);
			System.out.println("not firable");
			return "Trying to fire transition t" + t.name + "not firable";
		}

		if (!controlAllowsFiring(t)) {
			System.out.println("Trying to fire transition t" + t.name);
			System.out.println("Control does not annow Firing");
			return "Trying to fire transition t" + t.name + "Control does not annow Firing";
		}

		/*** log the fired transition **********/
		/*** nline : date : fired transition ***/
		if (enableLogs) {
			FileWriter fileLogs = null;
			try {
				fileLogs = new FileWriter(pathLogs, true);
				fileLogs.write(n + ":" + elapsedTime + ":" + t.getName() + "\n");
			} catch (IOException e) {
				logger.log(Level.WARNING, "error of writing in fileLogs in class Net");
			} finally {
				try {
					fileLogs.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the fileLogs in class Net");
				}
			}
		}
		n++;

		// From here, the transition will fire.
		t.setClock(-2); // reset the clock of the transition
		
		// compute a temporary marking M' = M - preset(t)
		Marking tempMarking = this.currentMarking.copy();

		for (EquPlace p : t.pre) {// for every place in the preset of t
			// remove the token in the preset.
			// it exists as t is enabled
			
			System.out.println("Removing " + p.getName() + " from marking");
			
			//p.contents = null;
			//p.setEmpty();
			boolean b = tempMarking.remove(p);
		}

		// If semantics forces control token consumption
		if (consume_control_tokens) {
			for (EquPlace p : t.getControlPre()) {// for every place in the control preset of t
				//p.contents = null;
				p.setEmpty();
			}

		}

		// make a copy of the marking without the consumed places
		Marking newMarking = tempMarking.copy();

		System.out.println("Temporary marking Computed ");
		// Uncomment following line to get temporary marking
		// tempMarking.drop();

		
		// compute a markin M'' = M' + postset(t)
		for (EquPlace p : t.getPost()) {// for every place in the postset of t
			boolean b = newMarking.add(p);
			//p.contents = new Float(0.0);
			float timeToBrowse = p.post.sample(s);
			System.out.println("Time for transition "+ p.post.getName()+" : "+timeToBrowse);
			
			//Token token = new Token(p, timeToBrowse);
			//p.addToken(token);
			t.pre.get(0).tokens.get(0).changePlace(p, timeToBrowse);
		}
		
		System.out.println("New marking");
		newMarking.drop();

		this.currentMarking = newMarking;

		// find all newly enabled transitions
		// and sample their clock value

		for (EquTransition etq : this.transitions.values()) {
			
			EquTransitionStopsLoic et=(EquTransitionStopsLoic) etq;
			
			if (etq.isNewlyEnabled(tempMarking, newMarking, t)) {
				// if et was not enabled in temp marking
				// sample times for all newly enabled transitions
				
				// HERE 
				//et.sample(s);
				
				// HERE : warning, tokens also carry time info.
				setSpeedAndDwell(et,AdhToTargetSpeed);
				
				
				
				this.Config.put(et.number, et.getClock());
				System.out.println("Newly enabled Transition " + et);
			}
		}

		this.enabled = new HashMap<>();
		this.blocked = new HashMap<>();
		this.setFirable(new HashMap<>());
		this.Config = new HashMap<>();

		// Rebuild list of enabled/blocked/firable transitions
		for (EquTransition etq : this.transitions.values()) {
			
			EquTransitionStopsLoic et=(EquTransitionStopsLoic) etq;
			
			if (et.presetMarked(newMarking)) {
				// transition enabled, look at clock value
				if (et.getClock().equals(new Float(0.0))) {
					// transition's clock has reached value 0
					if (et.busyPostSet()) { // if one place marked
						this.blocked.put(et.number, et);
						System.out.println(et.name + "blocked");

					} else {
						this.getFirable().put(et.number, et);
						System.out.println(et.name + "firable");
					}

				} else { // transition enabled
					this.enabled.put(et.number, et);
					Config.put(et.number, et.getClock());
				}

			}
		} // all transitions considered
		return "transition " + t.getName() + "fired";

	}

	
	
		

		
	
	
	
	
	
	public void multipleSteps(int steps) {
		for (int i = 1; i <= steps; i++) {
			Float delta = this.maxAllowedTimedMove();
			if (delta.compareTo(new Float(0.0)) > 0)// if a timed move is allowed
				this.progressTime(delta);
			else if (this.getFirable().size() != 0) // timed moves and discrete moves are exclusive
				this.discreteMoveWithControl("", 0, false);
		}
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// % DISPLAY Methods %%
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	public void listEnabled() {
		System.out.println("Enabled Transitions:");
		for (EquTransition t : enabled.values()) {
			// list every enabled transition
			System.out.print(t + " ");
			System.out.println();
		}
	}

	// displays the contents of net in console and in the user interface
	public void drop() {

		System.out.println("places:" + places.size());
		for (EquPlace p : this.places.values()) {
			if (p.isStartPlace()) {
			     System.out.print("*"+p.getName());
			} else { System.out.print(p.getName());}
			ArrayList<Token> lt = p.getTokens();
			for (Token tok:lt) {
				// for every token give speed and remaining time to arrival
				System.out.print("<"+tok.getSpeed()+","+tok.getTimeToBrowse()+">");
				
			}
			System.out.println(" ");
			
			
			
		}

		System.out.println("transitions:" + transitions.size());
		for (EquTransition et : this.transitions.values()) {
			
			EquTransitionStopsLoic t=(EquTransitionStopsLoic) et;
			
			System.out.print(t.name);
			if (t.getClock() != null) {
				System.out.print(":" + t.getClock());
			}
			if (this.isEnabled(t)) {
				System.out.print("<enabled>");
			} else {
				System.out.print("<not-enabled>");
			}

			if (t.isBlocked(this.currentMarking)) {
				System.out.println("<blocked>");
			} else {
				System.out.print("<not-blocked>");
			}
			if (isFirable(t)) {
				System.out.println("<firable>");
			} else {
				System.out.print("<not-firable>");
			}

			System.out.print("<pre=");
			for (EquPlace p : t.pre) {
				System.out.print(p.getName() + " ");
			}
			System.out.print(">");

			System.out.print("<post=");
			for (EquPlace p : t.post) {
				System.out.print(p.getName() + " ");
			}
			System.out.println(">");
		}
		System.out.println("Marking");
		this.currentMarking.drop();

		System.out.println("Clocks");

		for (Entry<Integer, Float> e : Config.entrySet()) {
			System.out.println(e.getKey() + "->" + e.getValue());
		}
	}

	public HashMap<Integer, EquTransition> getFirable() {
		return fireable;
	}

	
	
	
	public HashMap<Integer, EquTransition> getEnabled() {
		return enabled;
	}

	public void setFirable(HashMap<Integer,EquTransition> firable) {
		this.fireable = firable;
	}

	public int size() {
		return transitions.size();
	}

	/**
	 * A method to display the contents of places in a String
	 * 
	 * @return a string with the contents of all places
	 */
	public String dropConfig() {

		String s = new String("===========\n Net Config :\n ");
		s = s + ("Places:\n");
		for (EquPlace p : places.values()) {
			s = s + p + "\n";
		}

		s = s + ("Control Places:\n");
		for (EquPlace p : controlPlaces.values()) {
			s = s + p + "\n";
		}

		s = s + ("Transitions:\n");
		Set<Integer> ks = Config.keySet();

		for (EquTransition t : this.enabled.values()) {
			int ti = t.getNumber();
			Float val = this.Config.get(ti);
			s = s + "t" + ti + "->" + t.getClock() + "\n";
		}

		return s;
	}

	public boolean isGaussian() {
		return gaussian;
	}

	public boolean isWeibull() {
		return weibull;
	}

	public int getWeibullCoef() {
		return weibullCoef;
	}

	public void reset(boolean init) {// init set to true to reset at the initial state

		enabled = new HashMap<>();
		blocked = new HashMap<>();
		fireable = new HashMap<>();

		if (init)
			currentMarking = initMarking.copy();

		for (EquPlace p : places.values()) {
			//p.setContents(null);
			p.setEmpty();
			if (currentMarking.contains(p))
				//p.setContents(0f);
				p.addToken();
		}
		for (EquPlace p : controlPlaces.values()) {
			
			//p.setContents(null);
			p.setEmpty();
			
			if (currentMarking.contains(p))
				//p.setContents(0f);
				p.addToken();
		}

		Config = new HashMap<>();
		// Establishes relation between transition numbers
		// and the value of attached clock
		if (init) {
			for (EquTransition teq : this.transitions.values()) {
				
				EquTransitionStopsLoic t=(EquTransitionStopsLoic) teq;
				
				if (t.presetMarked(currentMarking)) {
					t.sample(s);
					enabled.put(t.number, t);
					this.Config.put(t.getNumber(), t.getClock());
				}
			}
		}

		n = 1;

		// loadFile();
	}
	
	public int numberFireable() {
		return getFirable().size();
	}
	
	public int numberBlocked() {
		return getBlocked().size();
	}
	
	public boolean isBlocked(int tnum) {
		return this.getBlocked().containsKey(tnum);
	}

	public String getFname() {
		return fname;
	}

	public HashMap<Integer, EquTransition> getTransitions() {
		return transitions;
	}

	public HashMap<Integer, EquPlace> getPlaces() {
		return places;
	}

	public HashMap<Integer, EquTransition> getBlocked() {
		return blocked;
	}

	public HashMap<Integer, EquPlace> getControlPlaces() {
		return controlPlaces;
	}

	public Marking getCurrentMarking() {
		return currentMarking;
	}

	public void setCurrentMarking(Marking currentMarking) {
		this.currentMarking = currentMarking;
	}

	public void setConfig(HashMap<Integer, Float> config) {
		Config = config;
	}

	public void setEnabled(HashMap<Integer, EquTransition> list) {
		enabled = list;
	}
	
	public void setFireable(HashMap<Integer, EquTransition> list) {
		fireable = list;
	}
	
	public void setBlocked(HashMap<Integer, EquTransition> list) {
		blocked = list;
	}
	
	public float minimumClock() {
		float minNetElapse = -1;// A dummy value for start

		// get the Petri net of the regulated net

		for (EquTransition t : this.getEnabled().values()) { // consider every firable transition

			Float tf = t.getClock();
			System.out.println(tf);
			if (tf > 0) {
				if (minNetElapse < 0) {
					minNetElapse = tf;
				} else {
					minNetElapse = Math.min(minNetElapse, tf);
				}
			}
			// From here we have the delay allowed by the net.
		}
		return minNetElapse;
	}

	@Override
	public ArrayList<TransitionAbstract> fireableTransition() {
		HashMap<Integer, EquTransition> tlistfirable = this.getFirable();
		ArrayList<TransitionAbstract> firecontr = new ArrayList<>();
		for (EquTransition t : tlistfirable.values()) {
			if (t.controlAllowsFiring()) {
				firecontr.add(t);
			}
		}
		return firecontr;
	}
	
	
	
        //**************************************************************
		//      SPEED MANAGEMENT and Advices                            
		//**************************************************************
		
		
		/**
		 * This method is a stub to define an abstract method of the physical model
		 * It is not used in this implementation
		 */
		public float calculTime(Token tok, EquTransition t) {	
			return 3.75f;  // Caution, dummy value to have a method prototype
			// This time (in minutes) represents the duration of a 2000m trip at 32K/h
		}
		
	
	
	
	
	/**
	 * Sets the speed and TTF of a particular transition that becomes 
	 * newly enabled. 
	 * If the transition is a departure, then one has to set a dwell time and compute the 
	 * speed for the next transition
	 * If the transition is an arrival, then it does not yet has a duration, but it has a speed
	 * So, set the duration accordingly 
	 * 
	 * @param et a transition that is newly enabled
	 */
	void setSpeedAndDwell(EquTransitionStopsLoic et, float AdhToTargetSpeed){
		
		
		float PreviousSpeed; // The speed of train preceding et
		float NextSpeed; // The speed of train preceding et
		
		
		if (et.getNumber() %2 == 0 ) { // et is a departure
			
            EquPlace p = et.getPre().get(0); // Get the place preceding transition et (no control assumed)
            // as transition t will fire, the duration represented by the 
            // clock value is the time needed before departure
            et.dwellSample(new Sampler(),0.75f,0.2f);
			et.setSpeed(0); // it is a departure so the speed is 0
			et.setGhostSpeed( 0.0f+INITIAL_SPEED); // sets the ghost speed of transition to an arbitrary value
			// TODO : compute a more appropriate speed, that will be used when the train leaves the station
			
			Token tok = p.getTokens().get(0);
			tok.timeToBrowse=et.getClock();
			tok.setSpeed(0); // sets the speed of token to 0
			
			
			float ghostSpeed = computeGhostSpeed(et);
			
			tok.setGhostSpeed(ghostSpeed); // sets the ghost speed of token to an arbitrary value
			// TODO : compute a more appropriate speed, that will be used when the train leaves the station
			
		
	      System.out.println("Current Train(departure): " 
	         + et.getNumber() + " " +et.getName() + "clock= "+ et.getClock()
	         + "spd =  " + et.getSpeed());
			
		
		
		} else { // arrival
			// We have to ask an advice to set speed of train, taking into a ccount 

		      System.out.println("Current Train(Arrival): " 
		         +et.getNumber() + " " +et.getName() + "spd =  " + et.getSpeed() +"Ghost Speed = "+et.getGhostSpeed());

		      
		      EquTransitionStopsLoic t1 = findPreviousTransition(et); // find where is the previous train
		      EquTransitionStopsLoic t2 = findNextEnabledTransition(et); // find where is the next train
				
				float dis1 = DistanceFromPrevious(t1,et); // compute the distance between predecessor and current train
				System.out.println("Distance prev,t = "+ dis1);
				
				float dis2 = DistanceToNext(et,t2);
				System.out.println("Distance et,next = "+ dis2);

				// ask for a speed advice 

				
				// Caution, if t1 represents a stopped train, its speed is 0
				// and then we need to take the speed after departure of train
				// i.e. the Ghost Speed
				if (t1.getNumber() %2 == 0 ) { // t1 is a departure, train has no speed 
					PreviousSpeed = t1.getGhostSpeed();
				} else { PreviousSpeed = t1.getSpeed();}
				
				
				// If t2 represents a stopped train, its speed is 0
				// and then we need to take the (ghost) speed set after departure of train
				if (t2.getNumber() %2 == 0) {
					NextSpeed = t2.getGhostSpeed();
				} else {NextSpeed = t2.getSpeed(); }
				
				// Compute a speed advice for current train
				float advice = giveSpeedAdvice(dis1,dis2,PreviousSpeed,NextSpeed,AdhToTargetSpeed);
				//float advice = giveStupidSpeedAdvice(dis1,dis2);
				
				if (advice <0) { System.out.println("\u001B[31m" + "Warning, negative speed");} //if the advice is a negative sppe then ther is an error 		
				if (advice >666) { advice = 666;} //if the advice is a too high speed 
				if (advice <466) { advice = 466;} //if the advice is a too low speed 
				
		      
				
				et.setSpeed(advice);// sets the speed of transition to the advised speed
				
		EquPlace p=et.getPre().get(0); // get the preceding place to have a distance
		et.setClock(p.getDistance()/advice); // Sets the clock of transition consistently with speed and distance to run

		
				Token tok = p.tokens.get(0); // find the first token in preceding place
				tok.setSpeed(et.getSpeed()); // sets its speed
				tok.timeToBrowse=et.getClock(); // sets its time to end of place
			
			System.out.println("Next evt (arrival): " +et.getNumber() + " " +et.getName() 
			+ " clock=" + et.getClock() 
			+ " spd =  " + et.getSpeed());		
			
		}

		
	}
	
	

	
	
	public float giveStupidSpeedAdvice(float d1,float d2) {
		
	
	if (d1<d2) { return TARGETSPEED+20;}
	if (d1>d2) { return TARGETSPEED-20;}
	return TARGETSPEED;
	
	}
	
	
	
	/**
	 * Computes a speed advice for a train, considering the distance to its predecessor, to its successor, 
	 * the speed of predecessor, the speed of successor
	 * @return
 * 
 * @param d1 distance from predecessor to current train 
 * @param d2 distance from current train to successor
 * @param spd1 speed of predecessor (in m/min)
 * @param spd2 speed of successor (in m/min)
 * @return
 */
	public float giveSpeedAdvice(float d1,float d2,float spd1,float spd2,float AdhToTargetSpeed) {
				
		
		if (d2 == 0) { System.out.println("Error, d2 = 0");System.exit(-1);}
		if (spd1 == 0) { System.out.println("Error, spd1 = 0");System.exit(-1);}
		
		float hm1 = d1/spd1; // headway of former train (in minutes)
		
		//We are looking for v, but we will first compute unk, 
		// the duration of move along next track portion
		// l_k is the length of this portion, currently 2km for each place 
		// representing a track portion
		
		//The time headway to the next train is hm= d2/v = d2/l_k*unk
		// U0k = Lk/tagetspped
		// U0k is the time needed to complete next track portion at ideal speed
		// U0k is used as a part of the quadratic criterion to minimize 
		float lk=2000.0f; // constant length of track
		float u0k = lk/TARGETSPEED; // trip time along this track portion at targetted speed
		
		//We want to minimize
		// (hm-hm1)^2 + alpha(unk - u0k)^2
		
		// we have hm = d2*unk/lk
		
		
		
		// If we consider a constant speed, we have to minimize 
		// (hm-hm1)^2 + alpha(unk - u0k)^2
		// hm^2 -2hm.hm1 +hm1^2  + alpha[ unk^2 -2unk.u0k + u0k^2]
		//hm^2 -2hm.hm1 +hm1^2  + alpha unk^2 -2.alpha.unk.u0k + alpha.u0k^2
		//unk^2*(d2/lk)^2  -2unk.(d2/lk).hm1 +hm1^2  + alpha.unk^2 -2.alpha.unk.u0k + alpha.u0k^2  
		//unk^2*(d2/lk)^2 + alpha.unk^2  -2unk.(d2/lk).hm1  -2.alpha.unk.u0k + alpha.u0k^2  +hm1^2 
		// unk^2 . (alpha + d2^2/lk^2) + unk (-2.(d2/lk).hm1 -2.alpha.u0k) + alpha.u0k^2  +hm1^2
		
		
		// We are hence are minimizing 
		// 
		
		// the minimal value for a polynom of the form y= ax^2+bX+c is reached for x = -b/2a
		// Here we have a=(alpha + d2^2/lk^2) and b = -2.(d2/lk).hm1 -2.alpha.u0k 
		// So the optimal value for unk is 
		// 
		
		// Get the alpha parameter ( importance of target speed adherence)
        System.out.println("Adherence: "+ AdhToTargetSpeed);

		
        float a = AdhToTargetSpeed +(d2*d2/(lk*lk));
        float bm = 2*hm1*d2/lk +2*u0k*AdhToTargetSpeed;
        
	float unk=bm/(2*a);	
	float v=lk/unk;
		
		
		System.out.println("D1 = : "+ d1 +" D2 = " + d2 + " Speed1 = "+spd1);
		System.out.println("Target Headway : "+ u0k + "("+ u0k*3600+" seconds)");
		
		System.out.println("Advised Speed: "+ v);
		System.out.println("Headway Hm1 = "+ hm1 + "("+ hm1*3600+" seconds)" + " Unk = "+ unk + "("+ unk*3600+" seconds)");
		return Math.min(v,MAX_SPEED);
	}

	
	
	// finds the next transition following tarr (firable or not)
			public EquTransitionStopsLoic findNextTransition(EquTransition tarr) {
				
				EquTransitionStopsLoic t;
				
				ArrayList<EquPlace> post =  tarr.getPost();
		
				EquPlace p =post.get(0);
				t = (EquTransitionStopsLoic) p.getPost();
				
				return t;
			}


	
		

			// finds the next transition that will be fired after arrival transition tarr
			// This will allow to compute distance between trains
			// Assumption : every transition/place has only one successor
			public EquTransitionStopsLoic findNextEnabledTransition(EquTransitionStopsLoic tarr) {
				
				EquTransitionStopsLoic t;
				EquPlace p;
				ArrayList<EquPlace> post;
				ArrayList<EquTransition> tpost;
				boolean found=false;
				
				t=tarr; // start from the current transition
				
				while (!found ) { // as long as the next train (firalbe transition) is not found
		  		
					post =  t.getPost();
					//System.out.println("Succ Place size:"+post.size());

					p =post.get(0); 
					t = (EquTransitionStopsLoic) p.getPost(); // find next transition
								
					if (p.isMarked()){ 
						found = true;// We found the next enabled transition 
					}
				}
				
				System.out.println("Next Train: " +t.getNumber() + " " +t.getName() + "spd =  " + t.getSpeed());
				return t;
			}

			
			
			// finds the previous transition firable before arrival transition tarr
				// This will allow to compute distance between trains
				// Assumption : every transiton/place has only one successor
				public EquTransitionStopsLoic findPreviousTransition(EquTransitionStopsLoic tarr) {
					
					EquTransitionStopsLoic t;
					EquPlace p;
					ArrayList<EquPlace> pre;
					ArrayList<EquTransition> tpre;
					boolean found=false;
					
					t=tarr;
					
					while (!found ) { // as long as the next train (firable transition) is not found
			  		
						pre =  t.getPre(); // find preset (places) before transition

						p =pre.get(0); // take the first place
												
						t=(EquTransitionStopsLoic) p.getPre(); // take the preceding transition
									
						if (t.isEnabled()){ 
							found = true;// We found the preceding train 
						}
					}
					
					System.out.println("Previous Train: "+t.getNumber() + " " +t.getName() + "spd =  " + t.getSpeed() );
					return t;
				}




				
				
				
				
				
				
				@Override
				public String discreteMove(TransitionAbstract onet, String logFile, float currentTime,
						boolean enableLogs) {
					
					// find alpha 
					System.out.println("Discrete Move in EqStopsLoic");
					String s = discreteMoveHeadway(onet, logFile, currentTime, enableLogs, ALPHA);
						
					
					return s;
				}


			

	void arrangeTokensOrder() {
		for (EquPlace p : places.values()) {
			if (!p.equals(garage)) {
				for (Token token : p.tokens) {
					boolean preTokenFound = false;
					EquPlace prePlace = p.pre.pre.get(0);// the previous place
					while (!preTokenFound) {
						if (prePlace.tokens.size() > 0) {
							preTokenFound = true;
							prePlace.tokens.get(0).postToken = token;
							token.previousToken = prePlace.tokens.get(0);
						}
						prePlace = prePlace.pre.pre.get(0);// the previous place
					}
				}
			}
		}
	}


	
	/************************************************************/
	/*                      SPEEDS                              */
	/************************************************************/
	
	// A stub to return a value for Ghost speed
	// Here the constant value INITAL SPEED
	float computeGhostSpeed2 (EquTransitionStopsLoic et) {
		
		return (INITIAL_SPEED);
		
	}
	
	
	float computeGhostSpeed(EquTransitionStopsLoic et) {
		
		float PreviousSpeed; // (ghost) speed of previous train
		float NextSpeed; // (ghost) speed of next train
		
		EquTransitionStopsLoic t1 = findPreviousTransition(et); // find where is the previous train
	      EquTransitionStopsLoic t2 = findNextEnabledTransition(et); // find where is the next train
			
			float dis1 = DistanceFromPrevious(t1,et); // compute the distance between predecessor and current train
			System.out.println("Distance prev,t = "+ dis1);
			
			float dis2 = DistanceToNext(et,t2);
			System.out.println("Distance et,next = "+ dis2);

			// ask for a speed advice 

			
			// Caution, if t1 represents a stopped train, its speed is 0
			// and then we need to take the speed after departure of train
			// i.e. the Ghost Speed
			if (t1.getNumber() %2 == 0 ) { // t1 is a departure, train has no speed 
				PreviousSpeed = t1.getGhostSpeed();
			} else { PreviousSpeed = t1.getSpeed();}
			
			
			// If t2 represents a stopped train, its speed is 0
			// and then we need to take the (ghost) speed set after departure of train
			if (t2.getNumber() %2 == 0) {
				NextSpeed = t2.getGhostSpeed();
			} else {NextSpeed = t2.getSpeed(); }
			
			// Compute a speed advice for current train
			// Reminder : ALPHA is the weight of adherence to target speed 
			// in the quadratic criterion optimized by giveSpeedAdvice
			float advice = giveSpeedAdvice(dis1,dis2,PreviousSpeed,NextSpeed,ALPHA);
			//float advice = giveStupidSpeedAdvice(dis1,dis2);
			
			if (advice <0) { System.out.println("\u001B[31m" + "Warning, negative speed");} //if the advice is a negative sppe then ther is an error 		
			if (advice >666) { advice = 666;} //if the advice is a too high speed 
			if (advice <466) { advice = 466;} //if the advice is a too low speed 
			

		return advice;
		
	}
	
	
	
	/************************************************************/
	/*                       DISTANCES                          */
	/************************************************************/
	
	
	/**
	 * 	
	 * @param t1 : the starting transition, i.e. a transition the represents a departure from a station
	 * or an arrival to the next station
	 * @param t2 : the next transition (departure or arrival)
	 * @return : the distance between the two trains that leave/arrive (in km)
	 */
	float ComputeDistance(EquTransitionStopsLoic t1, EquTransitionStopsLoic t2) {
		
	
		
		
		if (t1.getNumber() > t2.getNumber() ) { // if the first transition is before the initial 
			// transition of the net 
			
			float InvDist = ComputeDistance(t2,t1); 
			return (TOTALLENGTH - InvDist); 
		} else {
		
		
		float dist=0.0f; // the total distance computed
		float trackLength = 2000.0f; //length between two stations
		
		float ddep; // distance to next station of train firing t1
		float darr; // distance from previous station of train firing t2
		
		int nb1= t1.getNumber(); // number of first transition 
		int nb2 = t2.getNumber(); // number of second transition
		
		System.out.println("Computing Distance between " + nb1 + " and " + nb2);
		
		System.out.println("T1: " +t1.getNumber() + " " +t1.getName() + "  spd =  " 	
	             + t1.getSpeed() + "Clk" + t1.getClock());
		
		System.out.println("T2: " +t2.getNumber() + " " +t2.getName() + "  spd =  " 
				+ t2.getSpeed() + " Clk = " + t2.getClock() + " distance = "+t2.getPre().get(0).getDistance());
		
		
		if ( (nb1 % 2) == 0) { // if t1 is an arrival, ddpep is the distance to arrival 
			float spd=t1.getSpeed();
			float clk=t1.getClock();
			ddep = spd * clk;  // distance = speed in m per min * remaining time   
			
			
		
		} else { ddep =0; // transition is a departure, so the train has no distance to run 
		}
		
		if ((nb2 %2) == 0) { // if t2 is an arrival, darr is the distance since departure 


			darr = t2.getPre().get(0).getDistance() - (t2.getSpeed() * t2.getClock()) ; 
			
			if (darr <= 0 ) { // for debugging only this should not happen !
				System.out.println("DARR <= 0 : STOP");
				System.exit(-1);
			}
			
		} else {
			// transition is a departure, trains is stopped is a place of lenght 0
			darr = 0; 
			}
		
		//System.out.println("Dist Dep :"+ddep + " Dist Darr" +darr);
		
		dist = ddep+darr;
		
		// add the length of places between t1 and t2 
		EquTransition currentTrans = t1; 
		EquPlace np= currentTrans.getPost().get(0); //get the post place
		EquTransition nextTrans=np.getPost(); 
		
		while (nextTrans != t2) {
			
			float localdist = np.getDistance();
			
			currentTrans=nextTrans;
			np= currentTrans.getPost().get(0);
			nextTrans=np.getPost();
			if (nextTrans != t2) { dist = dist + localdist;}
			
		}
						
		// safety Check
		if (dist <0 ) {System.exit(-1);};
		
		return dist;
		}
	}


	
	
	// Computes the distance from a previous transition t1 to a transition
	// t2 that just became newly enabled
	public float DistanceFromPrevious(EquTransitionStopsLoic t1, EquTransitionStopsLoic tena) {
		
		float dist=0.0f; // the total distance computed
		float ddep; // distance to next station of train firing t1
		int nb1= t1.getNumber(); // number of first transition 
		int nb2 = tena.getNumber(); // number of second transition
		
		System.out.println("Computing Distance between from " + nb1 + " to " + nb2);
		
		System.out.println("T1: " +t1.getNumber() + " " +t1.getName() + "  spd =  " 	
	             + t1.getSpeed() + "Clk" + t1.getClock());
		
		System.out.println("T2(newly enabled): " +tena.getNumber() + " " +tena.getName() + "  spd =  " 
				+ tena.getSpeed() + " Clk = " + tena.getClock() 
				+ " distance = "+tena.getPre().get(0).getDistance());
		
		if ( (nb1 % 2) == 0) { // if t1 is an arrival, ddpep is the distance to arrival 
			float spd=t1.getSpeed();
			float clk=t1.getClock();
			ddep = spd * clk;  // distance = speed in m per min * remaining time   
		} else { ddep =0; // transition is a departure, so the train has no distance to run 
		}
		
		// tena can be an arrival or a departure. As it is newly enabled, the distance 
		// from preceding station is 0 
		
		dist = ddep;
		
		// add the length of places between t1 and tena (but not the last one 
		// Done via a loop that visits one successor after the other
		// CAUTION: The network has to be cyclic
		EquTransition currentTrans = t1; 
		EquPlace np= currentTrans.getPost().get(0); //get the post place
		EquTransition nextTrans=np.getPost(); 
		
		while (nextTrans != tena) {
			
			float localdist = np.getDistance(); //add the size of current place
			
			currentTrans=nextTrans;
			np= currentTrans.getPost().get(0);
			nextTrans=np.getPost();
			if (nextTrans != tena) { dist = dist + localdist;}
		}
						
		// safety Check before returning the result
		if (dist <0 ) {System.out.println("Negative Distance"); System.exit(-1);}		
			
		return dist;
	}

	
	// Computes the distance from a transition tena that is newly enabled to 
	// the next transition t2
	public float DistanceToNext(EquTransitionStopsLoic tena, EquTransitionStopsLoic t2) {

		
		
		float dist=0.0f; // the total distance computed
		float ddep; // distance to next station of train firing tena
		float darr; // distance from preceding station of train firing t2
		int nb1= tena.getNumber(); // number of first transition 
		int nb2 = t2.getNumber(); // number of second transition
		
		System.out.println("Computing Distance from " + nb1 + "(newly enabled) to Next " + nb2);
		
		System.out.println("T1: " +tena.getNumber() + " " +tena.getName() + "  spd =  " 	
	             + tena.getSpeed() + "Clk" + tena.getClock());
		
		System.out.println("T2(newly enabled): " +t2.getNumber() + " " +t2.getName() + "  spd =  " 
				+ t2.getSpeed() + " Clk = " + t2.getClock() 
				+ " distance = "+tena.getPre().get(0).getDistance());
		
		if ( (nb1 % 2) == 0) { // if tena is an arrival, ddpep is the distance to arrival 
// i.e. the size of track segment represented by the preset place
			ddep=tena.getPre().get(0).getDistance();
			
		} else { ddep =0; // transition is a departure, so the train has no distance to run 
		}
		
		
		if ( (nb2 % 2) == 0) { // if t2 is an arrival, darr is the distance from preceding station 
			float spd=t2.getSpeed();
			float clk=t2.getClock();
			EquPlace p = t2.getPre().get(0); // the place representing the track portion
			darr = p.getDistance() - spd * clk;  // distance = track size - speed in m per min * remaining time   
		} else { darr =0; // transition is a departure, so the train has no distance to run 
		}
		System.out.println("Ddep ="+ ddep +" Darr=" + darr);
		// tena can be an arrival or a departure. As it is newly enabled, the distance 
		// in the place is 0 
		
		dist = ddep+ darr; 
		
		// add the length of places between tena and t2 (but not the last one 
		// Done via a loop that visits one successor after the other
		// CAUTION: The network has to be cyclic
		EquTransition currentTrans = tena; 
		EquPlace np= currentTrans.getPost().get(0); //get the post place of newly enabled transition
		EquTransition nextTrans=np.getPost(); 
		
		while (nextTrans != t2) {
			
			float localdist = np.getDistance(); //add the size of current place
			if (nextTrans != t2) { dist = dist + localdist;}
			
			currentTrans=nextTrans;
			np= currentTrans.getPost().get(0);
			nextTrans=np.getPost();
			
		}
						
		// safety Check before returning the result
		if (dist <0 ) {System.out.println("Negative Distance"); System.exit(-1);}		
			
		return dist;

		
		
		
		
	}

	
	
	
}
