package fr.inria.mochy.core.equalization;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.sampler.Sampler;

/**
 * this is the first version of Equalization Net with fixed block (1 token per
 * place) V1 : if the previous token is at a stop the timetobrowse calculated is
 * sampled for a token which get into a moving place
 */
public class EquNetV1Fix extends EquNet {

	/**instantiate EquNetV1Fix, set the path of the input file and set the neural network used for the regulation*/
	public EquNetV1Fix(String fname) {
		super(fname);
	}

	@Override
	/**
	 * Advance time by a posiitv evalue Delta The effect is that tokens in places
	 * move toward the en of the physical support that the place represents. This
	 * time progress updates the lisy of blocked transition
	 */
	public void progressTime(Float delta) {
		stepsNb ++;
		discreteStep = false;
		timeElapsed += delta;

		// advance the time for the tokens not at the end of the place
		// and not linked to a blocked transition
		for (EquPlace p : places.values()) {
			for (Token token : p.getTokens()) {
				// if (!token.isAtTheEnd() && !blocked.containsKey(p.post.getNumber()))
				if (!token.isAtTheEnd() && !token.blocked)
					token.advanceTime(delta);
			}
		}
		// update the enabled, blocked and fireable hashmaps
		for (EquPlace p : places.values()) {
			for (Token token : p.getTokens()) {
				if (token.isAtTheEnd() && !p.equals(garage)) {
					enabled.remove(p.post.getNumber());
					EquPlace nextPlace = p.post.post.get(0);
					if (token.postToken.p.equals(nextPlace)) {
						if (!blocked.containsKey(p.post.getNumber())) {
							blocked.put(p.post.getNumber(), p.post);
							p.tokens.get(0).blockToken();
						}
					} else {
						if (blocked.containsKey(p.post.getNumber())) {
							blocked.remove(p.post.getNumber());
							p.tokens.get(0).unblockToken();
						}
						fireable.put(p.post.getNumber(), p.post);
						// System.out.println("make "+p.post.getName()+" fireable");
					}
				}
			}
		}
		// manage the insertion of a new token
		if (!allowInsertion() && garage.tokens.size() > 0) {
			updateGarageTransition();
		} else if (allowInsertion()) {// if we are currently ready to insert a new token
			// we rearrange the orders of the tokens (pre and post)
			rearrangeTokens();
			updateGarageTransition();
		}
	}

	@Override
	public String discreteMove(TransitionAbstract onet, String logFile, float currentTime, boolean enableLogs) {
		stepsNb ++;
		discreteStep = true;
		EquTransition t = (EquTransition) onet;
		if (!fireable.containsKey(t.getNumber())) {
			System.out.println("transition " + onet.getNumber() + " is not fireable");
			return "transition " + t.getNumber() + " is not fireable";
		}
		fireable.remove(t.getNumber());
		Token token = t.getPre().get(0).getReadyToken();
		t.getPre().get(0).getTokens().remove(token);

		EquTransition preTransition = token.previousToken.p.post;
		if (blocked.containsKey(preTransition.getNumber())) {
			blocked.remove(preTransition.getNumber());
			preTransition.pre.get(0).tokens.get(0).unblockToken();
			fireable.put(preTransition.getNumber(), preTransition);
		}

		/****************************************************************/
		/************************** add the J calculus ********************/
		float timeToBrowse = calculTime(token, t.getPost().get(0).getPost());

		// move the considered token in the next place in the ring, with
		// a trip duration equal to timetobrowse
		token.changePlace(token.p.getPost().getPost().get(0), timeToBrowse);

		lastTokenSpeed = token.speed;
		lastTokenTtb = token.timeToBrowse;

		// manage the insertion of a new token
		if (!allowInsertion() && garage.tokens.size() > 0) {
			updateGarageTransition();
		} else if (allowInsertion()) {// if we are currently ready to insert a new token
			// we rearrange the orders of the tokens (pre and post)
			rearrangeTokens();
			updateGarageTransition();
		}

		// t.getPost().get(0).getPost().getPost().get(0)
		// log the new Token information
		if (enableLogs) {
			FileWriter fileLogs = null;
			try {
				fileLogs = new FileWriter(logFile, true);
				fileLogs.write("Transition " + t.getName() + " fired: new Token in place " + token.p.getName()
						+ " : Distance " + token.p.getDistance() + " : Time " + token.timeToBrowse + " : Speed "
						+ token.speed + "\n");
				// System.out.println("Transition " + t.getName() + " fired: new Token in place
				// " + token.p.getName()
				// + " : Distance " + token.p.getDistance() + " : Time " + token.timeToBrowse +
				// " : Speed "
				// + token.speed + "\n");
			} catch (IOException e) {
				logger.log(Level.WARNING, "error of writing in fileLogs in class EquNetV1Fix");
			} finally {
				try {
					fileLogs.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the fileLogs in class EquNetV1Fix");
				}
			}
		}
		nbDiscreteSteps++;
		return "transition " + t.getNumber() + " fired";
	}

	/**
	 * compute the duration for the trip in the next segment that minimizes the
	 * headway difference, and the distance to a target speed See equation in the
	 * model 2 of the document L3 for details Warning : we assume the considered net
	 * is a ring.
	 */
	float calculTime(Token token, EquTransition t) {
		// float alpha = 1;

		float DnPlusUn = token.postToken.getXTotal() - token.getXTotal();
		// Distance to the next token in the net
		// computed as the distance of next token wrt origin of the ring net
		// minus the distance of current token to origin

		if (DnPlusUn < 0)
			DnPlusUn = token.postToken.getXTotal() + getTotalDistance() - token.getXTotal();
		// if the difference is negative, then the next token has already passed the
		// origin
		// Make a round of XTotal to prevent the distance to be lightly superior to the
		// distance of the place
		float Lk = t.pre.get(0).distance;
		float DnMoinsUn = token.getXTotal() - token.previousToken.roundXTotal();
		// distance to previous token in the ring net

		if (DnMoinsUn < 0)
			DnMoinsUn = token.getXTotal() + getTotalDistance() - token.previousToken.roundXTotal();
		float VnMoinsUn = token.previousToken.speed;
		float UkZero = t.getNormalTime();

		if (Lk == 0 || VnMoinsUn == 0f || DnMoinsUn == 0f) {// if the transition or the previous one is a stop
			float time = t.sample(s);
			// System.out.println("time sampled : "+time);
			return time;
		}

		float b = -2 * (((DnPlusUn * DnMoinsUn) / (Lk * VnMoinsUn)) + ALPHA * UkZero);
		float a = (float) (ALPHA + Math.pow(DnPlusUn, 2) / Math.pow(Lk, 2));
		// get the minimum value and add noise
		float timeToBrowse = -b / (2 * a);
		// The criterion to minimize is a quadratic criterion of the form a.x^2 + b.x +c
		// which is minimal for x=-b/2.a

		Random random = new Random();
		timeToBrowse = timeToBrowse - RANGE_NOISE / 2 + random.nextFloat() * RANGE_NOISE;
		// The duration of trip is the next segment is perturbed by some imprecision

		while (timeToBrowse < 0) {
			timeToBrowse = timeToBrowse - RANGE_NOISE / 2 + random.nextFloat() * RANGE_NOISE;
		}

		if (logs) {
			System.out.println(DnPlusUn);
			System.out.println(Lk);
			System.out.println(DnMoinsUn);
			System.out.println(VnMoinsUn);
			System.out.println(UkZero);
			System.out.println(b);
			System.out.println(a);
			System.out.println("==" + token.getXTotal() + "==" + token.previousToken.getXTotal());
		}

		return timeToBrowse;
	}

	@Override
	public float minimumClock() {
		// browse the tokens and get the minimal clock
		// for a token to be at the end of the place
		float clock = Float.POSITIVE_INFINITY;
		for (EquPlace p : places.values()) {
			for (Token token : p.getTokens()) {
				if (!token.blocked) {
					clock = Math.min(clock, token.timeToBrowse);
					// System.out.println("clock 1 : "+token.timeToBrowse+", speed : "+token.speed);
				}
			}
		}
		return clock;
	}

}
