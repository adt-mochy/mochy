package fr.inria.mochy.core.equalization;

import java.util.ArrayList;

import fr.inria.mochy.core.abstractClass.PlaceAbstract;

/**The EquPlace is a place which can contains multiple tokens*/
public class EquPlace extends PlaceAbstract{

	int number; // a unique identifier for the place
	String name; // a label attached to the transition
	int distance; // for places representing physical places, the size of the erpresented area
	ArrayList<Token> tokens = new ArrayList<>(); // all tokens contained in the place
	EquTransition pre; // transitions preceding this place
	EquTransition post; // transitions consuming tokens from this place
	EquNet net; // Net containing this partuicular place
	boolean startPlace = false; // True if the place has to be considered as the origin of a circular net
								// field used to compute a distance
	
	/**instantiate an EquPlace*/
	public EquPlace(EquNet net, int number, String name, int distance, boolean startPlace) {
		this.number = number;
		this.name = name;
		this.net = net;
		this.startPlace = startPlace;
		this.distance = distance;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public Integer getNumber() {
		return number;
	}
	
	/**get the previous transition of the place*/
	public EquTransition getPre() {
		return pre;
	}
	
	/**get the next transition of the place*/
	public EquTransition getPost() {
		return post;
	}
	
	/**set the next transition of the place
	 * @param t the EquTransition*/
	public void setPre(EquTransition t) {
		pre = t;
	}

	/**
	 * set the postset of current place
	 * @param t the EquTransition
	 */
	public void setPost(EquTransition t) {
		post = t;
	}

	
	
	@Override
	/**
	 * Adds a token in the place. The TTF of the timetobrowse of the token is supposed useless
	 * So it is set to 0  
	 */
	public void addToken() {
		this.addToken(new Token(this,0.0f));
	}
	
	/**add a token to this place*/
	public void addToken(Token token) {
		
		if (this.tokens==null) {
		 tokens=new ArrayList<Token>();
		} else {
	
		   if (this.getTokens().isEmpty()) {
				  net.enabled.put(this.post.number, this.post);
				  this.post.setClock(token.getTimeToBrowse());
			   }
		}
		token.p = this;
	
		this.getTokens().add(token);
	}
	
	/**
	 * empties the current place
	 */
	public void setEmpty() {
		tokens = new ArrayList<Token>();
	}
	
	/**get the length of this place*/
	public int getDistance() {
		return this.distance;
	}

	/**check if it place is the origin of the loop*/
	public boolean isStartPlace() {
		return startPlace;
	}
	
	/**get the token at the end of this place or return null is there are none*/
	public Token getReadyToken() {
		for (Token token : getTokens()) {
			if (token.isAtTheEnd())
				return token;
		}
		return null;
	}

	
	/**
	 * returns the current contents of the place
	 * @return
	 */
	public ArrayList<Token> getTokens() {
		return tokens;
	}
	
	
	/**
	 * True if the place contains some token
	 * @return
	 */
	public boolean isMarked() {
		
		return !tokens.isEmpty();
	}
	
	/**display information of the place in the console*/
	public String toString() {
		String s;
      if (tokens != null) {
		s= new String("place"+number+" "+name + tokens.size());
      } else { 
    		s= new String("place"+number+" "+name);
      
	  }
		return s;
	}
	
	
}
