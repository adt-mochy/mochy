package fr.inria.mochy.core.equalization;

import java.util.ArrayList;

import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.sampler.Sampler;

/**an EquTransition is a point of control which will define the evolution
*of a token in the next place, it manages the time a token will stay at the previous place*/
public class EquTransition extends TransitionAbstract {
	/*
	 * From TransitionAbstract class: int number; String name; Float clock = null;
	 * float lowerbound = 0; Float upperbound = null;
	 */
	/**the average between the lowerbound and the upperbound*/
	float normalTime;
	/**the list of the previous places of this transition*/
	ArrayList<EquPlace> pre = new ArrayList<>();
	/**the list of the next places of this transition*/
	ArrayList<EquPlace> post = new ArrayList<>();
	/**the list of the control places prior to this transition*/
	ArrayList<EquPlace> control = new ArrayList<>();
	
	/**set to true if this transition clock follows a gaussian distribution law*/
	boolean gaussian = false;
	/**set to true if this transition clock follows a weibull distribution law*/
	boolean weibull = false;
	/**the weibull coefficient if the transition clock follows a weibull distribution law*/
	int weibullCoef = 5;

	/**instantiate the EquTransition*/
	public EquTransition(int number, String name, float lowerbound, Float upperbound) {
		super();
		this.number = number;
		this.name = name;
		this.lowerBound = lowerbound;
		this.upperBound = upperbound;
		this.normalTime = (this.lowerBound + this.upperBound) / 2;
		this.max = Float.toString(upperbound);
	}

	/**add a previous place to this transition*/
	public void addPre(EquPlace pre) {
		this.pre.add(pre);
	}

	/**add a previous control place to this transition*/
	public void addControlPre(EquPlace pre) {
		this.control.add(pre);
	}

	/**add a next place to this transition*/
	public void addPost(EquPlace post) {
		this.post.add(post);
	}

	@Override
	public ArrayList<EquPlace> getPre() {
		return pre;
	}
	
	@Override
	public ArrayList<EquPlace> getPost() {
		return post;
	}

	
// CAUTION : this method is supposed to return an empty set of control places 
// in V1,V2,V3 nets 
//	
//	@Override
//	public ArrayList<? extends PlaceAbstract> getControlPre() {
//		return new ArrayList<>();
//	}

	@Override
	public ArrayList<EquPlace> getControlPre() {
		return control;
	}
	
	/**return true if the clock of this transition follow a gaussian probability law*/
	public boolean isGaussian() {
		return gaussian;
	}

	/**
	 * sets the Gaussian probability law of the transition
	 * @param b
	 */
	public void setGaussian(boolean b) {
		gaussian=b;
	}

	/**check if this transition follows a weibull probability law*/
	public boolean isWeibull() {
		return weibull;
	}
	
	/**get the weibull coefficient of the weibull probability law*/
	public int getWeibullCoef() {
		return weibullCoef;
	}
	
	/**set the weibull coefficient of the weibull probability law*/
	public void setWeibullCoef(int coef) {
		weibullCoef=coef ;
	}
		
	/**
	 * sets the weibul characterisitcs of distribution to true/false
	 * @param b
	 */
	public void setWeibull(boolean b) {
		weibull = b;
	}

	/**samples a value for current transition depending of the probability law (uniform, gaussian, weibull...)*/
	public float sample(Sampler s) {
		float value = 0f;
		if (!isGaussian() && !isWeibull()) {
			// perform a random choice between upper and lower bound
			// this.clock=new Float(10);
			// Sampler s = new Sampler();
			if (this.lowerBound == null && this.upperBound == null)
				value = (float) s.discreteUniform(0f, 1000f);
			else if (this.upperBound == null)
				value = (float) s.discreteUniform(this.lowerBound, 1000f);
			else {
				value = s.discreteUniform(this.lowerBound, this.upperBound);
			}
		} else if (isGaussian()) {// if Gaussian has been set in the net file
			if (this.lowerBound == null && this.upperBound == null)
				value =(float) s.gaussSampler(500, 500);
			else if (this.upperBound == null)
				value = (float) s.gaussSampler((1000 + this.lowerBound) / 2,
						(double) 1000 - (((double) 1000 + this.lowerBound) / 2));
			else
				value = (float) s.gaussSampler((this.upperBound + this.lowerBound) / 2,
						(double) this.upperBound - ((double) (this.upperBound + this.lowerBound) / 2));
			if (value <= 0)
				value = 0f;
		} else if (isWeibull()) {
			if (this.lowerBound == null && this.upperBound == null)
				value = (float) s.invertTransformWeibull(weibullCoef, 500);
			else if (this.upperBound == null)
				value = (float) s.invertTransformWeibull(weibullCoef, (1000 + this.lowerBound) / 2);
			else
				value = (float) s.invertTransformWeibull(weibullCoef, (this.lowerBound + this.upperBound) / 2);
			if (value <= 0)
				return 0f;
		}
		return value;
	}

	
	
	/**
	 * true if all places in the list of control places contain at least one token
	 * @return
	 */
	public boolean controlAllowsFiring() {

		for (EquPlace p : this.control) {
			if (!p.isMarked()) {
				return false;
			}
		}
		return true;

		
	}
	
	
	/** returns true if for the current transition
		* all its input places are marked*/
		boolean presetMarked(Marking m) {
			// check that all places in the preset are filled
			for (EquPlace p : this.pre) {

				if (m.contains(p) == false) {
					return false;
				}
			}
			return true;
		}

	
	

	/** says if a transition was newly enabled by a firing*/
	boolean isNewlyEnabled(Marking m1, Marking m2, EquTransition t) {
		// m1 is the temporary marking
		// m2 is the reached marking
		if (presetMarked(m2)) {
			if (!presetMarked(m1)) {
				return true;
			}
			if (this == t) {
				return true;
			}
		}
		return false;
	}
	

	/**
	 *  says if a transition is enabled
	 *  reminder : a transition is enabled iff its preset is filled
	 * @return
	 */
	boolean isEnabled() {
		
		for (EquPlace p:this.getPre()) {
			if (!p.isMarked()) { return false;}
		}
		return true; // if no empty place was found		

		
	}

	/**get the normal time of the transition, the average between the lowerbound and the upperbound*/
	public float getNormalTime() {
		return normalTime;
	}
	
	

}
