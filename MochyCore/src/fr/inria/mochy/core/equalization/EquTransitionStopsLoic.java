package fr.inria.mochy.core.equalization;

import java.util.Random;

import fr.inria.mochy.core.sampler.Sampler;

public class EquTransitionStopsLoic extends EquTransition{
	
	final static float TARGETSPEED=583.0f; // target speed of trains 583 m/min = 35k/h
	final static float MEANSPEED=533.0f; // mean speed of trains 533 m/min = 32k/h
	
	
	// Variable for headway Regulation
		boolean isDeparture; // true if the transition represents a departure
		Float speed; // Used only if departure transition : the speed given to the train by regulation
		Float ghostSpeed; // A fake speed for a stopped train

		//public Float clock; // the time remaining before firing of the transition

	
	EquTransitionStopsLoic(int number, String name, float lowerbound, Float upperbound){
		super(number,name,lowerbound,upperbound);
		float clockValue = (lowerbound+upperbound)/2;
		clock=new Float(clockValue);
		
		
		 if (number %2 != 0 ) { // if the transition is an arrival
			 
			 		this.setSpeed(MEANSPEED);
		 } else { this.setSpeed(-1);}
		
	}
	


	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		// % Useful Fucntions %
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		// returns true if for the current transition
		// all its input places are marked
		boolean presetMarked(Marking m) {
			// check that all places in the preset are filled
			for (EquPlace p : this.pre) {

				if (m.contains(p) == false) {
					return false;
				}
			}
			return true;
		}

		// returns true if a transitions has a non-empty postset
		public boolean busyPostSet() {

			for (EquPlace p : this.post) {
				// if one can find a place in postset that
				// contains something then t is blocked
				if (p.isMarked()) {
					return true;
				}
			}

			return false;
		}

		// returns true if an enabled transition is blocked,
		// i.e. its clock is zero but one place in its postset is
		// not empty
		public boolean isBlocked(Marking m) {

			// System.out.println("Transition "+ this.name + "blocked ?");
			if (this.clock != null) {
				// System.out.println("Clock "+ this.clock);

				if (this.clock.equals(new Float(0.0))) {
					// (check that all places in the preset are filled)
					// check that places in the postset are all empty
					for (EquPlace p : this.post) {
						// System.out.println("Place "+ p.name + p.contents);
						if (p.isMarked()) {
							return true;
						}
					}
					return false; // one didnt find an occupied place
				} else {
					return false; // clock is not zero transition not blocked
				}
			}
			return false;
		}

		// check that an enabled transition is not blocked
		// and has a zero clock
		// assumption : enabled transitions only
		public boolean isFireable() {

			if (this.clock == null) {
				return false;
			}
			if (this.clock.compareTo(new Float(0.0)) > 0) {
				return false;
			}

			return true;
		}

		
		// Sets the speed of a transition to a given value. Caution, no guarantee the spped and clocks are consistsn
		public void setSpeed(float f) {
			
			System.out.println("Transition "+number +", setting speed to "+f);
			speed=f;
			
		}
		
		
		
		// Sets the ghost speed of a transition to a given value.
				public void setGhostSpeed(float f) {
					
					//System.out.println("Transition "+number +", setting speed to "+f);
					ghostSpeed=f;
					
				}
					
	public float getSpeed() {return speed;}
	
	public float getGhostSpeed() {return ghostSpeed;}
	
	
	
	// samples a speed 
	// Originally a target speed is given
	// The reached speed is obtained by adding noise
	// while sampling a speed, the clock value is set accordingly
	
	public void dwellSample (Sampler s, Float targetDwell, float range) {
		
	
		 Random random = new Random();
		 float clock = -1.0f;
		 
		while (clock < 0 ) { // To prevent a bug where dwell time becomes negative
		 clock = targetDwell -(range/2) + random.nextFloat()*range;
		}
			
		this.setClock(clock);
	this.setSpeed(-1); // speed of -1 is a particular value for dwell transition the train 
	// does not move
	
	System.out.println("Dwell Sampling (tgt= "+targetDwell + " rng= "+ range+") : speed = " + speed +" clock = " + clock);

		
	}
	
	
	// samples a speed 
		// Originally a target speed (in m/min) is given
		// The reached speed is obtained by adding noise
		// while sampling a speed, the clock value is set accordingly
		public void speedSample (Sampler s, Float targetSpeed, float range) {
			
		
			 Random random = new Random();
			float spd = targetSpeed -(range/2) + random.nextFloat()*range;

			
			this.setSpeed(spd);
			
			EquPlace p = this.getPre().get(0); // Get the preceding place of transition (no control assumed)
			
			float clock = p.getDistance()/this.speed; // moving time in minutes
			this.setClock(clock);
			System.out.println("Speed Sampling (tgt= "+targetSpeed + " rng= "+ range+") : speed = " + speed +" clock = " + clock);
			
		}

	
		
		
		
	public String toString() {
		
		String s=new String("t = "+ this.getName() + " Speed = " + this.getSpeed() + " Clock= " + this.getClock());
		return s; 
		
	}
	
}
