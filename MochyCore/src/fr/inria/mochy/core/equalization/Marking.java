package fr.inria.mochy.core.equalization;

import java.util.ArrayList;

//import fr.inria.mochy.core.Marking;
import fr.inria.mochy.core.mochysim.Place;

/**
 * manage the state of the places
 * <p>
 * <img src="Marking.svg" alt="Marking class diagram" style="float: right;">
 */
public class Marking {

	ArrayList<EquPlace> listMarked;// list of places with non-empty contents
	
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%                      CONSTRUCTOR                       %%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	Marking(){
		listMarked = new ArrayList<>();		
	}
	
	public Marking(ArrayList<EquPlace> list){
		listMarked = list;
	}

	
	public boolean contains(EquPlace p){		
		return listMarked.contains(p);		
	}
		
	public boolean add(EquPlace p){
		return listMarked.add(p);
	}
	
	
	// removes a place from a marking
	public boolean  remove(EquPlace p){
		boolean b=	listMarked.remove(p);
		//for (EquPlace ep:listMarked){System.out.println(ep.getName());}
		return b;
	}
	
	// Copies a marking 
	// nb : places are the same
	public Marking copy (){
		Marking newMarking = new Marking();
		for(EquPlace p:listMarked){
			newMarking.add(p);	
		}
		return newMarking;
	}
	
	
	public void drop(){				
		/*for(EquPlace p:listMarked){
			System.out.print(p.getName()); 			
		}		
		System.out.println();	*/	
	}


	public ArrayList<EquPlace> getListMarked() {
		return listMarked;
	}
	
	
	
	/**
	 * 
	 * @param t : an input transition 
	 * @return tru if the preset of the input transitikon (EXCLUDING CONTROL PLACES° is marked
	 */
	public boolean presetMarked(EquTransition t) {
		
		for (EquPlace p : t.getPre()) {
			
			if (this.contains(p) == false) {
				return false;
			}
		}
		return true; 
	}
	
			
	
	
}
