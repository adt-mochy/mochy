package fr.inria.mochy.core.equalization;

// A class needed by Net EquNetStopsLoic
// TODO : make a superclass EquToken and let this class inherit

public class StopsLoicToken extends Token {
	
	
	float value;
	
	
	
	public StopsLoicToken (EquPlace p, float timeToBrowse){
		super(p,timeToBrowse);
		value=timeToBrowse;
		
	}
	
	public float getValue() { return value;}
	public void setValue(float f) { value=f;}
	

}
