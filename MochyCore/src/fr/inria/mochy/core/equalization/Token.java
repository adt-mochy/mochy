package fr.inria.mochy.core.equalization;

/**a token represents a train/metro... which stays a defined time in a place (moving or stopped/station) and is managed by the transitions*/
public class Token {
	float xPlace = 0;// the distance elapsed at the current place
	Float xTotal = 0f;// the total distance elapsed
	float timeToBrowse;
	float speed;
	float savedSpeed;
	EquPlace p = null;
	public Token previousToken;
	public Token postToken;
	boolean blocked = false;
	float ghostSpeed;// ghost speed is set from EquNet if the token is in a stop place, used in V3
	boolean ghostToken = false;

	/**instantiate a token*/
	public Token() {
	}

	/**instantiate a token, set his place, time to browse the place, speed and its position in the net loop */
	public Token(EquPlace p, float timeToBrowse) {
		this.p = p;
		// System.out.println("time to browse : "+timeToBrowse);
		// calcul the xTotal value : the distance from the start point of the places
		if (!p.isStartPlace()) {
			EquPlace calculPlace = p.getPre().getPre().get(0);// get the previous place
			while (!calculPlace.isStartPlace()) {
				setXTotal(getXTotal() + calculPlace.getDistance());
				calculPlace = calculPlace.getPre().getPre().get(0);
			}
			setXTotal(getXTotal() + calculPlace.getDistance());
		}

		this.timeToBrowse = timeToBrowse;
		this.speed = p.getDistance() / timeToBrowse;
	}

	/**check if the token is at the end of the place*/
	public boolean isAtTheEnd() {
		// return xPlace >= p.getDistance();
		return timeToBrowse <= 0;
	}

	/** change the place when a token is at the end
	* set the timeToBrowse for the new place*/
	public void changePlace(EquPlace p, float timeToBrowse) {
		this.p.tokens.remove(this);
		this.p = p;
		xPlace = 0;
		// System.out.println("Time To Browse : "+timeToBrowse);
		// avoid the token to overtake the next token
		float headwayDistance = p.net.MIN_HEADWAY_DISTANCE;
		EquTransition t = p.post;
		float headwayTime = 0;
		if (p.tokens.size() > 0) {
			// float speedInitial = p.getDistance() / timeToBrowse;
			// float headwayTime = headwayDistance / speedInitial;
			/**
			 * If there is already some token(s) in this place We calcul the headway time
			 * depending on the headway distance and the speed of the next token (because it
			 * can be slower than the current token, if it is faster there is no problem) if
			 * the linked transition is blocked, we take the saved speed (the current token
			 * speed will be calculated after but it will be saved into its savedSpeed and
			 * its speed will be set to 0)
			 */
			if (!this.postToken.blocked) {
				headwayTime = headwayDistance / this.postToken.speed;
				// System.out.println("not blocked s = "+this.postToken.speed);
			} else {
				headwayTime = headwayDistance / this.postToken.savedSpeed;
				// System.out.println("blocked transition s =
				// "+this.postToken.speed+"-"+this.postToken.savedSpeed);
			}
			if (this.postToken.xPlace <= this.p.net.MIN_HEADWAY_DISTANCE) {
				this.timeToBrowse = this.postToken.timeToBrowse + headwayTime;
			}
			/**
			 * then if (this.postToken.timeToBrowse >= timeToBrowse - headwayTime) which
			 * means that the time to browse of the next token plus the headway time will be
			 * longer that the current token time to browse calculated then we set the
			 * current time to browse equal to the next time to browse plus the headway time
			 * calculated to preserve the headway distance
			 */
			/*if (this.postToken.timeToBrowse >= timeToBrowse - headwayTime){
				this.timeToBrowse = this.postToken.timeToBrowse + headwayTime;
			}*/
			/*float minTimeToBrowse = (p.getDistance() - this.postToken.xPlace + headwayDistance) / this.postToken.speed;
			if (timeToBrowse < minTimeToBrowse) {
				this.timeToBrowse = minTimeToBrowse;
			}*/
			else {
				this.timeToBrowse = timeToBrowse;
			}
		} else {
			this.timeToBrowse = timeToBrowse;
		}

		// avoid the token to overtake the next token
		/*if (p.tokens.size()>0 && this.postToken.timeToBrowse >= timeToBrowse)
			this.timeToBrowse = this.postToken.timeToBrowse + 1f;
		else
			this.timeToBrowse = timeToBrowse;*/

		this.speed = p.getDistance() / this.timeToBrowse;

		// regulate the speed depending of the max speed value
		if (this.speed > this.p.net.MAX_SPEED) {
			// System.out.println("Excessive speed : "+this.speed);
			this.speed = this.p.net.MAX_SPEED;
			this.timeToBrowse = this.p.getDistance() / this.speed;
			// System.out.println("excessive speed");
		}
		// regulate the speed depending of the min speed value
		if (this.speed < this.p.net.MIN_SPEED && this.p.distance != 0 && !this.blocked) {
			this.speed = this.p.net.MIN_SPEED;
			this.timeToBrowse = this.p.getDistance() / this.speed;
		}
		
		// if the next token is in this place and is blocked, block this one too
		// will be unblocked with unblockTransition method of equTransition class
		// (called in equNet class):
		if (this.postToken.speed == 0 && this.postToken.p.equals(this.p)
				&& this.timeToBrowse <= this.postToken.timeToBrowse + headwayTime) {
			blockToken();
			// System.out.println("set speed to 0");
		}

		p.addToken(this);

		// calcul the xTotal value : the distance from the start point of the places
		setXTotal(0);
		if (!p.isStartPlace()) {
			EquPlace calculPlace = p.getPre().getPre().get(0);// get the previous place
			while (!calculPlace.isStartPlace()) {
				setXTotal(xTotal + calculPlace.getDistance());
				calculPlace = calculPlace.getPre().getPre().get(0);
			}
			setXTotal(xTotal + calculPlace.getDistance());
		}
	}

	/**return the time to browse the place where it is*/
	public float allowedTime() {
		return timeToBrowse;
	}

	/**advance the time as specified in parameter and update the position of the token
	 * @param time*/
	public void advanceTime(float time) {
		// System.out.println("advance time "+p.getName()+" time to browse :
		// "+timeToBrowse+" delay : "+time);
		this.timeToBrowse -= time;
		// System.out.println("ttb: "+timeToBrowse);
		float distance = speed * time;

		xPlace += distance;
		if (xPlace > p.distance) {
			xPlace = p.distance;
		}
		xTotal += distance;

		/*setXTotal(xPlace);
		if (!p.isStartPlace()) {
			EquPlace calculPlace = p.getPre().getPre().get(0);// get the previous place
			while (!calculPlace.isStartPlace()) {
				setXTotal(getXTotal() + calculPlace.getDistance());
				calculPlace = calculPlace.getPre().getPre().get(0);
			}
			setXTotal(getXTotal() + calculPlace.getDistance());
		}
		setXTotal(((int) (getXTotal()*100))/100);*/

		// if the token is linked to the clock of the post transition
		// (eg the token is the first transition of the place)
		if (this.equals(p.tokens.get(0)))
			this.p.getPost().setClock(this.p.getPost().getClock() - time);
	}

	/**block this token and the pre token at its headway*/
	public void blockToken() {
		// System.out.println("block token : speed :"+this.speed+" saved speed :
		// "+this.savedSpeed);
		float headwayTime = this.p.net.MIN_HEADWAY_DISTANCE / this.speed;
		this.savedSpeed = this.speed;
		this.speed = 0;
		blocked = true;
		if (this.p.tokens.contains(this.previousToken)
				&& this.previousToken.timeToBrowse <= this.timeToBrowse + headwayTime) {
			this.previousToken.blockToken();
		}
	}

	/**unblock this token and its previous blocked tokens*/
	public void unblockToken() {
		//System.out.println("unblock token : speed : "+this.speed+" saved speed :"+this.savedSpeed);
		// warning : we suppose the next tokens of this place have been unblocked before
		this.speed = this.savedSpeed;
		blocked = false;
		if (this.p.tokens.contains(this.previousToken) && this.previousToken.blocked) {
			this.previousToken.unblockToken();
		}
	}

	/**get the position of the token in the place*/
	public float getxPlace() {
		return xPlace;
	}

	/**get the place where the token is*/
	public EquPlace getPlace() {
		return p;
	}

	/**get the position of the token in the net loop*/
	public Float getXTotal() {
		return this.xTotal;
	}

	/**round the position of the token in the net loop up to 2 decimals*/
	public float roundXTotal() { 
		return (Math.round(xTotal * 100)) / 100;
	}

	/**set the position of the token in the net loop*/
	public void setXTotal(float xTotal) {
		this.xTotal = xTotal;
	}

	/**get the speed of the token*/
	public float getSpeed() {
		return speed;
	}

	/**set the speed of the token
	 * @param f the speed*/
	public void setSpeed(float f) {
		 speed = f;
	}
	
	/**get the ghost speed, the future speed the token will have when it will quit a stop place*/
	public float getGhostSpeed() {
		return ghostSpeed;
	}

	/**set the ghost speed, the future speed the token will have when it will quit a stop place
	 * @param f*/
	public void setGhostSpeed(float f) {
		 ghostSpeed = f;
	}
	
	/**check if the token is blocked*/
	public boolean isBlocked() {
		return blocked;
	}

	/**get the time to browse the place for the token*/
	public float getTimeToBrowse() {
		return timeToBrowse;
	}
	
	/**set the time to browse the place for the token*/
	public void setTimeToBrowse(float value) {
		this.timeToBrowse = value;
	}
	
}
