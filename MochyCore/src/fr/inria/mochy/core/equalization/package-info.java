/**
 * equalization : a package of nets/physical models where the net is a loop and the regulation aim to get to an equal distance between each of the successive tokens
 * <p>
 *
 * <img src="package.svg" alt="Package class diagram" style="float: right;">
 */
package fr.inria.mochy.core.equalization;
