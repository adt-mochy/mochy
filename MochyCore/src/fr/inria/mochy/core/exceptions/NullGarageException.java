package fr.inria.mochy.core.exceptions;

/**an exception which is called when there is an issue with a garage place in a net to insert new tokens*/
public class NullGarageException extends Exception{
	
	/**instantiate the exception when the is an issue with a garage place
	 * @param x the exception linked : NullPointerException*/
	public NullGarageException(NullPointerException x) {
		super("Warning: There is no garage place or it has no successor. It must be defined in the input file as garage:placeNb:name:length and a transition linked with an inflow.", x);
	}
	
}
