package fr.inria.mochy.core.exceptions;

public class TransitionNotFoundException extends Exception {
	/**instantiate the exception when the is an issue with a transition not found
	 * @param x the exception linked : NullPointerException*/
	public TransitionNotFoundException() {
		super("Warning: The transition is not found.");
	}
}
