/**
 * exceptions : package which manage the exceptions which can occur during the simulation of a net of MochyCore
 * <p>
 *
 * <img src="package.svg" alt="Package class diagram" style="float: right;">
 */
package fr.inria.mochy.core.exceptions;
