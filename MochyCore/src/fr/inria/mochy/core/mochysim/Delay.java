package fr.inria.mochy.core.mochysim;

/**
 * Used for create temporary delays at specific station for a specific duration
 * <p>
 * <img src="Delay.svg" alt="Delay class diagram" style="float: right;">
 */
public class Delay {
	float startDate;
	float endDate;
	int transitionNb;
	float delay;
	boolean valid;
	
	public Delay(float startDate, float endDate, int transitionNb, float delay) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.transitionNb = transitionNb;
		this.delay = delay;
		this.valid = false;
	}
	
	/**get the start date the delay will be effective*/
	public float getStartDate() {
		return startDate;
	}
	
	/**set the start date the delay will be effective*/
	public void setStartDate(float startDate) {
		this.startDate = startDate;
	}
	
	/**get the end date the delay will be effective*/
	public float getEndDate() {
		return endDate;
	}
	
	/**set the end date the delay will be effective*/
	public void setEndDate(float endDate) {
		this.endDate = endDate;
	}
	
	/**get the number of the transition on which the delay will be effective*/
	public int getTransitionNb() {
		return transitionNb;
	}
	
	/**set the number of the transition on which the delay will be effective*/
	public void setTransition(int transitionNb) {
		this.transitionNb = transitionNb;
	}
	
	/**get the value of the delay*/
	public float getDelay() {
		return delay;
	}
	
	/**set the value of the delay*/
	public void setDelay(float delay) {
		this.delay = delay;
	}
	
	/**used in RPN.RPNConfig.manageDelays() to check if the delay is valid depending on the current date and the start/end dates of the delay*/
	public boolean isValid() {
		return valid;
	}
	
	/**used in RPN.RPNConfig.manageDelays() to set a delay validity to false or true depending on the current date and the start/end dates of the delay, this prevent from adding the delay multiple times and help to remove it once the current date has overlapped the end date of the delay*/
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	
}
