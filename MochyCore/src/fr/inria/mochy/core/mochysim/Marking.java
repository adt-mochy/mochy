package fr.inria.mochy.core.mochysim;
import java.util.ArrayList;


/**
 * manage the state of the places : list the marked place e.g. they have a content not set to null
 * <p>
 * <img src="Marking.svg" alt="Marking class diagram" style="float: right;">
 */
public class Marking {

	ArrayList<Place> listMarked;// list of places with non-empty contents
	
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%                      CONSTRUCTOR                       %%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	Marking(){
		listMarked = new ArrayList<>();		
	}
	
	public Marking(ArrayList<Place> list){
		listMarked = list;
	}

	
	public boolean contains(Place p){		
		return listMarked.contains(p);		
	}
		
	public boolean add(Place p){
		return listMarked.add(p);
	}
	
	
	/**removes a place from a marking*/
	public boolean  remove(Place p){
		boolean b=	listMarked.remove(p);
		//for (Place ep:listMarked){System.out.println(ep.getName());}
		return b;
	}
	
	/** Copies a marking 
	* nb : places are the same*/
	public Marking copy (){
		Marking newMarking = new Marking();
		for(Place p:listMarked){
			newMarking.add(p);	
		}
		return newMarking;
	}
	
	
	public void drop(){				
		/*for(Place p:listMarked){
			System.out.print(p.getName()); 			
		}		
		System.out.println();*/		
	}


	public ArrayList<Place> getListMarked() {
		return listMarked;
	}
	
}
