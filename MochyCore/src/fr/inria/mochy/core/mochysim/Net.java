package fr.inria.mochy.core.mochysim;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.sampler.Sampler;

/**
 * A classic Net model with transitions, places and their state. It is loaded
 * from a net file.
 * 
 * A net contains transitions (actions) places (place holders for objects that
 * are moved, consumed or produced) control places : they are used to allow or
 * conversely disable transitions.
 *
 * the semantics is the following : as soon as a transition has all its places
 * in its presset filled, a value is sampled. This is the time to fire (TTF,
 * clock) of the transition. The time to fire decreases over time When the TTF
 * reaches 0, the transition is firable. However, it can fire only if its
 * control place is filled, and if the postset places of the net are free
 * <p>
 * <img src="Net.svg" alt="Net class diagram" style="float: right;">
 */
public class Net extends PhysicalModel {
	/**
	 * the list of the places which have a content "marked" if there is one token or
	 * none then its content is "null"
	 */
	public HashMap<Integer, Place> places;
	/**
	 * the list of the transitions which manage the time a token will stay at a
	 * place
	 */
	public HashMap<Integer, Transition> transitions;
	/** list of enabled transitions : previous places marked and clock != 0 */
	public HashMap<Integer, Transition> enabled;
	/**
	 * list of blocked transitions : previous places marked, and at least one next
	 * place is marked and clock = 0
	 */
	public HashMap<Integer, Transition> blocked;
	/**
	 * list of firable transitions : previous places marked, free next places,
	 * clock=0
	 */
	public HashMap<Integer, Transition> firable;

	Random rand; // random number generator

	// public ArrayList<Place> places; // places of the net
	/**
	 * the list of the control places of the net which gives authorization to fire a
	 * transition or not if it's not marked
	 */
	HashMap<Integer, Place> controlPlaces;

	// public ArrayList<Transition> transitions; // transitions of the net
	/** the initial marking of the places which have a content */
	Marking initMarking;
	/** the current marking of the places which have a content */
	Marking currentMarking;

	/**
	 * Establishes relation between transition numbers and the value of attached
	 * clock
	 */
	HashMap<Integer, Float> Config;

	HashMap<Integer, Float> initConfig;

	// public ArrayList<Transition> enabled; // list of enabled transitions
	// a transition is enabled if its preset is filled and its associated clock
	// is not zero
	// public ArrayList<Transition> blocked; // list of blocked transitions
	// a transition is blocked iff it is enabled, it clock has value 0 and
	// one place in its postset is not empty
	// public ArrayList<Transition> firable; // list of firable transitions
	// a transition is firable iff its preset is filled, its clock has value 0,
	// and its postset is empty

	int n = 1; // the number of line, the number of transitions fired

	Logger logger = Logger.getLogger("logger");

	Sampler s;

	// String fname;
	/**
	 * set to true so that the clocks of the transitions follow a gaussian
	 * distribution
	 */
	boolean gaussian = false;

	/**
	 * set to true so that the clocks of the transitions follow a weibull
	 * distribution
	 */
	boolean weibull = false;

	/**
	 * the weibull coeficient the clocks of the transitions will follow the weibull
	 * distribution
	 */
	int weibullCoef = 30;

	// ***********************************************
	// * Parameters of simulation *
	// ***********************************************
	/**
	 * set to true if contents of control places is consumed when firing transition
	 */
	boolean consume_control_tokens = true;

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// CONSTRUCTORS
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	/** take the path of the input file as parameter */
	public Net(String fname) {

		this.fname = fname;
		timeElapsed = 0;

		rand = new Random();
		s = new Sampler();

		places = new HashMap<>(); // Creates an empty list of places
		controlPlaces = new HashMap<>(); // Creates an empty list of control places
		transitions = new HashMap<>(); // Creates an empty list of transitions
		currentMarking = new Marking(); // creates a marking where all places are empty

		Config = new HashMap<>();
		// Establishes relation between transition numbers
		// and the value of attached clock

		enabled = new HashMap<>();
		blocked = new HashMap<>();
		setFirable(new HashMap<>());
	}

	/**
	 * Loads a net from a file
	 * 
	 * @return The method returns a strings to diplay success/failure in the log
	 *         area of the interface.
	 */
	public String loadFile() {
		// String logs = ""; // The string to return once the net is loaded
		StringBuffer logs = new StringBuffer();
		int lineNb = 1;
		// Open a file
		// reads places, transitions, flows
		// read distributions
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fname));
			System.out.println("*******************");
			System.out.println("Opening File : " + fname);

			while (br.ready()) { // Tant qu�on peut encore lire des choses
				logs.append(handleLine(br.readLine(), lineNb));
				// += handleLine(br.readLine(), lineNb);
				lineNb++;
			}

			// At this place all lines are read
			// i.e. all places, transitions and the current marking
			// We can hence sample duration for every enabled transition
			// set the gaussian value to true to all the transitions of the net
			// if the file read the Gaussian value and the same if this is weibull
			if (this.gaussian) {
				for (Transition t : this.transitions.values()) {
					if (!t.weibull) {
						t.setGaussian(true);
						t.setWeibull(false);
					}
					if (t.presetMarked(currentMarking)) {
						t.sample(s);
						enabled.put(t.number, t);
						this.Config.put(t.getNumber(), t.getClock());
					}
				}
			} else if (this.weibull) {
				for (Transition t : this.transitions.values()) {
					if (!t.gaussian) {
						t.setGaussian(false);
						t.setWeibull(true);
						t.setCoefWeibull(weibullCoef);
					}
					if (t.presetMarked(currentMarking)) {
						t.sample(s);
						enabled.put(t.number, t);
						this.Config.put(t.getNumber(), t.getClock());
					}
				}
			} else {
				for (Transition t : this.transitions.values()) {
					if (t.presetMarked(currentMarking)) {
						t.sample(s);
						enabled.put(t.number, t);
						this.Config.put(t.getNumber(), t.getClock());
					}
				}
			}

			br.close();
			System.out.println("*******************");

			// from initial marking, build initial configuration

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class Net");
				}
			}
		}

		initMarking = currentMarking.copy();
		initConfig = Config;

		return logs.toString();
	}

	/**
	 * load a line of the input file
	 * 
	 * @param line   the line to read
	 * @param lineNb the number of the line in the input file
	 * @return the errors which occur as a String
	 */
	protected String handleLine(String line, int lineNb) {
		//System.out.println(line);

		String logs = "";
		String[] segments = line.split(":");

		if (segments[0].equals("place")) {
			// System.out.println("A place");
			try {
				int nb = Integer.parseInt(segments[1]);
				places.put(nb, new Place(nb, segments[2]));
			} catch (NumberFormatException nfe) {
				logs += "Line " + lineNb + " : error segment 1 is not a number\n";
			}
		}

		else if (segments[0].equals("control")) {
			//System.out.println("A control place");

			try {
				controlPlaces.put(Integer.parseInt(segments[1]), new Place(Integer.parseInt(segments[1]), segments[2]));
			} catch (NumberFormatException nfe) {
				System.out.println("Failed");

				logs += "Line " + lineNb + " : error segment 1 is not a number\n";
			}
		}

		else if (segments[0].equals("transition")) {
			// System.out.println("A transition");
			Transition t;
			// if an interval has been set up in the parameters
			int transitionNb = 0;
			try {
				transitionNb = Integer.parseInt(segments[1]);
			} catch (NumberFormatException nfe) {
				logs += "Line " + lineNb + " : error segment 1 is not a number\n";
			}

			if (segments.length > 3) {
				String[] interval = segments[3].substring(1, segments[3].length() - 1).split(",");
				// if the upperBound is infinite

				if (interval[1].equals("inf")) {
					float min = 0;
					try {
						min = Float.parseFloat(interval[0]);
					} catch (NumberFormatException nfe) {
						logs += "Line " + lineNb + " : error the interval format is not [nb,nb] or [nb,inf]\\n";
					}
					t = new Transition(transitionNb, segments[2], min, null);
				}
				// if both bounds are definite
				else {
					float min = 0, max = 0;
					try {
						min = Float.parseFloat(interval[0]);
						max = Float.parseFloat(interval[1]);
					} catch (NumberFormatException nfe) {
						logs += "Line " + lineNb + " : error the interval format is not [nb,nb] or [nb,inf]\n";
					}
					t = new Transition(transitionNb, segments[2], min, max);
				}
				if (segments.length > 4) {
					if (segments[4].equals("Gaussian")) {
						t.setGaussian(true);
					} else if (segments[4].equals("Weibull") && segments.length > 5) {
						t.setWeibull(true);
						try {
							t.setCoefWeibull(Integer.parseInt(segments[5]));
						} catch (NumberFormatException nfe) {
							logs += "Line " + lineNb + " : error the coeficient of Weibull is not a number\n";
						}
					} else if (segments[4].equals("Weibull") && segments.length <= 5)
						logs += "Line " + lineNb + " : error the coeficient of Weibull is not defined\n";
				}
			}
			// if no bounds are definite
			else {
				t = new Transition(transitionNb, segments[2], 0f, null);
			}
			transitions.put(t.getNumber(), t);
		}

		else if (segments[0].equals("inflow")) {
			// System.out.println("Inflow");
			int trans = 0, place = 0;
			try {
				trans = Integer.parseInt(segments[1]);
				place = Integer.parseInt(segments[2]);
				if (this.findTransition(trans) == null)
					logs += "Line " + lineNb + " : error the transition, segment 1, is not found\n";
				if (this.findPlace(place) == null)
					logs += "Line " + lineNb + " : error the place, segment 2, is not found\n";
				if (this.findTransition(trans) != null && this.findPlace(place) != null)
					this.addInFlow1(trans, place);
			} catch (NumberFormatException nfe) {
				logs += "Line " + lineNb + " : error the origin or destination is not a number\n";
			}
		}

		else if (segments[0].equals("outflow")) {
			// System.out.println("Outflow");
			int trans = 0, place = 0;
			try {
				trans = Integer.parseInt(segments[1]);
				place = Integer.parseInt(segments[2]);
				if (this.findTransition(trans) == null)
					logs += "Line " + lineNb + " : error the transition, segment 1, is not found\n";
				if (this.findPlace(place) == null)
					logs += "Line " + lineNb + " : error the place, segment 2, is not found\n";
				if (this.findTransition(trans) != null && this.findPlace(place) != null)
					this.addOutFlow(trans, place);
			} catch (NumberFormatException nfe) {
				logs += "Line " + lineNb + " : error the origin or destination is not a number\n";
			}
		}

		else if (segments[0].equals("initial")) {
			//System.out.println("Initial Marking : " + (segments.length - 1) + " place(s) marked");

			for (int i = 1; i < segments.length; i++) {
				Integer pn = 0;
				try {
					pn = Integer.parseInt(segments[i]);
				} catch (NumberFormatException nfe) {
					logs += "Line " + lineNb + " : error the segment " + i + " is not a number\n";
				}
				//System.out.println("Place:" + pn);

				// add place to current marking
				Place pl = (Place) this.findPlace(pn);
				if (pl == null)
					logs += "Line " + lineNb + " : error the place, segment " + i + ", is not found\n";
				else {
					pl.contents = new Float(0);
					this.currentMarking.add(pl);
					nbTokens++;
				}
			}

		} // end of parsing of line for initial marking

		else if (segments[0].equals("Gaussian")) {
			gaussian = true;
			logs += "Law of Gauss";
		} else if (segments[0].equals("Weibull")) {
			weibull = true;
			weibullCoef = Integer.valueOf(segments[1]);
			logs += "Law of Weibull";
		} else if (segments[0].equals("%")) {
			// comment
		}

		// System.out.println("Initial Marking parsed");

		return logs;

	}// end of method Handleline

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// %% Net Construction functions %%
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	/**
	 * adds a place in the preset of a transition
	 * 
	 * @param tnum the id number of the transition
	 * @param pnum the id number of the place
	 */
	protected void addInFlow1(int tnum, int pnum) {

		Place p = this.findPlace(pnum);
		Transition t = this.findTransition(tnum);

		if (this.isAControlPlace(pnum)) {
			t.addControlPre(p);
		} else {
			t.addPre(p);
		}

	}

	/**
	 * adds a place in the postset of a transition
	 * 
	 * @param tnum the transition id number
	 * @param pnum the place id number
	 */
	protected void addOutFlow(int tnum, int pnum) {

		Place p = this.findPlace(pnum);
		Transition t = this.findTransition(tnum);
		t.addPost(p);

	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// %% Useful functions %%
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	/**
	 * Find a place from its number This place can be a standard place or a control
	 * place
	 * 
	 * @param pnum
	 * @return the place found
	 */
	public Place getPlace(int pnum) {

		if (places.containsKey(pnum))
			return places.get(pnum);

		if (controlPlaces.containsKey(pnum))
			return controlPlaces.get(pnum);

		return null;
	}

	/**
	 * @param the control place id number
	 * @return the control place with this id number
	 */
	public Place getControlPlace(int pnum) {

		if (controlPlaces.containsKey(pnum))
			return controlPlaces.get(pnum);

		return null;
	}

	/**
	 * Add a token in a standard place
	 * 
	 * @param pnum the place number
	 * @return true if the place exist
	 */
	public Boolean addToken(int pnum) {

		Place p = getPlace(pnum);
		if (p != null) {
			p.addToken();
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Adds a token in a control place
	 * 
	 * @param pnum
	 * @return
	 */
	public Boolean addControlToken(int pnum) {

		Place p = getControlPlace(pnum);
		if (p != null) {
			p.addToken();
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Checks is a place is a control places
	 * 
	 * @param pnum : the place number
	 * @return true if pnum is a control place, false otherwise
	 */
	public boolean isAControlPlace(int pnum) {
		if (controlPlaces.containsKey(pnum))
			return true;

		return false;
	}

	// Finds a transition from its number
	public Transition findTransition(Integer tnum) {
		if (transitions.containsKey(tnum))
			return transitions.get(tnum);
		return null;
	}

	// Finds a transition from its name
	public Transition findTransition(String name) {
		for (Transition t : this.transitions.values()) {
			if (t.getName().equals(name))
				return t;
		}
		return null;
	}

	// Checks if a transition is fireable, i.e belongs to the list of firable
	// transitions
	public boolean isFirable(Transition t) {
		if (firable.containsKey(t.getNumber()))
			return true;

		return false;
	}

	/**
	 * Checks if the control places used as input to the current transition allow
	 * firing of t
	 * 
	 * @param t
	 * @return
	 */
	public boolean controlAllowsFiring(Transition t) {

		return t.controlAllowsFiring();

	}

	// Checks if a transition is enabled, i.e belongs to the list of enabled
	// transitions
	public boolean isEnabled(Transition t) {
		if (enabled.containsKey(t.number))
			return true;

		return false;
	}

	// Finds a place from its number
	public Place findPlace(int pnum) {
		// System.out.println("searching place" + pnum);
		if (places.containsKey(pnum))
			return places.get(pnum);

		if (controlPlaces.containsKey(pnum))
			return controlPlaces.get(pnum);

		// System.out.println("Place not found");

		return null;
	}

	/**
	 * Returns true if pnum is the number of a control place
	 * 
	 * @param pnum
	 * @return
	 */
	public boolean isAControlPace(int pnum) {

		// if (places.containsKey(pnum))
		// return false;

		if (controlPlaces.containsKey(pnum))
			return true;

		return false;
	}

	// returns true if a transitions has a non-empty postset
	public boolean busyPostSet(Integer tnum) {

		Transition t = findTransition(tnum);
		ArrayList<Place> post = t.post;

		for (Place p : post) {
			// if one can find a place in psotset that
			// contains something then t is blocked
			if (p.contents != null) {
				return true;
			}
		}

		return false;
	}

	// returns a list of newly enabled transition
	public ArrayList<Transition> newlyEnabled(Marking m1, Marking m2, Transition t) {

		ArrayList<Transition> result = new ArrayList<>();

		for (Transition tr : this.transitions.values()) {
			if (t.isNewlyEnabled(m1, m2, tr)) {
				result.add(tr);
			}
		}
		return result;

	}

	public void setMarking(ArrayList<Place> l, ArrayList<Float> vallist) {

		// all places supposed empty at start
		// place list and contents list same size
		ListIterator<Float> it;

		it = vallist.listIterator();
		Float f;

		for (Place p : l) {
			f = it.next();
			p.setContents(f);
		}
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// SEMANTICS
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	public void blockTransition(Transition t) {
		// block a transition which clock reaches 0 and can
		// not be fired

		this.blocked.put(t.number, t);

	}

	public float maxAllowedTimedMove() {
		// returns the maximal allowed timed move, i.e.
		// the mimimal time to fire among enabled transitions
		if (!getFirable().isEmpty()) {
			return new Float(0);
		}
		if (!enabled.isEmpty()) {
			Float min = new Float(10000);
			// compare to all values in the table
			for (Transition t : this.enabled.values()) {
				// System.out.println(t.name);
				if (t.getClock() < min) {
					min = t.getClock();
				}
			}
			return min;
		} else {
			return new Float(0);
		}
	}

	public void timedMove(Float f) {
		timeElapsed += f;
		// this move implements timed move
		// assuming that this time elapsing is allowed
		Float clockval;
		Set<Integer> clk = this.Config.keySet();

		// update enabled transitions
		for (Transition t : enabled.values()) {
			t.setClock(t.getClock() - f);
		}

		for (Integer tnum : clk) {
			// for every entry in the hashtable
			clockval = Config.get(tnum);
			if (clockval > f) {
				// decrease by f if possible
				this.Config.put(tnum, new Float(clockval - f));
			} else { // the value is necessarily f

				// then compute blocked and fireable transitions

				// the transition must be removed from the clock configuration
				// and the corresponding transition becomes either
				// blocked or fireable
				this.Config.remove(tnum);

				if (busyPostSet(tnum)) {
					// System.out.println("Transition " + tnum + " is blocked");
					blocked.put(tnum, findTransition(tnum));
				} else {
					// System.out.println("Transition " + tnum + " is firable");

					getFirable().put(tnum, findTransition(tnum));
				}
			}
		} // end of for
	}

	/*
	 * public String toString() { String s = ""; s += "places:" + places.size(); s
	 * += "transitions:" + transitions.size(); return s;
	 * 
	 * }
	 */

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// % STPN MOVES %%
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	public void progressTime(Float delta) {
		stepsNb++;
		timeElapsed += delta;
		System.out.println("2"+timeElapsed);
		discreteStep = false;
		HashMap<Integer, Transition> stillEnabled; // list of transitions still enabled
		stillEnabled = new HashMap<>();

		// remove delta to all enabled transitions that are not blocked
		for (Transition t : enabled.values()) {

			if (t.getClock().compareTo(delta) > 0) {
				// if the value of the clock attached to this transition allows a delta
				// time move
				t.setClock(new Float(t.getClock() - delta));
				stillEnabled.put(t.number, t);
				// System.out.println("Transition " + t.name + " still enabled");
			} else { // clock is necessarily 0
				t.setClock(new Float(0));
				if (t.isBlocked(currentMarking)) {
					this.blocked.put(t.number, t);
					// System.out.println("Transition " + t.name + "blocked");

				} else {
					getFirable().put(t.number, t);
					// System.out.println("Transition " + t.name + "firable");

				}
			}
		} // all enabled transitions have been visited

		// Refresh list of enabled transitions
		this.enabled = stillEnabled;

	}

	public void discreteMove() {
		discreteMove("", 0, false);
	}

	public boolean discreteMove(String pathLogs, long elapsedTime, boolean enableLogs) {

		// randomly choose a particular transition among allowed ones
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive

		//System.out.print("Discrete Move : " + this.getFirable().size() + " firable transition(s)");

		// if there is no firable transitions, the is no discrete
		// move possible on a random transition
		if (this.getFirable().size() == 0) {
			return false;
		}

		int randomNum = rand.nextInt(this.getFirable().size());
		//System.out.print(randomNum + ":");

		// select randomly a transition to fire among firable ones
		Integer[] keys = (Integer[]) getFirable().keySet().toArray(new Integer[0]);
		Transition t = this.getFirable().get(keys[randomNum]);
		// System.out.println(t.name);

		// perform a discrete move with the selected transition
		discreteMove(t, pathLogs, elapsedTime, enableLogs);
		return true;
	}

	public boolean discreteMoveWithControl(String pathLogs, long elapsedTime, boolean enableLogs) {

		// randomly choose a particular transition among allowed ones
		// i.e. transitions with TTF to 0 and not blockd by an empty control place

		HashMap<Integer, Transition> firabletrans = this.getFirable();

		//System.out.print("Discrete Move : ");
		//System.out.println("this.firable.size : " + firabletrans.size());

		// if there is no firable transitions, the is no discrete
		// move possible on a random transition

		ArrayList<Transition> firecontrol = new ArrayList<Transition>();// list of firable and allowed transitions

		for (Transition t : firabletrans.values()) { // check every firable transition

			if (t.controlAllowsFiring()) {
				firecontrol.add(t);
			} // keep it if allowed by control

		}

		if (firecontrol.size() == 0) {
			return false;
		} // if the list is empty firing fails

		// otherwise pick one transition randomly
		int randomNum = rand.nextInt(firecontrol.size());
		//System.out.print(randomNum + ":");

		Transition t = firecontrol.get(randomNum);
		// System.out.println(t.name);

		discreteMove(t, pathLogs, elapsedTime, enableLogs);
		return true;
	}

	public String discreteMove(TransitionAbstract trans, String pathLogs, float elapsedTime, boolean enableLogs) {
		nbDiscreteSteps++;
		stepsNb++;
		discreteStep = true;
		// fires a transition t
		// IMPORTANT : t is supposed fireable
		Transition t = (Transition) trans;
		if (!isFirable(t)) {
			// System.out.println("Trying to fire transition t" + t.name);
			// System.out.println("not firable");
			return "Trying to fire transition t" + t.name + "not firable";
		}

		if (!controlAllowsFiring(t)) {
			// System.out.println("Trying to fire transition t" + t.name);
			// System.out.println("Control does not annow Firing");
			return "Trying to fire transition t" + t.name + "Control does not annow Firing";
		}

		/*** log the fired transition **********/
		/*** nline : date : fired transition ***/
		if (enableLogs) {
			FileWriter fileLogs = null;
			try {
				fileLogs = new FileWriter(pathLogs, true);
				fileLogs.write(n + ":" + this.getTimeElapsed() + ":" + t.getName() + "\n");
			} catch (IOException e) {
				logger.log(Level.WARNING, "error of writing in fileLogs in class Net");
			} finally {
				try {
					fileLogs.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the fileLogs in class Net");
				}
			}
		}
		n++;

		// compute new marking

		Marking tempMarking = this.currentMarking.copy();

		for (Place p : t.pre) {// for every place in the preset of t
			// System.out.println("Removing" + p.getName() + "from marking");
			p.contents = null;
			boolean b = tempMarking.remove(p);
		}

		// If semantics forces control token consumption
		if (consume_control_tokens) {
			for (Place p : t.controlPre) {// for every place in the control preset of t
				p.contents = null;
			}

		}

		Marking newMarking = tempMarking.copy();

		// System.out.println("Temporary marking");
		tempMarking.drop();

		for (Place p : t.post) {// for every place in the preset of t
			// System.out.println("Adding" + p.getName() + "to temp marking");
			boolean b = newMarking.add(p);
			p.contents = new Float(0.0);
		}
		// System.out.println("New marking");
		newMarking.drop();

		this.currentMarking = newMarking;

		// find all newly enabled transitions
		// and sample their clock value

		for (Transition et : this.transitions.values()) {
			if (et.isNewlyEnabled(tempMarking, newMarking, t)) {
				// if et was not enabled in temp marking
				// sample times for all newly enabled transitions
				et.sample(s);
				this.Config.put(et.number, et.getClock());
				// System.out.println("Newly enabled" + et.name);
			}
		}

		this.enabled = new HashMap<>();
		this.blocked = new HashMap<>();
		this.setFirable(new HashMap<>());
		this.Config = new HashMap<>();

		// Rebuild list of enabled/blocked/firable transitions
		for (Transition et : this.transitions.values()) {
			if (et.presetMarked(newMarking)) {
				// transition enabled, look at clock value
				if (et.getClock().equals(new Float(0.0))) {
					// transition's clock has reached value 0
					if (et.busyPostSet()) { // if one place marked
						this.blocked.put(et.number, et);
						// System.out.println(et.name + "blocked");

					} else {
						this.getFirable().put(et.number, et);
						// System.out.println(et.name + "firable");
					}

				} else { // transition enabled
					this.enabled.put(et.number, et);
					Config.put(et.number, et.getClock());
				}

			}
		} // all transitions considered
		return "transition " + t.getName() + "fired";

	}

	public void multipleSteps(int steps) {
		for (int i = 1; i <= steps; i++) {
			Float delta = this.maxAllowedTimedMove();
			if (delta.compareTo(new Float(0.0)) > 0)// if a timed move is allowed
				this.progressTime(delta);
			else if (this.getFirable().size() != 0) // timed moves and discrete moves are exclusive
				this.discreteMoveWithControl("", 0, false);
		}
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// % DISPLAY Methods %%
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	public void listEnabled() {
		System.out.println("Enabled Transitions:");
		for (Transition t : enabled.values()) {
			// list every enabled transition
			System.out.print(t + " ");
			System.out.println();
		}
	}

	// displays the contents of net in console and in the user interface
	/*
	 * public void drop() {
	 * 
	 * System.out.println("places:" + places.size()); for (Place p :
	 * this.places.values()) { System.out.println(p.getName() + ":" + p.contents); }
	 * 
	 * System.out.println("transitions:" + transitions.size()); for (Transition t :
	 * this.transitions.values()) { System.out.print(t.name); if (t.getClock() !=
	 * null) { System.out.print(":" + t.getClock()); } if (this.isEnabled(t)) {
	 * System.out.print("<enabled>"); } else { System.out.print("<not-enabled>"); }
	 * 
	 * if (t.isBlocked(this.currentMarking)) { System.out.println("<blocked>"); }
	 * else { System.out.print("<not-blocked>"); } if (isFirable(t)) {
	 * System.out.println("<firable>"); } else { System.out.print("<not-firable>");
	 * }
	 * 
	 * System.out.print("<pre="); for (Place p : t.pre) {
	 * System.out.print(p.getName() + " "); } System.out.print(">");
	 * 
	 * System.out.print("<post="); for (Place p : t.post) {
	 * System.out.print(p.getName() + " "); } System.out.println(">"); }
	 * System.out.println("Marking"); this.currentMarking.drop();
	 * 
	 * System.out.println("Clocks");
	 * 
	 * for (Entry<Integer, Float> e : Config.entrySet()) {
	 * System.out.println(e.getKey() + "->" + e.getValue()); } }
	 */

	public HashMap<Integer, Transition> getFirable() {
		return firable;
	}

	public HashMap<Integer, Transition> getEnabled() {
		return enabled;
	}

	public void setFirable(HashMap<Integer, Transition> firable) {
		this.firable = firable;
	}

	public int size() {
		return transitions.size();
	}

	/**
	 * A method to display the contents of places in a String
	 * 
	 * @return a string with the contents of all places
	 */
	public String dropConfig() {

		String s = new String("===========\n Net Config :\n ");
		s = s + ("Places:\n");
		for (Place p : places.values()) {
			s = s + p + "\n";
		}

		s = s + ("Control Places:\n");
		for (Place p : controlPlaces.values()) {
			s = s + p + "\n";
		}

		s = s + ("Transitions:\n");
		Set<Integer> ks = Config.keySet();

		for (Transition t : this.enabled.values()) {
			int ti = t.getNumber();
			Float val = this.Config.get(ti);
			s = s + "t" + ti + "->" + t.getClock() + "\n";
		}

		return s;
	}

	public boolean isGaussian() {
		return gaussian;
	}

	public boolean isWeibull() {
		return weibull;
	}

	public int getWeibullCoef() {
		return weibullCoef;
	}

	public void reset(boolean init) {// init set to true to reset at the initial state
		super.reset(init);
		enabled = new HashMap<>();
		blocked = new HashMap<>();
		firable = new HashMap<>();

		if (init)
			currentMarking = initMarking.copy();

		for (Place p : places.values()) {
			p.setContents(null);
			if (currentMarking.contains(p))
				p.setContents(0f);
		}
		for (Place p : controlPlaces.values()) {
			p.setContents(null);
			if (currentMarking.contains(p))
				p.setContents(0f);
		}

		Config = new HashMap<>();
		// Establishes relation between transition numbers
		// and the value of attached clock
		if (init) {
			for (Transition t : this.transitions.values()) {
				if (t.presetMarked(currentMarking)) {
					t.sample(s);
					enabled.put(t.number, t);
					this.Config.put(t.getNumber(), t.getClock());
				}
			}
		}

		n = 1;

		// loadFile();
	}

	public int numberFireable() {
		return getFirable().size();
	}

	public int numberBlocked() {
		return getBlocked().size();
	}

	public boolean isBlocked(int tnum) {
		return this.getBlocked().containsKey(tnum);
	}

	public String getFname() {
		return fname;
	}

	public HashMap<Integer, Transition> getTransitions() {
		return transitions;
	}

	public HashMap<Integer, Place> getPlaces() {
		return places;
	}

	public HashMap<Integer, Transition> getBlocked() {
		return blocked;
	}

	public HashMap<Integer, Place> getControlPlaces() {
		return controlPlaces;
	}

	public Marking getCurrentMarking() {
		return currentMarking;
	}

	public void setCurrentMarking(Marking currentMarking) {
		this.currentMarking = currentMarking;
	}

	public void setConfig(HashMap<Integer, Float> config) {
		Config = config;
	}

	public void setEnabled(HashMap<Integer, Transition> list) {
		enabled = list;
	}

	public void setFireable(HashMap<Integer, Transition> list) {
		firable = list;
	}

	public void setBlocked(HashMap<Integer, Transition> list) {
		blocked = list;
	}

	public float minimumClock() {
		float minNetElapse = -1;// A dummy value for start

		// get the Petri net of the regulated net

		for (Transition t : this.getEnabled().values()) { // consider every firable transition

			Float tf = t.getClock();
			// System.out.println(tf);
			if (tf > 0) {
				if (minNetElapse < 0) {
					minNetElapse = tf;
				} else {
					minNetElapse = Math.min(minNetElapse, tf);
				}
			}
			// From here we have the delay allowed by the net.
		}
		return minNetElapse;
	}

	@Override
	public ArrayList<TransitionAbstract> fireableTransition() {
		HashMap<Integer, Transition> tlistfirable = this.getFirable();
		ArrayList<TransitionAbstract> firecontr = new ArrayList<>();
		for (Transition t : tlistfirable.values()) {
			if (t.controlAllowsFiring()) {
				firecontr.add(t);
			}
		}
		return firecontr;
	}

	@Override
	public void drop() {
		// TODO Auto-generated method stub

	}
	@Override 
	public LinkedHashMap<Integer, Token> getTokens(){
		return new LinkedHashMap<Integer, Token>();
	}
}
