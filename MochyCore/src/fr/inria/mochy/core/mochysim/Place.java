package fr.inria.mochy.core.mochysim;
import java.util.ArrayList;

import fr.inria.mochy.core.abstractClass.PlaceAbstract;


/**
 * A place is set with its name, number and its content : marked or not
 * <p>
 * <img src="Place.svg" alt="Place class diagram" style="float: right;">
 */
public class Place extends PlaceAbstract{

	String name;
	Integer number;
	ArrayList<Transition> pre;
	ArrayList<Transition> post;
	
	Float  contents; // marking m(this)
	
	Place(int nb, String n){
		pre=new ArrayList<>();
		post=new ArrayList<>();
		name = n;
 		number = nb;	
	}
	
	void addPre(Transition t){					
			pre.add(t);
		}

	void addPost(Transition t){		
		post.add(t);
	}

	ArrayList<Transition> getPre(){
		return pre;
	}
		
	ArrayList<Transition> getPost(){
		return post;
	}
		
		
	public void resetPlace(){
		contents=null;
	}		
		
	void setContents(Float f){
		contents = f;
	}
		
	public Float getContents(){
		return this.contents; 
	}
		
	boolean isMarked(){
		return contents != null;
	}

	public String getName() {
		return name;
	}
		
	public Integer getNumber() {
		return number;
	}

	public void addToken() {
		if ( contents != null) {
		contents = contents+1;
		} else {contents=new Float(1);}
	}
	
	
	public String toString() {
		String s=new String(name+":"+number+":"+contents);
		return s;
		
	}
}
