package fr.inria.mochy.core.mochysim;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.KeyStore.Entry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.dom.Dom;
import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.equalization.EquPlace;
import fr.inria.mochy.core.equalization.EquTransition;
import fr.inria.mochy.core.trajectory.BooleanPlace;
import fr.inria.mochy.core.trajectory.TrajPlace;
import fr.inria.mochy.core.trajectory.TrajTransition;
import fr.inria.mochy.core.trajectory.TrajectoryNet;
import javafx.util.Pair;

/**
 * The Sim class allow to control the Net model
 * <p>
 * <img src="Sim.svg" alt="Sim class diagram" style="float: right;">
 */
public class Sim {

	int nbsteps;
	private PhysicalModel n;
	ArrayList<Integer> siminfo; // information on the simulation
	// currently : number of blocked transitions at each step
	long simtot; // total of blocked transitions during the simulation

	long elapsedTime;

	String pathLogs;
	boolean enableLogs;
	String classModel;
	String fname = null;
	// the methods to be called from logs/in.txt to logs/out.txt and the arguments
	// as String (list.number) :
	ArrayList<Pair<Method, String>> methods = new ArrayList<>();
	String pathRoot = "";// the path of the in and out files, the logs folder

	public Sim(int nb, String fname, String pathLogs, boolean enableLogs, String classModel)
			throws FileNotFoundException, ClassNotFoundException, NoSuchMethodException {
		this.enableLogs = enableLogs;
		this.fname = fname;
		nbsteps = nb;
		this.classModel = classModel;
		if (classModel.equals("Net"))
			setN(new Net(fname));
		else if (classModel.equals("TrajectoryNet"))
			setN(new TrajectoryNet(fname));
		else if (classModel.startsWith("EqualizationNet")) {
			Class<? extends EquNet> cls = null;
			EquNet n = null;
			String version = classModel.split("\\.")[1];
			if (enableLogs)
				System.out.println("version " + version);
			Class[] cArg = new Class[1]; // Our constructor has 1 argument
			cArg[0] = String.class;
			try {
				cls = (Class<? extends EquNet>) Class.forName("fr.inria.mochy.core.equalization." + version);
			} catch (ClassNotFoundException e) {
				System.out.println("class not found");
				e.printStackTrace();
			}
			try {
				n = cls.getConstructor(cArg).newInstance(fname);
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				if (e.getCause() != null && e.getCause().getCause() != null
						&& e.getCause().getCause() instanceof FileNotFoundException) {
					throw (FileNotFoundException) e.getCause().getCause();
				}
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			setN(n);

		}
		// if enableLogs is true, read the methods from which we want to retrieve a
		// value from
		// logs/in.txt
		if (enableLogs) {
			pathRoot = (new File(pathLogs).getParent());
			System.out.println("path root : " + pathRoot);
			File inFile = new File(pathRoot + "/in.txt");
			System.out.println("inFile : " + inFile.exists());
			if (inFile.exists()) {
				BufferedReader br = new BufferedReader(new FileReader(pathRoot + "/in.txt"));
				String className = "", methodName = "", arg = "";
				try {
					String line = br.readLine();
					while (line != null) {
						String[] segments = line.split(";");
						for (String segment : segments) {
							// segment of type v1=class.method=listName.argName : listName.argName is
							// optional
							// argName is the index of the object to be retrieved from the list listName
							String[] seg = segment.split("=");
							// get the method
							String[] path = seg[1].split("\\.");
							className = "";
							for (int i = 0; i < path.length - 2; i++)
								className += path[i] + ".";
							className += path[path.length - 2];
							methodName = path[path.length - 1];
							Class clas = Class.forName(className);
							// get the args
							if (seg.length == 2) {// a method
								Method method = clas.getMethod(methodName);
								methods.add(new Pair<Method, String>(method, ""));
							} else if (seg.length == 3) {// a method and an argument
								for (Method method : clas.getMethods()) {
									if (method.getName().equals(methodName)) {
										Method methodChosen = clas.getMethod(methodName, method.getParameterTypes()[0]);
										methods.add(new Pair<Method, String>(methodChosen, seg[2]));
									}
								}
							}
						}
						line = br.readLine();
					}
				} catch (IOException e) {
					System.err.println("error while reading in.txt file");
					e.printStackTrace();
				} /*
					 * catch (ClassNotFoundException e) { System.err.println("class " + className +
					 * " not found"); e.printStackTrace(); } catch (NoSuchMethodException e) {
					 * System.err.println("method " + methodName + " not found");
					 * e.printStackTrace(); }
					 */catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			try (FileWriter outputWriter = new FileWriter(pathRoot + "/out.txt")) {
				outputWriter.write("");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		simtot = 0;
		siminfo = new ArrayList<Integer>();
		this.pathLogs = pathLogs;
		this.enableLogs = enableLogs;
	}

	// perform a move of the TPN
	public ArrayList<Dom> move() {
		if (!n.startLogs && enableLogs) {// write in out.txt for the initial status
			writeOutFile();
			n.startLogs = true;
		}
		// System.out.println("***********************");
		// check if a timed move is allowed
		Float delta = this.getN().maxAllowedTimedMove();
		// System.out.println("Max-allowed timed move =" + delta);

		if (delta.compareTo(new Float(0.0)) > 0) { // if a timed move is allowed
			// System.out.println("Timed Move : " + delta);
			this.getN().progressTime(delta);
			this.elapsedTime += delta;
		} else if (this.getN().numberFireable() != 0) { // timed moves and discrete moves are exclusive
			this.getN().discreteMove(this.pathLogs, this.elapsedTime, enableLogs);
			// System.out.println("discrete move");
		} else {
			this.getN().progressTime(new Float(0.0));
			// System.out.println("progress time 0");
		}

		if (this.enableLogs) {
			writeOutFile();
		}
		return displayFeedback();
	}

	public ArrayList<Dom> oneStep() {
		// performs a move of the net
		// it can be a discrete step, or a timed move corresponding to the
		// maximal duration allowed by the net (smallest TTF)
		//getN().drop();
		ArrayList<Dom> feedback = this.move();
		//this.simtot += this.getN().numberBlocked();
		//this.siminfo.add(this.getN().numberBlocked());
		return feedback;
	}

	public ArrayList<Dom> main(int steps) {
		ArrayList<Dom> feedback = new ArrayList<>();
		this.nbsteps = steps;
		// read a net from a file
		getN().drop();

		long beg = System.currentTimeMillis();
		for (int i = 1; i <= steps; i++) {

			feedback = this.move();
			// s.n.drop();

			this.simtot += this.getN().numberBlocked();
			this.siminfo.add(this.getN().numberBlocked());
		}
		long end = System.currentTimeMillis();
		if (enableLogs) {
			System.out.println((beg - end) / 1000 + "secondes");
			System.out.println(this.simtot + "blocked transitions ");
			System.out.println("average :" + this.simtot / 10000);

			System.out.println("Symbolic time elapsed " + this.elapsedTime);
		}

		return feedback;
	}

	/** fire a specific transition after the user clicked twice on it */
	public ArrayList<Dom> fireTransition(int tnum) {
		this.simtot += this.getN().numberBlocked();
		this.siminfo.add(this.getN().numberBlocked());
		PhysicalModel net = this.getN();
		if (net.getFirable().containsKey(tnum)) {
			TransitionAbstract t = net.findTransition(tnum);
			net.discreteMove(t, this.pathLogs, elapsedTime, true);
		}
		return displayFeedback();
	}

	/**
	 * @return the state of the net make a discreteMove if there are some firable
	 *         transitions
	 */
	public ArrayList<Dom> discreteStep() {
		if (this.getN().numberFireable() != 0) {
			getN().discreteMove(this.pathLogs, this.elapsedTime, this.enableLogs);
		}
		return displayFeedback();
	}

	/**
	 * @return the list of transition and places to be displayed in the user
	 *         interface
	 */
	public ArrayList<Dom> displayFeedback() {

		// System.out.println("Entering displayFeedback");
		ArrayList<Dom> feedback = new ArrayList<>();
		if (this.classModel.equals("Net")) {
			Net n = (Net) getN();
			for (Transition t : n.transitions.values()) {
				String pre = "", post = "";
				String c;// the clock value of the transition
				for (Place p : t.pre) {
					if (p != null) {
						if (pre.equals(""))
							pre = p.name;
						else
							pre += "," + p.name;
					}
				}

				for (Place p : t.controlPre) {
					if (p != null) {
						if (pre.equals(""))
							pre = p.name;
						else
							pre += "," + p.name;
					}
				}

				// add places
				for (Place p : t.post) {
					if (p != null) {
						if (post.equals(""))
							post = p.name;
						else
							post += "," + p.name;
					}
				}

				if (t.getClock() == null)
					c = "null";
				else
					c = t.getClock().toString();
				Dom dom = null;
				if (n.enabled.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "enabled", "t", t.lowerBound, t.upperBound, c);
				if (n.getFirable().containsKey(t.number))
					dom = new Dom(t.name, pre, post, "fireable", "t", t.lowerBound, t.upperBound, c);
				if (n.blocked.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "blocked", "t", t.lowerBound, t.upperBound, c);
				if (!n.enabled.containsKey(t.number) && !n.getFirable().containsKey(t.number)
						&& !n.blocked.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "", "t", t.lowerBound, t.upperBound, c);
				if (dom != null) {
					dom.setNumber(String.valueOf(t.getNumber()));
					if (t.isGaussian())
						dom.setSampler("Gaussian");
					else if (t.isWeibull())
						dom.setSampler("Weibull:" + t.getCoefWeibull());
				}
				if (t.controlAllowsFiring())
					dom.setControl(true);
				feedback.add(dom);
			}

			// add places
			for (Place p : n.places.values()) {
				Dom pDom;
				if (p.contents != null)
					pDom = new Dom(p.name, "", "", "marked", "p", 0f, 0f, null);
				else
					pDom = new Dom(p.name, "", "", "null", "p", 0f, 0f, null);
				pDom.setNumber(String.valueOf(p.number));
				pDom.setControl(false);
				feedback.add(pDom);
			}

			// add Control Places

			for (Place p : n.controlPlaces.values()) {
				Dom pDom;
				if (p.contents != null)
					pDom = new Dom(p.name, "", "", "marked", "p", 0f, 0f, null);
				else
					pDom = new Dom(p.name, "", "", "null", "p", 0f, 0f, null);
				pDom.setControl(true);
				pDom.setNumber(String.valueOf(p.number));
				feedback.add(pDom);
			}
		} else if (this.classModel.equals("TrajectoryNet")) {
			TrajectoryNet n = (TrajectoryNet) getN();
			for (TrajTransition t : n.transitions.values()) {
				String pre = "", post = "";
				String c;// the clock value of the transition

				TrajPlace p = t.pre;
				if (p != null)
					pre = p.name;

				ArrayList<BooleanPlace> list = t.getControlPre();
				for (BooleanPlace cp : list) {
					pre += ", " + cp.getName();
				}

				// add places
				p = t.post;
				if (p != null)
					post = p.name;

				if (t.clock == null)
					c = "null";
				else
					c = t.clock.toString();

				Dom dom = null;
				if (n.enabled.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "enabled", "t", t.lowerBound, t.upperBound, c);
				if (n.fireable.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "fireable", "t", t.lowerBound, t.upperBound, c);
				if (n.blocked.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "blocked", "t", t.lowerBound, t.upperBound, c);
				if (!n.enabled.containsKey(t.number) && !n.fireable.containsKey(t.number)
						&& !n.blocked.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "", "t", t.lowerBound, t.upperBound, c);
				if (dom != null) {
					dom.setNumber(String.valueOf(t.getNumber()));
				}
				if (t.controlAllowsFiring())
					dom.setControl(true);
				feedback.add(dom);

				// controlPlaces
				if (t.getControlPre() != null) {
					Dom pDom;
					list = t.getControlPre();
					for (BooleanPlace bp : list) {
						if (bp.isValue())
							pDom = new Dom(bp.getName(), "", "", "true", "p", 0f, 0f, null);
						else
							pDom = new Dom(bp.getName(), "", "", "false", "p", 0f, 0f, null);
						pDom.setControl(true);
						pDom.setNumber(String.valueOf(bp.getNumber()));
						feedback.add(pDom);
					}
				}
			}

			// add places
			for (TrajPlace p : n.places.values()) {
				Dom pDom;
				if (p.trajectories.size() != 0)
					pDom = new Dom(p.name, "", "", Integer.toString(p.trajectories.size()), "p", 0f, 0f, null);
				else
					pDom = new Dom(p.name, "", "", "null", "p", 0f, 0f, null);
				pDom.setNumber(String.valueOf(p.number));
				pDom.setTrajectories(p.trajectories);
				feedback.add(pDom);
			}
		} else if (this.classModel.startsWith("EqualizationNet")) {

			// System.out.println("Equalization net found");

			EquNet n = (EquNet) this.n;
			// n.drop();// display the read net after parsing (uncomment if needed)

			// System.out.println("Feedback for Transitions");
			for (EquTransition t : n.transitions.values()) {
				String pre = "", post = "";
				String c;// the clock value of the transition

				if (t.getPre().size() > 0) {
					EquPlace p = t.getPre().get(0);
					if (p != null)
						pre = p.getName();
				}

				// add places.
				for (EquPlace p : t.getPost()) {
					if (p != null) {
						if (post.equals(""))
							post = p.getName();
						else
							post += "," + p.getName();
					}
				}

				if (t.getClock() == null)
					c = "null";
				else
					c = t.getClock().toString();

				// uncomment to show transition's clock and speed
				// System.out.println("T " + t.name + "[" + t.lowerBound+ ","+ t.upperBound+"]
				// c="+ c);

				Dom dom = null;
				if (n.enabled.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "enabled", "t", t.lowerBound, t.upperBound, c);
				if (n.fireable.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "fireable", "t", t.lowerBound, t.upperBound, c);
				if (n.blocked.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "blocked", "t", t.lowerBound, t.upperBound, c);
				if (!n.enabled.containsKey(t.number) && !n.fireable.containsKey(t.number)
						&& !n.blocked.containsKey(t.number))
					dom = new Dom(t.name, pre, post, "", "t", t.lowerBound, t.upperBound, c);
				if (dom != null) {
					dom.setNumber(String.valueOf(t.getNumber()));
				}
				/*
				 * if (t.controlAllowsFiring()) dom.setControl(true);
				 */
				if (t.isGaussian())
					dom.setSampler("Gaussian");
				else if (t.isWeibull())
					dom.setSampler("Weibull:" + t.getWeibullCoef());
				dom.setControl(true);
				feedback.add(dom);

				// controlPlaces
				/*
				 * if (t.getControlPre() != null) { Dom pDom; list = t.getControlPre(); for
				 * (BooleanPlace bp : list) { if (bp.isValue()) pDom = new Dom(bp.getName(), "",
				 * "", "true", "p", 0f, 0f, null); else pDom = new Dom(bp.getName(), "", "",
				 * "false", "p", 0f, 0f, null); pDom.setControl(true);
				 * pDom.setNumber(String.valueOf(bp.getNumber())); feedback.add(pDom); } }
				 */
			}

			// add places
			for (EquPlace p : n.places.values()) {
				Dom pDom;
				if (p.getTokens().size() != 0)
					pDom = new Dom(p.getName(), "", "", Integer.toString(p.getTokens().size()), "p", 0f, 0f, null);
				else
					pDom = new Dom(p.getName(), "", "", "null", "p", 0f, 0f, null);
				pDom.setNumber(String.valueOf(p.getNumber()));
				pDom.setTokens(p.getTokens());
				feedback.add(pDom);

			}
		}

		// System.out.println("End of DisplayFeedback. Feedback size = "+feedback
		// .size());

		return feedback;
	}

	void writeOutFile() {
		try (FileWriter outputWriter = new FileWriter((new File(pathLogs)).getParent() + "/out.txt", true)) {
			outputWriter.append(n.getTimeElapsed() + ";");
			// Object obj = method.invoke(n); instanceof Float, Integer...
			for (Pair<Method, String> method : methods) {
				try {
					if (method.getValue().equals(""))
						outputWriter.append(method.getKey().invoke(n).toString());
					else {
						// String list = method.getValue().split("\\.")[0];
						String argIndex = method.getValue().split("\\.")[1];
						String value = "";
						if (method.getKey().getParameterTypes()[0] == Integer.class) {
							value = method.getKey().invoke(n, Integer.parseInt(argIndex)).toString();
						}
						else if (method.getKey().getParameterTypes()[0] == String.class)
							value = method.getKey().invoke(n, argIndex).toString();
						outputWriter.append(value);

					}
				} catch (IllegalArgumentException e) {
					System.err.println("wrong argument Net object or args");
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					System.err.println("can't invoke " + method.getKey().getName());
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				outputWriter.append(";");
			}
			outputWriter.append("\n");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public PhysicalModel getN() {
		return n;
	}

	void setN(PhysicalModel n) {
		this.n = n;
	}

	public void reset() {
		// setN(new Net(getN().fname));
		// getN().loadFile();
		this.n.reset(true);
		simtot = 0;
		siminfo = new ArrayList<Integer>();
	}

	public String getPathLogs() {
		return pathLogs;
	}

	public String getFname() {
		return fname;
	}

	public void setEnableLogs(boolean enableLogs) {
		this.enableLogs = enableLogs;
	}

	public boolean isEnableLogs() {
		return enableLogs;
	}

	public String getPathRoot() {
		return pathRoot;
	}

	public void setMethods(ArrayList<Pair<Method, String>> methods) {
		this.methods = methods;
	}
}
