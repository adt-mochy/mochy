package fr.inria.mochy.core.mochysim;

import java.util.ArrayList;

import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.sampler.Sampler;
import fr.inria.mochy.core.timetable.TableEvent;

/**
 * A transition is set with its name, its content, its previous/next places, its clock and its state none/enabled/firable.
 * <p>
 * <img src="Transition.svg" alt="Transition class diagram" style="float: right;">
 */
public class Transition extends TransitionAbstract{

	String name; // the name of the transition
	public Integer number; // the number of the transition
	ArrayList<Place> pre;// predecessor places
	ArrayList<Place> controlPre;// predecessor control places
                                // Control places do not impact the elapsing of time, and are only used
								// to prevent or allow effective firing of a transition
	
	ArrayList<Place> post; // successor places

	Float lowerBound; // lower bound of the time interval associated with the transition
	Float upperBound; // upper bound of the time interval associated with the transition
	String min;
	String max;
	String distribution;

	boolean gaussian = false;
	boolean weibull = false;
	int coefWeibull = 5;

	//private Float clock; // the time remaining before firing of the transition
	
	ArrayList<TableEvent> linkedEvents = new ArrayList<>(); //the linked events to this transition

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// %% CONSTRUCTORS %
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	// Constructor
	// nb : number of transition
	// n:name of the transition
	Transition(int nb, String n) {
		pre = new ArrayList<>();
		post = new ArrayList<>();
		name = n;
		number = nb;
	}

	// Constructor
	// nb : number of transition
	// n:name of the transition
	// min: the lowerBound of the time interval
	// max: the uppedBound of the time interval
	Transition(int nb, String n, Float min, Float max) {
		pre = new ArrayList<>();
		controlPre = new ArrayList<>();
		post = new ArrayList<>();
		name = n;
		number = nb;
		lowerBound = min;
		upperBound = max;
		this.min = String.valueOf(min);
		this.max = String.valueOf(max);
	}

	// constructor with list of predecessors & successors
	Transition(int nb, String n, ArrayList<Place> pr, ArrayList<Place> pst) {

		pre = new ArrayList<Place>();
		controlPre = new ArrayList<>();
		post = new ArrayList<Place>();

		name = new String(n);
		number = nb;

		for (Place p : pr) {
			pre.add(p);
		}

		for (Place p : pst) {
			post.add(p);
		}

	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// %%% Construction functions %%
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	// adds a place to the preset of the transition
	/**
	 * @param p : the place that has to be added to the preset
	 */
	void addPre(Place p) {
		pre.add(p);
	}

	void addControlPre(Place p) {
		controlPre.add(p);
	}
	
	
	// adds a place to the preset of the transition
	/**
	 * @param p : the place to be added to the postset
	 */
	void addPost(Place p) {
		post.add(p);
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// % Useful Fucntions %
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	// returns true if for the current transition
	// all its input places are marked
	boolean presetMarked(Marking m) {
		// check that all places in the preset are filled
		for (Place p : this.pre) {

			if (m.contains(p) == false) {
				return false;
			}
		}
		return true;
	}

	// returns true if a transitions has a non-empty postset
	public boolean busyPostSet() {

		for (Place p : this.post) {
			// if one can find a place in postset that
			// contains something then t is blocked
			if (p.contents != null) {
				return true;
			}
		}

		return false;
	}

	// returns true if an enabled transition is blocked,
	// i.e. its clock is zero but one place in its postset is
	// not empty
	public boolean isBlocked(Marking m) {

		// System.out.println("Transition "+ this.name + "blocked ?");
		if (this.getClock() != null) {
			// System.out.println("Clock "+ this.clock);

			if (this.getClock().equals(new Float(0.0))) {
				// (check that all places in the preset are filled)
				// check that places in the postset are all empty
				for (Place p : this.post) {
					// System.out.println("Place "+ p.name + p.contents);
					if (p.contents != null) {
						return true;
					}
				}
				return false; // one didnt find an occupied place
			} else {
				return false; // clock is not zero transition not blocked
			}
		}
		return false;
	}

	// check that an enabled transition is not blocked
	// and has a zero clock
	// assumption : enabled transitions only
	public boolean isFireable() {

		if (this.getClock() == null) {
			return false;
		}
		if (this.getClock().compareTo(new Float(0.0)) > 0) {
			return false;
		}

		return true;
	}

	
	/**
	 * Checks if the control places used as input to the current transition allow firing of t
	 * @param t
	 * @return
	 */
	public boolean controlAllowsFiring() {
		for (Place p : this.controlPre) {
			if (!p.isMarked()) {
				return false;
			}
		}
		return true;
	}
	
	
	
	
	
	// says if a transition was newly enabled by a firing
	boolean isNewlyEnabled(Marking m1, Marking m2, Transition t) {
		// m1 is the temporary marking
		// m2 is the reached marking
		if (presetMarked(m2)) {
			if (!presetMarked(m1)) {
				return true;
			}
			if (this == t) {
				return true;
			}
		}
		return false;
	}

	// samples a value for current transition
	public void sample(Sampler s) {
		if (!isGaussian() && !isWeibull()) {
			// perform a random choice between upper and lower bound
			// this.clock=new Float(10);
			// Sampler s = new Sampler();
			if (this.lowerBound == null && this.upperBound == null)
				this.setClock((float) s.discreteUniform(0f, 1000f));
			else if (this.upperBound == null)
				this.setClock((float) s.discreteUniform(this.lowerBound, 1000f));
			else {
				this.setClock(s.discreteUniform(this.lowerBound, this.upperBound));
			}
		} else if (isGaussian()) {//if Gaussian has been set in the net file
			if (this.lowerBound == null && this.upperBound == null)
				this.setClock((float) s.gaussSampler(500, 500));
			else if (this.upperBound == null)
				this.setClock((float) s.gaussSampler((1000+this.lowerBound)/2, (double) 1000-(((double) 1000+this.lowerBound)/2)));
			else 
				this.setClock((float) s.gaussSampler((this.upperBound+this.lowerBound)/2, (double) this.upperBound-((double)(this.upperBound+this.lowerBound)/2)));
			if (this.getClock() < 0)
				this.setClock(0f);
		} else if (isWeibull()) {
			if (this.lowerBound == null && this.upperBound == null)
				this.setClock((float) s.invertTransformWeibull(coefWeibull, 500));
			else if (this.upperBound == null)
				this.setClock((float) s.invertTransformWeibull(coefWeibull, (1000+this.lowerBound)/2));
			else
				this.setClock((float) s.invertTransformWeibull(coefWeibull, (this.lowerBound + this.upperBound)/2));
			if (this.getClock() <0)
				this.setClock(0f);

		}
	}

	public String toString() {
		String s = "";
		return (s + this.number + this.name);
	}

	public String getName() {
		return this.name;
	}

	public String getDistribution() {
		return this.distribution;
	}
	
	
	/**
	 * returns the list of control places used as input of the current transition
	 * @return the list of in places that are also control places
	 */
	public ArrayList<Place> getInControlPlaces(){
		
		ArrayList<Place> incp = new ArrayList<Place>();
		
		
		return incp;
		
	}

	/**
	 * 
	 * @return the indentifier of the transition
	 */
	public int getNumber() {
		return this.number;
	}


	public String getMin() {
		return min;
	}


	public String getMax() {
		return max;
	}

	
	
	public boolean isGaussian() {
		return gaussian;
	}

	public void setGaussian(boolean gaussian) {
		this.gaussian = gaussian;
	}

	public boolean isWeibull() {
		return weibull;
	}

	public void setWeibull(boolean weibull) {
		this.weibull = weibull;
	}

	public void setCoefWeibull(int coefWeibull) {
		this.coefWeibull = coefWeibull;
	}

	public int getCoefWeibull() {
		return coefWeibull;
	}

	public ArrayList<TableEvent> getLinkedEvents() {
		return linkedEvents;
	}

	public void addLinkedEvent(TableEvent te) {
		this.linkedEvents.add(te);
	}
		
	public ArrayList<Place> getPre() {
		return pre;
	}

	public ArrayList<Place> getPost() {
		return post;
	}

	public ArrayList<Place> getControlPre() {
		return controlPre;
	}

	public Float getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(Float lowerBound) {
		this.lowerBound = lowerBound;
	}

	public Float getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(Float upperBound) {
		this.upperBound = upperBound;
	}
	
}
