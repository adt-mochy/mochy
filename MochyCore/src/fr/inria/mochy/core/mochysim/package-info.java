/**
 * Perform the simulation of a net model with transitions and places objects.
 * <p>
 *
 * <img src="package.svg" alt="Package class diagram" style="float: right;">
 */
package fr.inria.mochy.core.mochysim;
