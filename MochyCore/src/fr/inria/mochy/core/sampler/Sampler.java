package fr.inria.mochy.core.sampler;

import java.util.Random;

import org.apache.commons.math3.distribution.NormalDistribution;

/**
 * Used to generate random values for different distributions
 * <p>
 * <img src="Sampler.svg" alt="Sampler class diagram" style="float: right;">
 */
public class Sampler {
	// class with main sampling functions
	// uniform sampling with discrete set of values
	// uniform sampling of continuous function
	// Inverse transform sampling
	
	
	Random r; // random number generator

	public Sampler(){
		long timeNow = System.currentTimeMillis();
		r=new Random(timeNow);
	
	}
	
	/**
	 * Sample discrete values within a given interval
	 * 
	 * @param deb beginning of interval
	 * @param end end of interval
	 * @return
	 */
	public float discreteUniform(Float deb, Float end) {

		double d = r.nextDouble();
		// sample a value between 0 and 1
		double dv= deb + (end-deb)*d;
		//return (int)Math.round(dv);
		//return Math.round(dv*10)/10;
		return (float) dv;
	}
	
	/**---(1-ln(x))²*lambda
	 * @return an invertTransform value
	 * @param lambda the parameter to increase the result
	 */
	public int invertTransform(int lambda) {
		double x = r.nextDouble();
		double y = Math.pow(1 - Math.log(x), 2) * lambda;
		return (int)Math.round (y);
	}
	

	/**return the weibull result of the x input with the specific parameters
	 * the coef must be > 1 to have a correct shape*/
	public double weibull(double x, int coef, double mean) {
		double a = coef / mean;
		double b = Math.pow(x/mean, (double) coef-1);
		double c = Math.exp(-Math.pow(x/mean, coef));
		return a*b*c;
	}
	
	/**
	 * invert repartition function of Weibull = lambda*(-log(1-x))^(1/k)
	 * repartition function of Weibull = 1-exp(-(x/lambda)^k)
	 * @param k the shape of the function and to increase of the result
	 * @param lambda the coefficient to increase the result
	 * @return the value
	 */
	public float invertTransformWeibull(double k, float mean) {
		double y = r.nextDouble();
		double x = mean*(Math.pow((-Math.log(1-y)),1/k));
		//return (int)Math.round (x);
		return (float)x;
	}
	
	/**return a gaussian value dependending of x, the mean, and the deviation*/
	public double gaussian(double x, float mean, double deviation) {
		deviation = deviation/3;
		if (deviation < 0.5)
			deviation = 0.5;
		NormalDistribution gaussian = new NormalDistribution(mean, deviation);
		return gaussian.density(x);
	}
	
	/**
	 * @param deviation : the scale
	 * @param average : the lowerbound value
	 * @return a random value following the gaussian law
	 */
	public float gaussSampler(float average, double deviation){
		deviation = deviation/3;
		if (deviation < 0.5)
			deviation = 0.5;
		NormalDistribution gaussian = new NormalDistribution(average, deviation);
		//return (int) gaussian.sample();
		return (float) gaussian.sample();
	}
}
