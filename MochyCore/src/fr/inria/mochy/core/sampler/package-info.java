/**
 * Samples random values for different distributions
 * <p>
 *
 * <img src="package.svg" alt="Package class diagram" style="float: right;">
 */
package fr.inria.mochy.core.sampler;
