package fr.inria.mochy.core.timetable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class to verify well formedness of timetables
 * <p>
 * <img src="TTChecker.svg" alt="TTChecker class diagram" style="float: right;">
 */
public class TTChecker {

	TimeTable table;
	public ArrayList<TableDependency> WrongDependencies; // List of dependencies with constraint violation
	public int OriginOfCycle;

	public TTChecker(TimeTable tbl) {

		table = tbl;
		WrongDependencies = new ArrayList<TableDependency>(); // an a priori empty list of wrong dependencies
		OriginOfCycle = -1;
		
	}

	
	
	/**
	 * 
	 * @return a boolean set to true if the table contains no cycle, and respect all
	 *         dependencies
	 */
	public Boolean isSound() {

		Boolean nocycle = this.hasNoCycle();

		Boolean constOk = this.verifiedConstraints();

		return (nocycle && constOk);
	}

	/**
	 * 
	 * @return a boolean set to true if the dependencies in the TT do not form a
	 *         cycle
	 */
	public Boolean hasNoCycle() {

		System.out.println("Checking cycles");

		for (TableDependency d : table.getDependencies()) {
			//System.out.println("Dependency " + d.getStartEvent() + " -> " + d.getEndEvent());

			// get the list of dependencies next to d,
			// i.e. if d=(s,v,e) all dependencies of the form (e,v',z)
			ArrayList<TableDependency> list = this.move(d);

			// follow is set to true if it remains some dependencies to check
			boolean follow = !list.isEmpty();

			// Choose the next dependency to check
			TableDependency temp = null;
			if (follow) {
				temp = list.get(0);
				list.remove(0);
			}

			ArrayList<TableDependency> alreadyChecked = new ArrayList<>();
			// Lists of dependencies that are reachable from d and already visited

			// as soon as there is no cycle and some dependencies follow
			// the temp dependency
			while (!d.equals(temp) && follow) {

				// find the successor dependencies of dependency temp
				ArrayList<TableDependency> listTemp = move(temp);

				// add all dependencies that have not yet been visited
				list = mergeLists(list, listTemp, alreadyChecked);

				// Consider temp as visited
				alreadyChecked.add(temp);
				if (list.isEmpty())
					follow = false;
				else {
					temp = list.get(0);
					list.remove(0);
				}
			}
			if (d == temp) {// if there is a cycle
				this.OriginOfCycle = d.getStartEvent();
				System.out.println("The TimeTable has a cycle starting from" + OriginOfCycle);
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @return a boolean set to true if all constraints are satisfied in the TT,
	 *         i.e. for every dependency (org, dest,val) date(dest)> date(org)+val
	 */
	public Boolean verifiedConstraints() {
		for (TableDependency d : table.getDependencies()) {
			if (table.getEvent(d.getEndEvent()) == null || table.getEvent(d.getStartEvent()) == null)
				WrongDependencies.add(d);
			else {
				float dateDest = table.getEvent(d.getEndEvent()).getDate();
				float dateOrg = table.getEvent(d.getStartEvent()).getDate();
				float val = d.getDuration();
				if (dateDest < (dateOrg + val)) {
					WrongDependencies.add(d);
				}
			}
		}
		if (WrongDependencies.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * MOVE
	 * 
	 * For a given dependency d=(e,v,e') computes the list of dependencies of the
	 * form (e',v',v'') i.e. which start from e'
	 */
	ArrayList<TableDependency> move(TableDependency d) {

		ArrayList<TableDependency> list = new ArrayList<>();

		// consider every dependency in the timetable
		for (TableDependency d2 : table.getDependencies()) {
			if (d.getEndEvent() == d2.getStartEvent()) // if the dependency has as origin the target of d
				list.add(d2);
		}
		return list;
	}

	/**
	 * merge the list with the temporary list and delete already checked
	 * dependencies
	 */
	ArrayList<TableDependency> mergeLists(ArrayList<TableDependency> list, ArrayList<TableDependency> listTemp,
			ArrayList<TableDependency> alreadyChecked) {
		for (TableDependency d : listTemp) {
			if (!list.contains(d))
				list.add(d);
		}
		for (TableDependency d : alreadyChecked) {
			if (list.contains(d))
				list.remove(d);
		}
		return list;
	}
}
