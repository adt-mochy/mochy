package fr.inria.mochy.core.timetable;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.mochysim.Net;

/**
 * TTConfig is use to control the timetable model
 * <p>
 * <img src="TTConfig.svg" alt="TTConfig class diagram" style="float: right;">
 */
public class TTConfig {

	float currentDate; // the current time during the execution
	TimeTable table; // the timetable currenty simulated

	ArrayList<TableEvent> minevents; // minimal events yet unexecuted

	String pathLogs; // the path to log the realized events
	boolean enableLogs; // enable the logs of realized events in the file
	int nbLine; // increased by one when an event is realized
	Logger logger = Logger.getLogger("logger"); // to log the potential errors in the consol

	/**
	 * Constructor. By default, if no starting time is specified, simulation starts
	 * at date 0
	 * 
	 * @param tbl
	 */
	public TTConfig(TimeTable tbl) {

		this.currentDate = 0;
		this.table = tbl;

	}

	/**
	 * Constructor with a timetable and a starting date
	 * 
	 * @param tbl
	 * @param startDate
	 */
	TTConfig(TimeTable tbl, int startDate) {

		this.currentDate = startDate;
		this.table = tbl;

	}
	
public void init() {
    	
    	minevents = table.minList(); // computes the set of minimal events, i.e. the first ones to be eexecuted
    	table.resetMarked(); // resets all MarkesPlaces tags
    	
    }
 

	public float maxAllowedTime(float currentDate) {

		// Look at execution time of all minimal events
		// the maximal allowed timed move is
		// the minimal time for an event that has to fill some place
		// (and has not yet done it)
		// all events that await for a transition can be delayed for an
		// arbirary long time
		float max = -1;
		for (TableEvent te : minevents) {// for every event
			if (!te.getPlaceNames().isEmpty() || te.PlacesMarked) {
				if (max < 0) {
					max = te.initialDate - currentDate;
				} else {
					max = Math.min(max, te.initialDate - currentDate);
				}

			}
		}
		return max;
	}

	public void init(String pathLogs, boolean enableLogs) {
		this.pathLogs = pathLogs;
		this.enableLogs = enableLogs;
		this.nbLine = 1;
		minevents = table.minList();
		table.resetMarked(); // resets all MarkesPlaces tags
	}

	// ************************************************************
	// * SEMANTICS
	// ************************************************************

	public void timeMove(float duration,PhysicalModel n) {
		// lets time elapse
		// Note : contrarily to nets semantics, time can elapse
		// beyond the date of planned events
		// In this case, events are late and the delays must be propagated to
		// the successors of the late event in the table

		currentDate = currentDate + duration;
		
		// for every minimal event which date is the current date, and which has not yet sent
		// tokens to the place it allows, add a token to places
		
		for (TableEvent te:minevents) {
			
			if (te.getDate() == currentDate && te.getPlacesMarked()) {
				
				for (int pname : te.getPlaceNames() ) {
					n.addToken(pname);
				}
			}
			
		}
		
	}

	
	public void timeMove(float duration) {
		// lets time elapse
		// Note : contrarily to nets semantics, time can elapse
		// beyond the date of planned events
		// In this case, events are late and the delays must be propagated to
		// the successors of the late event in the table

		currentDate = currentDate + duration;
				
	}

	
	
	/**
	 * Executes an event at current date if it is a minimal event i.e., an event
	 * that has no unexecuted predecessor
	 * 
	 * @param nb the identifier of the event to execute
	 */
	public boolean discreteMove(int nb) {
		// return true if the move is realized, else return false
		boolean realized = false;
		//System.out.println("***********\n discreteMoveTTPN\n*************");

		// check if the event is minimal
		// if it is not, some event must be executed before
		Boolean isMin = false;
		TableEvent foundev = new TableEvent();
		for (TableEvent tev : minevents) {
			if (tev.number == nb) {
				isMin = true;
				foundev = tev;
				break;
			}
		}

		if (isMin) {
			// if event foundev is minimal, and its planned realization date is smaller than
			// current date then the event can be realized
			if (foundev.date <= currentDate) {

				realized = true;

				// log the realization of the event
				if (enableLogs)
					logRealizedEvent(foundev, currentDate);

				// memorize the delay in event firing
				float delayOfEvent = currentDate - foundev.date;
				
				//System.out.println("Delay ="+ (currentDate - foundev.date));

				
				// Warning : realization changes the date of the event
				foundev.realize(currentDate);
				// Compute the new minimal list

				// First, remove executed event
				minevents.remove(foundev);

				// Then, every successor of foundev
				// is minimal if all its predecessors
				// have been executed
				for (TableEvent nevt : foundev.getSuccessors()) {
					if (nevt.isMinimal()) {
						minevents.add(nevt);
					}
				}

				//if (foundev.date < currentDate) {
				if (delayOfEvent>0) {
					// propagate delay if needed
					//System.out.println("Evt date ="+ foundev.date + " Current Date "+currentDate);
					table.PropagateDelay(nb, currentDate - foundev.date);
				}
			}
		}

		return realized;

	}
	
	/**
	 * For the joint simulation
	 * Executes an event at current date if it is a minimal event i.e., an event
	 * that has no unexecuted predecessor
	 * 
	 * @param nb the identifier of the event to execute
	 */
	public boolean discreteMoveTTPN(int nb) {
		// return true if the move is realized, else return false
		boolean realized = false;

		// check if the event is minimal
		// if it is not, some event must be executed before
		Boolean isMin = false;
		TableEvent foundev = new TableEvent();
		for (TableEvent tev : minevents) {
			if (tev.number == nb) {
				isMin = true;
				foundev = tev;
				break;
			}
		}

		if (isMin) {
			// if event foundev is minimal, 
			// Then it can be realized

			realized = true;

			float firedEventDate=foundev.getDate();
			foundev.realize(currentDate);
			// update fired event dates
			
			// Compute the new minimal list
			// First, remove executed event
			minevents.remove(foundev);

			// Then, every successor of foundev
			// is minimal if all its predecessors
			// have been executed
			for (TableEvent nevt : foundev.getSuccessors()) {
				if (nevt.isMinimal()) {
					minevents.add(nevt);
				}
			}

			
			// log the realization of the event
			if (enableLogs) { logRealizedEvent(foundev, currentDate); }
 			
			// if  its planned realization date is smaller than
			// current date then the event can be realized
			
				if (firedEventDate <= currentDate) { // the fired event occurs late
					// propagate delay if needed
					realized = true;
					foundev.realize(currentDate);
					table.PropagateDelay(foundev.getNumber(), currentDate - foundev.date);
				    //System.out.println("Delay Propagated"+ (currentDate - foundev.date));	
				} 
		}
		return realized; // True if the discrete move was successful
	}

	/**
	 * log the realized event in the pathlog file with the number of the lin nbLine
	 * 
	 * @param evt         : the event to log as it was realized
	 * @param currentDate : the date when it was realized
	 */
	void logRealizedEvent(TableEvent evt, float currentDate) {
		FileWriter fileLogs = null;
		try {
			fileLogs = new FileWriter(this.pathLogs, true);
			fileLogs.write(nbLine + ": evt "+ evt.getNumber() + " " + evt.getLabel() + ":initial date " + evt.getInitialDate() +" : realized date " + currentDate + "\n");
		} catch (IOException e) {
			logger.log(Level.WARNING, "error of writing in fileLogs " + this.pathLogs);
		} finally {
			if (fileLogs != null) {
				try {
					fileLogs.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the fileLogs");
				}
			}
		}
		nbLine++;
	}

	public ArrayList<TableEvent> getMinevents() {
		return minevents;
	}

	public float getCurrentDate() {
		return currentDate;
	}

	public TimeTable getTable() {
		return table;
	}

	public ArrayList<TableEvent> getMinList() {
		return minevents;
	}

	// **

	/*public String dropConfig() {
	
		String s=new String("============\nTT Config : "+ currentDate+"\n");

		ArrayList<TableEvent> minlist = this.getMinList();

		for (TableEvent te : minlist) {
			s = s + te +"\n";
		}

		
		s=s+"===============\n";


		return s;

	}*/

	/**return true if all events are realized*/
	public boolean allRealized() {
		for (TableEvent evt : table.getEvents().values()) {
			if (!evt.getRealized())
				return false;
		}
		return true;
	}
	
	public void reset() {
		currentDate = 0;
		for (TableEvent evt : table.getEvents().values()) {
			evt.setRealized(false);
			evt.setDate(evt.initialDate);
			//evt.setTransnames(new ArrayList<Integer>());
			evt.setRecordedDate(0);
		}
		init();
	}

	public void setMinevents(ArrayList<TableEvent> minevents) {
		this.minevents = minevents;
	}

	public void setCurrentDate(float currentDate) {
		this.currentDate = currentDate;
	}

	
         public float getTableDelay() {
		
		// get earliest unexecuted date in the TT
					ArrayList<TableEvent> mlist = getMinevents();
					float minTableTimeElapse = -1; // A dummyvalue to start
					
					float CurrentDate=getCurrentDate();
					
					// tlist = n.getEnabled().values();

					for (TableEvent te : mlist) {
                        float evtDate = te.getDate();
                        float evtDelay = evtDate - CurrentDate;
                        
						if (te.getPlaceNames().size() > 0) {// if the event fills some place
							if (!te.getPlacesMarked()) { // If we did not already mark the place
								if (minTableTimeElapse < 0) {
									minTableTimeElapse = evtDelay;
								} else {
									minTableTimeElapse = Math.min(minTableTimeElapse, evtDelay);
								}
							}
						} else {
							// the event is not connected to any control place 
							if (evtDelay>0) { // If we did not already mark the place
								if (minTableTimeElapse < 0) {
									minTableTimeElapse = evtDelay;
								} else {
									minTableTimeElapse = Math.min(minTableTimeElapse, evtDelay);
								}
							}
							
						}
					} // From here we have a delay for the Timetable
					
		return Math.max(minTableTimeElapse,0);
		
	}

	
	
	
	
	
}
