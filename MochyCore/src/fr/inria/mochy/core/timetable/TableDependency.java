package fr.inria.mochy.core.timetable;

/**
 * A dependency between a pair of events is a constraint imposing
 * that a start event ocurs before an End event
 * a duration also specifies the minimal amount of time
 * that have to elapse between two events.
 * <p>
 * <img src="TableDependency.svg" alt="TableDependency class diagram" style="float: right;">
 */
public class TableDependency {

	int startEvent;
	int endEvent;
	float duration; // the minimal duration between start and end event
	
	
	TableDependency(int s,int e,float d){
		this.startEvent=s;
		this.endEvent=e;
		this.duration=d;
		
	}
	
	public TableDependency() {
		this.startEvent=0;
		this.endEvent=0;
		this.duration=0;
	}

	/**
	 * returns the origin of a table dependency (event number)
	 * @return
	 */
	public Integer getOrigin() {
		return new Integer(this.startEvent);
	}
	
	/**
	 * returns the goal of a table dependency (event number)
	 * @return
	 */
	public Integer getGoal() {
		return new Integer(this.endEvent);
	}

	public int getStartEvent() {
		return startEvent;
	}

	public int getEndEvent() {
		return endEvent;
	}

	public float getDuration() {
		return duration;
	}
	
	public void setStartEvent(Number number) {
		this.startEvent = number.intValue();
	}
	
	public void setEndEvent(Number number) {
		this.endEvent = number.intValue();
	}
	
	public void setDuration(float number) {
		//this.duration = number.intValue();
		this.duration = number;
	}
}
