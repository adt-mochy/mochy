package fr.inria.mochy.core.timetable;

import java.util.ArrayList;

/**
 * A TableEvent is an object which has a label, number, date to realize the event at the most early, tag(s) 
 * and can have links to the net model with transitions or/and places. 
 * <p>
 * <img src="TableEvent.svg" alt="TableEvent class diagram" style="float: right;">
 */
public class TableEvent {

	int number; // a unique identifier for the event
	ArrayList<String> tags;// list of tags associated to this event
	String tagsString = "";
	// These tags are useful for stats, but alos to map transitions
	// in a net and events in a timetable
	float date; // the expected date for the realization of this event
	float initialDate;// the original date set for this event
	String label; // a label describing the event (number are not informative enough)
	ArrayList<Integer> transnames; // a list of transitions that could be realizations of this event
	// this supposes that the timetable is realized by some Petri net variant
	// Note : there can be more than one transition that realizes this event

	ArrayList<Integer> placenames; // a list of places that are filled as soon as
	// the event is ready to be executed.(control places)

	Boolean PlacesMarked; // set to true when the places filled by this event have been marked

	int indegree;// number of predecessors
	ArrayList<TableEvent> Predecessors; // List of predecessors
	ArrayList<TableEvent> Successors; // List of successors
	ArrayList<TableDependency> dlist; // List of incoming dependencies

	Boolean realized; // true if the event has been executed
	float recordedDate; // the effective realization date of an event

	/**
	 * Constructor to build a dummy event
	 */
	public TableEvent() {

		this.number = 0;
		this.tags = new ArrayList<String>();
		this.date = 0;
		this.initialDate = 0;

		this.label = "";
		this.indegree = 0; // initially 0, will be updated

		this.transnames = null;
		this.placenames = new ArrayList<Integer>();

		this.Predecessors = null;
		this.Successors = null;
		this.dlist = null;
		this.PlacesMarked = false; // at the beginning no place is marked

	}

	TableEvent(int nb, float date, String lbl, ArrayList<Integer> tlist) {

		this.number = nb;
		this.tags = new ArrayList<String>();

		this.date = date;
		this.initialDate = date;

		this.label = lbl;
		this.indegree = 0; // initially 0, will be updated

		this.transnames = new ArrayList<>();
		this.placenames = new ArrayList<Integer>();
		this.Predecessors = new ArrayList<>();
		this.Successors = new ArrayList<>();
		this.dlist = new ArrayList<>();
		this.PlacesMarked = false; // at the beginning no place is marked

		for (Integer tnb : tlist) { // deep copy of transition numbers
			this.transnames.add(tnb);
		}

	}

	public TableEvent(String lbl) {
		this.label = lbl;
	}

	TableEvent(int nb, ArrayList<String> tgs, float date, String lbl, ArrayList<Integer> tlist) {

		this.number = nb;
		this.tags = tgs;
		this.date = date;
		this.initialDate = date;

		this.label = lbl;
		this.indegree = 0; // initially 0, will be updated

		this.transnames = new ArrayList<>();
		this.placenames = new ArrayList<Integer>();

		this.Predecessors = new ArrayList<>();
		this.Successors = new ArrayList<>();
		this.dlist = new ArrayList<>();

		for (Integer tnb : tlist) { // deep copy of transition numbers
			this.transnames.add(tnb);
		}

	}

	public void addPlaceNames(ArrayList<Integer> pn) {
		placenames = pn;
	}

	// returns the identifier of a table event
	public Integer getNumber() {
		return new Integer(this.number);
	}

	/**
	 * Increments the in-degree of an event (used when a dependency is found)
	 */
	public void incDegree() {
		this.indegree++;
	}

	/**
	 * 
	 * @param evt
	 */
	public void addPredecessor(TableEvent evt) {
		Predecessors.add(evt);
	}

	/**
	 * 
	 * @param evt
	 */
	public void addSuccessor(TableEvent evt) {
		Successors.add(evt);
	}

	public void addDependency(TableDependency td) {
		this.dlist.add(td);

	}

	/**
	 * returns the in degree of an event, useful in topological ordering
	 * 
	 * @return the in degree of an event
	 */
	public int getInDegree() {
		return this.indegree;
	}

	public void setPlacesMarked(boolean b) {
		PlacesMarked = b;
	}

	public boolean getPlacesMarked() {
		return PlacesMarked;
	}

	
	
	
	/**
	 * 
	 * marks an event as realized and sets its recorded date
	 * 
	 * @param date
	 */
	void realize(float newdate) {
		this.recordedDate = newdate;
		this.date = newdate;
		this.realized = true;
	}

	/**
	 * Says if an even is minimal in the current execution of a table i.e. all its
	 * predecessors are realized
	 * 
	 * @return true if all predecessors have been realized
	 */
	public Boolean isMinimal() {

		for (TableEvent pred : this.Predecessors) {
			if (!pred.getRealized()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * returns true if the considered event is late
	 * 
	 * @return
	 */
	Boolean isLate() {
		return this.realized && (this.recordedDate > initialDate);
	}

	/**
	 * Changes the realization date
	 * 
	 * @param d
	 */
	void changeDate(float d) {
		this.date = d;
	}

	public float getDate() {
		return date;
	}

	public String getLabel() {
		return label;
	}

	public ArrayList<Integer> getTransnames() {
		return transnames;
	}

	public String getTransitions() {
		String s = "";
		if (transnames != null) {
			for (Integer i : transnames)
				s += i + " ";
		}
		return s;
	}

	public ArrayList<TableDependency> getDependencies() {
		return this.dlist;
	}

	public Boolean getRealized() {
		if (realized == null) {
			return false;
		}
		return realized;
	}

	public void setRealized(Boolean realized) {
		this.realized = realized;
	}

	public float getRecordedDate() {
		return recordedDate;
	}

	public ArrayList<Integer> getTransNames() {
		return transnames;
	}

	public ArrayList<Integer> getPlaceNames() {
		return placenames;
	}

	public ArrayList<TableEvent> getPredecessors() {
		return Predecessors;
	}

	public ArrayList<TableEvent> getSuccessors() {
		return Successors;
	}

	public float getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(float initialDate) {
		this.initialDate = initialDate;
	}


	public ArrayList<String> getTags() {
		return tags;
	}

	public String toString() {
		String s = new String("evt " + number + " at " + date +"Pfill="+PlacesMarked);
		return s;
	}

	public boolean realizedBy(int tnum) {

		for (Integer val : transnames) {
			if (val.equals(tnum)) {
				return true;
			}
		}

		return false;
	}

	/*public String drop() {
		String s = new String("Evt:" + number + " dt " + date + " idt: " + initialDate + "rdt: " + recordedDate +" "+placenames.size());
		return s;
	}*/

	public void setNumber(Number number) {
		this.number = number.intValue();
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setDate(float number) {
		this.date = number;
	}

	public void setTransnames(ArrayList<Integer> transnames) {
		this.transnames = transnames;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public String getTagsString() {
		return tagsString;
	}

	public void setTagsString(String tagsString) {
		this.tagsString = tagsString;
	}

	public void addTransName(int tnum) {
		transnames.add(tnum);	
		
	}

	public void addPlaceName(int pnum) {
		placenames.add(pnum);			
	}

	public void setMarked(Boolean val) {
		PlacesMarked=val;
	}
	
	public ArrayList<Integer> getPlacesNames(){return placenames;}

	public void setRecordedDate(float recordedDate) {
		this.recordedDate = recordedDate;
	}
	
	
}
