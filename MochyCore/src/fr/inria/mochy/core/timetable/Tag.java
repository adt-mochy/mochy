package fr.inria.mochy.core.timetable;

import java.util.ArrayList;

/**
 * specify a tag linked to some events
 * used for the statistics
 * <p>
 * <img src="Tag.svg" alt="Tag class diagram" style="float: right;">
 */
public class Tag {
	String name;
	float delaySum;
	int nbEvents;
	ArrayList<TableEvent> events;
	ArrayList<Float> delays;
	int alpha;
	int beta;

	public Tag(String name) {
		this.name = name;
		this.delaySum = 0;
		this.nbEvents = 0;
		this.events = new ArrayList<>();
		this.delays = new ArrayList<>();
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof Tag)) return false;
	    Tag otherMyClass = (Tag)other;
		return otherMyClass.getName().equals(this.getName());
	}
	
	public void addDelayOfAnEvent(float delay) {
		this.delaySum += delay;
		this.nbEvents ++;
		this.delays.add(delay);
	}
	
	public void addEvent(TableEvent evt) {
		this.events.add(evt);
	}
	
	public boolean containEvent(TableEvent evt) {
		return this.events.contains(evt);
	}
	
	public int getAverrageDelay() {
		if (nbEvents > 0)
			return (int) delaySum/nbEvents;
		else
			return 0;
	}
	
	public void computeConfidenceInterval() {
		int mean = getAverrageDelay();
		int deviationSum = 0;
		for (float deviation : this.delays) {
			deviationSum += deviation * deviation;
		}
		double standardDeviation = Math.sqrt(deviationSum/nbEvents);
		alpha = (int) (mean - 2 * standardDeviation / Math.sqrt(nbEvents));
		beta =  (int) (mean + 2 * standardDeviation / Math.sqrt(nbEvents));
	}
	
	public void reset () {
		this.delaySum = 0;
		this.nbEvents = 0;
		this.delays = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public float getDelaySum() {
		return delaySum;
	}

	public int getNbEvents() {
		return nbEvents;
	}

	public ArrayList<TableEvent> getEvents() {
		return events;
	}

	public int getAlpha() {
		return alpha;
	}

	public int getBeta() {
		return beta;
	}
	
	

}
