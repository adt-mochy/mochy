package fr.inria.mochy.core.timetable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.abstractClass.Schedular;

/**
 * A timetable is a list of events, with imposed dependencies.
 * Timetables can be connected to a physical model of moving objects
 * In MOCHY V 1.0, this physical model is a Stochastic Petri net
 * Each event is the timetable description of a physiscal event occurring in the physical model
 * This has two consequences during a joint simulation
 * occurrences of physical events which are listed in the timetable fix the date during TT simulation
 * when the current date has passed the date of a realizable event in the table
 * it "allows" the corresponding event in the physical model
 * <p>
 * <img src="TimeTable.svg" alt="TimeTable class diagram" style="float: right;">
 */
public class TimeTable extends Schedular{

	public HashMap<Integer, TableEvent> events;
	// The event list is a HashMap to find an event rapidly

	ArrayList<TableDependency> dependencies;
	// List of causal dependencies in the time table
	// i.e. triples of the form (evt1,val,evt2) meaning that
	// evt1 precedes evt2, and at least val time units must elapse between evt1
	// and evt2

	Logger logger = Logger.getLogger("logger");
	// logger to display error messages (file opening mainly)
	int nbDepParsed = 0; // number of dependencies parsed
	int nbEvtParsed = 0; // number of events parsed

	HashMap<Integer, Integer> marked; // set of already visited events and their marks
	// mark : 0 = temporary mark, 1 = definitive mark
	ArrayList<Integer> topoOrder; // Topological ordering discovered

	String filename;

	// **************************************************
	// Constructors
	// ***************************************************

	TimeTable() {
		events = new HashMap<>();
		dependencies = new ArrayList<>();
		topoOrder = new ArrayList<>();
	}

	// *****************************************************************************

	/**
	 * Creates a timetable from a file
	 * 
	 * @param filename
	 */
	public TimeTable(String filename) {
		events = new HashMap<>();
		dependencies = new ArrayList<>();
		topoOrder = new ArrayList<>();
		this.filename = filename;
	}

	public String fileLoading() {
		BufferedReader br = null;
		//String logs = "";
		StringBuffer logs = new StringBuffer();
		int lineNb = 1;
		try {
			br = new BufferedReader(new FileReader(filename));
			System.out.println("*******************");
			System.out.println("Opening File :" + filename);

			while (br.ready()) { // until end of file
				String line = br.readLine();
				logs.append(handleLine(line, lineNb));
				lineNb++;
			}
			br.close();
			System.out.println("Evts Parsed :" + nbEvtParsed);
			System.out.println("Deps Parsed :" + nbDepParsed);

			System.out.println("*******************");

		} catch (Exception e) {
			logs.insert(0, "Warning : can't read the file "+filename+"\n");
			System.out.println(e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class TimeTable");
				}
			}
		}
		System.out.println("timetable loaded");
		return logs.toString();
	}

	/**
	 * Handles a line of a timetable read from a file Lines are either of the form
	 * event:<number>:<date>:<label>:t1:t2:....tk
	 * now the load of the transition is done through the cor file and tag have been added : 
	 * event:<number>:{tag1,tag2,...tagk}:<date>:<label>
	 * dependency:<number>:<number>:<duration>
	 * 
	 * @param ln
	 */
	protected String handleLine(String ln, int lineNb) {
		String logs = "";
		// Split the line in different fields
		String[] segments = ln.split(":");
		if (segments[0].equals("event")) {

			nbEvtParsed++;// one more event parsed

			ArrayList<Integer> tlist = new ArrayList<>();
			// the line is an event description

			// Handle tag at field2
			ArrayList<String> tags=this.handleTags(segments[2]);
			String tagsString = "";
			for (String tag : tags) {
				tagsString += tag+",";
			}
			
			// read transition list one by one
			// Caution: after tags insertion, transition list starts at field 5 (was 4 before)
			/*for (int i = 5; i < segments.length; i++) {
				try {
					tlist.add(Integer.parseInt(segments[i]));
				} catch (NumberFormatException nfe) {
					logs += "timetable line " + lineNb + ", error segment " + i + " : not a number\n";
				}
			}*/
			if (segments.length > 5)
				logs += "timetable line " + lineNb + ", error segment 5 and more : max line items\n";
			
			int number = 0;
			float date = 0;
			try {
				number = Integer.parseInt(segments[1]);
			} catch (NumberFormatException nfe) {
				logs += "timetable line " + lineNb + ", error segment 1 : not a number\n";
			}
			try {
				// Caution, the index of date is 3 after tags insertion
				date = Float.parseFloat(segments[3]);
			} catch (NumberFormatException nfe) {
				logs += "timetable line " + lineNb + ", error segment 3 : not a number\n";
			}
			TableEvent evt = new TableEvent(number, // number
					tags,
					date, // date
					segments[4], // label
					tlist); // integer list
			evt.setPlacesMarked(false); // initially, the event has caused no placefilling
			evt.setTagsString(tagsString);
			events.put(evt.getNumber(), evt);
		}

		if (segments[0].equals("dependency")) {

			// the line is a dependency description
			nbDepParsed++;// count one more dependency
			boolean error = false;

			Integer org = 0;
			Integer dst = 0;
			float val = 0;

			try {
				org = Integer.parseInt(segments[1]);
			} catch (NumberFormatException nfe) {
				logs += "timetable line " + lineNb + ", error segment 1 : not a number\n";
				error = true;
			}
			try {
				dst = Integer.parseInt(segments[2]);
			} catch (NumberFormatException nfe) {
				logs += "timetable line " + lineNb + ", error segment 2 : not a number\n";
				error = true;
			}
			try {
				val = Float.parseFloat(segments[3]);
				error = true;
			} catch (NumberFormatException nfe) {
				logs += "timetable line " + lineNb + ", error segment 3 : not a number\n";
			}

			TableDependency dep = new TableDependency(org, // number
					dst, // number
					val); // duration (integer)

			dependencies.add(dep); // add this dependency in the list

			TableEvent evt1 = this.events.get(org);
			TableEvent evt2 = this.events.get(dst);

			if (evt1 == null)
				logs += "timetable line " + lineNb + ", error event1 not found\n";

			if (evt2 == null)
				logs += "timetable line " + lineNb + ", error event2 not found\n";

			if (evt1 != null && evt2 != null) {
				// update events indegree
				evt2.incDegree();

				// update Successor list
				evt1.addSuccessor(evt2);

				// update predecessor list
				evt2.addPredecessor(evt1);

				// adds the dependency to the list of incoming dependencies
				evt2.addDependency(dep);
			}
		}else if (segments[0].equals("%")) {
			//comments
		}
		return logs;
	}

	private ArrayList<String> handleTags(String taglist) {
		
		ArrayList<String> l= new ArrayList<String>();
		
		String s=taglist.substring(taglist.indexOf("{") + 1);
		s = s.substring(0, s.indexOf("}"));
		
		// collect all tags separated by ","
		String[] segments = s.split(",");
		
		for (String tg : segments) {// for every tag
			l.add(tg);
			//System.out.println(tg);
		}
		
		return l;
	}

	// ******************************************************************
	// * Getters
	// ******************************************************************

	public HashMap<Integer, TableEvent> getEvents() {
		return events;
	}

	public ArrayList<TableDependency> getDependencies() {
		return dependencies;
	}

	/**
	 * 
	 * @param evtid the number of event to search
	 * @return the found event
	 */
	public TableEvent getEvent(Integer evtid) {

		return events.get(evtid);

	}

	// *********************************************

	/**
	 * Adds an event to a timetable
	 * 
	 * @param evt
	 */
	void addEvent(TableEvent evt) {
		events.put(evt.getNumber(), evt);
	}

	/**
	 * add a dependency to the list
	 * 
	 * @param td
	 */
	void addDependency(TableDependency td) {
		dependencies.add(td);
	}

	// **********************************************************

	/**
	 * 
	 * @param evtid an event id
	 * @return the list of successors of that event
	 */
	public ArrayList<Integer> nextEvents(Integer evtid) {

		ArrayList<Integer> succ = new ArrayList<Integer>();

		for (TableDependency dep : dependencies) { // look at all dependencies
			if (evtid.equals(dep.getStartEvent())) {
				succ.add(dep.getEndEvent());
			}
		}

		return succ;
	}

	/**
	 * 
	 * @param evtid an event id
	 * @return the list of predecessors of that event
	 */
	public ArrayList<Integer> PredecessorEvents(Integer evtid) {

		ArrayList<Integer> pred = new ArrayList<Integer>();

		for (TableDependency dep : dependencies) { // look at all dependencies
			if (evtid.equals(dep.getEndEvent())) {
				pred.add(dep.getStartEvent());
			}
		}

		return pred;
	}

	/**
	 * 
	 * @param evtid an event
	 * @return a list off dependencies which origin is that event
	 */
	public ArrayList<TableDependency> DependenciesFromEvent(Integer evtid) {

		ArrayList<TableDependency> dlist = new ArrayList<TableDependency>();

		for (TableDependency dep : dependencies) { // look at all dependencies
			if (evtid.equals(dep.getStartEvent())) {
				dlist.add(dep);
			}
		}

		return dlist;
	}

	/**
	 * returns the list of minimal events for a timetable
	 * 
	 * @return
	 */
	public ArrayList<TableEvent> minList() {

		ArrayList<TableEvent> min = new ArrayList<TableEvent>();

		TreeSet<Integer> dests = new TreeSet<>();
		// Destination of dependencies. TreeSet allows log(n) accesses

		// TreeSet<Integer> evtlist = (TreeSet<Integer>) this.events.keySet();
		// list of events in the timetable

		for (TableDependency dep : dependencies) {
			// read every dependency
			// minimal events are events that have no predecessor
			dests.add(dep.getGoal());

		}

		for (Integer nb : this.events.keySet()) { // look at all events

			if (!dests.contains(nb)) { // if this event is not in the destination list
				// i.e. if it has no predecessor

				min.add(events.get(nb));
			}
		}

		return min;
	}

	/**
	 * returns the list of minimal events that are not yet executed in a timetable
	 * we search every event that has only executed predecessors
	 * 
	 * @return
	 */
	public ArrayList<TableEvent> minListUnexecuted() {

		ArrayList<TableEvent> min = new ArrayList<TableEvent>();

		// TreeSet<Integer> evtlist = new TreeSet<>();
		// Get a list of event numbers

		// evtlist=(TreeSet<Integer>) this.events.keySet();

		for (Integer nb : this.events.keySet()) { // for every event

			TableEvent evnb = this.events.get(nb);
			if (!evnb.getRealized()) { // if nb is not realized

				ArrayList<Integer> predlist = this.PredecessorEvents(nb);
				Boolean allmarked = true;
				for (Integer pr : predlist) {
					TableEvent ev = this.events.get(pr);

					if (!ev.getRealized()) {
						allmarked = false;
					}
					// if ev is a non-realzed predecessor then nb is not
					// the identifier of a minimal event
				}

				if (allmarked) { // if all predecessors are realized
					min.add(evnb);
				}
			}
		}

		return min;
	}

	/**
	 * Propagates the effect of a delay in a timetable
	 * 
	 * Important : we assume that the table is acyclic, so there is no need to mark
	 * events without predecessors
	 * 
	 *  
	 * @param nb    : the event that is delayed
	 * @param Delay : the value of delay
	 */
	public void PropagateDelay(Integer nb, float Delay) {

		// Find a topological ordering (linear time) on successors of nb

		//visit(nb);
		// visit does not build a correct topological order
		
		// Test : display successors of nb
		ArrayList<TableEvent> suclist = this.getEvent(nb).getSuccessors();
		for (TableEvent suc : suclist) {
			//System.out.println(suc.number+ "Pred Size = " + suc.Predecessors.size());
		}
		
		visit(nb);
		// When visit returns, a topological order on successors of nb is built
		// and available in global variable topoOrder

		//************ Uncomment the next lines to display topological order in console ********
		// System.out.println("Topological Order");
		// for (Integer i:topoOrder) {
		// 	System.out.print(" "+i+"-> ");
		// }
		//System.out.println();
		//*****************************************************************************
		
		
		// for every vertex v in the topological ordering

		// The new date of the vertex is the max between the current date, and
		// the maximal value of the date of a predecessor p + the minimal duration
		// between p and v

		for (Integer v : topoOrder) {
			TableEvent evt = this.events.get(v);
			float date = evt.getDate();
			for (TableDependency dep : evt.getDependencies()) {
				// for every constraint (pred,val,evt) coming from predecessors
				
				//System.out.println(evt.drop());
				
				TableEvent evpred = events.get(dep.startEvent);
				float pdate =evpred.getDate();
				//System.out.println("Predecessor  : "+evpred.drop());
				if (pdate + dep.duration > date) {
					date = pdate + dep.duration;
					evt.changeDate(date);
				}
			}

		}

	}

	
	/**
	 * This method is used to build a topological order.
	 * 
	 * This delay order construction is an adaptation of Kahn's_algorithm
	 * https://en.wikipedia.org/wiki/Topological_sorting#Kahn's_algorithm
	 * that works by exploring a graph in a DFS manner. 
	 * for each visited vertex v, one suppresses the edges from v to its successor
	 * The next vertex to vist are among vertices without incoming edge
	 * 
	 *  Here the idea is the same but we visit vertices that are successors of nv (call them Succ(nv)
	 *  we do not suppress edges but rather count the number of unvisited connections 
	 *  to a vertex in Succ(nv) x Succ(nv)

	 * @param nv number of a vertex
	 */	
	private void visit(Integer nv) {

		// resets topological order
		this.topoOrder=new ArrayList<Integer>();
		
		// initalize a hashmap that associates a number of unvisited incoming edges 
		// to each visited event 
		HashMap<Integer, Integer> nbpred = new HashMap<Integer, Integer>();

		// Initialize the queue of events to explore
		// starting with nv
		ArrayDeque<Integer> vq = new ArrayDeque<Integer>();
		vq.addLast(nv);
		
		//NB : all events appear in VQ if their predecessors have been visited

		while (!vq.isEmpty()) { // As long as there exists an event to reach

			// Get the id of the first event in the queue
			Integer currentEvent = vq.removeFirst();

			// for each successor of current event
			ArrayList<Integer> ne = this.nextEvents(currentEvent);

			for (Integer suc : ne) {

				// check is the event was already visited during the order construction
				
			   Integer nps = nbpred.get(suc);
				
				if (nps == null) { // the event was not met before 
				
					// find the event in the table
				      TableEvent tes=this.getEvent(suc);
				      
				      
				    // compute the number of unvisited Predecessors  
						int sizepred = 0;

				      for (TableEvent preevt : tes.Predecessors) {
				    	  
				    	  if (!preevt.getRealized() && (preevt.number != nv) ) { sizepred++;}
				    	  
				      }


						if (sizepred <= 1 ) {
							
							// If the successor has only one incoming edge (currentEvetn,suc)
							// put it in vq
							// particulat=r case : sizepred =0 means the unique predecessor of the event is
							// nv, the event from which exploration started
							vq.addLast(suc);
						} else {
							
							   nbpred.put(suc, sizepred - 1);
						}
						
				} else { 
					// event was already seen as a successor, 
					// nps is an integer containing the size of unvisited predecessors of suc
				
				
					
					   if (nps == 1) { // we have visited the last predecessor of suc
						
						  vq.add(suc);
					   } else { 
						   
						   nbpred.put(suc, nps-1);
						   }
				       }
				

			} // treated all successors of currentEvent

			topoOrder.add(currentEvent);
		} // Exploration complete, queue is empty

	}

	
	
	
	public int size() {
		return events.size();
	}
	
	
	/**
	 * Resets the marked field for all events in the table, i.e. all events are unexecuted and did not allow any transition in the net
	 */
	public void resetMarked() {
		Set<Integer> teks =events.keySet(); 
		for (Integer ten : teks ) { // for all events
			TableEvent te=events.get(ten); // find the table event
			te.setMarked(false);// rest its placesmarked tag
		}
	}
	
	
	public String dropConfig() {
		String s= new String("");
		s.concat("Timetable: " + this.filename);
		return s;
	}

	public String getFilename() {
		return filename;
	}
	

}// end of class