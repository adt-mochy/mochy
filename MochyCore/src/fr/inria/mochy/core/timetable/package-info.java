/**
 * A timetable model can be run independently with the successions of events to be realized 
 * or in parallel with a net model to add time constraints to this last model.
 * <p>
 *
 * <img src="package.svg" alt="Package class diagram" style="float: right;">
 */
package fr.inria.mochy.core.timetable;
