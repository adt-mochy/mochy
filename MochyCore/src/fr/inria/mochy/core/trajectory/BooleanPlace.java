package fr.inria.mochy.core.trajectory;

import fr.inria.mochy.core.abstractClass.PlaceAbstract;

/**used as control place. 
 * They block a transition if set to false. 
 * Events can enable a boolean place
 * <p>
 * <img src="BooleanPlace.svg" alt="Boolean Place class diagram" style="float: right;">*/
public class BooleanPlace extends PlaceAbstract{
	String name;
	Integer number;
	boolean value = false;
	
	public BooleanPlace(int number, String name) {
		this.name = name;
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public Integer getNumber() {
		return number;
	}

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	@Override
	public void addToken() {
		this.value = true;
	}
	
}
