package fr.inria.mochy.core.trajectory;
import java.util.ArrayList;


/**
 * manage the state of the places
 * <p>
 * <img src="Marking.svg" alt="Marking class diagram" style="float: right;">
 */
public class Marking {

	ArrayList<TrajPlace> listMarked = new ArrayList<>();// list of places with non-empty contents
	ArrayList<BooleanPlace> booleanPlaces = new ArrayList<>();
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%                      CONSTRUCTOR                       %%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	public Marking() {}
	
	/*public Marking(ArrayList<TrajPlace> list){
		listMarked = list;
	}*/

	
	public boolean contains(TrajPlace p){		
		return listMarked.contains(p);		
	}
		
	public boolean add(TrajPlace p){
		return listMarked.add(p);
	}
	
	// removes a place from a marking
	public boolean  remove(TrajPlace p){
		boolean b=	listMarked.remove(p);
		return b;
	}
	
	//for the boolean places
	public boolean contains(BooleanPlace p){		
		return booleanPlaces.contains(p);		
	}
		
	public boolean add(BooleanPlace p){
		return booleanPlaces.add(p);
	}
	
	public boolean  remove(BooleanPlace p){
		boolean b=	booleanPlaces.remove(p);
		return b;
	}
	
	
	// Copies a marking 
	// nb : places are the same
	public Marking copy (){
		Marking newMarking = new Marking();
		for(TrajPlace p:listMarked){
			newMarking.add(p);	
		}
		for (BooleanPlace p : booleanPlaces) {
			newMarking.add(p);
		}
		return newMarking;
	}
	
	
	public void drop(){				
		/*for(TrajPlace p:listMarked){
			System.out.print(p.getName()); 			
		}		
		System.out.println();*/		
	}


	public ArrayList<TrajPlace> getListMarked() {
		return listMarked;
	}
	
	public void reset() {
		listMarked = new ArrayList<>();
		booleanPlaces = new ArrayList<>();
	}
	
}
