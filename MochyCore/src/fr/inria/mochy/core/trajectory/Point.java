package fr.inria.mochy.core.trajectory;

/**
 * A point which define an extrimity of a segment on the time/duration graph
 * <p>
 * <img src="Point.svg" alt="point class diagram" style="float: right;">
 */
public class Point {
	float x;//the abscissa of the point, can be infinite, represent a specific time
	float y;//the ordinate of the point, represent a specific distance
	
	public Point(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean equals (Point p) {
		return this.x == p.getX() && this.y == p.getY();
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	
}
