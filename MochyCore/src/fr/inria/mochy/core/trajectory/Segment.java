package fr.inria.mochy.core.trajectory;

/**segment has increasing abscissae and a decreasing ordinate
 * it defines a part of a trajectory and contains two points 
 * <p>
 * <img src="Segment.svg" alt="Segment class diagram" style="float: right;">
 */
public class Segment {
	private Point p0;//the abscissae of this point can't be infinite
	private Point p1;
	//the parameters of the linear function of the segment : 
	//y = alpha * x + beta
	float alpha;
	float beta;
	Segment preSegment = null;
	Segment nextSegment = null;
	
	public Segment (Point p0, Point p1){//constructor with two points
		this.p0 = p0;
		this.p1 = p1;
		calculFunction();
	}
	
	public Segment (float x0, float y0, float x1, float y1){//constructor with the coordinates of two points
		this.p0 = new Point(x0, y0);
		this.p1 = new Point(x1, y1);
		calculFunction();
	}
	
	/**return the ordinate of a x abscissa value
	 * return null if x is not on the segment*/
	public Float ordinate (float x) {
		if (x < this.getP0().getX() || x > this.getP1().getX())
			return null;
					
		if (this.getP0().x == this.getP1().x)
			return this.getP0().y;
		
		float coef = (this.getP1().getY() - this.getP0().getY()) / (this.getP1().getX() - this.getP0().getX());
		float value = (x - this.getP0().getX()) * coef + this.getP0().getY();
		
		if (value < 0)
			value = 0;
		
		return value;
	}
	
	public boolean belong (Point p) {//return true if the point p belong to this segment
		if (getP0().equals(getP1()) && p.equals(getP0()))
			return true;
		else if (getP0().equals(getP1()))
			return false;
		
		if (getP1().getX() == Float.POSITIVE_INFINITY)
			return p.getX() >= this.getP0().getX() && p.getY() >= this.getP0().getY() && p.getY() <= this.getP1().getY();
			
		return p.getX() >= this.getP0().getX() && p.getX() <= this.getP1().getX() && p.getY() >= this.getP0().getY() && p.getY() <= this.getP1().getY();
	}
	
	/**return true if the x parameter belong to the domain of the current segment*/
	boolean belongToDomain(float x) {
		return x >= this.getP0().getX() && x <= this.getP1().getX(); 
	}
	
	/**return true if the y parameter belong to the image of the current segment*/
	boolean belongToImage(float y) {
		return y <= this.getP0().getY() && y >= this.getP1().getY();
	}
	
	/**calcul alpha and beta so that the segment is a linear function y = alpha*x+beta */
	void calculFunction(){
		if (this.getP1().x == this.getP0().x)
			this.alpha = 0;
		else
			this.alpha = (this.getP1().getY() - this.getP0().getY()) / (this.getP1().getX() - this.getP0().getX());
		this.beta = - this.getP0().getX() * alpha + this.getP0().getY();
	}

	/**return null if the current segment never cross the segment in parameter
	 * else : return the abscissa of the cross' point
	 * the equation is alpha*x+beta = alpha2*x+beta2 => x = (-beta+beta2)/(alpha-alpha2)*/
	public Float cross (Segment segment) {
		//retrieve the alpha and beta of the function alpha*x+beta of the segments
		calculFunction();
		segment.calculFunction();
		
		if (alpha == segment.getAlpha())//the segments are parallel
			return null;
		
		float x = (-beta + segment.getBeta()) / (alpha - segment.getAlpha());
		//if x is in the domain of the two segments
		if (x>=this.getP0().getX() && x<=this.getP1().getX() && x>=segment.getP0().getX() && x<=segment.getP1().getX())
			return x;
		
		return null;
	}
	
	public Segment copy() {
		Segment segment = new Segment(getP0().x, getP0().y, getP1().x, getP1().y);
		if (nextSegment != null)
			segment.nextSegment = new Segment(nextSegment.p0.x, nextSegment.p0.y, nextSegment.p1.x, nextSegment.p1.y);
		if (preSegment != null)
			segment.preSegment = new Segment(preSegment.p0.x, preSegment.p0.y, preSegment.p1.x, preSegment.p1.y);
		return segment;
	}
	
	public void shiftRight(float x) {
		this.p0.x += x;
		this.p1.x += x;
	}
	
	public void shiftLeft(float x) {
		this.p0.x -= x;
		this.p1.x -= x;
	}
	
	public boolean isHorizontal() {
		if (this.p0.y == this.p1.y)
			return true;
		return false;
	}
	
	public void drop() {
		//System.out.println("("+p0.getX()+","+p0.getY()+")-("+p1.getX()+","+p1.getY()+")");
	}
	
	public float getAlpha() {
		return alpha;
	}

	public float getBeta() {
		return beta;
	}
	
	public boolean hasNext() {
		return nextSegment != null;
	}

	public Point getP0() {
		return p0;
	}

	public void setP0(Point p0) {
		this.p0 = p0;
		if (p1 != null)
			calculFunction();
	}

	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
		if (p0 != null)
			calculFunction();
	}
	
}
