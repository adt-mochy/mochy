package fr.inria.mochy.core.trajectory;

import java.util.ArrayList;

import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.sampler.Sampler;

/**
 * It contains none, one or multiple Trajectory object(s) and symbolize a track
 * portion
 * <p>
 * <img src="Place.svg" alt="Place class diagram" style="float: right;">
 */
public class TrajPlace extends PlaceAbstract{
	public String name;
	public Integer number;
	float distance;// the length of the track portion
	float headway;// the minimum safety distance between two tokens in the place content
	public ArrayList<Trajectory> trajectories = new ArrayList<>();// the list of the trajectories in the place content
	TrajTransition post;
	Sampler sampler = new Sampler();

	public TrajPlace(String name, int number, float distance, float headway) {
		this.name = name;
		this.number = number;
		this.distance = distance;
		this.headway = headway;
	}

	/** create a simple trajectory (one segment) and add it to the content */
	public boolean createTrajectory(Sampler sampler) {
		// if there is a transition in the post set
		if (post != null) {
			Trajectory trajectory = new Trajectory();
			float min = post.getLowerBound();
			float max = post.getUpperBound();
			float time = sampler.discreteUniform(min, max);
			//float time = post.clock;
			Segment segment = new Segment(0, distance, time, 0);
			trajectory.addSegment(segment);
			this.addTrajectory(trajectory);
			return true;
		}
		return false;
	}

	/**
	 * add a trajectory to the content by checking if there is no blocking
	 * trajectories and update it if there are, so that the place is
	 * headway-consistent
	 */
	public void addTrajectory(Trajectory trajectoryToAdd) {
		// retrieve the upper blocking trajectory if there are some.
		Trajectory trajectoryBlocking = null;
		Trajectory upperTrajectory = null;
		Float xBlocking = -1f;
		/*for (Trajectory currentTrajectory : trajectories) {
			Float x = trajectoryToAdd.isBlockedBy(currentTrajectory);
			System.out.println("trajectories are blocked in x="+x);
			if (x != null && trajectoryBlocking == null) {
				trajectoryBlocking = currentTrajectory;
				xBlocking = x;
			} else if (x != null && trajectoryBlocking != null && currentTrajectory.upwardShift()
					.ordinate(x) > trajectoryBlocking.upwardShift().ordinate(xBlocking)) {
				trajectoryBlocking = currentTrajectory;
				xBlocking = x;
			}
		}*/
		if (trajectories.size() > 0) {
			upperTrajectory = trajectories.get(trajectories.size() - 1);
			xBlocking = trajectoryToAdd.isBlockedBy(upperTrajectory);
		}

		// add the trajectory to the content if there is no blocking points
		if (xBlocking == null || xBlocking == -1f) {//trajectoryBlocking == null
			trajectoryToAdd.setHeadway(headway);
			trajectoryToAdd.place = this;
			trajectories.add(trajectoryToAdd);
			return;
		}

		//indicate to the trajectory that block which trajectory it blocks
		//it allows to recover to a normal state when the trajectory
		//that block is removed for a new place
		upperTrajectory.blockTraj = trajectoryToAdd;
		
		// correct the trajectory if there is some blocking trajectories in the content
		trajectoryToAdd.truncateFrom(xBlocking);
		trajectoryToAdd.followFrom(xBlocking, upperTrajectory);

		// add the trajectory to the content
		trajectoryToAdd.setHeadway(headway);
		trajectoryToAdd.place = this;
		trajectories.add(trajectoryToAdd);
	}
	
	/**remove the trajectory which is a single point shape*/
	public boolean removeTrajectory() {
		Trajectory trajectory = usableTrajectory();
		//System.out.println("remove trajectory");
		if (trajectory != null) {
			/*for (Trajectory blockedTrajectory : trajectories) {
				if (blockedTrajectory == trajectory.blockTraj) {
					blockedTrajectory.recover();
				}
			}*/
			if (trajectory.blockTraj != null)
				trajectory.blockTraj.recover();
			trajectories.remove(trajectory);
			refactor();//all the trajectories have been recovered their originate 
			//traject so it now must make a refactor to preserve some trajectories
			//with their headway
			return true;
		}
		return false;
	}

	/**get a usable trajectory if there is one which means that it
	 * go to the final point <0,0>*/
	public Trajectory usableTrajectory() {
		for (Trajectory trajectory : trajectories) {
			if (trajectory.isSinglePoint()) {
				//System.out.println("trajectory in place "+this.name+" is singlepoint.");
				return trajectory;
			}
		}
		return null;
	}
	
	/**check if it is possible to add a new trajectory
	 * if the max ordinates of the segments of the trajectories is less than
	 * the distance minus the headway*/
	public boolean availableForNewPlace() {
		Trajectory maxTrajectory;
		float maxValue = -1;
		for (Trajectory trajectory : trajectories) {
			float trajMax = trajectory.getMax();
			if (trajMax > maxValue) {
				maxValue = trajMax;
				maxTrajectory = trajectory;
			}
		}
		if (maxValue != -1 && maxValue < distance - headway)
			return true;
		if (maxValue == -1)
			return true;
		return false;
	}
	
	/**correct the trajectories if there is some crossings*/
	public void refactor() {
		for (int i = 1; i < trajectories.size(); i++) {
			Float x = trajectories.get(i).isBlockedBy(trajectories.get(i-1));
			if (x != null) {
				// correct the trajectory if there is some blocking trajectories in the content
				trajectories.get(i).truncateFrom(x);
				trajectories.get(i).followFrom(x, trajectories.get(i-1));
			}
		}
	}
	
	public String getName() {
		return name;
	}

	public Integer getNumber() {
		return number;
	}

	public void setPost(TrajTransition transition) {
		this.post = transition;
	}
	
	public TrajTransition getPost() {
		return this.post;
	}

	@Override
	public void addToken() {
		if (availableForNewPlace()) {
			createTrajectory(sampler);
		}
	}
}
