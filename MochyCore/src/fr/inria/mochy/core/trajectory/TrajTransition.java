package fr.inria.mochy.core.trajectory;

import java.util.ArrayList;

import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.sampler.Sampler;
import fr.inria.mochy.core.timetable.TableEvent;

public class TrajTransition extends TransitionAbstract{
	public String name; // the name of the transition
	public int number; // the number of the transition
	public TrajPlace pre = null;// predecessor places
	public TrajPlace post = null; // successor places
	BooleanPlace controlPre = null;// predecessor control places
									// Control places do not impact the elapsing of time, and are only used
									// to prevent or allow effective firing of a transition
	public Float lowerBound; // lower bound of the time interval associated with the transition
	public Float upperBound; // upper bound of the time interval associated with the transition
	String min;
	String max;
	String distribution;
	boolean gaussian = false;
	boolean weibull = false;
	int weibullCoef = 5;

	public Float clock; // the time remaining before firing of the transition

	ArrayList<TableEvent> linkedEvents = new ArrayList<>(); // the linked events to this transition

	public TrajTransition(String name, int number, Float min, Float max) {
		this.name = name;
		this.number = number;
		lowerBound = min;
		upperBound = max;
		this.min = String.valueOf(min);
		this.max = String.valueOf(max);
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// % Useful Fucntions %
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	// returns true if for the current transition
	// all its input places are marked
	boolean presetMarked(Marking m) {
		// check that all places in the preset are filled
		if (m.contains(this.pre) == false)
			return false;
		return true;
	}

	// returns true if a transitions has a non-empty postset
	public boolean busyPostSet(Marking m) {
		TrajPlace place = this.post;
		if (m.contains(this.post) && !place.availableForNewPlace())
			return true;

		return false;
	}
	
	public boolean busyPostSet() {
		TrajPlace p = this.post;
		if (p.availableForNewPlace())
			return false;
		return true;
	}

	// returns true if an enabled transition is blocked,
	// i.e. its clock is zero but one place in its postset is
	// not empty
	public boolean isBlocked(Marking m) {
		if (this.clock != null) {
			if (this.clock.equals(new Float(0.0))) {
				// (check that all places in the preset are filled)
				// check that places in the postset are all empty
				//if (m.contains(this.pre) && m.contains(this.post) && !this.post.availableForNewPlace())
				if (this.pre.usableTrajectory() != null && m.contains(this.post) && !this.post.availableForNewPlace())
					return true;
			}
		}
		return false;
	}

	// check that an enabled transition is not blocked
	// and has a zero clock
	// assumption : enabled transitions only
	public boolean isFireable() {
		if (this.clock == null) {
			return false;
		}
		if (this.clock.compareTo(new Float(0.0)) > 0) {
			return false;
		}
		if (!this.post.availableForNewPlace())
			return false;
		return true;
	}
	
	// says if a transition was newly enabled by a firing
	boolean isNewlyEnabled(Marking m1, Marking m2, TrajTransition t) {
		// m1 is the temporary marking
		// m2 is the reached marking
		if (presetMarked(m2)) {
			if (!presetMarked(m1)) {
				return true;
			}
			/*if (this == t) {
				return true;
			}*/
		}
		return false;
	}

	/*****************************************************************/

	// samples a value for current transition
	public void sample(Sampler s) {
		if (!isGaussian() && !isWeibull()) {
			// perform a random choice between upper and lower bound
			// this.clock=new Float(10);
			// Sampler s = new Sampler();
			if (this.lowerBound == null && this.upperBound == null)
				this.clock = (float) s.discreteUniform(0f, 1000f);
			else if (this.upperBound == null)
				this.clock = (float) s.discreteUniform(this.lowerBound, 1000f);
			else {
				this.clock = s.discreteUniform(this.lowerBound, this.upperBound);
			}
		} else if (isGaussian()) {//if Gaussian has been set in the net file
			if (this.lowerBound == null && this.upperBound == null)
				this.clock = (float) s.gaussSampler(500, 500);
			else if (this.upperBound == null)
				this.clock = (float) s.gaussSampler((1000+this.lowerBound)/2, (double) 1000-(((double) 1000+this.lowerBound)/2));
			else 
				this.clock = (float) s.gaussSampler((this.upperBound+this.lowerBound)/2, (double) this.upperBound-((double)(this.upperBound+this.lowerBound)/2));
			if (this.clock < 0)
				this.clock = 0f;
		} else if (isWeibull()) {
			if (this.lowerBound == null && this.upperBound == null)
				this.clock = (float) s.invertTransformWeibull(weibullCoef, 500);
			else if (this.upperBound == null)
				this.clock = (float) s.invertTransformWeibull(weibullCoef, (1000+this.lowerBound)/2);
			else
				this.clock = (float) s.invertTransformWeibull(weibullCoef, (this.lowerBound + this.upperBound)/2);
			if (this.clock <0)
				this.clock = 0f;

		}
	}

	private boolean isGaussian() {
		return gaussian;
	}

	private boolean isWeibull() {
		return weibull;
	}

	public String getName() {
		return name;
	}

	public int getNumber() {
		return number;
	}

	public ArrayList<TrajPlace> getPre() {
		ArrayList<TrajPlace> list = new ArrayList<>();
		if (pre != null)
			list.add(pre);
		return list;
	}
	
	public void setPre(TrajPlace pre) {
		this.pre = pre;
	}

	public ArrayList<TrajPlace> getPost() {
		ArrayList<TrajPlace> list = new ArrayList<>();
		if (post != null)
			list.add(post);
		return list;
	}
	
	public void setPost(TrajPlace post) {
		this.post = post;
	}

	public ArrayList<BooleanPlace> getControlPre() {
		ArrayList<BooleanPlace> list = new ArrayList<>();
		if (controlPre != null)
			list.add(controlPre);
		return list;
	}

	void setControlPre(BooleanPlace place) {
		controlPre = place;
	}

	public Float getClock() {
		return this.clock;
	}

	public String toString() {
		String s = "";
		return (s + this.number + this.name);
	}

	public String getDistribution() {
		return this.distribution;
	}

	public String getMin() {
		return min;
	}

	public String getMax() {
		return max;
	}

	public ArrayList<TableEvent> getLinkedEvents() {
		return linkedEvents;
	}

	public void addLinkedEvent(TableEvent te) {
		this.linkedEvents.add(te);
	}

	public Float getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(Float lowerBound) {
		this.lowerBound = lowerBound;
	}

	public Float getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(Float upperBound) {
		this.upperBound = upperBound;
	}

	public void setClock(Float clock) {
		this.clock = clock;
	}

	public boolean controlAllowsFiring() {
		if (controlPre != null && controlPre.isValue())
			return true;
		else if (controlPre == null)
			return true;
		return false;
	}

	public void setGaussian(boolean b) {
		gaussian = b;
	}
	
	public void setWeibull (boolean b) {
		weibull = b;
	}
	
	public void setWeibullCoef (int value) {
		weibullCoef = value;
	}
}
