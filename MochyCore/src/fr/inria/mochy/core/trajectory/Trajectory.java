package fr.inria.mochy.core.trajectory;

import java.util.ArrayList;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * It contains one or multiple Segment object(s) to simulate the journey of a
 * token on a graph with the time in the abscissa and the distance in ordinate
 * <p>
 * <img src="Trajectory.svg" alt="Trajectory class diagram" style="float:
 * right;">
 */
public class Trajectory {
	TrajPlace place;
	ArrayList<Segment> segments = new ArrayList<>();
	// the safety headway is the minimal distance in which no previous token must be
	float headway = 0;
	boolean blocked = false;
	//Float timeBlocked = null;
	Trajectory blockTraj = null;// the trajectory that is blocked by this one
	Trajectory savedTraj = null;// the trajectory saved before it is blocked

	public Trajectory() {
	}

	public Trajectory(ArrayList<Segment> segments) {
		this.segments = segments;
	}

	/**
	 * add the segment parameter to the segments of the trajectory if the segment
	 * parameter has a previous link segment : add it to its next segment
	 */
	public void addSegment(Segment segment) {
		for (Segment seg : segments) {
			//if two segments have a point in common
			float x1 = (float) Math.round(seg.getP1().x * 1000f) / 1000f;
			float x0 = (float) Math.round(segment.getP0().x * 1000f) / 1000f;
			float y1 = (float) Math.round(seg.getP1().y * 1000f) / 1000f;
			float y0 = (float) Math.round(segment.getP0().y * 1000f) / 1000f;
			//if (seg.getP1().getX() == segment.getP0().getX() && seg.getP1().getY() == segment.getP0().getY()) {
			if(x1 == x0 && y1 == y0) {
				seg.nextSegment = segment;
				segment.preSegment = seg;
			}
		}
		//if the two segments are horizontal and are on the same level
		//lengthen the previous segment to be a combination of the two segments
		if (segment.preSegment != null && segment.isHorizontal() && segment.preSegment.isHorizontal() && segment.getP0().y == segment.preSegment.getP1().y)
			segment.preSegment.getP1().x = segment.getP1().x;
		else
			segments.add(segment);
	}

	public ArrayList<Segment> getSegments() {
		return segments;
	}

	public float getHeadway() {
		return this.headway;
	}

	public void setHeadway(float value) {
		this.headway = value;
	}

	/** return the ordinate of the x abscissa value on the trajectory */
	public Float ordinate(float x) {
		// browse the segments of the trajectory
		for (Segment segment : segments) {
			// if x is in the segment domain, return the ordinate value of x
			if (x >= segment.getP0().getX() && x <= segment.getP1().getX()) {
				return segment.ordinate(x);
			}
		}
		return null;
	}
	
	/**return the absciss of the y value on the trajectory*/
	public Float absciss(float y) {
		// browse the segments of the trajectory
		for (Segment segment : segments) {
			if (segment.belongToImage(y)) {
				segment.calculFunction();
				return (y - segment.getBeta())/segment.getAlpha();
			}
		}
		return (float) 0;
	}

	/**
	 * create a new Trajectory object based on the current Trajectory with new
	 * values of ordinate increased by the headway value and a last segment with an
	 * ordinate of headway where the current trajectory ordinate is 0
	 */
	public Trajectory upwardShift() {
		//System.out.println("upwardShift");
		ArrayList<Segment> newSegments = new ArrayList<>();
		Segment lastSegment = null;
		Segment segment = segments.get(0);
		Trajectory trajectory = new Trajectory();
		/*for (Segment segment : this.segments) {
			segments.add(new Segment(segment.getP0().getX(), segment.getP0().getY() + headway, segment.getP1().getX(),
					segment.getP1().getY() + headway));
			if (segment.getP1().getY() == 0)
				lastSegment = segment;
		}*/
		while (segment != null) {
			trajectory.addSegment(new Segment(segment.getP0().getX(), segment.getP0().getY() + headway, segment.getP1().getX(),
					segment.getP1().getY() + headway));
			lastSegment = segment;
			segment = segment.nextSegment;
		}
		if (lastSegment != null) {
			//System.out.print("lastSegment / ");
			segment = new Segment(lastSegment.getP1().x, lastSegment.getP1().getY() + headway, lastSegment.getP1().x + 100, lastSegment.getP1().getY() + headway);
			segment.drop();
			trajectory.addSegment(segment);
		}
		//return new Trajectory(newSegments);
		return trajectory;
	}

	public Trajectory getBlockTraj() {
		return blockTraj;
	}

	public void setBlockTraj(Trajectory blockTraj) {
		this.blockTraj = blockTraj;
	}

	/**
	 * return the abscissa of the point where the current Trajectory is blocked by
	 * the one set in parameter and its headway else, if it is not blocked : return
	 * null
	 */
	public Float isBlockedBy(Trajectory trajectoryParam) {
		float minx = -1;//get the smallest x where there is a cross
		Trajectory trajectory = trajectoryParam.upwardShift();
		// check if and where two segments of the two trajectories cross themselves
		for (Segment segmentCurrent : this.getSegments()) {
			for (Segment segmentToPreserve : trajectory.getSegments()) {
				Float x = segmentCurrent.cross(segmentToPreserve);
				if (x != null) {
					// if the segment of the current trajectory cross
					// a segment to the trajectory to preserve with the headway
					blocked = true;
					trajectoryParam.setBlockTraj(this);
					if (minx == -1)
						minx = x;
					else
						minx = Math.min(minx, x);
				}
			}
		}
		if (minx == -1)
			return null;
		else 
			return minx;
	}

	/**
	 * advance the time to the delta value as specified in parameter -it performs a
	 * shift to the segments on the left by decreasing the abscissa values -it
	 * deletes the segments with P1.x < 0 -it truncate the segments with P0.x < 0
	 * return true if the trajectory has no more segments
	 */
	/*
	 * public void leftShift(float delta) { System.out.println("leftshift");
	 * ArrayList<Segment> segmentsToRemove = new ArrayList<>(); for (Segment segment
	 * : this.getSegments()) { if (this.ordinate(delta) != null) { segment.getP0().y
	 * = this.ordinate(delta); } else { segment.getP0().y = 0; } segment.getP0().x
	 * -= delta; segment.getP1().x -= delta; if (segment.getP1().x < 0)// remove the
	 * segment if its full interval is negative segmentsToRemove.add(segment); else
	 * if (segment.getP0().x < 0){// truncate the segment of its negative part
	 * segment.getP0().y = this.ordinate(0); segment.getP0().x = 0; } } for (Segment
	 * segment : segmentsToRemove) { segments.remove(segment); } if (segments.size()
	 * == 0) { this.segments.add(new Segment(0, 0, 0, 0)); }
	 * System.out.println("P0 : ("+segments.get(0).getP0().getX()+","+segments.get(0
	 * ).getP0().getY()+")");
	 * System.out.println("P1 : ("+segments.get(0).getP1().getX()+","+segments.get(0
	 * ).getP1().getY()+")"); }
	 */

	public void leftShift(float delta) {
		ArrayList<Segment> segmentsToRemove = new ArrayList<>();

		for (Segment segment : segments) {
			segment.shiftLeft(delta);
			if (segment.getP1().x < 0)
				segmentsToRemove.add(segment);
			else if (segment.getP0().x < 0) {
				segment.getP0().y = segment.ordinate(0);
				segment.getP0().x = 0;
			}
		}

		for (Segment segment : segmentsToRemove) {
			if (segment.isHorizontal() && segment.nextSegment == null)
				this.addSegment(new Segment(0, segment.getP1().y, 100, segment.getP1().y));
			if (segment.nextSegment != null)
				segment.nextSegment.preSegment = null;
			segments.remove(segment);
		}

		if (segments.size() == 0)
			this.addSegment(new Segment(0, 0, 0, 0));
	}

	/**
	 * truncate the trajectory from the x parameter value truncate the segment in
	 * which x is in its interval remove the segments which begin after x
	 */
	public void truncateFrom(float x) {
		savedTraj = new Trajectory();
		ArrayList<Segment> segmentsToRemove = new ArrayList<>();
		for (Segment segment : segments) {
			if (segment.belongToDomain(x) && x != segment.getP0().getX()) {
				savedTraj.addSegment(segment.copy());
				segment.setP1(new Point(x, segment.ordinate(x)));
				segment.nextSegment = null;
			} else if (segment.getP0().getX() >= x) {
				savedTraj.addSegment(segment);
				segmentsToRemove.add(segment);
			}
		}
		for (Segment segment : segmentsToRemove) {
			if (segment.nextSegment != null)
				segment.nextSegment.preSegment = null;
			segments.remove(segment);
		}
	}

	/**
	 * after a truncate is done, the trajectory can be updated to follow a
	 * trajectory set in parameter from a x abscissa value also set in parameter
	 */
	public void followFrom(float x, Trajectory trajectory) {
		//System.out.println("****FollowFrom****");
		Trajectory trajectoryToFollow = trajectory.upwardShift();
		Segment lastSegment = null;
		boolean firstSegmentSet = false;
		for (Segment segToFollow : trajectoryToFollow.segments) {
			if (segToFollow.belongToDomain(x)) {
				//System.out.print("seg to follow : ");
				segToFollow.drop();
				Point p0 = new Point(x, segToFollow.ordinate(x));
				Point p1 = segToFollow.getP1();
				addSegment(new Segment(p0, p1));
				firstSegmentSet = true;
			}
			if (firstSegmentSet == true) {
				lastSegment = segToFollow;
				segToFollow = segToFollow.nextSegment;
				if (segToFollow != null)
					segToFollow.drop();
				while (segToFollow != null) {
					addSegment(segToFollow);
					lastSegment = segToFollow;
					segToFollow = segToFollow.nextSegment;
					if (segToFollow != null)
						segToFollow.drop();
				}
				Point p0 = lastSegment.getP1();
				// Point p1 = new Point(Float.POSITIVE_INFINITY,
				// trajectoryToFollow.ordinate(p0.getX()));
				Point p1 = new Point(lastSegment.getP1().x+100, lastSegment.getP1().getY());
				addSegment(new Segment(p0.x, p0.y, p1.x, p1.y));
				break;
			}
		}
		blocked = true;
		//timeBlocked = x;
	}
	/*
	 * public void followFrom (float x, Trajectory trajectory) { Trajectory
	 * trajectoryToFollow = trajectory.upwardShift(); Segment segment = null; for
	 * (Segment segToFollow : trajectoryToFollow.segments) { if
	 * (segToFollow.belongToDomain(x)) { segment = segToFollow; } } if (segment !=
	 * null) { Point p0 = new Point(x, segment.ordinate(x)); Point p1 =
	 * segment.getP1(); addSegment(new Segment(p0, p1)); } Segment lastSegment =
	 * segment; segment = segment.nextSegment; while (segment != null) {
	 * addSegment(segment); lastSegment = segment; segment = segment.nextSegment; }
	 * Point p0 = lastSegment.getP1(); // Point p1 = new
	 * Point(Float.POSITIVE_INFINITY, // trajectoryToFollow.ordinate(p0.getX()));
	 * Point p1 = new Point(100, lastSegment.getP1().getY()); addSegment(new
	 * Segment(p0, p1)); }
	 */

	/**
	 * after a trajectory has been followed to preserve headways the segments of
	 * this trajectory can get to the origin segments
	 */
	/*
	 * public void recoverFrom(float x) { Segment lastSegment =
	 * segments.stream().filter(o -> o.nextSegment ==
	 * null).collect(Collectors.toList()).get(0); lastSegment.getP1().x = x; for
	 * (Segment segment : savedTraj.segments) { //
	 * System.out.println("segment/"+lastSegment.getP1().y+"/"+segment.getP0().getY(
	 * )+"/"+segment.getP1().getY()); if
	 * (segment.belongToImage(lastSegment.getP1().y)) { Point p0 = new Point(x,
	 * lastSegment.getP1().y); segment.shiftRight(x - timeBlocked); Point p1 =
	 * segment.getP1(); Segment seg = new Segment(p0, p1); addSegment(seg); Segment
	 * nextSegment = segment.nextSegment; while (nextSegment != null) {
	 * nextSegment.shiftRight(x - timeBlocked); addSegment(nextSegment); nextSegment
	 * = nextSegment.nextSegment; } } } timeBlocked = null; }
	 */
	public void recover() {
		/*if (this.place != null)
			System.out.println("recover "+this.place.getName());*/
		for (Segment segment : savedTraj.segments) {
			if (segment.belongToImage(this.ordinate(0))) {
				//Segment firstSegment = new Segment(0, this.ordinate(0), segment.getP1().x - timeBlocked,
						//segment.getP1().y);
				segment.calculFunction();
				float y0 = this.ordinate(0);
				float x0 = 0;
				float y1 = segment.getP1().y;
				float alpha = segment.alpha;
				float beta = - x0 * alpha + y0;
				float x1 = (y1-beta) / alpha; // as y = alpha * x + beta
				Segment firstSegment = new Segment(x0, y0, x1, y1);
				firstSegment.calculFunction();
				this.segments = new ArrayList<>();
				this.addSegment(firstSegment);
				Segment nextSegment = segment.nextSegment;
				while (nextSegment != null) {
					//nextSegment.shiftLeft(timeBlocked);
					x0 = x1;
					y0 = y1;
					y1 = nextSegment.getP1().y;
					nextSegment.calculFunction();
					alpha = nextSegment.alpha;
					beta = - x0 * alpha + y0;
					x1 = (y1-beta) / alpha;
					nextSegment = new Segment(x0, y0, x1, y1);
					nextSegment.calculFunction();
					addSegment(nextSegment);
					nextSegment = nextSegment.nextSegment;
				}
				break;
			}
		}

		//timeBlocked = null;
		
		if (this.blockTraj != null) {
			this.blockTraj.recover();
		}
		
	}

	public boolean isSinglePoint() {// if a segment is a single point shape at 0
		if (segments.size() == 1) {
			Segment segment = segments.get(0);
			float x0 = Math.round(segment.getP0().x * 1000f) / 1000f;
			float y0 = Math.round(segment.getP0().y * 1000f) / 1000f;
			float x1 = Math.round(segment.getP1().x * 1000f) / 1000f;
			float y1 = Math.round(segment.getP1().y * 1000f) / 1000f;
			if (x0 == 0 && y0 == 0 && y1 == 0 && (x1 == 0 || segment.getP1().x == Float.POSITIVE_INFINITY)) {
				return true;
			}
			if (segment.isHorizontal() && y0 == 0)
				return true;
		}
		return false;
	}

	float getMax() {
		float max = -1;
		for (Segment segment : segments) {
			max = Math.max(max, segment.getP0().y);
		}
		return max;
	}

	float allowedTime() {
		//System.out.print("Trajectory allowedTime : ");
		for (Segment segment : segments) {
			if (segment.belongToImage(0)) {
				//System.out.println(segment.getP1().getX());
				return segment.getP1().getX();
			}
		}
		return -1;
	}
	
	float getClock() {
		return this.absciss(0);
	}
}
