package fr.inria.mochy.core.trajectory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.dom.Dom;
import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.sampler.Sampler;
import fr.inria.mochy.core.mochysim.Transition;
import javafx.util.Pair;

public class TrajectoryNet extends PhysicalModel {
	public HashMap<Integer, TrajPlace> places = new HashMap<>();
	// HashMap<Integer, BooleanPlace> controlPlaces = new HashMap<>();
	public HashMap<Integer, TrajTransition> transitions = new HashMap<>();
	public HashMap<Integer, TrajTransition> enabled = new HashMap<>();
	public HashMap<Integer, TrajTransition> fireable = new HashMap<>();
	public HashMap<Integer, TrajTransition> blocked = new HashMap<>();
	public HashMap<Integer, BooleanPlace> booleanPlaces = new HashMap<>();
	HashMap<Integer, TrajPlace> usable = new HashMap<>();
	public Marking currentMarking = new Marking();// contains the list of marked places
	Marking initMarking = new Marking();// save the init marking
	Unblocked unblocked = new Unblocked();// the pair of unblocked trajectories of places
	HashMap<Integer, Float> config = new HashMap<>();// associate a transition number and its clock
	Sampler s = new Sampler();
	// String fname;
	Logger logger = Logger.getLogger("logger");
	Sampler sampler = new Sampler();
	boolean weibull = false;
	boolean gaussian = false;
	int weibullCoef = 5;
	int n = 1;// the number of fired transitions

	public TrajectoryNet(String fname) {
		this.fname = fname;
	}

	@Override
	public String loadFile() {
		StringBuffer logs = new StringBuffer();
		int lineNb = 1;
		// Open a file
		// reads places, transitions, trajectories, flows
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fname));
			System.out.println("*******************");
			System.out.println("Opening File : " + fname);

			while (br.ready()) { // Tant qu'on peut encore lire des choses
				logs.append(handleLine(br.readLine(), lineNb));
				lineNb++;
			}

			// At this place all lines are read
			// i.e. all places, transitions and the current marking
			// We can hence sample duration for every enabled transition
			// set the gaussian value to true to all the transitions of the net
			// if the file read the Gaussian value and the same if this is weibull
			if (this.gaussian) {
				for (TrajTransition t : this.transitions.values()) {
					t.setGaussian(true);
					t.setWeibull(false);
					if (t.presetMarked(currentMarking)) {
						t.sample(s);
						enabled.put(t.number, t);
					}
				}
			} else if (this.weibull) {
				for (TrajTransition t : this.transitions.values()) {
					t.setGaussian(false);
					t.setWeibull(true);
					t.setWeibullCoef(weibullCoef);
					if (t.presetMarked(currentMarking)) {
						t.sample(s);
						enabled.put(t.number, t);
					}
				}
			} else {
				for (TrajTransition t : this.transitions.values()) {
					if (t.presetMarked(currentMarking)) {
						t.sample(s);
						enabled.put(t.number, t);
					}
				}
			}
			br.close();
			System.out.println("*******************");

			// from initial marking, build initial configuration

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class Net");
				}
			}
		}

		return logs.toString();
	}

	protected String handleLine(String line, int lineNb) {
		String logs = "";
		String[] segments = line.split(":");
		ArrayList<TrajPlace> placesToCreate = new ArrayList<>();
		try {
			if (segments[0].equals("place")) {// place:nb:name:distance:headway
				int nb = Integer.parseInt(segments[1]);
				TrajPlace place = new TrajPlace(segments[2], nb, Float.parseFloat(segments[3]),
						Float.parseFloat(segments[4]));
				places.put(nb, place);
			} else if (segments[0].equals("control")) {// control:nb:name
				int nb = Integer.parseInt(segments[1]);
				BooleanPlace booleanPlace = new BooleanPlace(nb, segments[2]);
				booleanPlaces.put(nb, booleanPlace);
				// currentMarking.add(booleanPlace);
			} else if (segments[0].equals("transition")) {// transition:nb:name:[min,max]
				int nb = Integer.parseInt(segments[1]);
				TrajTransition transition;
				String[] interval = segments[3].substring(1, segments[3].length() - 1).split(",");
				// if the upperBound is infinite
				if (interval[1].equals("inf")) {
					float min = Float.parseFloat(interval[0]);
					transition = new TrajTransition(segments[2], nb, min, null);
				} else {
					float min = Float.parseFloat(interval[0]);
					float max = Float.parseFloat(interval[1]);
					transition = new TrajTransition(segments[2], nb, min, max);
				}
				transitions.put(nb, transition);
			} else if (segments[0].equals("trajectory")) {// trajectory:placeNb:x1,y1,x2,y2:x2,y2,x3,y3 an id and the
				int placeNb = Integer.parseInt(segments[1]); // points of its segments separated by :
				Trajectory trajectory = new Trajectory();
				for (int i = 2; i < segments.length; i++) {
					String[] points = segments[i].split(",");
					float x0 = Float.parseFloat(points[0]);
					float y0 = Float.parseFloat(points[1]);
					float x1 = Float.parseFloat(points[2]);
					float y1 = Float.parseFloat(points[3]);
					Segment segment = new Segment(x0, y0, x1, y1);
					trajectory.addSegment(segment);
				}
				TrajPlace place = places.get(placeNb);
				place.addTrajectory(trajectory);
				currentMarking.add(place);

			} else if (segments[0].equals("inflow")) {
				int trans = Integer.parseInt(segments[1]);
				int place = Integer.parseInt(segments[2]);
				this.addInFlow(trans, place);
			} else if (segments[0].equals("outflow")) {
				int trans = Integer.parseInt(segments[1]);
				int place = Integer.parseInt(segments[2]);
				this.addOutFlow(trans, place);
			} else if (segments[0].equals("initial")) {
				for (int i = 1; i < segments.length; i++) {
					int placeNb = Integer.parseInt(segments[i]);
					TrajPlace place = this.places.get(placeNb);
					// place.createTrajectory();
					currentMarking.add(place);
					placesToCreate.add(place);
					nbTokens++;
				}
			} else if (segments[0].equals("Gaussian")) {
				gaussian = true;
				logs += "Law of Gauss";
			} else if (segments[0].equals("Weibull")) {
				weibull = true;
				weibullCoef = Integer.valueOf(segments[1]);
				logs += "Law of Weibull";
			}
		} catch (NumberFormatException nfe) {
			logs += "error net line " + lineNb + " : a value is not a number\n";
		} catch (Exception e) {
			logs += "error in the net file at line " + lineNb + "\n";
			e.printStackTrace();
		}
		for (TrajTransition t : this.transitions.values()) {
			if (t.presetMarked(currentMarking)) {
				t.sample(s);
				enabled.put(t.number, t);
				this.config.put(t.getNumber(), t.getClock());
			}
		}
		for (TrajPlace p : placesToCreate) {
			p.createTrajectory(sampler);
		}
		initMarking = currentMarking.copy();
		return logs;
	}

	protected void addInFlow(int tnum, int pnum) {
		TrajPlace p = this.places.get(pnum);
		TrajTransition t = this.transitions.get(tnum);
		if (p == null) {
			BooleanPlace p2 = this.booleanPlaces.get(pnum);
			t.controlPre = p2;
			return;
		}
		t.setPre(p);
		p.setPost(t);
	}

	protected void addOutFlow(int tnum, int pnum) {
		TrajPlace p = this.places.get(pnum);
		TrajTransition t = this.transitions.get(tnum);
		t.setPost(p);
	}

	/**
	 * Add a token in a place
	 * 
	 * @param pnum
	 * @return
	 */
	public Boolean addToken(int pnum) {
		TrajPlace place = places.get(pnum);
		if (place == null) {
			if (booleanPlaces.get(pnum) != null) {
				booleanPlaces.get(pnum).value = true;
				return true;
			}
			return false;
		}
		if (!place.availableForNewPlace())
			return false;
		place.createTrajectory(sampler);
		currentMarking.add(place);
		return true;
	}

	@Override
	public float maxAllowedTimedMove() {
		if (!fireable.isEmpty()) {
			return 0f;
		}
		Float min = Float.POSITIVE_INFINITY;
		if (!enabled.isEmpty()) {
			// compare to all values in the table
			for (TrajTransition t : this.enabled.values()) {
				// System.out.println(t.name);
				if (t.clock < min && t.clock != 0) {
					min = t.clock;
				}
				// get the minimum time to make a trajectory fireable
				// -> (single point shape (0,0)-(0,0))
				for (Trajectory trajectory : t.pre.trajectories) {
					if (t.post.availableForNewPlace() && t.clock == 0) {
						float time = trajectory.allowedTime();
						if (time != -1 && time != 0)// if the trajectory target is y=0
							min = Math.min(min, time);
					}
				}
			}
			// return min;
		}
		// get the minimum time which allows to make places available for new
		// trajectories/tokent
		for (TrajPlace place : places.values()) {
			// the y value which can allow the place to be available for new
			// trajectories/token
			float y = place.distance - place.headway;
			if (place.trajectories.size() > 0) {
				// the upper trajectory of the place
				Trajectory trajectory = place.trajectories.get(place.trajectories.size() - 1);
				// get the x value depending on y and the segment which contain y
				for (Segment segment : trajectory.segments) {
					if (segment.belongToImage(y)) {
						segment.calculFunction();
						float x = 1 + ((y - segment.beta) / segment.alpha); // as y = alpha * x + beta
						if (x > 0)
							min = Math.min(min, x);
					}
				}
			}
		}
		if (min == Float.POSITIVE_INFINITY)
			return 0;
		/*
		 * else { Float min = Float.POSITIVE_INFINITY; for (TrajPlace place :
		 * places.values()) { for (Trajectory trajectory : place.trajectories) { min =
		 * Math.min(min, trajectory.allowedTime()); } } return min; //return 0f; }
		 */
		// return 0f;
		return min;
	}

	@Override
	public int numberFireable() {
		return fireable.size();
	}

	@Override
	public int numberBlocked() {
		return blocked.size();
	}

	@Override
	public boolean isBlocked(int tnum) {
		if (blocked.containsKey(tnum))
			return true;
		return false;
	}

	@Override
	public void progressTime(Float delta) {
		timeElapsed += delta;
		stepsNb++;
		discreteStep = false;
		// this move implements timed move
		// assuming that this time elapsing is allowed
		HashMap<Integer, TrajTransition> stillEnabled; // list of transitions still enabled
		stillEnabled = new HashMap<>();
		// blocked = new HashMap<>();
		// fireable = new HashMap<>();
		//System.out.println("progess time : " + delta);

		// perform a left shift on all the trajectories of all the places
		for (TrajPlace place : places.values()) {
			for (Trajectory trajectory : place.trajectories) {
				//System.out.println("left shift trajectory");
				trajectory.leftShift(delta);
			}
		}
		// remove delta to all enabled transitions that are not blocked
		for (TrajTransition t : enabled.values()) {
			if (t.clock.compareTo(delta) > 0) {
				// if the value of the clock attached to this transition allows a delta
				// time move
				t.clock = new Float(t.clock - delta);
				stillEnabled.put(t.number, t);
				//System.out.println("Transition " + t.name + " still enabled");
			} else { // clock is necessarily 0
				t.clock = new Float(0);
				if (t.busyPostSet()) {
					this.blocked.put(t.number, t);
					//System.out.println("Transition " + t.name + "blocked");
				} else if (t.controlAllowsFiring() && t.pre.usableTrajectory() != null
						&& t.post.availableForNewPlace()) {
					fireable.put(t.number, t);
					//System.out.println("Transition " + t.name + "firable");
				} else if (t.pre.trajectories.size() > 0) {// t.presetMarked(currentMarking)) {
					stillEnabled.put(t.number, t);
				}
			}
		} // all enabled transitions have been visited

		for (TrajTransition t : blocked.values()) {
			if (t.controlAllowsFiring() && t.pre.usableTrajectory() != null && t.post.availableForNewPlace()) {
				blocked.remove(t.getNumber());
				fireable.put(t.getNumber(), t);
			}
		}

		/*
		 * for (TrajTransition t : transitions.values()) { if (t.pre.usableTrajectory()
		 * != null && t.clock != null && t.clock == 0 && t.post.availableForNewPlace())
		 * { fireable.put(t.number, t); } }
		 */

		// Refresh list of enabled transitions
		this.enabled = stillEnabled;
	}

	@Override
	public void discreteMove() {
		discreteMove("", 0, false);
	}

	@Override
	public void multipleSteps(int steps) {
		for (int i = 1; i <= steps; i++) {
			Float delta = this.maxAllowedTimedMove();
			if (delta.compareTo(new Float(0.0)) > 0)// if a timed move is allowed
				this.progressTime(delta);
			else if (fireable.size() != 0) // timed moves and discrete moves are exclusive
				this.discreteMove("", 0, false);
		}
	}

	@Override
	public void reset(boolean init) {
		super.reset(init);
		enabled = new HashMap<>();
		blocked = new HashMap<>();
		fireable = new HashMap<>();

		n = 1; // the number of the step set to 1, used for the logs

		if (init)
			currentMarking = initMarking.copy();
		else {
			for (TrajTransition t : transitions.values()) {
				t.clock = null;
			}
		}

		for (TrajPlace p : places.values()) {
			p.trajectories = new ArrayList<>();
			if (currentMarking.contains(p))
				p.createTrajectory(sampler);
		}
		for (BooleanPlace p : booleanPlaces.values()) {
			p.value = false;
			if (currentMarking.contains(p))
				p.value = true;
		}

		// Establishes relation between transition numbers
		// and the value of attached clock
		if (init) {
			for (TrajTransition t : this.transitions.values()) {
				if (t.presetMarked(currentMarking)) {
					t.sample(s);
					enabled.put(t.number, t);
				}
			}
		}
	}

	public boolean discreteMove(String pathLogs, long elapsedTime, boolean enableLogs) {
		stepsNb++;
		nbDiscreteSteps++;
		discreteStep = true;
		// fire a transition t
		Random rand = new Random();
		int randomNum = rand.nextInt(fireable.size());
		Integer[] keys = (Integer[]) fireable.keySet().toArray(new Integer[0]);
		TrajTransition t = fireable.get(keys[randomNum]);
		if (t == null) {
			//System.out.println("discreteMove : Trying to fire transition t" + t.name);
			//System.out.println("not firable");
			return false;
		}

		Marking tempMarking = this.currentMarking.copy();

		TrajPlace p = t.pre;
		//System.out.println("Removing" + p.getName() + "from marking");
		if (!p.removeTrajectory())
			System.out.println("can't remove trajectory");
		//if (p.trajectories.isEmpty())
			System.out.println("remove marking : " + p.getName() + " : " + tempMarking.remove(p));
		if (t.controlPre != null)
			t.controlPre.value = false;

		Marking newMarking = tempMarking.copy();

		//System.out.println("Temporary marking");
		tempMarking.drop();

		p = t.post;
		//System.out.println("Adding" + p.getName() + "to temp marking");
		newMarking.add(p);
		p.createTrajectory(sampler);

		/*** log the fired transition **********/
		/*** nline : date : fired transition ***/
		if (enableLogs) {
			FileWriter fileLogs = null;
			try {
				fileLogs = new FileWriter(pathLogs, true);
				fileLogs.write(n + ":" + elapsedTime + ":" + t.getName() + "\n");
			} catch (IOException e) {
				logger.log(Level.WARNING, "error of writing in fileLogs in class TrajectoryNet");
			} finally {
				try {
					fileLogs.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the fileLogs in class TrajectoryNet");
				}
			}
			n++;
		}

		System.out.println("New marking");
		newMarking.drop();

		this.currentMarking = newMarking;

		// find all newly enabled transitions
		// and sample their clock value

		for (TrajTransition et : this.transitions.values()) {
			if (et.isNewlyEnabled(tempMarking, newMarking, t)) {
				// if et was not enabled in temp marking
				// sample times for all newly enabled transitions
				et.sample(s);
				this.config.put(et.number, et.clock);
				System.out.println("Newly enabled" + et.name);
			}
		}

		// if the new marking still contains a place with a trajectory in the previous
		// place of the fired
		// transition
		if (newMarking.contains(t.pre)) {
			float clock = t.pre.trajectories.get(0).getClock();
			//System.out.println("clock : " + clock);
			t.clock = clock;
			this.config.put(t.number, clock);
		}

		this.enabled = new HashMap<>();
		this.blocked = new HashMap<>();
		this.fireable = new HashMap();
		this.config = new HashMap<>();

		// Rebuild list of enabled/blocked/firable transitions
		for (TrajTransition et : this.transitions.values()) {
			if (et.pre.trajectories.size() > 0) {
				// transition enabled, look at clock value
				if (et.clock.equals(new Float(0.0))) {
					// transition's clock has reached value 0
					if (et.busyPostSet()) { // if one place marked
						this.blocked.put(et.number, et);
						//System.out.println(et.name + "blocked");

					} else if (et.controlAllowsFiring() && et.pre.usableTrajectory() != null
							&& et.post.availableForNewPlace()) {
						this.fireable.put(et.number, et);
						//System.out.println(et.name + "firable");
					} else {
						this.enabled.put(et.number, et);
					}

				} else { // transition enabled
					this.enabled.put(et.number, et);
					config.put(et.number, et.clock);
				}

			}
		} // all transitions considered
		return true;
	}

	@Override
	public void drop() {
		// TODO Auto-generated method stub

	}

	// returns true if a transitions has a non-empty postset
	public boolean busyPostSet(Integer tnum) {
		TrajTransition t = transitions.get(tnum);
		TrajPlace placePost = t.post;
		// if one can find a place in psotset that
		// contains something then t is blocked
		if (!placePost.availableForNewPlace()) {
			return true;
		}
		return false;
	}

	@Override
	public TransitionAbstract findTransition(Integer tnum) {
		return transitions.get(tnum);
	}

	@Override
	public String dropConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PlaceAbstract findPlace(int pnum) {
		PlaceAbstract p = places.get(pnum);
		if (p != null)
			return p;
		p = booleanPlaces.get(pnum);
		return p;
	}

	@Override
	public PlaceAbstract getControlPlace(int aplace) {
		return booleanPlaces.get(aplace);
	}

	@Override
	public float minimumClock() {
		return maxAllowedTimedMove();
	}

	@Override
	public ArrayList<TransitionAbstract> fireableTransition() {
		HashMap<Integer, TrajTransition> tlistfirable = this.getFirable();
		ArrayList<TransitionAbstract> firecontr = new ArrayList<>();
		for (TrajTransition t : tlistfirable.values()) {
			if (t.controlAllowsFiring()) {
				firecontr.add(t);
			}
		}
		return firecontr;
	}

	@Override
	public String discreteMove(TransitionAbstract onet, String logFile, float currentTime, boolean enableLogs) {
		stepsNb++;
		nbDiscreteSteps++;
		discreteStep = true;
		// fire a transition t
		TrajTransition t = (TrajTransition) onet;
		if (!fireable.containsKey(t.getNumber())) {
			//System.out.println("discrete move of a specific transition\nTrying to fire transition t" + t.name);
			//System.out.println("not firable");
			return "Trying to fire transition t" + t.name + "not firable";
		}

		Marking tempMarking = this.currentMarking.copy();

		TrajPlace p = t.pre;
		//System.out.println("Removing" + p.getName() + "from marking");
		if (!p.removeTrajectory())
			System.out.println("can't remove trajectory");
		// if (p.trajectories.isEmpty())
		tempMarking.remove(p);
		if (t.controlPre != null)
			t.controlPre.value = false;

		Marking newMarking = tempMarking.copy();

		//System.out.println("Temporary marking");
		tempMarking.drop();

		p = t.post;
		//System.out.println("Adding" + p.getName() + "to temp marking");
		newMarking.add(p);
		p.createTrajectory(sampler);

		/*** log the fired transition **********/
		/*** nline : date : fired transition ***/
		if (enableLogs) {
			FileWriter fileLogs = null;
			try {
				fileLogs = new FileWriter(logFile, true);
				fileLogs.write(n + ":" + currentTime + ":" + t.getName() + "\n");
				n++;
			} catch (IOException e) {
				logger.log(Level.WARNING, "error of writing in fileLogs in class Net");
			} finally {
				try {
					fileLogs.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the fileLogs in class Net");
				}
			}
		}

		//System.out.println("New marking");
		newMarking.drop();

		this.currentMarking = newMarking;

		// find all newly enabled transitions
		// and sample their clock value

		for (TrajTransition et : this.transitions.values()) {
			if (et.isNewlyEnabled(tempMarking, newMarking, t)) {
				// if et was not enabled in temp marking
				// sample times for all newly enabled transitions
				et.sample(s);
				this.config.put(et.number, et.clock);
				//System.out.println("Newly enabled" + et.name);
			}
		}

		// if the new marking still contains a place with a trajectory in the previous
		// place of the fired
		// transition
		if (newMarking.contains(t.pre)) {
			float clock = t.pre.trajectories.get(0).getClock();
			//System.out.println("clock : " + clock);
			t.clock = clock;
			this.config.put(t.number, clock);
		}

		this.enabled = new HashMap<>();
		this.blocked = new HashMap<>();
		this.fireable = new HashMap();
		this.config = new HashMap<>();

		// Rebuild list of enabled/blocked/firable transitions
		for (TrajTransition et : this.transitions.values()) {
			if (et.pre.trajectories.size() > 0) {
				// transition enabled, look at clock value
				if (et.clock.equals(new Float(0.0))) {
					// transition's clock has reached value 0
					if (et.busyPostSet()) { // if one place marked
						this.blocked.put(et.number, et);
						//System.out.println(et.name + "blocked");

					} else if (et.controlAllowsFiring() && et.pre.usableTrajectory() != null
							&& et.post.availableForNewPlace()) {
						this.fireable.put(et.number, et);
						//System.out.println(et.name + "firable");
					} else {
						this.enabled.put(et.number, et);
					}

				} else { // transition enabled
					this.enabled.put(et.number, et);
					config.put(et.number, et.clock);
				}

			}
		} // all transitions considered
		//System.out.println("transition " + t.getName() + "fired");
		return "transition " + t.getName() + "fired";
	}

	@Override
	public HashMap<Integer, ? extends TransitionAbstract> getEnabled() {
		return enabled;
	}

	@Override
	public HashMap<Integer, TrajTransition> getFirable() {
		return fireable;
	}

	@Override
	public HashMap<Integer, TrajTransition> getTransitions() {
		// TODO Auto-generated method stub
		return transitions;
	}

	@Override
	public boolean isGaussian() {
		return gaussian;
	}

	@Override
	public boolean isWeibull() {
		return weibull;
	}

	@Override
	public int getWeibullCoef() {
		return weibullCoef;
	}

	@Override
	public TransitionAbstract findTransition(String string) {
		for (TrajTransition t : transitions.values()) {
			if (t.getName().equals(string))
				return t;
		}
		return null;
	}

	@Override
	public HashMap<Integer, TrajPlace> getPlaces() {
		return places;
	}

	@Override
	public HashMap<Integer, ? extends PlaceAbstract> getControlPlaces() {
		return booleanPlaces;
	}
	@Override 
	public LinkedHashMap<Integer, Token> getTokens(){
		return new LinkedHashMap<Integer, Token>();
	}
}
