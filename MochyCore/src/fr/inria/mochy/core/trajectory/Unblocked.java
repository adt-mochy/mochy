package fr.inria.mochy.core.trajectory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javafx.util.Pair;

public class Unblocked {
	ArrayList<Pair<Integer, Trajectory>> unblocked = new ArrayList<>();
	public void add(TrajPlace place, Trajectory trajectory) {
		if (trajectory.blocked == false)
			unblocked.add(new Pair<Integer, Trajectory>(place.getNumber(), trajectory));
	}
	public void remove(TrajPlace place, Trajectory trajectory) {
		for (Pair<Integer, Trajectory> pair : unblocked) {
			if (pair.getValue().equals(trajectory))
				unblocked.remove(pair);
		}
	}
	public Trajectory isBlockedBy(TrajPlace place) {
		for (Pair<Integer, Trajectory> pair : unblocked) {
			if (pair.getKey().equals(place.getNumber()))
				return pair.getValue();
		}
		return null;
	}
	public void setBlocked(TrajPlace place, Trajectory trajectory) {
		for (Pair<Integer, Trajectory> pair : unblocked) {
			if (pair.getKey().equals(place.getNumber()) && pair.getValue().equals(trajectory)) {
				unblocked.remove(pair);
			}
		}
	}
}
