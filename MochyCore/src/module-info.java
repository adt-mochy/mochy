/**
 * Mochy Core Module. It contains models for the net, timetable and the joint of a net and a timetable.
 * <p> 
 * <img src="../package-dependencies.svg" alt="Package dependencies diagram" style="float: right;">
 */
module fr.inria.MochyCore {

	
    exports fr.inria.mochy.core.dom;
    exports fr.inria.mochy.core.mochysim;
    exports fr.inria.mochy.core.timetable;
    exports fr.inria.mochy.core.RPN;
    exports fr.inria.mochy.core.sampler;
    exports fr.inria.mochy.core.abstractClass;
    exports fr.inria.mochy.core.trajectory;
    exports fr.inria.mochy.core.equalization;
    exports fr.inria.mochy.core.exceptions;

    requires java.sql;
	requires commons.math3;
	requires javafx.base;
	requires java.desktop;
	requires neuroph;
} 