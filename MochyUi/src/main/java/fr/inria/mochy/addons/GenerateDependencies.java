package fr.inria.mochy.addons;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TimeTable;
import fr.inria.mochy.ui.JfxUtils;
import fr.inria.mochy.ui.LoadFiles;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**add-on for generate dependencies between different lines of events
 * works for linear events (no fork)
 * <p>
 * <img src="GenerateDependencies.svg" alt="GenerateDependencies class diagram" style="float: right;">*/
public class GenerateDependencies extends Application implements Initializable {

	Logger logger = Logger.getLogger("logger");
	TTConfig ttConfig;
	Stage stage;
	@FXML TextField startEventTF;
	@FXML TextField endEventTF;
	@FXML TextField intervalTF;
	@FXML TextField output;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("generateDependencies.fxml"));
		stage.setScene(scene);
		stage.setTitle("Generate dependencies from a timetable");
		stage.show();
		
	}
	
	@FXML void loadTimeTable(ActionEvent evt) {
		LoadFiles loadFiles = new LoadFiles();
		StringProperty logs = new SimpleStringProperty();//not attached to a window item
		ttConfig = loadFiles.loadTT(stage, logs);
	}
	
	@FXML void generateDependencies (ActionEvent evt) {
		
		String dependencies = "";
		TimeTable table = ttConfig.getTable();
		int startEventNb = Integer.parseInt(startEventTF.getText());
		int endEventNb = Integer.parseInt(endEventTF.getText());
		float duration = Float.parseFloat(intervalTF.getText());
		dependencies += "dependency:"+startEventNb+":"+endEventNb+":"+duration+"\n";
		while (table.nextEvents(startEventNb).size()>0 && table.nextEvents(endEventNb).size()>0) {
			startEventNb = table.nextEvents(startEventNb).get(0);
			endEventNb = table.nextEvents(endEventNb).get(0);
			dependencies += "dependency:"+startEventNb+":"+endEventNb+":"+duration+"\n";
		}
		
		FileWriter file = null;
		LoadFiles loadFiles = new LoadFiles();
		try {
			file = new FileWriter(loadFiles.getNetsFolder()+"/"+output.getText()+".tt", false);
			file.write(dependencies);
		} catch (IOException e) {
			logger.log(Level.WARNING, "error of writing in file in class GenerateDependencies");
		} finally {
			try {
				file.close();
			} catch (IOException e) {
				logger.log(Level.WARNING, "error while closing the file in class GenerateDependencies");
			}
		}
	}

}
