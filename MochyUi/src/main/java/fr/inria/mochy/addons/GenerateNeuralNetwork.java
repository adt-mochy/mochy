package fr.inria.mochy.addons;

import java.util.Random;

import org.neuroph.core.Connection;
import org.neuroph.core.Layer;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.Neuron;
import org.neuroph.core.Weight;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.Perceptron;
import org.neuroph.util.TransferFunctionType;

/**UNUSED TO DELETE**/
public class GenerateNeuralNetwork {
	public static void main(String[] args) {
		// create new perceptron network 
		NeuralNetwork neuralNetwork = new MultiLayerPerceptron(TransferFunctionType.STEP, 2, 3, 1);
		Layer l = neuralNetwork.getLayerAt(1);
		Neuron n = l.getNeuronAt(0);
		Weight w = n.getWeights()[1];
		System.out.println(neuralNetwork.getLayerAt(1).getNeuronAt(0).getWeights()[0].value);
		
		// save the network into file 
		neuralNetwork.save("neuralNetwork.nnet");
	}
	/*****randomize*****/
	public void randomize(Neuron neuron) {
        int numberOfInputConnections = neuron.getInputConnections().length;
        double coefficient = 1d / Math.sqrt(numberOfInputConnections);
        coefficient = coefficient == 0 ? 1 : coefficient;
        for (Connection connection : neuron.getInputConnections()) {
//            connection.getWeight().setValue(coefficient * nextRandomWeight());
            connection.getWeight().setValue(nextRandomWeight());

        }
    }
	private double nextRandomWeight() {
		Random random = new Random();
		random.nextDouble();
		return 0;
	}
	/**********************/
}
