package fr.inria.mochy.addons;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.jzy3d.chart.AWTChart;
import org.jzy3d.chart.Chart;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.maths.Scale;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Polygon;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.neuroph.core.NeuralNetwork;

import fr.inria.mochy.core.equalization.EquNetNeural;
import fr.inria.mochy.core.equalization.EquTransition;
import fr.inria.mochy.ui.JfxUtils;
import fr.inria.mochy.ui.LoadFiles;
import fr.inria.mochy.ui.StartController;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**analyse a nnet outputs depending of the inputs*/
/**
 * assume that the second transition is a moving one and that all the
 * transitions have the same normal time... used for 2 inputs nnet... see view.display3DChart...
 */
public class ThreeDimChartAnalyseNNet extends Application implements Initializable {
	Stage stage;
	float normalTime = (float) 1.3;//2;//1.3;
	int max = 600;//8000; //20000;
	float tmin = (float) 1.11;//1.6;//1.11;
	//NeuralNetwork nnet = NeuralNetwork.createFromFile("neural\\CircleEquNet\\2inputs1layer.nnet");
	NeuralNetwork nnet = NeuralNetwork.createFromFile("neural\\6-multiTokensNbSantiago\\30tokens\\3.nnet");
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("3DChartAnalyseNnet.fxml"));
		stage.setScene(scene);
		stage.setTitle("Equalization Chart of Neural Net");
		stage.show();

	}

	@FXML
	void loadFile(ActionEvent evt) {
		Chart chart = new AWTChart(Quality.Advanced);
		chart.getAxeLayout().setXAxeLabel("Vn-1");
		chart.getAxeLayout().setYAxeLabel("Vn+1");
		chart.getAxeLayout().setZAxeLabel("calculated time");
		ArrayList<Float> DnMoinsUn = new ArrayList<>();
		ArrayList<Float> DnPlusUn = new ArrayList<>();

		// set the x and y values
		for (float i = 0; i < max; i += max / 30) {
			DnMoinsUn.add(i);
			DnPlusUn.add(i);
		}

		// get the output timetobrowse value depending on the Dn,n-1 and Dn,n+1 values
		float[][] distDataProp = new float[DnMoinsUn.size()][DnPlusUn.size()];
		for (int i = 0; i < DnMoinsUn.size(); i++) {
			for (int j = 0; j < DnPlusUn.size(); j++) {
				//nnet.setInput(new double[] { (double) DnMoinsUn.get(i), (double) DnPlusUn.get(j), (double) DnMoinsUn.get(i) + 2000, (double) DnPlusUn.get(j) + 2000, 400, 600 });
				nnet.setInput(new double[] {300, 500, 800, 1000, (double) DnMoinsUn.get(i), (double) DnPlusUn.get(j)});
				//nnet.setInput(new double[] { (double) DnMoinsUn.get(i), (double) DnPlusUn.get(j)});
				nnet.calculate();
				float k = (float) (normalTime * (1 + nnet.getOutput()[0]));
				if (k < tmin) {
					k = (float) tmin;
				}
				System.out.println(k);
				distDataProp[i][j] = k;
			}
		}
		
		/* display connections weights between inputs and first layer
		 * for (int i = 0; i < nnet.getLayerAt(0).getNeuronsCount(); i++) {
			System.out.println("neuron "+i);
			for (int j = 0; j < nnet.getLayerAt(0).getNeuronAt(i).getOutConnections().length; j++) {
				System.out.println(nnet.getLayerAt(0).getNeuronAt(i).getOutConnections()[j].getWeight().value);
			}	
		}*/

		List<Polygon> polygons = new ArrayList<Polygon>();
		for (int i = 0; i < distDataProp.length - 1; i++) {
			for (int j = 0; j < distDataProp[i].length - 1; j++) {
				Polygon polygon = new Polygon();
				polygon.add(new Point(new Coord3d(DnMoinsUn.get(i), DnPlusUn.get(j), distDataProp[i][j])));
				polygon.add(new Point(new Coord3d(DnMoinsUn.get(i), DnPlusUn.get(j + 1), distDataProp[i][j + 1])));
				polygon.add(
						new Point(new Coord3d(DnMoinsUn.get(i + 1), DnPlusUn.get(j + 1), distDataProp[i + 1][j + 1])));
				polygon.add(new Point(new Coord3d(DnMoinsUn.get(i + 1), DnPlusUn.get(j), distDataProp[i + 1][j])));
				polygons.add(polygon);
			}
		}

		// Creates the 3d object
		Shape surface = new Shape(polygons);
		surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(),
				surface.getBounds().getZmax(), new org.jzy3d.colors.Color(1, 1, 1, .5f)));
		surface.setWireframeDisplayed(true);
		surface.setWireframeColor(org.jzy3d.colors.Color.BLACK);
		chart.add(surface);
		//chart.setScale(new Scale(20, 100));
		chart.open("", 600, 600);
	}
}
