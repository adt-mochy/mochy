package fr.inria.mochy.addons;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

import org.jzy3d.chart.AWTChart;
import org.jzy3d.chart.Chart;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.maths.Scale;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Polygon;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;

import fr.inria.mochy.ui.JfxUtils;
import fr.inria.mochy.ui.LoadFiles;
import fr.inria.mochy.ui.StartController;
import javafx.application.Application;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Create 3D chart by loading data of type "noise;alpha;time" as float values*/
public class ThreeDimChartEqualization extends Application implements Initializable {
	Stage stage;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("3DChartEqu.fxml"));
		stage.setScene(scene);
		stage.setTitle("Equalization Chart of Time Elapsed");
		stage.show();

	}

	@FXML
	void loadFile(ActionEvent evt) {
		LoadFiles loadFiles = new LoadFiles();

		for (int k = 0; k < 3; k++) {
			Chart chart = new AWTChart(Quality.Advanced);
			chart.getAxeLayout().setXAxeLabel("noise");
			chart.getAxeLayout().setYAxeLabel("alpha");
			chart.getAxeLayout().setZAxeLabel("time elapsed");
			String fname = loadFiles.loadFile(stage);
			float[] noise = {0, 0.5f, 1, 1.5f, 2, 2.5f, 3};
			float[] alpha = {0.5f, 1f, 1.5f, 2f, 2.5f, 3f, 3.5f, 4f, 4.5f, 5f };
			float[][] distDataProp = new float[noise.length][alpha.length];

			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(fname));
				while (br.ready()) { // Tant qu'on peut encore lire des choses
					String line = br.readLine();
					String[] segments = line.split(";");
					int x = 0, y = 0;
					for (int i = 0; i < noise.length; i++) {
						if (Float.parseFloat(segments[0]) == noise[i])
							x = i;
					}
					for (int i = 0; i < alpha.length; i++) {
						if (Float.parseFloat(segments[1]) == alpha[i])
							y = i;
					}
					distDataProp[x][y] = Float.parseFloat(segments[2]);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			List<Polygon> polygons = new ArrayList<Polygon>();
			for (int i = 0; i < distDataProp.length - 1; i++) {
				for (int j = 0; j < distDataProp[i].length - 1; j++) {
					Polygon polygon = new Polygon();
					polygon.add(new Point(new Coord3d(noise[i], alpha[j], distDataProp[i][j])));
					polygon.add(new Point(new Coord3d(noise[i], alpha[j + 1], distDataProp[i][j + 1])));
					polygon.add(new Point(new Coord3d(noise[i + 1], alpha[j + 1], distDataProp[i + 1][j + 1])));
					polygon.add(new Point(new Coord3d(noise[i + 1], alpha[j], distDataProp[i + 1][j])));
					polygons.add(polygon);
				}
			}

			// Creates the 3d object
			Shape surface = new Shape(polygons);
			surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(),
					surface.getBounds().getZmax(), new org.jzy3d.colors.Color(1, 1, 1, .5f)));
			surface.setWireframeDisplayed(true);
			surface.setWireframeColor(org.jzy3d.colors.Color.BLACK);
			chart.add(surface);
			chart.setScale(new Scale(20, 100));
			int v = k + 1;
			chart.open("V"+v, 600, 600);
		}
	}
}
