package fr.inria.mochy.addons;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.jzy3d.chart.AWTChart;
import org.jzy3d.chart.Chart;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Polygon;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;

/**load a chart from a csv file with the time average elapsed depending of the number of stops and of tokens*/
public class ThreeDimChartTimeElapsed {

	public static void main(String[] args) {
		/**get the data from the csv file*/
		BufferedReader br = null;
		int[] stops = {12,13,14,15,16};
		int[] tokens = {3,4,5,6,7,8,9,10};
		int[][] data = new int[stops.length][tokens.length];
		try {
			br = new BufferedReader(new FileReader("C:\\Users\\athebaul\\Desktop\\tests\\data9\\TimeElapsedTokensV1Mov.csv"));
			br.readLine();br.readLine();
			while (br.ready()) { // Tant qu'on peut encore lire des choses
				String line = br.readLine();
				String[] segments = line.split(";");
				for (int i = 0; i < stops.length; i++) {
					for (int j = 0; j < tokens.length; j++) {
						if (Integer.parseInt(segments[0]) == stops[i] && Integer.parseInt(segments[1]) == tokens[j]) {
							data[i][j] = Integer.parseInt(segments[2]);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		/**load the chart*/
		List<Polygon> polygons = new ArrayList<Polygon>();
		for (int i = 0; i < data.length - 1; i++) {
			for (int j = 0; j < data[i].length - 1; j++) {
				Polygon polygon = new Polygon();
				polygon.add(new Point(new Coord3d(stops[i], tokens[j], data[i][j])));
				polygon.add(new Point(new Coord3d(stops[i], tokens[j + 1], data[i][j + 1])));
				polygon.add(new Point(new Coord3d(stops[i + 1], tokens[j + 1], data[i + 1][j + 1])));
				polygon.add(new Point(new Coord3d(stops[i + 1], tokens[j], data[i + 1][j])));
				polygons.add(polygon);
			}
		}

		// Creates the 3d object
		Shape surface = new Shape(polygons);
		surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(),
				surface.getBounds().getZmax(), new org.jzy3d.colors.Color(1, 1, 1, .5f)));
		surface.setWireframeDisplayed(true);
		surface.setWireframeColor(org.jzy3d.colors.Color.BLACK);

		// Chart chart = new Chart();
		// chart.getScene().getGraph().add(surface);
		Chart chart = new AWTChart(Quality.Advanced);
		chart.getAxeLayout().setXAxeLabel("stops");
		chart.getAxeLayout().setYAxeLabel("tokens");
		chart.getAxeLayout().setZAxeLabel("time elapsed");
		chart.add(surface);
		chart.open("Jzy3d Demo", 600, 600);
	}

}
