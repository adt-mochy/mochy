package fr.inria.mochy.statsAndTasks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.ui.StartController;
import javafx.concurrent.Task;

public abstract class AbstractStats extends Task{
	//must implement call() which perform the simulation and write/display stats
	//this.updateProgress can be used to display the progress bar
	//this.updateMessage can be used to show information about the status of the task
	//isCancelled() can be used to break the runnning loop
	//the heritated classes in this package can be launch through stats/launch a class (see View.displayGenericStats)
	
	/**************data file operations******************/
	FileWriter file = null;
	void open() throws IOException {
		file = new FileWriter("logs/data.csv", false);
	}
	void close() throws IOException {
		file.close();
	}
	void write(float data) throws IOException {
		file.write(String.valueOf(data).replace(".", ",")+";");
	}
	void write(int data) throws IOException {
		file.write(String.valueOf(data)+";");
	}
	void write(String data) throws IOException {
		file.write(data+";");
	}
	void breakLine() throws IOException {
		file.write("\n");
	}
	
	/***************net operations**************/
	Sim getSimu() {
		return StartController.getSimu();
	}
	PhysicalModel getModel(){
		return getSimu().getN();
	}
	void oneStep() {//progress time or perform a discrete move
		getSimu().oneStep();
	}
	float getElapsedTime() {
		return getModel().getTimeElapsed();
	}
	int getNbSteps() {
		return getModel().getStepsNb();
	}
	int getNbDiscreteSteps() {
		return getModel().getNbDiscreteSteps();
	}
	boolean isDiscreteStep() {//is a discrete step if to true, a progress time else
		return getModel().isDiscreteStep();
	}
	/*********specific operations for EquNet networks**********/
	float standardDeviation() {
		EquNet n = ((EquNet) getModel());
		return n.getStandardDeviation(StartController.getSimu().getPathLogs());
	}
	float lastTokenSpeed() {
		EquNet n = ((EquNet) getModel());
		return n.getLastTokenSpeed();
	}
	float averageSpeed() {//the average speed at the current status
		EquNet n = ((EquNet) getModel());
		return n.getAvgSpeed();
	}
	float lastTimeToBrowse() {
		EquNet n = ((EquNet) getModel());
		return n.getLastTokenTtb();
	}
	
}
