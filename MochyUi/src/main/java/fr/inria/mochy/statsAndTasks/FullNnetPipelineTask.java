package fr.inria.mochy.statsAndTasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.util.TransferFunctionType;

import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.equalization.EquNetNeural;
import fr.inria.mochy.ui.FullNeuralNetPipelineController;
import javafx.util.Pair;

/**
 * generate 1000 random neural nets calculate for each the value for 100 runs :
 * time elapsed + beta*|target speed - average speed| + beta*|target speed -
 * average speed after equalization| select the 5 ones with the minimal values
 * optimize them 3 times by updating their weights if it leads to a best value
 * keep the best one of the 5 optimized neural nets
 */
public class FullNnetPipelineTask extends AbstractStats {

	EquNetNeural n;
	NeuralNetwork currentNnet;
	int standardDeviationTarget;
	int maximumSteps;
	int startTokensNb;
	int endTokensNb;
	int targetSpeed;
	int stepTokens;
	int runsNb = 100;
	int nnetGeneration = 1000;
	int stepsAfterEqualization = 1000;
	int pass = 3;// the number of pass through the weights of the nnet for the optimisation
	int nnetChosenNb = 5;// the number of chosen nnet among the best generated

	@Override
	protected Object call() throws Exception {
		n = (EquNetNeural) getSimu().getN();
		targetSpeed = n.getTargetSpeed();
		standardDeviationTarget = FullNeuralNetPipelineController.standardDeviation;
		maximumSteps = FullNeuralNetPipelineController.maximumSteps;
		startTokensNb = FullNeuralNetPipelineController.startTokensNb;
		endTokensNb = FullNeuralNetPipelineController.endTokensNb;
		stepTokens = FullNeuralNetPipelineController.stepToken;
		this.updateMessage("Step 1/2 : generation");
		ArrayList<NeuralNetwork> neuralNets = generateNnets();
		/*int i = 1;
		for (NeuralNetwork nnet : neuralNets) {
			nnet.save("neural/neuralNet"+i+".nnet");
			i++;
		}*/
		this.updateMessage("Step 2/2 : optimization");
		NeuralNetwork nnet = optimizeNeuralNets(neuralNets);
		if (nnet != null)
			nnet.save("neural/neuralNet.nnet");
		this.updateProgress(nnetGeneration, nnetGeneration);
		return null;
	}

	/**
	 * generate 1000 neural nets and return the 5 best depending of their simulation
	 */
	ArrayList<NeuralNetwork> generateNnets() throws Exception {
		HashMap<Float, NeuralNetwork> nnets = new HashMap<>();
		for (int i = 0; i < nnetGeneration; i++) {
			MultiLayerPerceptron nnet = new MultiLayerPerceptron(TransferFunctionType.TANH, 6, 10, 10, 10, 1);
			n.setNeuralNetwork(nnet);
			float value = launchSim();
			if (value != Float.POSITIVE_INFINITY)
				nnets.put(value, nnet);
			if (this.isCancelled())
				break;
			this.updateProgress(i, nnetGeneration);
		}
		Map<Float, NeuralNetwork> sortedNnets = nnets.entrySet().stream().sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> newValue,
						LinkedHashMap::new));
		ArrayList<NeuralNetwork> result = new ArrayList<>();
		Iterator<Entry<Float, NeuralNetwork>> iterator = sortedNnets.entrySet().iterator();
		int i = 1;// get the 5 best neural network
		while (i <= nnetChosenNb && iterator.hasNext()) {
			Entry<Float, NeuralNetwork> set = iterator.next();
			result.add((NeuralNetwork) set.getValue());
			System.out.println(set.getKey());
			i++;
		}
		return result;
	}

	/**optimize the nnets setting in parameter and return the one with the best results*/
	NeuralNetwork optimizeNeuralNets(ArrayList<NeuralNetwork> nnets) throws Exception {
		HashMap<Float, NeuralNetwork> result = new HashMap<>();
		float value = Float.POSITIVE_INFINITY;
		int i = 0;
		this.updateMessage("optimize nnet "+(i+1)+"/"+nnets.size());
		for (NeuralNetwork nnet : nnets) {
			Pair<Float, NeuralNetwork> pair = optimizeNeuralNet(nnet);
			result.put(pair.getKey(), pair.getValue());
			i++;
			this.updateProgress(i, nnets.size());
			if (this.isCancelled())
				break;
			this.updateMessage("optimize nnet "+(i+1)+"/"+nnets.size());
		}
		Map<Float, NeuralNetwork> sortedNnets = result.entrySet().stream().sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> newValue,
						LinkedHashMap::new));
		Iterator<Entry<Float, NeuralNetwork>> iterator = sortedNnets.entrySet().iterator();
		Entry<Float, NeuralNetwork> nnet = iterator.next();
		this.updateProgress(100, 100);
		return nnet.getValue();
	}

	Pair<Float, NeuralNetwork> optimizeNeuralNet(NeuralNetwork nnet) throws Exception {
		n.setNeuralNetwork(nnet);
		float value = launchSim();
		System.out.println("original value = " + value);
		loop:
		for (int j = 1; j <= pass; j++) {
			for (int i = 0; i < nnet.getWeights().length; i++) {
				if (this.isCancelled())
					break loop;
				NeuralNetwork mutantPlus = mutant(nnet, i, true);// mutant += 0.1 on current weight
				n.setNeuralNetwork(mutantPlus);
				float tempValue = launchSim();
				if (tempValue < value) {
					value = tempValue;
					nnet = mutantPlus;
					System.out.println("mutant plus on weight " + i + " : " + value);
				} else {
					NeuralNetwork mutantMoins = mutant(nnet, i, false);// mutant -= 0.1 on current weight
					n.setNeuralNetwork(mutantMoins);
					tempValue = launchSim();
					if (tempValue < value) {
						value = tempValue;
						nnet = mutantMoins;
						System.out.println("mutant moins on weight " + i + " : " + value);
					} //else 
						//System.out.println("weight "+i);
				}
				this.updateProgress((i+1)*j, pass*nnet.getWeights().length);
			}
		}
		Pair<Float, NeuralNetwork> pair = new Pair<>(value, nnet);
		return pair;
	}

	/**creating a mutant on the neural network specified at a specific weight index : +0.1 if plus is true, -0.1 else*/
	NeuralNetwork mutant(NeuralNetwork nnet, int index, boolean plus) {
		NeuralNetwork mutant = new MultiLayerPerceptron(TransferFunctionType.TANH, 6, 10, 10, 1);
		double[] weights = Stream.of(nnet.getWeights()).mapToDouble(Double::doubleValue).toArray();
		if (plus)
			weights[index] += 0.1;
		else
			weights[index] -= 0.1;
		mutant.setWeights(weights);
		return mutant;
	}

	/**
	 * launch the simulation with the neural net associated to the net return the
	 * value calculated : time elapsed + beta * |average speed - target speed | +
	 * beta * |average speed after equalization - target speed|
	 */
	Float launchSim() throws Exception {
		float value = Float.POSITIVE_INFINITY;
		float timeElapsed = 0;
		float averageSpeed = 0;
		float averageSpeedAfterEqu = 0;
		int nb = 0;// number of times that the sim get into the standard deviation target
		boolean outOfTarget = false;
		//ArrayList<Float> dataSpeed = new ArrayList<>();
		ArrayList<Float> dataSpeedAfterEqu = new ArrayList<>();
		// loop on the number of tokens
		loop1:
		for (int j = startTokensNb; j <= endTokensNb; j += stepTokens) {
			// loop on the number of runs
			for (int i = 1; i <= runsNb; i++) {
				n.setBunchingState(j);
				n.resetSpeedData();
				// loop until equalization or max steps number
				while (n.getStandardDeviation() > standardDeviationTarget && n.getStepsNb() < maximumSteps) {
					getSimu().oneStep();
				}
				if (n.getStandardDeviation() <= standardDeviationTarget) {
					timeElapsed += n.getTimeElapsed();
					averageSpeed += n.getAvgSpeed();
					//dataSpeed.add(n.getAvgSpeed());
					n.resetSpeedData();
					// loop on 100 steps after equalization
					for (int k = 0; k < stepsAfterEqualization; k++)
						getSimu().oneStep();
					averageSpeedAfterEqu += n.getAvgSpeed();
					dataSpeedAfterEqu.add(n.getAvgSpeed());
					nb++;
				} else {
					outOfTarget = true;
					break loop1;
				}
			}
			if (this.isCancelled())
				break;
		}
		if (nb != 0 && !outOfTarget) {
			timeElapsed = timeElapsed / nb;
			averageSpeed = averageSpeed / nb;
			//averageSpeedAfterEqu = averageSpeedAfterEqu / nb;
			//value = (float) (n.getBeta()
			//		* (0.2 * Math.abs(averageSpeedAfterEqu - targetSpeed) + 0.8 * Math.abs(averageSpeed - targetSpeed)));
			//averageSpeed = calculVariance(dataSpeed);
			averageSpeedAfterEqu = calculVariance(dataSpeedAfterEqu);
			value = (float) (n.getBeta() * (0 * averageSpeed + 1 * averageSpeedAfterEqu));
			//System.out.println(n.getBeta()+" / "+averageSpeedAfterEqu+"/"+averageSpeed+"/"+targetSpeed);
			//System.out.println(timeElapsed + " - " + value);
			value = timeElapsed + value;
		}
		return value;
	}
	
	float calculVariance(ArrayList<Float> data) {
		float squareSum = 0;
		for (float value : data)
			squareSum += Math.pow(value - targetSpeed, 2);
		float var = (float) squareSum / data.size();
		return var;
	}
}
