package fr.inria.mochy.statsAndTasks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.CompetitiveLearning;
import org.neuroph.util.TransferFunctionType;

import fr.inria.mochy.core.equalization.EquNetNeural;
import fr.inria.mochy.core.equalization.EquNetNeuralMov;
import fr.inria.mochy.ui.GenerateNeuralController;
import fr.inria.mochy.ui.StartController;

/**generate random neural networks, only those wich make the equalization network to get to an 
 * equalization are saved in the neural folder.*/
public class GenerateNeuralTask extends AbstractStats {

	@Override
	protected Object call() throws Exception {
		int standardDeviationTarget = GenerateNeuralController.sdTarget;
		int runsAllNetworks = GenerateNeuralController.nnNb;// nb of runs for each new neural network
		int runsForEachNetwork = GenerateNeuralController.r;// nb of runs for each neural network to get the nb of out
															// of target runs
		// (depending of maxSteps)
		int maxSteps = GenerateNeuralController.max;
		float preAverageTimeElapsed = Float.POSITIVE_INFINITY;
		File file = new File("neural/data.csv");
		if (file.exists())
			file.delete();
		file.createNewFile();// create new file
		FileWriter writer = new FileWriter(file);
		writer.write("file;time elapsed;average speed;outOfTargetNb;;speedAfterEqu\n");
		writer.close();
		EquNetNeural net = (EquNetNeural) StartController.simu.getN();
		this.updateProgress(0, runsAllNetworks);
		outerloop: for (int i = 0; i < runsAllNetworks; i++) {//loop on neural networks
			if (isCancelled())
				break;
			this.updateMessage("run " + i + "/" + runsAllNetworks);
			int outOfTargetNb = 0;
			float averageTimeElapsed = 0;
			float averageSpeed = 0;
			//net.setNeuralNetwork(new MultiLayerPerceptron(TransferFunctionType.TANH, 6, 10, 50, 20, 5, 1));
			NeuralNetwork nnet = new MultiLayerPerceptron(TransferFunctionType.TANH, 6, 10, 10, 10, 1);
			net.setNeuralNetwork(nnet);
			for (int j = 0; j < runsForEachNetwork; j++) {//loop on physical networks
				if (isCancelled())
					break outerloop;
				net.reset(false);
				int steps = 0;
				while (steps < maxSteps && net.getStandardDeviation("") > standardDeviationTarget) {
					if (isCancelled())
						break outerloop;
					StartController.simu.oneStep();
					steps++;
				}
				if (steps == maxSteps)
					outOfTargetNb++;
				else {
					averageTimeElapsed += net.getTimeElapsed();
					averageSpeed += net.getAvgSpeed();
				}
			}
			
			if (outOfTargetNb < runsForEachNetwork) {
				averageTimeElapsed = averageTimeElapsed / (runsForEachNetwork - outOfTargetNb);
				averageSpeed = averageSpeed / (runsForEachNetwork - outOfTargetNb);
				//perform 1000 steps to check the average speed after equalization
				net.resetSpeedData();
				for (int j = 0; j < 1000; j++) {
					if (isCancelled())
						break outerloop;
					StartController.simu.oneStep();
				}
			} else {
				averageTimeElapsed = 0;
				averageSpeed = 0;
			}
			if (outOfTargetNb == 0){// && averageTimeElapsed < preAverageTimeElapsed) {
				net.saveNeuralNetwork("neural/data.csv", outOfTargetNb, 0, averageSpeed, averageTimeElapsed, false);
				preAverageTimeElapsed = averageTimeElapsed;
			}
			this.updateProgress(i, runsAllNetworks);
		}
		return null;
	}

}
