package fr.inria.mochy.statsAndTasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.util.TransferFunctionType;

import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.equalization.EquNetNeural;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.ui.GenerateNeuralMultiTokensNbController;
import fr.inria.mochy.ui.LoadFiles;
import fr.inria.mochy.ui.MultipleRunsController;
import fr.inria.mochy.ui.StartController;

public class GenerateNnetMultiTokensNbTask extends AbstractStats {

	@Override
	protected Object call() throws Exception {
		LoadFiles loadFiles = new LoadFiles();
		String netFileUrl = StartController.getSimu().getFname();
		int standardDeviationTarget = GenerateNeuralMultiTokensNbController.sdTarget;
		int tokensMin = GenerateNeuralMultiTokensNbController.tokMin;
		int tokensMax = GenerateNeuralMultiTokensNbController.tokMax;
		int tokensInterval = GenerateNeuralMultiTokensNbController.tokInterval;
		int runs = GenerateNeuralMultiTokensNbController.r;
		int maxSteps = GenerateNeuralMultiTokensNbController.max;
		int nnetNb = GenerateNeuralMultiTokensNbController.nnNb;
		//MultipleRunsController controller = new MultipleRunsController();
		//controller.equalizationFullStats(false, false, true, new GenerateNnetMultiTokensNbTask());
		boolean outOfMaxSteps = false;
		float avgTimeElapsed = 0;
		float avgSpeed = 0;
		float avgSpeedAfterEqu = 0;
		int nb = 0; // used to calculate the average avg
		
		File file = new File("neural/data.csv");
		if (file.exists())
			file.delete();
		file.createNewFile();// create new file
		FileWriter writer = new FileWriter(file);
		writer.write("file;time elapsed;average speed;speedAfterEqu;outOfTargetNb;;\n");
		writer.close();
		
		this.updateProgress(0, nnetNb);
		
		outerloop: for (int k = 1; k <= nnetNb; k++) {
			EquNetNeural n = null;
			NeuralNetwork nnet = new MultiLayerPerceptron(TransferFunctionType.TANH, 6, 10, 10, 10, 1);
			avgTimeElapsed = 0;
			avgSpeed = 0;
			avgSpeedAfterEqu = 0;
			nb = 0;
			outerloop2: for (int tokensInitial = tokensMin; tokensInitial <= tokensMax; tokensInitial += tokensInterval) {
				String initial = "initial:";
				for (int i = 1; i < tokensInitial; i++) {
					initial += i + ":";
				}
				initial += tokensInitial;

				// add the initial line to the input net file
				FileWriter fw = new FileWriter(netFileUrl, true);
				fw.write(initial);
				fw.close();

				// generate the net file
				loadFiles.generateSimu(netFileUrl);
				Sim simu = StartController.getSimu();
				n = (EquNetNeural) simu.getN();
				n.setNeuralNetwork(nnet);

				for (int i = 1; i <= runs; i++) {
					int steps = 0;
					//System.out.println(maxSteps+"-"+n.getStandardDeviation()+"-"+standardDeviationTarget);
					while (steps < maxSteps && n.getStandardDeviation() > standardDeviationTarget) {
						if (isCancelled())
							break outerloop;
						simu.oneStep();
						steps++;
						//System.out.println(maxSteps+"-"+n.getStandardDeviation()+"-"+standardDeviationTarget);
					}
					if (steps == maxSteps) {
						outOfMaxSteps = true;
						eraseLastLine();
						break outerloop2;
					} else {
						avgTimeElapsed += n.getTimeElapsed();
						avgSpeed += n.getAvgSpeed();
						nb++;
						n.resetSpeedData();
						for (int j = 1; j <= 1000; j++) {
							if (isCancelled())
								break outerloop;
							simu.oneStep();
						}
						avgSpeedAfterEqu += n.getAvgSpeed();
					}
					n.reset(false);					
				}
				eraseLastLine();
			}
			if (!outOfMaxSteps) {
				// save nnet & write stats
				n.saveNeuralNetwork("neural/data.csv", 0, avgSpeedAfterEqu/nb, avgSpeed/nb, avgTimeElapsed/nb, false);
			}
			outOfMaxSteps = false;
			this.updateProgress(k, nnetNb);
		}

		return null;
	}
	
	public void eraseLastLine() throws IOException {
		BufferedReader br = null;
		br = new BufferedReader(new FileReader(StartController.getSimu().getFname()));

		// StringBuilder s = new StringBuilder();
		String file = "";
		String line = "";
		while (br.ready()) {
			line = br.readLine();
			// s.append(line+System.lineSeparator());
			file += line + "\n";
		}
		String string = file.replace(line + "\n", "");
		FileWriter fWriter = new FileWriter(StartController.getSimu().getFname());
		fWriter.write(string);
		fWriter.close();
	}

}
