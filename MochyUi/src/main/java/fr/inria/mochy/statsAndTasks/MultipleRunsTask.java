package fr.inria.mochy.statsAndTasks;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import fr.inria.mochy.core.RPN.RPNConfig;
import fr.inria.mochy.core.RPN.RegulNet;
import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.sampler.Sampler;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.Tag;
import fr.inria.mochy.core.timetable.TimeTable;
import fr.inria.mochy.core.trajectory.TrajPlace;
import fr.inria.mochy.ui.StartController;
import javafx.beans.property.StringProperty;
import javafx.scene.control.CheckBox;

public class MultipleRunsTask extends AbstractStats{
	
	int runsNb = 1;
	CheckBox logsItems;
	String statsString = "";
	public boolean console = false;
	
	public MultipleRunsTask(int nbRuns, CheckBox logsItems) {
		this.runsNb = nbRuns;
		this.logsItems = logsItems;
	}

	@Override
	protected Object call() throws Exception {
		statsString = "";
		run();
		return null;
	}
	
	public void run() {
		StringProperty logs = StartController.getLogsTextString();
		StringProperty stats = StartController.getStatsTextString();
		Sampler sampler = new Sampler();
		HashMap<Tag, Boolean> listTags = StartController.getListTags();
		float delaySumTotal = 0;
		int realizedEvtsTotal = 0;
		int deadLocksNb = 0; // the number of locks during the simulation, eg there is no move available to
								// realize the next events
		float timeBetweenTwoPlacesSum = 0;// the sum of the time between start and end places to calculate the average
											// time
		
		//HashMap<Integer, ArrayList<Float>> startPlaceTime = new HashMap<>();// the list of the time at the start place
																			// for a specific token
		//HashMap<Integer, ArrayList<Float>> endPlaceTime = new HashMap<>();// the list of the time at the end place for a
																			// specific token
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		// used for the EqualizationNet steps
		float standardDeviationAvg = 0;
		float timeElapsedAvg = 0;
		int discreteStepsAvg = 0;
		int stepsAvg = 0;
		float minTime = Float.MAX_VALUE;
		float maxTime = 0;
		int minSteps = Integer.MAX_VALUE;
		int maxSteps = 0;
		float speedAvg = 0;
		int speedNb = 0;// the number of steps done when the speed sum is calculated
		int sumDifferenceTime = 0;

		stats.set("");
		String log = "logs/Joint" + timestamp.toString().replace(":", "") + ".txt";
		/*int runs = 1;
		if (nbRuns != null) {
			runs = Integer.parseInt(nbRuns.getText());
		}*/
		if (!console)
			this.updateProgress(0, runsNb);
		for (int i = 1; i <= runsNb; i++) {
			if (!console && isCancelled()) {
				break;
			}
			TTConfig ttConfig = StartController.getTtConfig();
			Sim simu = StartController.getSimu();
			RPNConfig rpnc = StartController.getRpnc();
			RegulNet rn = StartController.getRn();
			String correspPath = StartController.getCorrespondancePath();
			float delaySum = 0;
			int nbRealizedEvt = 0;
			String firstDelay = "";
			float timeStartPlace = -1;
			float timeEndPlace = -1;
			int nbJourneyBetweenTwoPlaces = 0;// the number of journey between the start and end places to calculate the
			// average time
			ArrayList<Float>[] startList = new ArrayList[StartController.simu.getN().nbTokens];
			for (int j = 0; j < startList.length; j++)
				startList[j] = new ArrayList<>();
			
			ArrayList<Float>[] endList = new ArrayList[StartController.simu.getN().nbTokens];
			for (int j = 0; j < endList.length; j++)
				endList[j] = new ArrayList<>();
			
			if (ttConfig != null && !StartController.getConnectTT()) {// if we make stats on the table only
				ttConfig.reset();
				while (!ttConfig.allRealized()) {// while all events are not realized
					float timedMove = sampler.invertTransform(1);
					timedMove = -1;
					for (TableEvent te : ttConfig.getMinevents()) {
						if ((te.getDate() - ttConfig.getCurrentDate()) <= 0) {
							timedMove = -1;
							break;
						} else if (timedMove == -1 && (te.getDate() - ttConfig.getCurrentDate()) > 0)
							timedMove = te.getDate() - ttConfig.getCurrentDate();
						else if ((te.getDate() - ttConfig.getCurrentDate()) > 0)
							timedMove = Math.min(timedMove, (te.getDate() - ttConfig.getCurrentDate()));
					}
					if (timedMove != -1)
						ttConfig.timeMove(timedMove);
					if (ttConfig.getMinevents().size() > 0) {
						TableEvent minEvt = ttConfig.getMinevents().get(0);
						for (TableEvent e : ttConfig.getMinevents()) {
							if (e.getDate() < minEvt.getDate())
								minEvt = e;
						}
						if (ttConfig.discreteMove(minEvt.getNumber())) {
							nbRealizedEvt++;
							delaySum += ttConfig.getCurrentDate() - minEvt.getInitialDate();
							realizedEvtsTotal++;
							for (Tag tag : listTags.keySet()) {
								if (tag.containEvent(minEvt))
									tag.addDelayOfAnEvent(ttConfig.getCurrentDate() - minEvt.getInitialDate());
							}
						}
					}
				}
				delaySumTotal += delaySum;
				if (nbRealizedEvt > 0)
					statsString += "\nrun " + i + " - delay sum : " + delaySum + " - average delay : "
							+ (int) (delaySum / nbRealizedEvt);
				ttConfig.reset();
			}
			/** if the timetable is connected to the net */
			else if (ttConfig != null && simu != null && rpnc != null && StartController.isCorrespLoaded()
					&& StartController.getConnectTT()) {
				rpnc.reset();
				PhysicalModel n = rpnc.getNetAndTT().getNet();
				// n.reset();
				TimeTable tt = rpnc.getNetAndTT().getTable();

				boolean enableLogs = false;
				if (!this.console && StartController.enableLogs.get())
					enableLogs = true;
				rpnc = new RPNConfig(n, ttConfig, log, enableLogs);
				rpnc.setDelays(StartController.getDelays());

				rn.setNet(n);
				rn.setTable(ttConfig.getTable());
				StartController.setTtConfig(ttConfig);
				StartController.setRpnc(rpnc);

				boolean deadLock = false; // true when there is no firable transition to continue the realization of the
											// events

				HashMap<Integer, Integer> positionToken = new HashMap<>();// <placeNb,tokenNb>
				boolean net = false;//true if the type of physical model is Net
				if (StartController.isCalculTime()) {
					int j = 0;
					for (PlaceAbstract aP : StartController.simu.getN().getPlaces().values()) {
						if (aP instanceof Place) {
							net = true;
							Place p = (Place) aP;
							if (p.getContents() != null) {
								positionToken.put(p.getNumber(), j);
								j++;
							}
						} else if (aP instanceof TrajPlace) {
							TrajPlace p = (TrajPlace) aP;

						}
					}
				}

				while (!ttConfig.allRealized() && !deadLock) {// while all events are not realized
					StringProperty logsTextString = StartController.getLogsTextString();

					boolean dmoveFired = StartController.tryDiscreteMove();

					if (dmoveFired && StartController.isEventFired()) {// If firinf the table Event was successful
						TableEvent cte = StartController.getEvent();
						TransitionAbstract onet = StartController.getTransition();
						nbRealizedEvt++;
						if (ttConfig.getCurrentDate() - cte.getInitialDate() > StartController.getDelayEscape()) {
							if (StartController.getMaxInterval() == null)
								delaySum += ttConfig.getCurrentDate() - cte.getInitialDate();
							else if (cte.getDate() > StartController.getMinInterval()
									&& cte.getDate() < StartController.getMaxInterval())
								delaySum += ttConfig.getCurrentDate() - cte.getInitialDate();
						}
						if (firstDelay.equals("") && ttConfig.getCurrentDate() - cte.getInitialDate() > 0) {
							firstDelay = "event " + cte.getLabel() + " - nb " + cte.getNumber() + " - value "
									+ (ttConfig.getCurrentDate() - cte.getInitialDate() + " - date : "
											+ ttConfig.getCurrentDate());
						}
						realizedEvtsTotal++;
						for (Tag tag : listTags.keySet()) {
							if (tag.containEvent(cte))
								tag.addDelayOfAnEvent(ttConfig.getCurrentDate() - cte.getInitialDate());
						}
						if (StartController.isCalculTime() && net == true) {
							positionToken.put(onet.getPost().get(0).getNumber(),
									positionToken.get(onet.getPre().get(0).getNumber()));
							int startPlaceNb = Integer.parseInt(StartController.getStartPlace().split("-")[0]);
							if (onet.getPre().contains(n.findPlace(startPlaceNb))) {
								timeStartPlace = rpnc.getCurrentTime();
								// startPlaceTime.put(positionToken.get(onet.getPre().get(0).getNumber()),timeStartPlace);
								startList[positionToken.get(onet.getPre().get(0).getNumber())].add(timeStartPlace);
							}
							int endPlaceNb = Integer.parseInt(StartController.getEndPlace().split("-")[0]);
							if (onet.getPre().contains(n.findPlace(endPlaceNb))
									&& startList[positionToken.get(onet.getPre().get(0).getNumber())]
											.size() > endList[positionToken.get(onet.getPre().get(0).getNumber())]
													.size()) {
								timeEndPlace = rpnc.getCurrentTime();
								endList[positionToken.get(onet.getPre().get(0).getNumber())].add(timeEndPlace);
								// endPlaceTime.put(positionToken.get(onet.getPre().get(0).getNumber()),timeEndPlace);
								// timeBetweenTwoPlacesSum += timeEndPlace - timeStartPlace;
								// nbJourneyBetweenTwoPlaces++;
								// timeStartPlace = -1;
							}
							positionToken.remove(onet.getPre().get(0).getNumber());
						}
						n.discreteMove(onet, log, rpnc.getCurrentTime(), StartController.enableLogs.get());
						if (!this.console && logsItems.isSelected()) {
							logsTextString.setValue(logsTextString.getValue() + "\nFiring t" + onet.getNumber() + "x e"
									+ cte.getNumber() + " Successful");
						}

					} else if (!dmoveFired) { // No discrete move is urgent, so the move is a timed move
						// or a timed move followed by a place filling

						// No discrete move is urgent, so the move is a timed move
						// or a timed move followed by a place filling

						float minTableTimeElapse = ttConfig.getTableDelay();
						float MinNetElapse = StartController.getNetDelay();
						float delayControlFill = StartController.delayPFill();
						if (!this.console && logsItems.isSelected()) {
							logsTextString.setValue(logsTextString.getValue() + "\nTTelapse=" + minTableTimeElapse
									+ "\nNetelapse=" + MinNetElapse + "\nPFillelapse=" + delayControlFill);
						}
						float delayTmove = -3; // set to 0 for now

						if (delayControlFill == 0) { // some control places need to be filled now
							StartController.FillControlNow();
						} else {

							if (delayControlFill > 0) {

								delayTmove = delayControlFill;
							}

							if (minTableTimeElapse > 0) {
								if (delayTmove < 0) {
									delayTmove = minTableTimeElapse;
								} else {
									delayTmove = Math.min(minTableTimeElapse, delayControlFill);
								}
							}

							if (MinNetElapse > 0) {
								if (delayTmove < 0) {
									delayTmove = MinNetElapse;
								} else {
									delayTmove = Math.min(delayTmove, MinNetElapse);
								}
							}

						}
						if (!this.console && logsItems.isSelected()) {
							logsTextString.setValue(logsTextString.getValue() + "\nTimed Move: " + delayTmove);
						}
						// Ready to perform timed move
						// the timed move cannot be negative
						if (delayTmove <= 0) {
							delayTmove = 0;
							deadLock = true;
						}
						rpnc.advanceVerifiedTime(delayTmove);
						// ttConfig.timeMove((int) delayTmove);

						// It remains to fill the control places if new event reach their occurence date
						StartController.FillControlNow();

					}
				}
				if (deadLock)
					deadLocksNb++;
				delaySumTotal += delaySum;
				if (nbRealizedEvt > 0)
					statsString += "\nrun " + i + " - delay sum : " + delaySum + " - average delay : "
							+ (delaySum / nbRealizedEvt) + "\nFirst Delay : " + firstDelay + "\n";
				rpnc.reset();
			} else if (!StartController.isCorrespLoaded() && StartController.getConnectTT()
					&& !StartController.classModel.startsWith("EqualizationNet")) {
				logs.set("the correspondancies file must be loaded");
			} else if (StartController.classModel.startsWith("EqualizationNet")) {
				EquNet n = (EquNet) StartController.simu.getN();
				n.reset(false);
				float standardDeviation = Float.POSITIVE_INFINITY;
				int steps = 0;
				int limit = 1;
				try {
					limit = Integer.parseInt(StartController.getTargetEqualization());
					StartController.getLogsTextString().setValue("");
				} catch (NumberFormatException e) {
					e.printStackTrace();
					StartController.getLogsTextString()
							.setValue("Warning : the field next to steps equalization is not filled with a number");
				}

				while (standardDeviation > limit && steps != 10000) {
					simu.oneStep();
					standardDeviation = n.getStandardDeviation(StartController.getSimu().getPathLogs());
					if (n.isDiscreteMove()) {
						speedAvg += n.getLastTokenSpeed();
						speedNb++;
					}
					steps++;
				}

				if (steps != 10000) {
					stepsAvg += steps;
					standardDeviationAvg += standardDeviation;
					timeElapsedAvg += n.getTimeElapsed();
					discreteStepsAvg += n.getNbDiscreteSteps();
					minTime = Math.min(minTime, n.getTimeElapsed());
					maxTime = Math.max(maxTime, n.getTimeElapsed());
					minSteps = Math.min(minSteps, steps);
					maxSteps = Math.max(maxSteps, steps);
				} else
					deadLocksNb++;

			}
			timeBetweenTwoPlacesSum = 0;
			float difference = 0;
			nbJourneyBetweenTwoPlaces = 0;
			int nbTokens = StartController.simu.getN().nbTokens;// the number of marked places
			for (int j = 0; j < nbTokens; j++) {
				ArrayList<Float> start = startList[j];
				ArrayList<Float> end = endList[j];
				while (start.size() > 0 && end.size() > 0) {
					nbJourneyBetweenTwoPlaces++;
					//difference = endPlaceTime.get(0) - startPlaceTime.get(0);
					difference = end.get(0) - start.get(0);
					timeBetweenTwoPlacesSum += difference;
					//startPlaceTime.remove(0);
					//endPlaceTime.remove(0);
					start.remove(0);
					end.remove(0);
				}
			}
			sumDifferenceTime += timeBetweenTwoPlacesSum/nbJourneyBetweenTwoPlaces;
			if (!this.console)
				this.updateProgress(i+1, runsNb);
		}
		stats.set(statsString);
		for (Tag tag : listTags.keySet()) {
			if (listTags.get(tag)) {// si le tag est actif pour les stats
				tag.computeConfidenceInterval();
				stats.set(stats.get() + "\nAverage delay at tag " + tag.getName() + " : " + tag.getAverrageDelay()
						+ " / 95% confidence interval is between " + tag.getAlpha() + " and " + tag.getBeta());
			}
			tag.reset();
		}
		if (realizedEvtsTotal > 0)
			stats.set(stats.get() + "\nTotal average delay : " + delaySumTotal / realizedEvtsTotal);

		
		
		// if (startPlaceTime.size() > 0 && endPlaceTime.size() > 0)
		// difference = endPlaceTime.get(endPlaceTime.size() - 1) -
		// startPlaceTime.get(0);//
		// timeBetweenTwoPlacesSum += difference;//
		
		// System.out.println("start and end PlaceTime size: " + startPlaceTime.size() +
		// " - " + endPlaceTime.size());
		// System.out.println(startPlaceTime.get(0) + "-" +
		// endPlaceTime.get(endPlaceTime.size() - 1));
		// for (int i = 0; i < nbTokens; i++) {
		// ArrayList<Float> startTimes = new ArrayList<>();
		// startTimes = startPlaceTime.entrySet().stream().filter(e ->
		// e.getValue().equals(i)).collect(Collectors.toList());
		// }
		

		if (sumDifferenceTime > 0)
			stats.set(stats.get() + "\nAverage time between " + StartController.getStartPlace() + " and "
					+ StartController.getEndPlace() + " : " + (sumDifferenceTime / runsNb));

		stats.set(stats.get() + "\nNumber of lock : " + deadLocksNb);

		if (StartController.classModel.startsWith("EqualizationNet")) {
			stepsAvg = stepsAvg / (runsNb - deadLocksNb);
			standardDeviationAvg = standardDeviationAvg / (runsNb - deadLocksNb);
			timeElapsedAvg = timeElapsedAvg / (runsNb - deadLocksNb);
			if (speedNb != 0)
				speedAvg = speedAvg / speedNb;
			discreteStepsAvg = discreteStepsAvg / (runsNb - deadLocksNb);
			int limit = Integer.parseInt(StartController.getTargetEqualization());
			stats.set(stats.get() + "\n steps and time necessary to get to a standard deviation < " + limit);
			stats.set(stats.get() + "\n for " + runsNb + " runs : ");
			stats.set(stats.get() + "\n steps average : " + stepsAvg + " min steps : " + minSteps + " max steps : "
					+ maxSteps);
			stats.set(stats.get() + "\n discrete steps average : " + discreteStepsAvg);
			stats.set(stats.get() + "\n time elapsed average : " + timeElapsedAvg + " min time : " + minTime
					+ " max time : " + maxTime);
			EquNet n = (EquNet) StartController.simu.getN();
			stats.set(stats.get() + "\n speed average : " + n.getAvgSpeed());
		}
	}

}
