package fr.inria.mochy.statsAndTasks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.ui.LoadFiles;
import fr.inria.mochy.ui.MultipleTokensController;
import fr.inria.mochy.ui.StartController;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class MultipleTokensNbTask extends AbstractStats {
	/** the file url of the net to simulate */
	String netFileUrl = "";
	int tokensMin = MultipleTokensController.fromValue;
	int tokensMax = MultipleTokensController.toValue; // the maximum number of tokens which we want to get statistics
	int maxRuns = MultipleTokensController.runsValue; // the maximum number of runs we want to perform for each number
														// of tokens
	int standardDeviationMax = MultipleTokensController.standardDeviation; // the target of the standard deviation
																			// distance between the tokens
	int maxSteps = MultipleTokensController.stepsValue; // the limit of the number of steps
	boolean logs = MultipleTokensController.enableLogs;

	public MultipleTokensNbTask() {
		this.netFileUrl = StartController.getSimu().getFname();
	}

	public MultipleTokensNbTask(String netFileUrl) {
		this.netFileUrl = netFileUrl;
	}

	protected ArrayList[] call() throws IOException, Exception {
		ArrayList<Integer> x = new ArrayList<>();// the number of tokens
		ArrayList<Float> y = new ArrayList<>();// the average time elapsed
		ArrayList<Integer> y2 = new ArrayList<>();// the number of times, the max nb of steps is achieved
		ArrayList<Float> y3 = new ArrayList<>();//the average speed for the runs of a tokens number

		LoadFiles loadFiles = new LoadFiles();
		// loadFiles.initFolders();
		// netFileUrl = loadFiles.netsFolder+"\\CircleEquNetSantiago.net";
		// netFileUrl = loadFiles.netsFolder + "\\" + netFileUrl;

		FileWriter file = null;
		file = new FileWriter("logs/data.csv", false);
		file.write("tokens nb;average elapsed time;steps average;average speed;over steps nb;min speed;standard deviation avg after equalization;speed after equalization\n");
		
		FileWriter cloudDots = null;
		cloudDots = new FileWriter("logs/cloudDots.csv", false);
		cloudDots.write("tokens nb;elapsed time\n");

		for (int i = tokensMin; i <= tokensMax; i++) {
			if (isCancelled())
				break;
			this.updateMessage(i + " tokens on the run");
			String initial = "initial";
			for (int j = 1; j <= i; j++) {
				initial += ":" + j;
			}
			System.out.println(initial);

			// add the initial line to the input net file
			FileWriter fw = new FileWriter(netFileUrl, true);
			fw.write(initial);
			fw.close();

			// generate the net file
			loadFiles.generateSimu(netFileUrl);
			Sim simu = StartController.getSimu();
			simu.setEnableLogs(logs);
			EquNet n = (EquNet) simu.getN();
			float timeElapsedSum = 0;
			int nb = 0;
			int overTarget = 0;
			int stepsSum = 0;
			float speedSum = 0;
			int discreteSteps = 0;
			float minSpeed = Float.MAX_VALUE;

			// launch the runs and get the stats
			for (int runs = 0; runs < maxRuns; runs++) {
				if (isCancelled()) {
					break;
				}
				n.reset(false);
				int steps = 0;
				float standardDeviation = Float.POSITIVE_INFINITY;
				while (standardDeviation > standardDeviationMax && steps != maxSteps) {
					if (isCancelled()) {
						break;
					}
					simu.oneStep();
					standardDeviation = n.getStandardDeviation(StartController.getSimu().getPathLogs());
					if (n.isDiscreteMove() && n.getLastTokenSpeed() != 0f) {
						//speedSum += n.getLastTokenSpeed();
						discreteSteps++;
						minSpeed = Math.min(n.getLastTokenSpeed(), minSpeed);
					}
					steps++;
				}
				if (steps != maxSteps) {
					timeElapsedSum += n.getTimeElapsed();
					nb++;
					stepsSum += steps;
					cloudDots.write(i+";"+Float.toString(n.getTimeElapsed()).replace(".", ",")+"\n");
					speedSum += n.getAvgSpeed();
				} else
					overTarget++;
			}

			float averageTimeElapsed;
			float averageNbSteps;
			float averageSpeed;

			if (nb != 0) {
				averageTimeElapsed = timeElapsedSum / nb;
				averageNbSteps = stepsSum / nb;
				averageSpeed = speedSum / nb;
			} else {
				averageTimeElapsed = 0;
				averageNbSteps = 0;
				averageSpeed = 0;
			}

			/*if (maxRuns == 0)
				averageSpeed = 0;
			else
				averageSpeed = speedSum / maxRuns;*/
			
			/**check the average speed after an equalization*/
			//StartController.setDisplayChart(false);
			//float standardDeviationAvg = StartController.launchStepsEqu(10000);
			//StartController.setDisplayChart(true);
			float standardDeviationAvg = 0;
			for (int j = 0; j < 1000; j++) {
				StartController.simu.oneStep();
				standardDeviationAvg += n.getStandardDeviation();
			}
			standardDeviationAvg = standardDeviationAvg / 1000;

			try {
				file.write(i + ";" + String.valueOf(averageTimeElapsed).replace(".", ",") + ";"
						+ String.valueOf(averageNbSteps).replace(".", ",") + ";"
						+ String.valueOf(averageSpeed).replace(".", ",") + ";" + overTarget + ";" + 
						String.valueOf(minSpeed).replace(".", ",")+";"+
						String.valueOf(standardDeviationAvg).replace(".", ",")+";"
						+String.valueOf(n.getAvgSpeed()).replace(".", ",")+"\n");
			} catch (IOException e) {
				e.printStackTrace();
			}

			x.add(i);
			y.add(averageTimeElapsed);
			y2.add(overTarget);
			y3.add(averageSpeed);

			// delete the initial line to the input file
			eraseLastLine();

			this.updateProgress(i, tokensMax);
		}

		file.close();
		cloudDots.close();

		ArrayList[] results = {x, y, y2, y3};

		return results;
	}

	public void eraseLastLine() throws IOException {
		BufferedReader br = null;
		br = new BufferedReader(new FileReader(netFileUrl));

		// StringBuilder s = new StringBuilder();
		String file = "";
		String line = "";
		while (br.ready()) {
			line = br.readLine();
			// s.append(line+System.lineSeparator());
			file += line + "\n";
		}
		String string = file.replace(line + "\n", "");
		FileWriter fWriter = new FileWriter(netFileUrl);
		fWriter.write(string);
		fWriter.close();
	}

}
