package fr.inria.mochy.statsAndTasks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Stream;

import org.neuroph.core.NeuralNetwork;

import fr.inria.mochy.core.equalization.EquNetNeural;
import fr.inria.mochy.core.equalization.EquNetNeuralMov;
import fr.inria.mochy.ui.OptimizeNeuralController;
import fr.inria.mochy.ui.StartController;

/**
 * add/remove 0.1 to the weights and save the new network if it has better
 * results called from OptimizeNeuralController and
 * MultipleRunsController.equalizationFullStats
 */
public class OptimizeNeuralNetTask extends AbstractStats {

	@Override
	protected Object call() throws Exception {
		int standardDeviationTarget = OptimizeNeuralController.standardDeviationTarget;
		int maxSteps = OptimizeNeuralController.maxSteps;
		int runs = OptimizeNeuralController.runs;
		float originalTimeElapsed = 0;
		//float avgSpeedAfterEqu = 0;
		ArrayList<Float> speedAfterEqu = new ArrayList<>();
		int runsInTarget = 0;
		File file = new File("neural/data.csv");
		try {
			file.createNewFile();// create new file
			FileWriter writer = new FileWriter(file);
			writer.write(
					"file;time elapsed;speed;time elapsed+ beta * |target speed - average speed|; runs out of target; speedAfterEqu\n");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		EquNetNeural net = (EquNetNeural) StartController.simu.getN();
		NeuralNetwork nnet = net.getNnet();

		// get the original average time elapsed
		//for (int k = 5; k <= 50; k += 5) {
			for (int i = 0; i < runs; i++) {
				int steps = 0;
				net.reset(false);
				//net.setBunchingState(k);
				net.resetSpeedData();
				while (steps < maxSteps && net.getStandardDeviation("") > standardDeviationTarget) {
					StartController.simu.oneStep();
					steps++;
				}
				if (steps != maxSteps) {
					originalTimeElapsed += net.getTimeElapsed();
					/********/
					net.resetSpeedData();
					for (int l = 0; l < 1000; l++)
						StartController.simu.oneStep();
					//avgSpeedAfterEqu += net.getAvgSpeed();
					speedAfterEqu.add(net.getAvgSpeed());
					/********/
					runsInTarget++;
				}
			}
		//}
		// we add the beta*|targetSpeed - avgSpeed| to the calcul of the time as we want
		// to be
		// as near possible of the target speed
		// beta and targetSpeed can be filled in the input file
		System.out.println("original time elapsed : " + originalTimeElapsed / runsInTarget);
		//avgSpeedAfterEqu = avgSpeedAfterEqu / runsInTarget;
		originalTimeElapsed = (originalTimeElapsed / runsInTarget)
				+ net.getBeta() * calculVariance(speedAfterEqu, net.getTargetSpeed());

		// add/remove 0.1 to each weight of the neural network and check if
		// it gives a better configuration
		this.updateProgress(0, nnet.getWeights().length);
		for (int i = 0; i < nnet.getWeights().length; i++) {
			if (isCancelled())
				break;
			this.updateMessage("weight n°" + i + "/" + nnet.getWeights().length);

			// add 0.1 to the weight
			NeuralNetwork mutantPlus = NeuralNetwork.createFromFile(net.getNnetPath());
			double[] weights = Stream.of(mutantPlus.getWeights()).mapToDouble(Double::doubleValue).toArray();
			weights[i] += 0.1;
			mutantPlus.setWeights(weights);
			net.setNeuralNetwork(mutantPlus);
			// get the new average time elapsed
			float value = 0;
			runsInTarget = 0;
			float averageSpeed = 0;
			//avgSpeedAfterEqu = 0;
			speedAfterEqu = new ArrayList<>();
			//for (int k = 5; k <= 50; k += 5) {
				for (int j = 0; j < runs; j++) {
					int steps = 0;
					net.reset(false);
					//net.setBunchingState(k);
					net.resetSpeedData();
					while (steps < maxSteps && net.getStandardDeviation("") > standardDeviationTarget) {
						StartController.simu.oneStep();
						steps++;
					}
					if (steps != maxSteps) {
						value += net.getTimeElapsed();
						/********/
						net.resetSpeedData();
						for (int l = 0; l < 1000; l++)
							StartController.simu.oneStep();
						//avgSpeedAfterEqu += net.getAvgSpeed();
						speedAfterEqu.add(net.getAvgSpeed());
						/********/
						runsInTarget++;
					}
				}
			//}
			float time = 0;
			if (runsInTarget != 0) {
				time = value / runsInTarget;
				//avgSpeedAfterEqu = avgSpeedAfterEqu / runsInTarget;
				value = time
						+ net.getBeta() * calculVariance(speedAfterEqu, net.getTargetSpeed());
				averageSpeed = net.getAvgSpeed();
			}
			if (runsInTarget != 0 && value < originalTimeElapsed) {
				System.out.println("add to weight " + i);
				originalTimeElapsed = value;
				System.out.println("time : " + time + " value : " + value);
				net.resetSpeedData();
				for (int j = 0; j < 1000; j++) {
					StartController.simu.oneStep();
				}
				net.saveNeuralNetwork("neural/data.csv", runs - runsInTarget, value, averageSpeed, time, true);
			}

			// remove 0.1 to the weight
			NeuralNetwork mutantMoins = NeuralNetwork.createFromFile(net.getNnetPath());
			weights = Stream.of(mutantMoins.getWeights()).mapToDouble(Double::doubleValue).toArray();
			weights[i] -= 0.1;
			mutantMoins.setWeights(weights);
			net.setNeuralNetwork(mutantMoins);
			// get the new average time elapsed
			value = 0;
			runsInTarget = 0;
			averageSpeed = 0;
			speedAfterEqu = new ArrayList<>();
			//avgSpeedAfterEqu = 0;
			float minSpeed = Float.POSITIVE_INFINITY;
			//for (int k = 5; k <= 50; k += 5) {
				for (int j = 0; j < runs; j++) {
					int steps = 0;
					net.reset(false);
					//net.setBunchingState(k);
					net.resetSpeedData();
					while (steps < maxSteps && net.getStandardDeviation("") > standardDeviationTarget) {
						StartController.simu.oneStep();
						steps++;
						if (net.getCurrentMinSpeed() != 0)
							minSpeed = Math.min(minSpeed, net.getCurrentMinSpeed());
					}
					if (steps != maxSteps) {
						value += net.getTimeElapsed();
						/********/
						net.resetSpeedData();
						for (int l = 0; l < 1000; l++)
							StartController.simu.oneStep();
						speedAfterEqu.add(net.getAvgSpeed());
						//avgSpeedAfterEqu += net.getAvgSpeed();
						/********/
						runsInTarget++;
					}
				}
			//}
			if (runsInTarget != 0) {
				time = value / runsInTarget;
				//avgSpeedAfterEqu = avgSpeedAfterEqu / runsInTarget;
				value = time
						+ net.getBeta() * calculVariance(speedAfterEqu, net.getTargetSpeed());
				averageSpeed = net.getAvgSpeed();
			}
			if (runsInTarget != 0 && value < originalTimeElapsed) {
				System.out.println("remove to weight " + i);
				originalTimeElapsed = value;
				System.out.println("time : " + time + " value : " + value + " minspeed : " + minSpeed);
				net.resetSpeedData();
				for (int j = 0; j < 1000; j++) {
					StartController.simu.oneStep();
				}
				net.saveNeuralNetwork("neural/data.csv", runs - runsInTarget, value, averageSpeed, time, true);
			}

			this.updateProgress(i, nnet.getWeights().length);
		}
		return null;
	}

	float calculVariance(ArrayList<Float> data, float targetSpeed) {
		float squareSum = 0;
		for (float value : data)
			squareSum += Math.pow(value - targetSpeed, 2);
		float var = (float) squareSum / data.size();
		return var;
	}

}
