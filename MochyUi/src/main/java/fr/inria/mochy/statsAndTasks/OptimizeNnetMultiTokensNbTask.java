package fr.inria.mochy.statsAndTasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.stream.Stream;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.util.TransferFunctionType;

import fr.inria.mochy.core.equalization.EquNetNeural;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.ui.LoadFiles;
import fr.inria.mochy.ui.OptimizeNeuralMultiTokensNbController;
import fr.inria.mochy.ui.StartController;

public class OptimizeNnetMultiTokensNbTask extends AbstractStats {

	String netFileUrl;
	Sim simu;
	EquNetNeural net;
	int tokensMin = OptimizeNeuralMultiTokensNbController.tokMin;
	int tokensMax = OptimizeNeuralMultiTokensNbController.tokMax;
	int tokensInterval = OptimizeNeuralMultiTokensNbController.tokInterval;
	int runs = OptimizeNeuralMultiTokensNbController.runs;
	int maxSteps = OptimizeNeuralMultiTokensNbController.maxSteps;
	int standardDeviationTarget = OptimizeNeuralMultiTokensNbController.standardDeviationTarget;
	float speedAfterEqu;
	float timeElapsed;
	float avgSpeed;
	boolean outOfTarget = false;

	@Override
	protected Object call() throws Exception {
		LoadFiles loadFiles = new LoadFiles();
		netFileUrl = StartController.getSimu().getFname();
		EquNetNeural net = (EquNetNeural) StartController.simu.getN();
		NeuralNetwork nnet = net.getNnet();
		
		boolean outOfMaxSteps = false;
		
		float avgSpeedAfterEqu = 0;
		int speedDiffAfterEqu = 20;
		int nb = 0; // used to calculate the average avg

		File file = new File("neural/data.csv");
		if (file.exists())
			file.delete();
		file.createNewFile();// create new file
		FileWriter writer = new FileWriter(file);
		writer.write("file;time elapsed;average speed;speedAfterEqu;outOfTargetNb;;\n");
		writer.close();

		// get the original average time elapsed
		simMultiTokensNb(nnet);
		float originalSpeedAfterEqu = speedAfterEqu;
		float originalTimeElapsed = timeElapsed;
		
		// loop on the weights of the neural net
		this.updateProgress(0, nnet.getWeights().length);
		for (int i = 0; i < nnet.getWeights().length; i++) {
			if (isCancelled()) {
				eraseLastLine();
				break;
			}
			this.updateMessage("weight n°" + i + "/" + nnet.getWeights().length);

			// add 0.1 to the weight
			NeuralNetwork mutantPlus = NeuralNetwork.createFromFile(net.getNnetPath());
			double[] weights = Stream.of(mutantPlus.getWeights()).mapToDouble(Double::doubleValue).toArray();
			if (weights[i] < 0.90)
				weights[i] += 0.1;
			else
				weights[i] = 1;
			mutantPlus.setWeights(weights);
			
			simMultiTokensNb(mutantPlus);
			System.out.println("values : " + timeElapsed+"/"+originalTimeElapsed+"-"+speedAfterEqu+"/"+originalSpeedAfterEqu+outOfTarget);
			if (timeElapsed < originalTimeElapsed && (speedAfterEqu >= net.getTargetSpeed() || speedAfterEqu >= originalSpeedAfterEqu) && !outOfTarget) {
				System.out.println("-> add to weight " + i);
				System.out.println("values : " + timeElapsed+"/"+speedAfterEqu);
				originalTimeElapsed = timeElapsed;
				originalSpeedAfterEqu = speedAfterEqu;
				net.saveNeuralNetwork("neural/data.csv", 0, speedAfterEqu, avgSpeed, originalTimeElapsed, true);
			}
			outOfTarget = false;
			
			//remove 0.1 to the weight
			NeuralNetwork mutantMoins = NeuralNetwork.createFromFile(net.getNnetPath());
			weights = Stream.of(mutantMoins.getWeights()).mapToDouble(Double::doubleValue).toArray();
			if (weights[i] > -0.90)
				weights[i] -= 0.1;
			else
				weights[i] = - 1;
			mutantMoins.setWeights(weights);
			
			simMultiTokensNb(mutantMoins);
			
			if (timeElapsed < originalTimeElapsed && (speedAfterEqu >= net.getTargetSpeed() || speedAfterEqu >= originalSpeedAfterEqu) && !outOfTarget) {
				System.out.println("-> remove to weight " + i);
				System.out.println("values : " + timeElapsed+"/"+speedAfterEqu);
				originalTimeElapsed = timeElapsed;
				originalSpeedAfterEqu = speedAfterEqu;
				net.saveNeuralNetwork("neural/data.csv", 0, speedAfterEqu, avgSpeed, originalTimeElapsed, true);
			}
			outOfTarget = false;

			this.updateProgress(i, nnet.getWeights().length);
		}

		return null;
	}

	void simMultiTokensNb(NeuralNetwork nnet) throws IOException {
		LoadFiles loadFiles = new LoadFiles();
		timeElapsed = 0;
		avgSpeed = 0;
		speedAfterEqu = 0;
		
		//loop on the nb of tokens
		outerloop: for (int tokensInitial = tokensMin; tokensInitial <= tokensMax; tokensInitial += tokensInterval) {
			//generate the initial line
			String initial = "initial:";
			for (int i = 1; i < tokensInitial; i++) {
				initial += i + ":";
			}
			initial += tokensInitial;

			// add the initial line to the input net file
			FileWriter fw = new FileWriter(netFileUrl, true);
			fw.write(initial);
			fw.close();

			// generate the net file
			loadFiles.generateSimu(netFileUrl);
			simu = StartController.getSimu();
			net = (EquNetNeural) simu.getN();
			net.setNeuralNetwork(nnet);

			//launch the simulation on multiple runs
			for (int i = 0; i < runs; i++) {
				int steps = 0;
				net.reset(false);
				while (steps < maxSteps && net.getStandardDeviation("") > standardDeviationTarget) {
					StartController.simu.oneStep();
					steps++;
				}
				if (steps == maxSteps) {
					outOfTarget = true;
					eraseLastLine();
					break outerloop;
				}
				timeElapsed += net.getTimeElapsed();
				avgSpeed += net.getAvgSpeed();
				net.resetSpeedData();
				for (int j = 0; j < 1000; j++) {
					StartController.simu.oneStep();
				}
				speedAfterEqu += net.getAvgSpeed();
			}
			eraseLastLine();
		}
		timeElapsed = timeElapsed / (runs * ((tokensMax - tokensMin + tokensInterval) / tokensInterval));
		avgSpeed = avgSpeed / (runs * ((tokensMax - tokensMin + tokensInterval) / tokensInterval));
		speedAfterEqu = speedAfterEqu / (runs * ((tokensMax - tokensMin + tokensInterval) / tokensInterval));
	}

	public void eraseLastLine() throws IOException {
		BufferedReader br = null;
		br = new BufferedReader(new FileReader(StartController.getSimu().getFname()));

		// StringBuilder s = new StringBuilder();
		String file = "";
		String line = "";
		while (br.ready()) {
			line = br.readLine();
			// s.append(line+System.lineSeparator());
			file += line + "\n";
		}
		String string = file.replace(line + "\n", "");
		FileWriter fWriter = new FileWriter(StartController.getSimu().getFname());
		fWriter.write(string);
		fWriter.close();
	}

}
