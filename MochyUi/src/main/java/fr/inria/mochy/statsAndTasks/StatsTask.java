package fr.inria.mochy.statsAndTasks;

import java.io.FileWriter;
import java.io.IOException;

import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.equalization.EquPlace;
import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.ui.AlphasNoisesController;
import fr.inria.mochy.ui.StartController;
import fr.inria.mochy.ui.View;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;

public class StatsTask extends AbstractStats{

	@Override
	protected Object call() throws Exception {
		StringProperty stats = StartController.getStatsTextString();
		stats.set("");
		Sim simu = StartController.getSimu();
		EquNet n = (EquNet) simu.getN();
		float savedNoise = n.getRANGE_NOISE();
		float savedAlpha = n.getALPHA();
		int limit = AlphasNoisesController.standardDeviation;
		int runs = AlphasNoisesController.run;
		boolean logs = AlphasNoisesController.enableLogs;
		simu.setEnableLogs(logs);
		int maxStepsPerRun = 5000;
		/****************************/
		/**values of noise and alpha*/
		/****************************/
		float[] noise = {0, 1};
		float[] alpha = {0, 0.5f, 1f, 1.5f, 2, 2.5f, 3, 3.5f, 4, 4.5f, 5};
		/*alpha = new float[51];
		float value = 0;
		for (int i = 0; i < alpha.length; i++) {
			alpha[i] = value;
			value += 0.1;
		}
		alpha = new float[1];alpha[0]=1;*/
		alpha = AlphasNoisesController.alpha;
		noise = AlphasNoisesController.noise;
		/****************************/
		/****************************/
		float[][] distDataProp = new float[noise.length][alpha.length];
		
		FileWriter file = null;
		FileWriter deadLockFile = null;
		try {
			file = new FileWriter("logs/data.csv", false);
			deadLockFile = new FileWriter("logs/deadlock.csv", false);
			file.write("alpha;steps;steps speed<300;time elapsed;time speed<300;average speed;min speed;max speed\n");
			deadLockFile.write("alpha;deadlockNb\n");
		}catch (IOException e) {
			e.printStackTrace();
		}
		/*FileWriter fileLogs = null;
		FileWriter fileLogs2 = null;
		FileWriter fileLogs3 = null;
		try {
			fileLogs = new FileWriter("logs/dataSteps.csv", false);
			fileLogs.write("alpha;0-200;200-400;400-600;600-800;800-1000\n");
			fileLogs2 = new FileWriter("logs/dataTime.csv", false);
			fileLogs2.write("alpha;0-200;200-400;400-600;600-800;800-1000\n");
			fileLogs3 = new FileWriter("logs/dataNb.csv", false);
			fileLogs3.write("alpha;0-200;200-400;400-600;600-800;800-1000\n");
		} catch (IOException e1) {
			e1.printStackTrace();
		}*/
		int l = 0;
		this.updateProgress(l, noise.length*alpha.length);
		outerloop:
		for (int i = 0; i < noise.length; i++) {
			for (int k = 0; k < alpha.length; k++) {
				//System.out.println("noise index " + i + " alpha index : " + k);
				this.updateMessage("noise index " + i + " alpha index : " + k);
				int deadLocksNb = 0;
				float stepsAvg = 0;
				float standardDeviationAvg = 0;
				float timeElapsedAvg = 0;
				float discreteStepsAvg = 0;
				float minTime = Float.MAX_VALUE;
				float maxTime = 0;
				int minSteps = Integer.MAX_VALUE;
				int maxSteps = 0;
				int stepsUnder300 = 0;
				int stepsOver300 = 0;
				float speedAverageUnder300Steps = 0;
				float speedAverageOver300Steps = 0;
				float speedAverage = 0;
				/*int steps0200 = 0;
				int steps200400 = 0;
				int steps400600 = 0;
				int steps600800 = 0;
				int steps8001000 = 0;
				float time0200 = 0;
				float time200400 = 0;
				float time400600 = 0;
				float time600800 = 0;
				float time8001000 = 0;
				int nb0200 = 0;
				int nb200400 = 0;
				int nb400600 = 0;
				int nb600800 = 0;
				int nb8001000 = 0;*/				
				n.setRANGE_NOISE(noise[i]);
				n.setALPHA(alpha[k]);
				for (int j = 0; j < runs; j++) {
					n.reset(false);
					float standardDeviation = Float.POSITIVE_INFINITY;
					int steps = 0;
					//int limit = Integer.MAX_VALUE;
					float speed = 0;
					int discreteSteps = 0;
					float minSpeed = Float.POSITIVE_INFINITY;
					float maxSpeed = 0;
					/*try {
						limit = Integer.parseInt(StartController.getTargetEqualization());
						StartController.getLogsTextString().setValue("");
					}catch (NumberFormatException e) {
						e.printStackTrace();
						StartController.getLogsTextString().setValue("Warning : the field near to steps equalization is not filled with a number.");
					}*/
					//System.out.println("noise index " + i + "alpha index : " + k + " run : "+j);
					//launch a run while a targeted standard deviation (in the UI on the right of steps equalization)
					//is not reached and before a limit number of steps
					while (standardDeviation > limit && steps != maxStepsPerRun) {
						if (isCancelled()) {
							break outerloop;
						}
						simu.oneStep();
						standardDeviation = n.getStandardDeviation(StartController.getSimu().getPathLogs());
						if (n.isDiscreteMove() && n.getLastTokenSpeed() != 0f) {
							speedAverage += n.getLastTokenSpeed();
							speed += n.getLastTokenSpeed();
							minSpeed = Math.min(minSpeed, n.getLastTokenSpeed());
							maxSpeed = Math.max(maxSpeed, n.getLastTokenSpeed());
							/*if (n.getLastTokenSpeed() < 10) {
								System.out.println("!!speed<10!! run:"+j+" step:"+steps);
								for (EquPlace p : n.places.values()) {
									for (Token token : p.getTokens())
										System.out.println("token in place "+p.getName()+" position : "+token.getxPlace()+"/"+token.getXTotal()+ " speed : "+token.getSpeed()+" ttb : "+token.getTimeToBrowse()+" blocked? "+token.isBlocked());
								}
							}*/
							if (n.getLastTokenTtb() > 1000000) {
								System.out.println("!!ttb>1 000 000!! run:"+j+" step:"+steps);
								for (EquPlace p : n.places.values()) {
									for (Token token : p.getTokens())
										System.out.println("token in place "+p.getName()+" position : "+token.getxPlace()+"/"+token.getXTotal()+ " speed : "+token.getSpeed()+" ttb : "+token.getTimeToBrowse()+" blocked? "+token.isBlocked());
								}
							}
							discreteSteps ++;
							if (steps <= 300) {
								speedAverageUnder300Steps += n.getLastTokenSpeed();
								stepsUnder300 ++;
							} else {
								speedAverageOver300Steps += n.getLastTokenSpeed();
								stepsOver300 ++;
							}
						}
						steps++;
					}
					/*if (speed/discreteSteps<400) {
						System.out.println("==speed low : "+speed/discreteSteps+" steps : "+steps+" time : "+n.getTimeElapsed());
					}else
						System.out.println("==speed correct : "+speed/discreteSteps+" steps : "+steps+" time : "+n.getTimeElapsed());
*/
					if (steps != maxStepsPerRun) {
						/*if (steps >=0 && steps < 200) {
							steps0200 += steps;
							time0200 += n.getTimeElapsed();
							nb0200++;
						}else if (steps >= 200 && steps < 400) {
							steps200400 += steps;
							time200400 += n.getTimeElapsed();
							nb200400++;
						}else if (steps >= 400 && steps < 600) {
							steps400600 += steps;
							time400600 += n.getTimeElapsed();
							nb400600++;
						}else if (steps >= 600 && steps < 800) {
							steps600800 += steps;
							time600800 += n.getTimeElapsed();
							nb600800++;
						}else if (steps >= 800 && steps < 1000) {
							steps8001000 += steps;
							time8001000 += n.getTimeElapsed();
							nb8001000++;
						}*/
						
						stepsAvg += steps;
						standardDeviationAvg += standardDeviation;
						timeElapsedAvg += n.getTimeElapsed();
						discreteStepsAvg += n.getNbDiscreteSteps();
						minTime = Math.min(minTime, n.getTimeElapsed());
						maxTime = Math.max(maxTime, n.getTimeElapsed());
						minSteps = Math.min(minSteps, steps);
						maxSteps = Math.max(maxSteps, steps);
						speed = speed / discreteSteps;
						try {
							if (speed < 300) {
								file.write(String.valueOf(alpha[k]).replace(".", ",")+";"+
									String.valueOf(steps).replace(".", ",")+";"+
									String.valueOf(steps).replace(".", ",")+";"+
									String.valueOf(n.getTimeElapsed()).replace(".", ",")+";"+
									String.valueOf(n.getTimeElapsed()).replace(".", ",")+";"+
									String.valueOf(speed).replace(".", ",")+";"+
									String.valueOf(minSpeed).replace(".", ",")+";"+
									String.valueOf(maxSpeed).replace(".", ",")+"\n");
							}else {
								file.write(String.valueOf(alpha[k]).replace(".", ",")+";"+
										String.valueOf(steps).replace(".", ",")+";"+
										"=NA();"+
										String.valueOf(n.getTimeElapsed()).replace(".", ",")+";"+
										"=NA();"+
										String.valueOf(speed).replace(".", ",")+";"+
										String.valueOf(minSpeed).replace(".", ",")+";"+
										String.valueOf(maxSpeed).replace(".", ",")+"\n");
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						deadLocksNb++;
						System.out.println("!!deadlock!! run:"+j+" step:"+steps);
						for (EquPlace p : n.places.values()) {
							for (Token token : p.getTokens())
								System.out.println("token in place "+p.getName()+" position : "+token.getxPlace()+"/"+token.getXTotal()+ " speed : "+token.getSpeed()+" ttb : "+token.getTimeToBrowse()+" blocked? "+token.isBlocked());
						}
					}
				}
				try {
					deadLockFile.write(String.valueOf(alpha[k]).replace(".", ",")+";"+deadLocksNb+"\n");
				}catch (IOException e) {
					e.printStackTrace();
				}
				speedAverage = speedAverage / (stepsUnder300 + stepsOver300);
				if (stepsUnder300 != 0)
					speedAverageUnder300Steps = speedAverageUnder300Steps / stepsUnder300;
				if (stepsOver300 != 0)	
					speedAverageOver300Steps = speedAverageOver300Steps / stepsOver300;
				stepsAvg = stepsAvg / (runs - deadLocksNb);
				standardDeviationAvg = standardDeviationAvg / (runs - deadLocksNb);
				timeElapsedAvg = timeElapsedAvg / (runs - deadLocksNb);
				discreteStepsAvg = discreteStepsAvg / (runs - deadLocksNb);
				//int limit = Integer.parseInt(StartController.getTargetEqualization());
				stats.set(stats.get() + "\n steps and time necessary to get to a standard deviation < " + limit
						+ "("+runs+" runs)");
				stats.set(stats.get() + "\n - noise : " + noise[i] + " alpha : " + alpha[k]);
				stats.set(stats.get() + "\n steps average : " + stepsAvg + " min steps : " + minSteps + 
						" max steps : " + maxSteps);
				stats.set(stats.get() + "\n discrete steps average : " + discreteStepsAvg);
				stats.set(stats.get() + "\n time elapsed average : " + timeElapsedAvg + " min time : " + minTime + 
						" max time : " + maxTime);
				stats.set(stats.get() + "\nspeed average = " + speedAverage);
				stats.set(stats.get() + "\n deadlocksNb : " + deadLocksNb);
				distDataProp[i][k] = timeElapsedAvg;
				/*try {
					//fileLogs.write(noise[i]+";"+alpha[k]+";"+timeElapsedAvg+"\n");
					fileLogs.write(String.valueOf(alpha[k]).replace(".", ",")+";"+
							String.valueOf(stepsAvg).replace(".", ",")+";"+
							String.valueOf(minSteps).replace(".", ",")+";"+
							String.valueOf(maxSteps).replace(".", ",")+";"+
							String.valueOf(timeElapsedAvg).replace(".", ",")+";"+
							String.valueOf(minTime).replace(".", ",")+";"+
							String.valueOf(maxTime).replace(".", ",")+";"+
							String.valueOf(speedAverage).replace(".", ",")+";"+
							String.valueOf(speedAverageUnder300Steps).replace(".", ",")+";"+
							String.valueOf(speedAverageOver300Steps).replace(".", ",")+"\n");
					if (nb0200 == 0)
						nb0200 = 1;
					if (nb200400 == 0)
						nb200400 = 1;
					if (nb400600 == 0)
						nb400600 = 1;
					if (nb600800 == 0)
						nb600800 = 1;
					if (nb8001000 == 0)
						nb8001000 = 1;
					fileLogs.write(String.valueOf(alpha[k]).replace(".", ",")+";"+
							String.valueOf(steps0200/nb0200).replace(".",",")+";"+
							String.valueOf(steps200400/nb200400).replace(".",",")+";"+
							String.valueOf(steps400600/nb400600).replace(".",",")+";"+
							String.valueOf(steps600800/nb600800).replace(".",",")+";"+
							String.valueOf(steps8001000/nb8001000).replace(".",",")+"\n");
					fileLogs2.write(String.valueOf(alpha[k]).replace(".", ",")+";"+
							String.valueOf(time0200/nb0200).replace(".",",")+";"+
							String.valueOf(time200400/nb200400).replace(".",",")+";"+
							String.valueOf(time400600/nb400600).replace(".",",")+";"+
							String.valueOf(time600800/nb600800).replace(".",",")+";"+
							String.valueOf(time8001000/nb8001000).replace(".",",")+"\n");
					fileLogs3.write(String.valueOf(alpha[k]).replace(".", ",")+";"+
							String.valueOf(nb0200).replace(".",",")+";"+
							String.valueOf(nb200400).replace(".",",")+";"+
							String.valueOf(nb400600).replace(".",",")+";"+
							String.valueOf(nb600800).replace(".",",")+";"+
							String.valueOf(nb8001000).replace(".",",")+"\n");
				} catch (IOException e) {
					e.printStackTrace();
				}*/
				l++;
				this.updateProgress(l, noise.length*alpha.length);
			}
		}
		System.out.println("done");
		n.setALPHA(savedAlpha);
		n.setRANGE_NOISE(savedNoise);
		
		/*try {
			fileLogs.close();
			fileLogs2.close();
			fileLogs3.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		try {
			file.close();
			deadLockFile.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
		View.display3DChart(distDataProp, noise, alpha);
		return null;
	}

}
