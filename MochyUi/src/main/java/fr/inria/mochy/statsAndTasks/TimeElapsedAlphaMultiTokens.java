package fr.inria.mochy.statsAndTasks;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;

import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.equalization.EquNetNeural;
import fr.inria.mochy.core.equalization.EquNetNeuralMov;
import fr.inria.mochy.core.equalization.EquNetV2Mov;
import fr.inria.mochy.ui.FullNeuralNetPipelineController;

public class TimeElapsedAlphaMultiTokens extends AbstractStats{
	EquNet n;
	int standardDeviationTarget;
	int maximumSteps;
	int startTokensNb;
	int endTokensNb;
	int stepTokens;
	int runsNb = 100;
	float startAlpha = (float) 0;
	float endAlpha = (float) 0;
	float stepAlpha = (float) 1;
	int outOfTarget = 0;

	@Override
	protected Object call() throws Exception {
		n = (EquNet) getSimu().getN();
		/*EquNetNeural net = (EquNetNeural) n;
		System.out.println("inputs : "+net.nnet.getInputsCount());*/
		standardDeviationTarget = 300;
		maximumSteps = 20000;
		startTokensNb = 5;
		endTokensNb = 50;
		stepTokens = 5;
		open();
		write("alpha;tokens nb;time elapsed;speed during equ.;speed after equ.;out of target;dispersion during equ;dispersion after equ;");
		//write ("alpha;standard deviation");
		breakLine();
		this.updateProgress(0, endAlpha - startAlpha);
		for (float alpha = startAlpha; alpha <= endAlpha; alpha += stepAlpha) {
			this.updateMessage("alpha = "+Float.toString(alpha));
			n.setALPHA(alpha);
			float timeElapsed = launchSim(alpha);
			/*write(alpha);
			if (timeElapsed == Float.POSITIVE_INFINITY)
				write("max steps");
			else
				write(timeElapsed);
			write(outOfTarget);
			breakLine();*/
			this.updateProgress(alpha - startAlpha, endAlpha - startAlpha);
			if (this.isCancelled())
				break;
		}
		close();
		this.updateMessage("Done");
		return null;
	}

	/**
	 * launch the simulation with the neural net associated to the net return the
	 * value calculated : time elapsed + beta * |average speed - target speed | +
	 * beta * |average speed after equalization - target speed|
	 */
	Float launchSim(float alpha) throws Exception{
		float value = Float.POSITIVE_INFINITY;
		float timeElapsed = 0;
		int nb = 0;// number of times that the sim get into the standard deviation target
		outOfTarget = 0;
		//ArrayList<Float> data = new ArrayList<>();
		// loop on the number of tokens
		for (int j = startTokensNb; j <= endTokensNb; j += stepTokens) {
			// loop on the number of runs
			//System.out.println("tokens : "+j+"-"+startTokensNb+"-"+endTokensNb+"-"+stepTokens);
			outOfTarget = 0;
			timeElapsed = 0;
			float speedDuringEqu = 0;
			float speedAfterEqu = 0;
			ArrayList<Float> dispersionDuringEqu = new ArrayList<>();
			ArrayList<Float> dispersionAfterEqu = new ArrayList<>();
			nb = 0;
			int steps = 0;
			for (int i = 1; i <= runsNb; i++) {
				n.setBunchingState(j);
				n.resetSpeedData();
				//System.out.println("tokens : "+j+" - "+"run = "+i);
				//n.drop();
				// loop until equalization or max steps number
				//System.out.println(n.getStandardDeviation()+"/"+standardDeviationTarget+" - "+n.getStepsNb()+"/"+maximumSteps);
				while (n.getStandardDeviation() > standardDeviationTarget && n.getStepsNb() < maximumSteps) {
					getSimu().oneStep();
					for (int k = 0; k < j; k++) {
						if (n.getSpeed(k) != 0)
							dispersionDuringEqu.add(n.getSpeed(k));
					}
				}
				if (n.getStandardDeviation() <= standardDeviationTarget) {
					timeElapsed += n.getTimeElapsed();
					//data.add(n.getTimeElapsed());
					speedDuringEqu += n.getAvgSpeed();
					n.resetSpeedData();
					for (int k = 0; k < 1000; k++) {//loop to get the avg speed after equalization
						getSimu().oneStep();
						for (int l = 0; l < j; l++) {
							if (n.getSpeed(l) != 0)
								dispersionAfterEqu.add(n.getSpeed(l));
						}
					}
					speedAfterEqu += n.getAvgSpeed();
					nb++;
					//System.out.println("nb = "+nb+" - time elapsed = "+timeElapsed);
				} else
					outOfTarget ++;
				
			}
			write(alpha);
			write(j);//nb of tokens
			write(timeElapsed/nb);
			write(speedDuringEqu/nb);
			write(speedAfterEqu/nb);
			write(outOfTarget);
			write(calculDispersion(dispersionDuringEqu, 500));
			write(calculDispersion(dispersionAfterEqu, 500));
			breakLine();
		}
		/*if (nb != 0) {
			value = timeElapsed / nb;
		}*/
		//value = calculStandardDeviation(data);
		return value;
	}
	
	float calculStandardDeviation(ArrayList<Float> data) {
		float sum = 0, avg;
		for (float value : data)
			sum += value;
		avg = sum / data.size();
		float squareSum = 0;
		for (float value : data)
			squareSum += Math.pow(Math.abs(value - avg), 2);
		float sd = (float) Math.sqrt(squareSum / data.size());
		return sd;
	}
	
	float calculDispersion(ArrayList<Float> data, float target) {
		float squareSum = 0;
		for (float value : data)
			squareSum += Math.pow(Math.abs(value - target), 2);
		float sd = (float) Math.sqrt(squareSum / data.size());
		//System.out.println(data.size());
		return sd;
	}
}
