package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

/**allow to fill the alpha and noises values to make multiple runs for each pair and get statistics*/
/**it is linked to StatsTask*/
/**it is called from "stats"/"Multi runs full stats" menu once an equalization net is loaded
 * it doesn't work with neural networks*/
public class AlphasNoisesController  implements Initializable{
	@FXML
	TextField noises;
	
	@FXML
	TextField alphas;
	
	@FXML
	TextField runs;
	
	@FXML
	TextField standardDev;
	
	@FXML
	CheckBox logs;
	
	public static float[] alpha;
	public static float[] noise;
	public static int run;
	public static int standardDeviation;
	public static boolean enableLogs;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		enableLogs = false;
		logs.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				enableLogs = newValue;
				StartController.enableLogs.set(newValue);
			}
		});
	}
	
	@FXML
	void run (ActionEvent evt) {
		String[] temp = alphas.getText().split(":");
		alpha = new float[temp.length];
		for (int i = 0; i < alpha.length; i++) {
			alpha[i] = Float.parseFloat(temp[i]);
		}
		
		temp = noises.getText().split(":");
		noise = new float[temp.length];
		for (int i = 0; i < noise.length; i++) {
			noise[i] = Float.parseFloat(temp[i]);
		}
		
		run = Integer.parseInt(runs.getText());
		
		standardDeviation = Integer.parseInt(standardDev.getText());
		
		MultipleRunsController controller = new MultipleRunsController();
		controller.equalizationFullStats(true, false, false, null);
	}

}
