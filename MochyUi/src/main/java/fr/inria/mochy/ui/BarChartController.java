package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.core.abstractClass.PhysicalModel;

//import org.apache.commons.math3.analysis.function.Gaussian;
//import org.apache.commons.math3.distribution.WeibullDistribution;

import fr.inria.mochy.core.dom.TransitionDom;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.sampler.Sampler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;

/**
 * Display the bar chart of the distribution as set in the net file for the clock of the transition.
 * There is three distributions : uniform, gauss and weibull
 * It is available from the chart button in the line of a transition.
 * <p>
 * <img src="BarChartController.svg" alt="BarChartController class diagram" style="float: right;">
 */
public class BarChartController implements Initializable {
	@FXML
	BarChart<String, Double> barChart;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		/** the transition which we want to display the probabilities of the clock */
		TransitionDom t = View.t;

		XYChart.Series<String, Double> serie = new XYChart.Series<String, Double>();
		double sampling = StartController.getSampling();

		boolean gaussian = false;
		boolean weibull = false;
		int weibullCoef = 0;
		PhysicalModel net = StartController.getSimu().getN();
		if (net.isGaussian())
			gaussian = true;
		if (net.isWeibull()) {
			weibull = true;
			weibullCoef = net.getWeibullCoef();
		}
		
		// if the uniform distribution is set
		if (!gaussian && !weibull && t.getSampler() == null) {
			if (t.getUpperBound() == null || sampling >= (t.getUpperBound() - t.getLowerBound()))
				serie.getData().add(new XYChart.Data<String, Double>(t.getLowerBound() + "-" + t.getUpperBound(), (double) 1));
			else {
				double steps = (t.getUpperBound() - t.getLowerBound()) / sampling;
				for (double i = t.getLowerBound(); i < t.getUpperBound(); i += sampling) {
					if ((i + sampling) <= t.getUpperBound())
						serie.getData().add(new XYChart.Data<String, Double>(i + "-" + (i + sampling), 1 / steps));
					else
						serie.getData().add(new XYChart.Data<String, Double>(i + "-" + t.getUpperBound(),
								(t.getUpperBound() - i) / (t.getUpperBound() - t.getLowerBound())));
				}

			}
		} 
		//if the gaussian distribution is set
		else if (gaussian || t.getSampler().equals("Gaussian")) {
			barChart.getYAxis().setAutoRanging(true);
			float upperBound;
			if (t.getUpperBound() == null)
				upperBound = 1000;
			else
				upperBound = t.getUpperBound();
			float mean = (upperBound + t.getLowerBound()) / 2;
			double deviation = (double) upperBound - (double) (upperBound + t.getLowerBound()) / 2;
			Sampler sampler = new Sampler();
			for (double i = (double) t.getLowerBound() - 2; i < upperBound + 2; i += sampling) {
				double step = (sampler.gaussian((i + sampling), mean, deviation) + sampler.gaussian(i, mean, deviation)) / 2;
				serie.getData().add(new XYChart.Data<String, Double>(i + "-" + (i + sampling), step));
			}
		}
		//if the weibull distribution is set
		else if (weibull || t.getSampler().contains("Weibull")) {
			Sampler sampler = new Sampler();
			barChart.getYAxis().setAutoRanging(true);
			int coef;
			if (!t.getSampler().contains("Weibull"))
				coef = weibullCoef;
			else {
				String[] segments = t.getSampler().split(":");
				coef = Integer.parseInt(segments[1]);
			}
			float upperBound;
			if (t.getUpperBound() == null)
				upperBound = 1000;
			else
				upperBound = t.getUpperBound();
			double mean = (double) (upperBound + t.getLowerBound()) / 2;
			//WeibullDistribution weibull = new WeibullDistribution(mean, coef);
			for (double i = (double) t.getLowerBound() - 2; i < upperBound + 2; i += sampling) {
				double j = (double)Math.round(i * 10) / 10;
				double step = (sampler.weibull(j+sampling, coef, mean) + sampler.weibull(j, coef, mean)) / 2;
				serie.getData().add(new XYChart.Data<String, Double>(j + "-" + (j + sampling), step));
			}
		}
		
		barChart.getData().addAll(serie);
	}

	
}
