package fr.inria.mochy.ui;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.dom.Dom;
import fr.inria.mochy.core.dom.PlaceDom;
import fr.inria.mochy.core.dom.TransitionDom;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.Tag;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.FloatStringConverter;
import javafx.util.converter.NumberStringConverter;

/**
 * It allows to create/edit net and timetable files. It is available from the
 * Config/Config menu.
 * <p>
 * <img src="Config.svg" alt="Config class diagram" style="float: right;">
 */
public class Config extends Application implements Initializable {

	Logger logger = Logger.getLogger("log");

	static Stage primaryStage;
	LoadFiles loadFiles = new LoadFiles();
	View view = new View();
	Sim simu;
	StringProperty logsTextString = new SimpleStringProperty();
	StringProperty infosValue = new SimpleStringProperty();

	@FXML
	TableView<PlaceDom> placesTable;
	TableColumn<PlaceDom, String> name = new TableColumn<>("name");
	TableColumn<PlaceDom, String> content = new TableColumn<>("content");
	TableColumn<PlaceDom, String> numberPlace = new TableColumn<>("number");
	TableColumn<PlaceDom, Boolean> control = new TableColumn<>("control");

	@FXML
	TableView<TransitionDom> transitionsTable;
	TableColumn<TransitionDom, String> transitionName = new TableColumn<>("name");
	TableColumn<TransitionDom, String> numberTransition = new TableColumn<>("number");
	// TableColumn<TransitionDom, String> distribution = new
	// TableColumn<>("distribution");
	TableColumn<TransitionDom, String> pre = new TableColumn<>("pre");
	TableColumn<TransitionDom, String> post = new TableColumn<>("post");
	TableColumn<TransitionDom, String> lowerBound = new TableColumn<>("lowerBound");
	TableColumn<TransitionDom, String> upperBound = new TableColumn<>("upperBound");
	TableColumn gaussian = new TableColumn("gaussian");
	TableColumn<TransitionDom, Boolean> weibull = new TableColumn<>("weibull");
	TableColumn<TransitionDom, Number> weibullCoef = new TableColumn<>("weibullCoef");

	@FXML
	TableView<TableEvent> eventsTable = new TableView<>();
	TableColumn<TableEvent, Number> number = new TableColumn<>("nb");
	TableColumn<TableEvent, Float> Date = new TableColumn<>("Date");
	TableColumn<TableEvent, String> label = new TableColumn<>("label");
	// TableColumn<TableEvent, String> transitions = new
	// TableColumn<>("transnames");
	TableColumn<TableEvent, String> tags = new TableColumn<>("tags");

	@FXML
	TableView<TableDependency> dependenciesTable = new TableView<>();
	TableColumn<TableDependency, Number> startEvent = new TableColumn<>("startEvent");
	TableColumn<TableDependency, Number> endEvent = new TableColumn<>("endEvent");
	TableColumn<TableDependency, Float> duration = new TableColumn<>("duration");

	static ArrayList<TransitionDom> gaussianTransitions = new ArrayList<>();
	static HashMap<TransitionDom, Integer> weibullTransitions = new HashMap<>();

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * load the start interface-prepare the components menu, tables, fiels and
	 * buttons
	 */
	@Override
	public void start(Stage stage) {

		primaryStage = stage;

		// load the xml file of the user interface
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("config.fxml"));
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mochy Config");
		primaryStage.show();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		/** prepare the columns of the transitions and places tables **/
		placesTable.setEditable(true);
		name.setCellValueFactory(new PropertyValueFactory("name"));
		name.setEditable(true);
		name.setCellFactory(TextFieldTableCell.forTableColumn());
		name.setOnEditCommit(new EventHandler<CellEditEvent<PlaceDom, String>>() {
			@Override
			public void handle(CellEditEvent<PlaceDom, String> event) {
				PlaceDom place = (PlaceDom) placesTable.getSelectionModel().getSelectedItem();
				place.setName(event.getNewValue());
			}
		});
		content.setCellValueFactory(new PropertyValueFactory("content"));
		content.setEditable(true);
		content.setCellFactory(TextFieldTableCell.forTableColumn());
		content.setOnEditCommit(new EventHandler<CellEditEvent<PlaceDom, String>>() {
			@Override
			public void handle(CellEditEvent<PlaceDom, String> event) {
				PlaceDom place = (PlaceDom) placesTable.getSelectionModel().getSelectedItem();
				place.setContent(event.getNewValue());
			}
		});
		numberPlace.setCellValueFactory(new PropertyValueFactory("number"));
		numberPlace.setEditable(true);
		numberPlace.setCellFactory(TextFieldTableCell.forTableColumn());
		numberPlace.setOnEditCommit(new EventHandler<CellEditEvent<PlaceDom, String>>() {
			@Override
			public void handle(CellEditEvent<PlaceDom, String> event) {
				PlaceDom place = (PlaceDom) placesTable.getSelectionModel().getSelectedItem();
				place.setNumber(event.getNewValue());
			}
		});
		control.setCellValueFactory(new Callback<CellDataFeatures<PlaceDom, Boolean>, ObservableValue<Boolean>>() {
			// This callback tell the cell how to bind the data model 'Registered' property
			// to
			// the cell, itself.
			@Override
			public ObservableValue<Boolean> call(CellDataFeatures<PlaceDom, Boolean> param) {
				return param.getValue().controlProperty();
			}
		});
		control.setCellFactory(CheckBoxTableCell.forTableColumn(control));
		placesTable.getColumns().addAll(name, numberPlace, content, control);

		transitionsTable.setEditable(true);
		transitionName.setCellValueFactory(new PropertyValueFactory("name"));
		transitionName.setEditable(true);
		transitionName.setCellFactory(TextFieldTableCell.forTableColumn());
		transitionName.setOnEditCommit(new EventHandler<CellEditEvent<TransitionDom, String>>() {
			@Override
			public void handle(CellEditEvent<TransitionDom, String> event) {
				TransitionDom transition = (TransitionDom) transitionsTable.getSelectionModel().getSelectedItem();
				transition.setName(event.getNewValue());
			}
		});
		numberTransition.setCellValueFactory(new PropertyValueFactory("number"));
		numberTransition.setEditable(true);
		numberTransition.setCellFactory(TextFieldTableCell.forTableColumn());
		numberTransition.setOnEditCommit(new EventHandler<CellEditEvent<TransitionDom, String>>() {
			@Override
			public void handle(CellEditEvent<TransitionDom, String> event) {
				TransitionDom transition = (TransitionDom) transitionsTable.getSelectionModel().getSelectedItem();
				transition.setNumber(event.getNewValue());
			}
		});
		pre.setCellValueFactory(new PropertyValueFactory("pre"));
		pre.setEditable(true);
		pre.setCellFactory(TextFieldTableCell.forTableColumn());
		pre.setOnEditCommit(new EventHandler<CellEditEvent<TransitionDom, String>>() {
			@Override
			public void handle(CellEditEvent<TransitionDom, String> event) {
				TransitionDom transition = (TransitionDom) transitionsTable.getSelectionModel().getSelectedItem();
				transition.setPre(event.getNewValue());
			}
		});
		post.setCellValueFactory(new PropertyValueFactory("post"));
		post.setEditable(true);
		post.setCellFactory(TextFieldTableCell.forTableColumn());
		post.setOnEditCommit(new EventHandler<CellEditEvent<TransitionDom, String>>() {
			@Override
			public void handle(CellEditEvent<TransitionDom, String> event) {
				TransitionDom transition = (TransitionDom) transitionsTable.getSelectionModel().getSelectedItem();
				transition.setPost(event.getNewValue());
			}
		});
		// distribution.setCellValueFactory(new PropertyValueFactory("distribution"));
		lowerBound.setCellValueFactory(new PropertyValueFactory("min"));
		lowerBound.setEditable(true);
		lowerBound.setCellFactory(TextFieldTableCell.forTableColumn());
		lowerBound.setOnEditCommit(new EventHandler<CellEditEvent<TransitionDom, String>>() {
			@Override
			public void handle(CellEditEvent<TransitionDom, String> event) {
				TransitionDom transition = (TransitionDom) transitionsTable.getSelectionModel().getSelectedItem();
				transition.setMin(event.getNewValue());
			}
		});
		upperBound.setCellValueFactory(new PropertyValueFactory("max"));
		upperBound.setEditable(true);
		upperBound.setCellFactory(TextFieldTableCell.forTableColumn());
		upperBound.setOnEditCommit(new EventHandler<CellEditEvent<TransitionDom, String>>() {
			@Override
			public void handle(CellEditEvent<TransitionDom, String> event) {
				TransitionDom transition = (TransitionDom) transitionsTable.getSelectionModel().getSelectedItem();
				transition.setMax(event.getNewValue());
			}
		});
		gaussian.setCellValueFactory(
				new Callback<CellDataFeatures<TransitionDom, Boolean>, ObservableValue<Boolean>>() {
					// This callback tell the cell how to bind the data model 'Registered' property
					// to
					// the cell, itself.
					@Override
					public ObservableValue<Boolean> call(CellDataFeatures<TransitionDom, Boolean> param) {
						return param.getValue().gaussianProperty();
					}
				});
		gaussian.setCellFactory(CheckBoxTableCell.forTableColumn(gaussian));
		weibull.setCellValueFactory(new Callback<CellDataFeatures<TransitionDom, Boolean>, ObservableValue<Boolean>>() {
			// This callback tell the cell how to bind the data model 'Registered' property
			// to
			// the cell, itself.
			@Override
			public ObservableValue<Boolean> call(CellDataFeatures<TransitionDom, Boolean> param) {
				return param.getValue().weibullProperty();
			}
		});
		weibull.setCellFactory(CheckBoxTableCell.forTableColumn(weibull));
		weibullCoef.setCellValueFactory(new PropertyValueFactory("weibullCoef"));
		weibullCoef.setEditable(true);
		weibullCoef.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		weibullCoef.setOnEditCommit(new EventHandler<CellEditEvent<TransitionDom, Number>>() {
			@Override
			public void handle(CellEditEvent<TransitionDom, Number> event) {
				TransitionDom transition = (TransitionDom) transitionsTable.getSelectionModel().getSelectedItem();
				transition.setWeibullCoef(event.getNewValue());
			}
		});
		transitionsTable.getColumns().addAll(transitionName, numberTransition, pre, post, lowerBound, upperBound,
				gaussian, weibull, weibullCoef);

		/** prepare the columns of the events and dependencies **/
		eventsTable.setEditable(true);
		number.setCellValueFactory(new PropertyValueFactory("number"));
		number.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.08));
		number.setEditable(true);
		number.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		number.setOnEditCommit(new EventHandler<CellEditEvent<TableEvent, Number>>() {
			@Override
			public void handle(CellEditEvent<TableEvent, Number> event) {
				TableEvent tableEvent = (TableEvent) eventsTable.getSelectionModel().getSelectedItem();
				tableEvent.setNumber(event.getNewValue());
			}
		});
		Date.setCellValueFactory(new PropertyValueFactory("Date"));
		Date.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.15));
		Date.setEditable(true);
		Date.setCellFactory(TextFieldTableCell.forTableColumn(new FloatStringConverter()));
		Date.setOnEditCommit(new EventHandler<CellEditEvent<TableEvent, Float>>() {
			@Override
			public void handle(CellEditEvent<TableEvent, Float> event) {
				TableEvent tableEvent = (TableEvent) eventsTable.getSelectionModel().getSelectedItem();
				tableEvent.setDate(event.getNewValue());
			}
		});
		label.setCellValueFactory(new PropertyValueFactory("label"));
		label.setEditable(true);
		label.setCellFactory(TextFieldTableCell.forTableColumn());
		label.setOnEditCommit(new EventHandler<CellEditEvent<TableEvent, String>>() {
			@Override
			public void handle(CellEditEvent<TableEvent, String> event) {
				TableEvent evt = eventsTable.getSelectionModel().getSelectedItem();
				evt.setLabel(event.getNewValue());
			}
		});
		/*
		 * transitions.setCellValueFactory(new PropertyValueFactory("transitions"));
		 * transitions.setEditable(true);
		 * transitions.setCellFactory(TextFieldTableCell.forTableColumn());
		 * transitions.setOnEditCommit(new EventHandler<CellEditEvent<TableEvent,
		 * String>>() {
		 * 
		 * @Override public void handle(CellEditEvent<TableEvent, String> event) {
		 * TableEvent evt = eventsTable.getSelectionModel().getSelectedItem();
		 * ArrayList<Integer> list = new ArrayList<>(); String[] trans =
		 * event.getNewValue().split(" "); for (int i = 0; i < trans.length; i++)
		 * list.add(Integer.valueOf(trans[i])); evt.setTransnames(list); } });
		 */
		tags.setCellValueFactory(new PropertyValueFactory("tagsString"));
		tags.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.10));
		tags.setEditable(true);
		tags.setCellFactory(TextFieldTableCell.forTableColumn());
		tags.setOnEditCommit(new EventHandler<CellEditEvent<TableEvent, String>>() {
			@Override
			public void handle(CellEditEvent<TableEvent, String> event) {
				TableEvent evt = eventsTable.getSelectionModel().getSelectedItem();
				evt.setTagsString(event.getNewValue());
			}
		});
		eventsTable.getColumns().addAll(number, Date, label, tags);

		startEvent.setCellValueFactory(new PropertyValueFactory("startEvent"));
		startEvent.setEditable(true);
		startEvent.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		startEvent.setOnEditCommit(new EventHandler<CellEditEvent<TableDependency, Number>>() {
			@Override
			public void handle(CellEditEvent<TableDependency, Number> event) {
				TableDependency dep = dependenciesTable.getSelectionModel().getSelectedItem();
				dep.setStartEvent(event.getNewValue());
			}
		});
		endEvent.setCellValueFactory(new PropertyValueFactory("endEvent"));
		endEvent.setEditable(true);
		endEvent.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		endEvent.setOnEditCommit(new EventHandler<CellEditEvent<TableDependency, Number>>() {
			@Override
			public void handle(CellEditEvent<TableDependency, Number> event) {
				TableDependency dep = dependenciesTable.getSelectionModel().getSelectedItem();
				dep.setEndEvent(event.getNewValue());
			}
		});
		duration.setCellValueFactory(new PropertyValueFactory("duration"));
		duration.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.15));
		duration.setEditable(true);
		duration.setCellFactory(TextFieldTableCell.forTableColumn(new FloatStringConverter()));
		duration.setOnEditCommit(new EventHandler<CellEditEvent<TableDependency, Float>>() {
			@Override
			public void handle(CellEditEvent<TableDependency, Float> event) {
				TableDependency dep = dependenciesTable.getSelectionModel().getSelectedItem();
				dep.setDuration(event.getNewValue());
			}
		});
		dependenciesTable.getColumns().addAll(startEvent, endEvent, duration);

	}

	@FXML
	/** load the net file and display its data */
	void loadNet(ActionEvent evt) {
		Sim simu = loadFiles.loadNetFile(primaryStage);
		if (simu != null) {
			// logsTextString.setValue(simu.getN().loadFile());
			view.displayFeedback(simu.displayFeedback(), placesTable, transitionsTable);

		}
	}

	@FXML
	/** load the timetable and display its data */
	void loadTT(ActionEvent evt) {
		TTConfig ttConfig = loadFiles.loadTT(primaryStage, logsTextString);
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
	}

	@FXML
	/** quit */
	void quit(ActionEvent evt) {
		System.exit(-1);
	}

	@FXML
	/** save the net file with places and transitions */
	void saveNet(ActionEvent evt) {
		FileChooser fileChooser = new FileChooser();
		LoadFiles loadFiles = new LoadFiles();
		if (!loadFiles.getNetsFolder().equals("")) {
			fileChooser.setInitialDirectory(new File(loadFiles.getNetsFolder()));
		}

		// Set extension filter for text files
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt", "*.net");
		fileChooser.getExtensionFilters().add(extFilter);

		// Show save file dialog
		File file = fileChooser.showSaveDialog(primaryStage);

		// Write the data of the table in the file
		if (file != null) {
			String flow = "";
			String initial = "initial";
			try {
				PrintWriter writer;
				writer = new PrintWriter(file);
				for (Object o : placesTable.getItems()) {
					PlaceDom place = (PlaceDom) o;
					if (!place.isControl())
						writer.println("place:" + place.getNumber() + ":" + place.getName());
					else
						writer.println("control:" + place.getNumber() + ":" + place.getName());
					if (place.getContent().equals("marked"))
						initial += ":" + place.getNumber();
				}
				for (Object o : transitionsTable.getItems()) {
					TransitionDom transition = (TransitionDom) o;
					if (transition.isGaussian())
						writer.println("transition:" + transition.getNumber() + ":" + transition.getName() + ":["
								+ transition.getMin() + "," + transition.getMax() + "]:Gaussian");
					else if (transition.isWeibull())
						writer.println("transition:" + transition.getNumber() + ":" + transition.getName() + ":["
								+ transition.getMin() + "," + transition.getMax() + "]:Weibull:"
								+ transition.getWeibullCoef());
					else
						writer.println("transition:" + transition.getNumber() + ":" + transition.getName() + ":["
								+ transition.getMin() + "," + transition.getMax() + "]");
					for (Object o2 : placesTable.getItems()) {
						PlaceDom place = (PlaceDom) o2;
						if (transition.getPre() != null) {
							for (String pre : transition.getPre().split(",")) {
								if (pre.equals(place.getName()))
									flow += "inflow:" + transition.getNumber() + ":" + place.getNumber() + "\n";
							}
						}
						if (transition.getPost() != null) {
							for (String post : transition.getPost().split(",")) {
								if (post.equals(place.getName()))
									flow += "outflow:" + transition.getNumber() + ":" + place.getNumber() + "\n";
							}
						}
					}
				}
				writer.println(flow);
				writer.println(initial);
				writer.close();
			} catch (IOException ex) {
				logger.log(Level.WARNING, "Warning message");
			}
		}
	}

	@FXML
	/** add a temp place to be edited */
	void addPlace(ActionEvent evt) {
		PlaceDom place = new PlaceDom();
		placesTable.getItems().add(place);
		placesTable.refresh();
	}

	@FXML
	/** delete the place selected */
	void deletePlace(ActionEvent evt) {
		ObservableList<PlaceDom> placesList = placesTable.getItems();
		PlaceDom placeToRemove = placesTable.getSelectionModel().getSelectedItem();
		placesList.remove(placeToRemove);
		placesTable.setItems(placesList);
		placesTable.refresh();
	}

	@FXML
	/** add a transition to be edited */
	void addTransition(ActionEvent evt) {
		TransitionDom transition = new TransitionDom();
		transitionsTable.getItems().add(transition);
		transitionsTable.refresh();
	}

	@FXML
	/** save the net file with places and transitions */
	void saveTT(ActionEvent evt) {
		FileChooser fileChooser = new FileChooser();
		LoadFiles loadFiles = new LoadFiles();
		if (!loadFiles.getTimeTablesFolder().equals("")) {
			fileChooser.setInitialDirectory(new File(loadFiles.getTimeTablesFolder()));
		}

		// Set extension filter for text files
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt", "*.tt");
		fileChooser.getExtensionFilters().add(extFilter);

		// Show save file dialog
		File file = fileChooser.showSaveDialog(primaryStage);

		// Write the data of the table in the file
		if (file != null) {
			try {
				PrintWriter writer;
				writer = new PrintWriter(file);
				for (Object o : eventsTable.getItems()) {
					TableEvent event = (TableEvent) o;
					/*
					 * String transitions = ""; if (event.getTransnames() != null) { for (int trans
					 * : event.getTransnames()) { transitions += ":" + trans; } }
					 */
					writer.println("event:" + event.getNumber() + ":tag={" + event.getTagsString() + "}:"
							+ event.getDate() + ":" + event.getLabel());
				}
				for (Object o : dependenciesTable.getItems()) {
					TableDependency dep = (TableDependency) o;
					writer.println("dependency:" + dep.getOrigin() + ":" + dep.getGoal() + ":" + dep.getDuration());
				}
				writer.close();
			} catch (IOException ex) {
				logger.log(Level.WARNING, "Warning message");
			}
		}
	}

	@FXML
	/** delete selected transition */
	void deleteTransition(ActionEvent evt) {
		ObservableList<TransitionDom> transitionsList = transitionsTable.getItems();
		TransitionDom transitionToRemove = transitionsTable.getSelectionModel().getSelectedItem();
		transitionsList.remove(transitionToRemove);
		transitionsTable.setItems(transitionsList);
		transitionsTable.refresh();
	}

	@FXML
	/** add an event to be edited */
	void addEvent(ActionEvent evt) {
		TableEvent event = new TableEvent("Temp");
		eventsTable.getItems().add(event);
		eventsTable.refresh();
	}

	@FXML
	/** delete selected transition */
	void deleteEvent(ActionEvent evt) {
		ObservableList<TableEvent> eventsList = eventsTable.getItems();
		TableEvent eventToRemove = eventsTable.getSelectionModel().getSelectedItem();
		eventsList.remove(eventToRemove);
		eventsTable.setItems(eventsList);
		eventsTable.refresh();
	}

	@FXML
	/** add a dependency to be edited */
	void addDependency(ActionEvent evt) {
		TableDependency dep = new TableDependency();
		dependenciesTable.getItems().add(dep);
		dependenciesTable.refresh();
	}

	@FXML
	/** delete selected dependency */
	void deleteDependency(ActionEvent evt) {
		ObservableList<TableDependency> dependenciesList = dependenciesTable.getItems();
		TableDependency dependencyToRemove = dependenciesTable.getSelectionModel().getSelectedItem();
		dependenciesList.remove(dependencyToRemove);
		dependenciesTable.setItems(dependenciesList);
		dependenciesTable.refresh();
	}

	public static void setGaussian(boolean b, TransitionDom transition) {
		if (b)
			gaussianTransitions.add(transition);
		else
			gaussianTransitions.remove(transition);

	}

	public static void setWeibull(boolean b, TransitionDom transition) {
		if (b)
			weibullTransitions.put(transition, 0);
		else
			weibullTransitions.remove(transition);

	}
}
