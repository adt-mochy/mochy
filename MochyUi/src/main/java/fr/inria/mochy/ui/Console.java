package fr.inria.mochy.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.statsAndTasks.MultipleRunsTask;
import javafx.event.ActionEvent;
import javafx.scene.control.CheckBox;

public class Console {
	public static void main(String[] args) {
		//inputs
		boolean consoleTrue = true;
		Scanner in = new Scanner(System.in);
		System.out.println("Chemin du fichier du projet : ");
		String file = in.next();//args[0]; //in.next();
		System.out.println("Nombre de runs : ");
		int runs = Integer.parseInt(in.next());//args[1]);//in.next());
		
		//loading
		long time1 = System.currentTimeMillis();
		LoadFiles loadFiles = new LoadFiles(consoleTrue);
		loadFiles.loadProjectFile(new File(file));
		
		//running
		long time2 = System.currentTimeMillis();
		MultipleRunsTask task = new MultipleRunsTask (runs, null);
		task.console = consoleTrue;
		System.out.println("running simulation...");
		task.run();
		
		//calcul the time of the loading file steps and the running steps
		long time3 = System.currentTimeMillis();
		long timeLoading = (time2 - time1) / 1000;
		long timeRunning = (time3 - time2) / 1000;
		
		//to conclude
		System.out.println("runs done : " + timeLoading + 
				"seconds for loading / " + timeRunning + 
				"seconds for " + runs + " run(s)");
	}
}
