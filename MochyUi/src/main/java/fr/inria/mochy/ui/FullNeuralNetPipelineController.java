package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.statsAndTasks.FullNnetPipelineTask;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class FullNeuralNetPipelineController implements Initializable {

	@FXML TextField sd;
	@FXML TextField maxSteps;
	@FXML TextField fromTokensNb;
	@FXML TextField toTokensNb;
	@FXML TextField stepTokens;
	public static int standardDeviation;
	public static int maximumSteps;
	public static int startTokensNb;
	public static int endTokensNb;
	public static int stepToken;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	@FXML
	void launch(ActionEvent e) {
		try {
			standardDeviation = Integer.parseInt(sd.getText());
			maximumSteps = Integer.parseInt(maxSteps.getText());
			startTokensNb = Integer.parseInt(fromTokensNb.getText());
			endTokensNb = Integer.parseInt(toTokensNb.getText());
			stepToken = Integer.parseInt(stepTokens.getText());
			MultipleRunsController controller = new MultipleRunsController();
			controller.equalizationFullStats(false, false, true, new FullNnetPipelineTask());
		} catch (NumberFormatException exc) {
			StartController.logsTextString.set("Warning : values must be integer numbers");
		}
	}

}
