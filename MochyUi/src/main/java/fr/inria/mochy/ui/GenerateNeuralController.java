package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.statsAndTasks.GenerateNeuralTask;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class GenerateNeuralController implements Initializable{
	@FXML
	TextField standardDeviationTarget;
	public static int sdTarget;
	
	@FXML
	TextField neuralNetworksNb;
	public static int nnNb;
	
	@FXML
	TextField runs;
	public static int r;
	
	@FXML
	TextField maxSteps;
	public static int max;

	@Override
	public void initialize(URL location, ResourceBundle resources) {		
	}
	
	@FXML
	void launch (ActionEvent evt) {
		sdTarget = Integer.parseInt(standardDeviationTarget.getText());
		nnNb = Integer.parseInt(neuralNetworksNb.getText());
		r = Integer.parseInt(runs.getText());
		max = Integer.parseInt(maxSteps.getText());
		MultipleRunsController controller = new MultipleRunsController();
		controller.equalizationFullStats(false, false, true, new GenerateNeuralTask());
	}
}
