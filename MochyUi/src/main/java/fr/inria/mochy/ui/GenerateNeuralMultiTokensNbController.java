package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.statsAndTasks.GenerateNeuralTask;
import fr.inria.mochy.statsAndTasks.GenerateNnetMultiTokensNbTask;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class GenerateNeuralMultiTokensNbController implements Initializable{
	@FXML
	TextField standardDeviationTarget;
	public static int sdTarget;
	
	@FXML
	TextField neuralNetworksNb;
	public static int nnNb;
	
	@FXML
	TextField runs;
	public static int r;
	
	@FXML
	TextField maxSteps;
	public static int max;
	
	@FXML
	TextField tokensMin;
	public static int tokMin;
	
	@FXML
	TextField tokensMax;
	public static int tokMax;
	
	@FXML
	TextField tokensInterval;
	public static int tokInterval;

	@Override
	public void initialize(URL location, ResourceBundle resources) {		
	}
	
	@FXML
	void launch (ActionEvent evt) {
		sdTarget = Integer.parseInt(standardDeviationTarget.getText());
		nnNb = Integer.parseInt(neuralNetworksNb.getText());
		r = Integer.parseInt(runs.getText());
		max = Integer.parseInt(maxSteps.getText());
		tokMin = Integer.parseInt(tokensMin.getText());
		tokMax = Integer.parseInt(tokensMax.getText());
		tokInterval = Integer.parseInt(tokensInterval.getText());
		MultipleRunsController controller = new MultipleRunsController();
		controller.equalizationFullStats(false, false, true, new GenerateNnetMultiTokensNbTask());
	}
}
