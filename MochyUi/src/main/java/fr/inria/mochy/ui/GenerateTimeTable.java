package fr.inria.mochy.ui;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.mochysim.Transition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**Generate correspondance and timetable files from a net and the parameters set in the window
 * work for linear schemes (no fork)
 * <p>
 * <img src="GenerateTimeTable.svg" alt="GenerateTimeTable class diagram" style="float: right;">*/
public class GenerateTimeTable extends Application implements Initializable {

	Logger logger = Logger.getLogger("logger");
	Stage stage;
	LoadFiles loadFiles = new LoadFiles();
	Sim simu = null;
	
	@FXML TextField intervalText;
	@FXML TextField startDateText;
	@FXML TextField endDateText;
	@FXML TextField startPlaceNbText;
	@FXML TextField startEvtNbText;
	@FXML TextField output;
	
	@Override public void initialize(URL location, ResourceBundle resources) {
		
	}

	@Override public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("generateTimeTable.fxml"));
		primaryStage.setScene(scene);
		primaryStage.setTitle("Generate TimeTable");
		primaryStage.show();
	}

	@FXML void loadNet(ActionEvent evt) {
		LoadFiles loadFiles = new LoadFiles();
		simu = loadFiles.loadNetFile(stage);
	}
	
	@FXML void generate(ActionEvent evt) {
		String textTT = "";
		String textCor = "";
		float interval = Float.parseFloat(intervalText.getText());
		float startDate = Float.parseFloat(startDateText.getText());
		float endDate = Float.parseFloat(endDateText.getText());
		int startPlaceNb = Integer.parseInt(startPlaceNbText.getText());
		int evtNb = Integer.parseInt(startEvtNbText.getText());
		
		if (StartController.getClassModel().equals("Net")) {
			Net net = (Net) simu.getN();
		//initial setup
		Place p = net.findPlace(startPlaceNb);
		if (p == null) {
			p = net.getControlPlace(startPlaceNb);
		}
		Transition t = null;
		boolean found = false;
		textTT += "event:"+evtNb+":tag={}:"+startDate+":"+p.getName()+"\n";
		for (Transition trans : net.transitions.values()) {
			if (trans.getPre().contains(p) && found == false) {
				p = trans.getPost().get(0);
				t = trans;
				found = true;
			}
			if (trans.getControlPre().contains(p) && found == false) {
				p = trans.getPost().get(0);
				t = trans;
				found = true;
			}
		}
		textCor += "transition "+t.getNumber()+" implements event "+evtNb+"\n";
		if (t.getControlPre().size()>0) {
			Place control = t.getControlPre().get(0);
			textCor += "event "+evtNb+" allows place "+control.getNumber()+"\n";
		}
		evtNb++;
		//loop for each event to insert
		for (float i = startDate + interval; i <= endDate; i += interval) {
			//insert the events and dependency
			textTT += "event:"+evtNb+":tag={}:"+i+":"+p.getName()+"\n";
			textTT += "dependency:"+(evtNb-1)+":"+evtNb+":"+interval+"\n";
			//seek for the next place and transition
			found = false;
			for (Transition trans : net.transitions.values()) {
				if (trans.getPre().contains(p) && found == false) {
					p = trans.getPost().get(0);
					t = trans;
					found = true;
				}
			}
			//insert the correspondance informations
			textCor += "transition "+t.getNumber()+" implements event "+evtNb+"\n";
			if (t.getControlPre().size()>0) {
				Place control = t.getControlPre().get(0);
				textCor += "event "+evtNb+" allows place "+control.getNumber()+"\n";
			}
			evtNb ++;
		}
		}
		writeTT(textTT);
		writeCor(textCor);
		
	}
	
	void writeTT(String s) {
		FileWriter fileTT = null;
		try {
			fileTT = new FileWriter(loadFiles.getNetsFolder()+"/"+output.getText()+".tt", false);
			fileTT.write(s);
		} catch (IOException e) {
			logger.log(Level.WARNING, "error of writing in fileTT in class GenerateTimeTable");
		} finally {
			try {
				fileTT.close();
			} catch (IOException e) {
				logger.log(Level.WARNING, "error while closing the fileTT in class GenerateTimeTable");
			}
		}
	}
	
	void writeCor(String s) {
		FileWriter fileCor = null;
		try {
			fileCor = new FileWriter(loadFiles.getNetsFolder()+"/"+output.getText()+".cor", false);
			fileCor.write(s);
		} catch (IOException e) {
			logger.log(Level.WARNING, "error of writing in fileTT in class GenerateTimeTable");
		} finally {
			try {
				fileCor.close();
			} catch (IOException e) {
				logger.log(Level.WARNING, "error while closing the fileTT in class GenerateTimeTable");
			}
		}
	}
}
