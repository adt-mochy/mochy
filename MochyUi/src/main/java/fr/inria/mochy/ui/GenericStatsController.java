package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.statsAndTasks.AbstractStats;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**allow to launch a statistics class from the stats/launch a class menu*/
public class GenericStatsController implements Initializable{

	@FXML TextField className;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
	}
	
	@FXML void launch (ActionEvent evt) {
		Class<? extends AbstractStats> cls = null;
		AbstractStats stats = null;
		String name = className.getText();
		try {
			cls = (Class<? extends AbstractStats>) Class.forName("fr.inria.mochy.statsAndTasks." + name);
		} catch (ClassNotFoundException e) {
			System.out.println("class not found");
			e.printStackTrace();
		}
		try {
			stats = cls.getConstructor().newInstance();
		}catch (Exception e) {
			e.printStackTrace();
		}
		MultipleRunsController multipleRunsController = new MultipleRunsController();
		multipleRunsController.equalizationFullStats(false, false, true, stats);
	}

}
