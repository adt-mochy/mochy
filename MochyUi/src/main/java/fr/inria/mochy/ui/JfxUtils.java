package fr.inria.mochy.ui;

import java.io.IOException;

import fr.inria.mochy.core.dom.PlaceDom;
import fr.inria.mochy.core.dom.TransitionDom;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * tools for javafx : load the xml file fxml for the user interface and display a button for the chart column of the transitions
 * <p>
 * <img src="JfxUtils.svg" alt="JfxUtils class diagram" style="float: right;">
 */
public class JfxUtils {
	 
    /**
     * @param fxml the name of the fxml file in ressources
     * @return the Node to set up the window
     */
    public static Node loadFxml(String fxml) {
        FXMLLoader loader = new FXMLLoader();
        try {
            loader.setLocation(JfxUtils.class.getResource(fxml));
            Node root = (Node) loader.load(Main.class.getResource(fxml));
            return root;
        } catch (IOException e) {
            throw new IllegalStateException("cannot load FXML screen", e);
        }
    }
    
    /**add a button to transition table to display the chart of the clock distribution*/
	public static void addButtonToTable(TableView table, Stage primaryStage) {
        TableColumn<TransitionDom, Void> colBtn = new TableColumn("distribution");
        //colBtn.prefWidthProperty().bind(table.widthProperty().multiply(0.06));
        
        Callback<TableColumn<TransitionDom, Void>, TableCell<TransitionDom, Void>> cellFactory = new Callback<TableColumn<TransitionDom, Void>, TableCell<TransitionDom, Void>>() {
            @Override
            public TableCell<TransitionDom, Void> call(final TableColumn<TransitionDom, Void> param) {
                final TableCell<TransitionDom, Void> cell = new TableCell<TransitionDom, Void>() {
                    private final Button btn = new Button("o");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                        	TransitionDom data2 = getTableView().getItems().get(getIndex());
                            View view = new View();
                            view.displayChart(primaryStage, data2);
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                        	TransitionDom data = getTableView().getItems().get(getIndex());
                        	if (data.gaussianProperty().get())
                        		btn.setText("gaussian");
                        	else if (data.weibullProperty().get())
                        		btn.setText("weibull");
                        	else
                        		btn.setText("uniform");
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        table.getColumns().add(colBtn);
    }
	
	/**add a button to place table to display the chart of the trajectories of a place*/
	public static void addButtonToTablePlace(TableView table, Stage primaryStage) {
        TableColumn<PlaceDom, Void> colBtn = new TableColumn("Chart");
        colBtn.prefWidthProperty().bind(table.widthProperty().multiply(0.25));

        Callback<TableColumn<PlaceDom, Void>, TableCell<PlaceDom, Void>> cellFactory = new Callback<TableColumn<PlaceDom, Void>, TableCell<PlaceDom, Void>>() {
            @Override
            public TableCell<PlaceDom, Void> call(final TableColumn<PlaceDom, Void> param) {
                final TableCell<PlaceDom, Void> cell = new TableCell<PlaceDom, Void>() {
                    private final Button btn = new Button("o");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                        	PlaceDom data = getTableView().getItems().get(getIndex());
                            System.out.println("selectedData: " + data);
                            View view = new View();
                            view.displayChart(primaryStage, data);
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        table.getColumns().add(colBtn);
    }
	
	public static void addGaussianCheckBoxToTable(TableView table) {
        TableColumn<TransitionDom, Void> colCb = new TableColumn("Gaussian");
        colCb.prefWidthProperty().bind(table.widthProperty().multiply(0.2));

        Callback<TableColumn<TransitionDom, Void>, TableCell<TransitionDom, Void>> cellFactory = new Callback<TableColumn<TransitionDom, Void>, TableCell<TransitionDom, Void>>() {
            @Override
            public TableCell<TransitionDom, Void> call(final TableColumn<TransitionDom, Void> param) {
                final TableCell<TransitionDom, Void> cell = new TableCell<TransitionDom, Void>() {
                    private final CheckBox cb = new CheckBox();
                    {
                    	ChangeListener<Boolean> listener = (observable, oldValue, newValue) ->
                    	{
                        	TransitionDom data = getTableView().getItems().get(getIndex());
                    		if (newValue) Config.setGaussian(true, data);
                    		else Config.setGaussian(false, data);
                    	};
                    	cb.selectedProperty().addListener(listener);
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(cb);
                        }
                    }
                };
                return cell;
            }
        };
        colCb.setCellFactory(cellFactory);
        table.getColumns().add(colCb);
    }
	public static void addWeibullCheckBoxToTable(TableView table) {
        TableColumn<TransitionDom, Void> colCb = new TableColumn("Weibull");
        colCb.prefWidthProperty().bind(table.widthProperty().multiply(0.2));

        Callback<TableColumn<TransitionDom, Void>, TableCell<TransitionDom, Void>> cellFactory = new Callback<TableColumn<TransitionDom, Void>, TableCell<TransitionDom, Void>>() {
            @Override
            public TableCell<TransitionDom, Void> call(final TableColumn<TransitionDom, Void> param) {
                final TableCell<TransitionDom, Void> cell = new TableCell<TransitionDom, Void>() {
                    private final CheckBox cb = new CheckBox();
                    {
                    	ChangeListener<Boolean> listener = (observable, oldValue, newValue) ->
                    	{
                        	TransitionDom data = getTableView().getItems().get(getIndex());
                    		if (newValue) Config.setWeibull(true, data);
                    		else Config.setWeibull(false, data);
                    	};
                    	cb.selectedProperty().addListener(listener);
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(cb);
                        }
                    }
                };
                return cell;
            }
        };
        colCb.setCellFactory(cellFactory);
        table.getColumns().add(colCb);
    }
	
}

