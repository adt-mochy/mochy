package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.core.mochysim.Delay;
import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TableEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * display all the delays set as scenario 
 * displayed from menu Stats/scenario/ and button display all delays
 * <p>
 * <img src="ScenarioController.svg" alt="ScenarioController class diagram"
 * style="float: right;">
 */
public class ListAllDelaysController implements Initializable{

	/**set the table*/
	@FXML TableView<Delay> delaysTable = new TableView<>();
	/**set the transition number*/
	TableColumn<Delay, Integer> transitionNumber = new TableColumn<>("trans. nb");
	/**set the start date column*/
	TableColumn<Delay, Float> start = new TableColumn<>("start");
	/**set the end date column*/
	TableColumn<Delay, Float> end = new TableColumn<>("end");
	/**set the delay value column*/
	TableColumn<Delay, Float> delay = new TableColumn<>("delay");
	/**set the actual validity column : true or false*/
	TableColumn<Delay, Boolean> valid = new TableColumn<>("valid"); 
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		/**link the property names of the Delay Class to the associated columns*/
		transitionNumber.setCellValueFactory(new PropertyValueFactory("transitionNb"));
		start.setCellValueFactory(new PropertyValueFactory("startDate"));
		end.setCellValueFactory(new PropertyValueFactory("endDate"));
		delay.setCellValueFactory(new PropertyValueFactory("delay"));
		valid.setCellValueFactory(new PropertyValueFactory("valid"));
		
		/**add the columns to the table*/
		delaysTable.getColumns().addAll(transitionNumber, start, end, delay, valid);
		
		/**display the content*/
		refreshValues();
	}

	/**refresh the values of the table based on the delays set in the StartController from ScenarioController*/
	public void refreshValues() {
		/**Create the list*/
		ObservableList<Delay> delaysList = FXCollections.observableArrayList();
		for (Delay d : StartController.delays)
			delaysList.add(d);

		/**Load and display the content of the list to the table*/
		delaysTable.setItems(delaysList);
		delaysTable.refresh();
	}
	
}
