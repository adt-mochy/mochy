package fr.inria.mochy.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.inria.mochy.core.RPN.RPNConfig;
import fr.inria.mochy.core.RPN.RegulNet;
import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.mochysim.Delay;
import fr.inria.mochy.core.mochysim.Marking;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.timetable.TTChecker;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.Tag;
import fr.inria.mochy.core.timetable.TimeTable;
import fr.inria.mochy.core.trajectory.BooleanPlace;
import fr.inria.mochy.core.trajectory.Segment;
import fr.inria.mochy.core.trajectory.TrajPlace;
import fr.inria.mochy.core.trajectory.TrajTransition;
import fr.inria.mochy.core.trajectory.Trajectory;
import fr.inria.mochy.core.trajectory.TrajectoryNet;
import guru.nidi.graphviz.attribute.Rank;
import guru.nidi.graphviz.attribute.Records;
import guru.nidi.graphviz.attribute.Style;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Graph;
import guru.nidi.graphviz.model.Node;

import static guru.nidi.graphviz.model.Factory.*;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * LoadFiles contains tools to load the files (net, timetable, correspondencies,
 * and project) and initialize the models
 * <p>
 * <img src="LoadFiles.svg" alt="LoadFiles class diagram" style="float: right;">
 */
public class LoadFiles {
	String logsFolder = "";
	String netsFolder = "";
	String timeTablesFolder = "";
	Logger logger;
	boolean console = false;

	/**
	 * Constructor for the file loader class Assumes preferences are set.
	 */
	public LoadFiles() {
		logger = Logger.getLogger("log");
		initFolders();
	}
	
	public LoadFiles(boolean console) {
		logger = Logger.getLogger("log");
		this.console = console;
	}

	/** load the file of the petrinet to simulate */
	public Sim loadNetFile(Stage primaryStage) {
		Sim simu = null;
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("NET files", "*.net");
		fileChooser.getExtensionFilters().add(extFilter);
		/** init the directory set by the preferences pane */
		initFolders();
		if (!netsFolder.equals("")) {
			File directory = new File(netsFolder);
			if (directory.exists())
				fileChooser.setInitialDirectory(new File(netsFolder));
		}
		/** the file of the network to be loaded for the simulation */
		File netFile = fileChooser.showOpenDialog(primaryStage);
		/** the file url of the net to simulate */
		String netFileUrl;
		if (netFile != null) {
			netFileUrl = netFile.getAbsolutePath();

			System.out.println("*****new simu****");
			simu = generateSimu(netFileUrl);
		}
		return simu;
	}

	/** generate the Sim from the url of the net */
	public Sim generateSimu(String netFileUrl) {
		/**before loading the file, check and adapt the tool to the model it fits*/
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(netFileUrl));
			String line = br.readLine();
			if (line.startsWith("TrajectoryNet")) {
				StartController.setClassModel("TrajectoryNet");
				StartController.setFileModel("TrajectoryNet");
				StartController.setDisplayEqualizationFields(false);
			}else if (line.startsWith("EqualizationNet")) {
				StartController.setClassModel(line);
				StartController.setFileModel("EqualizationNet");
				StartController.setDisplayEqualizationFields(true);
			}else {
				StartController.setClassModel("Net");
				StartController.setFileModel("Net");
				StartController.setDisplayEqualizationFields(false);
			}
			StartController.getLogsTextString().set("");
		} catch (Exception e) {
			StartController.getLogsTextString().set("Warning : the net file is not found : "+netFileUrl);
			logger.log(Level.WARNING, "error while reading the BufferedReader in class LoadFile");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class LoadFiles");
				}
			}
		}
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		File directory = new File("logs");
		if (!directory.exists()) {
			directory.mkdir();
		}

		/** set the path logs as defined in the preferences pane */
		initFolders();
		String pathLogs = "logs/" + timestamp.toString().replace(":", "") + ".txt";
		if (!logsFolder.equals("")) {
			directory = new File(logsFolder);
			if (directory.exists())
				pathLogs = logsFolder + "/" + timestamp.toString().replace(":", "") + ".txt";

		}
		System.out.println("new load : "+StartController.getClassModel());
		String[] infos = StartController.getClassModel().split("\\.");
		StringProperty logs = StartController.getLogsTextString();
		if (infos[0].equals("EqualizationNet")) {
			logs.set(logs.get()+"\nload Model : "+infos[0]+" Version : "+infos[1]+"\n");
			Main.primaryStage.setTitle("MOCHY - "+infos[0]+" - "+infos[1]);
		}
		else if (!console){
			logs.set(logs.get()+"\nload Model : "+infos[0]+"\n");
			Main.primaryStage.setTitle("MOCHY - "+infos[0]);
		}
		Sim simu = null;
		try {
			simu = new Sim(0, netFileUrl, pathLogs, StartController.enableLogs.get(), StartController.getClassModel());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			if (StartController.getClassModel().startsWith("EqualizationNet.EquNetNeural"))
				StartController.getLogsTextString().setValue("Warning : neural net may not be found : "+e.getMessage());
		} catch (ClassNotFoundException e) {
			StartController.getLogsTextString().setValue("Warning : class not found in in.txt : "+e.getMessage());
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			StartController.getLogsTextString().setValue("Warning : method not found in in.txt : "+e.getMessage());
			e.printStackTrace();
		}
		StartController.getLogsTextString().setValue(StartController.getLogsTextString().get()+simu.getN().loadFile());
		StartController.setCorrespLoaded(false);
		StartController.getInfosCorrespString().setValue("");
		StartController.setSimu(simu);
		return simu;
	}

	/**
	 * load the timetable file and add the components to the grid the is a component
	 * for the events and another for the dependencies
	 */
	public TTConfig loadTT(Stage primaryStage, StringProperty logsTextString) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TimeTable files", "*.tt");
		fileChooser.getExtensionFilters().add(extFilter);
		/** init the directory set by the preferences pane */
		initFolders();
		if (!timeTablesFolder.equals("")) {
			File directory = new File(timeTablesFolder);
			if (directory.exists())
				fileChooser.setInitialDirectory(new File(timeTablesFolder));
		}
		File ttFile = fileChooser.showOpenDialog(primaryStage);
		TTConfig ttConfig = null;
		if (ttFile != null) {
			String ttFileUrl = ttFile.getAbsolutePath();
			ttConfig = generateTTConfig(ttFileUrl);
		}

		return ttConfig;
	}

	/** generate the TTConfig from the url of the tt file */
	TTConfig generateTTConfig(String ttFileUrl) {
		StringProperty logsTextString = StartController.getLogsTextString();
		TimeTable timeTable = new TimeTable(ttFileUrl);
		String logs = timeTable.fileLoading();
		if (logs.startsWith("Warning"))//to set the logs area text color to red
			logsTextString.setValue("Warning\n"+logsTextString.get() + "\n" + logs);
		else
			logsTextString.setValue(logsTextString.get() + "\n" + logs);

		TTConfig ttConfig = new TTConfig(timeTable);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		File directory = new File("logs");
		if (!directory.exists()) {
			directory.mkdir();
		}
		String pathLogs = "logs/TT" + timestamp.toString().replace(":", "") + ".txt";
		initFolders();
		if (!logsFolder.equals("")) {
			directory = new File(logsFolder);
			if (directory.exists())
				pathLogs = logsFolder + "/TT" + timestamp.toString().replace(":", "") + ".txt";
		}
		ttConfig.init(pathLogs, true);

		TTChecker ttChecker = new TTChecker(timeTable);
		if (!ttChecker.hasNoCycle()) {
			logsTextString.setValue(logsTextString.getValue() + "The TimeTable has a cycle starting from "
					+ ttChecker.OriginOfCycle + "\n");
		} else {
			logsTextString.setValue(logsTextString.getValue() + "The TimeTable has no cycle\n");

		}

		if (!ttChecker.verifiedConstraints()) {
			logsTextString.setValue(logsTextString.getValue() + "The TimeTable has unsatisfied time constraints\n");

			// log wrong dependencies
			for (TableDependency d : ttChecker.WrongDependencies) {
				logsTextString.setValue(logsTextString.getValue() + d.getStartEvent() + "->" + d.getEndEvent() + "\n");
			}
		} else {
			logsTextString.setValue(logsTextString.getValue() + "All constraints in the TimeTable are satisfied\n");
		}
		HashMap<Tag, Boolean> listTags = new HashMap<>();
		if (ttConfig != null) {
			for (TableEvent event : ttConfig.getTable().events.values()) {
				for (String s : event.getTags()) {
					boolean existTag = false;
					for (Tag tag : listTags.keySet()) {
						if (tag.getName().equals(s)) {
							existTag = true;
							tag.addEvent(event);
						}
					}
					if (!existTag) {
						Tag tag = new Tag(s);
						tag.addEvent(event);
						listTags.put(tag, false);
					}
				}
			}
		}
		StartController.setListTags(listTags);
		StartController.setDelaySum(0);
		StartController.setNbOfRealizedEvents(0);
		if (StartController.getResetDateTTLoad())// && StartController.getDateTT() != null)
			// StartController.getDateTT().setText("0");
			StartController.setDateTT("0");
		StartController.setCorrespLoaded(false);
		StartController.getInfosCorrespString().setValue("");

		/*
		 * Graph graph = graph().directed(); for (TableDependency d :
		 * ttConfig.getTable().getDependencies()) { TableEvent start =
		 * ttConfig.getTable().getEvent(d.getStartEvent()); TableEvent end =
		 * ttConfig.getTable().getEvent(d.getEndEvent()); graph =
		 * graph.with(node(start.getLabel()).link(end.getLabel())); } try {
		 * Graphviz.fromGraph(graph).render(Format.PNG).toFile(new
		 * File("graphviz/tt.png")); } catch (IOException e) { 
		 * catch block e.printStackTrace(); }
		 */
		StartController.setTtConfig(ttConfig);

		return ttConfig;
	}

	/** choose the correspondance file and return the path */
	void loadCorrespondance(Stage primaryStage, StringProperty infosCorrespString) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CORespondancies files", "*.cor");
		fileChooser.getExtensionFilters().add(extFilter);
		/** init the directory set by the preferences pane */
		initFolders();
		if (!netsFolder.equals("")) {
			File directory = new File(netsFolder);
			if (directory.exists())
				fileChooser.setInitialDirectory(new File(netsFolder));
		}
		/** the file of the correspondancies to be loaded for the simulation */
		File correspFile = fileChooser.showOpenDialog(primaryStage);
		/** the file url of the correspondance file to simulate */
		String correspFileUrl = null;
		if (correspFile != null) {
			correspFileUrl = correspFile.getAbsolutePath();
			generateCorrespondance(correspFileUrl);
		}

	}

	/** generate the RPNConfig and RegulNet objects from the correspondance file */
	void generateCorrespondance(String correspFileUrl) {
		StringProperty infosCorrespString = StartController.getInfosCorrespString();
		/** get the simu and ttconfig to build the RegulNet */
		Sim simu = StartController.getSimu();
		TTConfig ttConfig = StartController.getTtConfig();
		if (simu == null || ttConfig == null)
			infosCorrespString.setValue("You must load a net and a timetable before a correspondance file");
		else if (correspFileUrl != null) {
			StartController.setCorrespondancePath(correspFileUrl);
			PhysicalModel n = simu.getN();
			TimeTable tt = ttConfig.getTable();
			RegulNet rn = new RegulNet(tt, n);
			StartController.setRn(rn);
			StringProperty logs = StartController.getLogsTextString();
			String rnLoadResult = rn.LoadCorrespondance(correspFileUrl);
			if (rnLoadResult.startsWith("Warning"))
				logs.set("Warning \n"+logs.get()+"\n"+rnLoadResult);
			else
				logs.set(logs.get()+"\n"+rnLoadResult);
			rn.buildCorrespondance();
			infosCorrespString.setValue("The correspondancies file is loaded");
			StartController.setCorrespLoaded(true);

			/** set up the log file for the RPNConfig */
			ttConfig.init();
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			File directory = new File("logs");
			if (!directory.exists()) {
				directory.mkdir();
			}
			String pathLogs = "logs/Joint" + timestamp.toString().replace(":", "") + ".txt";
			initFolders();
			String logsFolder = getLogsFolder();
			if (!logsFolder.equals("")) {
				directory = new File(logsFolder);
				if (directory.exists())
					pathLogs = logsFolder + "/Joint" + timestamp.toString().replace(":", "") + ".txt";
			}
			boolean enableLogs = false;
			if (!this.console && StartController.enableLogs.get())
				enableLogs = true;
			RPNConfig rpnc = new RPNConfig(n, ttConfig, pathLogs, enableLogs);
			StartController.setRpnc(rpnc);
			StartController.setConnectTT(true);
		}else {
			
		}
	}

	/** load the project file which contain net, tt and correspondancies */
	void loadProject() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PROJECT files", "*.project");
		fileChooser.getExtensionFilters().add(extFilter);
		/** init the directory set by the preferences pane */
		initFolders();
		if (!getNetsFolder().equals("")) {
			File directory = new File(getNetsFolder());
			if (directory.exists())
				fileChooser.setInitialDirectory(new File(getNetsFolder()));
		}
		/**
		 * the file of the project file (net, tt, correspondance) to be loaded for the
		 * simulation
		 */
		File projectFile = fileChooser.showOpenDialog(Main.getPrimaryStage());
		if (projectFile != null) {
			loadProjectFile(projectFile);
		}
	}

	void loadProjectFile(File projectFile) {
		BufferedReader br = null;
		StartController.setProjectFileName(projectFile.getName());
		String path = projectFile.getParentFile().getPath();
		ArrayList<Delay> delays = new ArrayList<>();
		try {
			br = new BufferedReader(new FileReader(projectFile));
			while (br.ready()) { // until end of file
				// System.out.println("LINE");
				String line = br.readLine();
				String[] segments = line.split(":");
				if (segments.length > 1)
					System.out.println(segments[1]);
				if (segments[0].equals("net")) {
					//StartController.setSimu(generateSimu(getNetsFolder() + "/" + segments[1]));
					StartController.setSimu(generateSimu(path + "/" + segments[1]));
				}else if (segments[0].equals("tt")) {
					//StartController.setTtConfig(generateTTConfig(getTimeTablesFolder() + "/" + segments[1]));
					StartController.setTtConfig(generateTTConfig(path + "/" + segments[1]));
				}else if (segments[0].equals("corresp")) {
					//generateCorrespondance(getNetsFolder() + "/" + segments[1]);
					generateCorrespondance(path + "/" + segments[1]);
				}else if (segments[0].equals("scenario")) {
					PhysicalModel n = StartController.getSimu().getN();
					TransitionAbstract t = n.findTransition(segments[1]);
					String[] dates = segments[2].replace("[", "").replace("]", "").split(",");
					Delay delay = new Delay(Float.parseFloat(dates[0]), Float.parseFloat(dates[1]), t.getNumber(), Integer.parseInt(segments[3]));
					delays.add(delay);
				}
			}
			br.close();
			StartController.setDelays(delays, false);
		} catch (Exception e) {
			e.printStackTrace();
			if (!this.console) {
				logger.log(Level.WARNING, "error while reading the BufferedReader in class LoadFiles/loadProjectFile");
				StartController.logsTextString.set("Warning : error while loading the project : " + projectFile.getAbsolutePath() + "\nCheck edit/preferences/nets folder");
			}
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class LoadFiles/loadProjectFile");
				}
			}
		}
	}

	/**
	 * save the state of the project : current project, net state, tt state called
	 * from the StartController.saveState() method
	 */
	void saveState() {
		String projectFileName = StartController.getProjectFileName();
		FileChooser fileChooser = new FileChooser();
		initFolders();
		if (!getNetsFolder().equals("")) {
			fileChooser.setInitialDirectory(new File(getNetsFolder()));
		}

		// Set extension filter for text files
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("States files (*.state)", "*.state",
				"*.project");
		fileChooser.getExtensionFilters().add(extFilter);

		// Show save file dialog
		File file = fileChooser.showSaveDialog(Main.getPrimaryStage());

		// Write the data in the file
		if (file != null) {
			try {
				PrintWriter writer;
				writer = new PrintWriter(file);
				writer.println("Project:" + projectFileName + ":" + StartController.ttConfig.getCurrentDate());
				for (Delay delay : StartController.getDelays()) {
					writer.println("Delay:"+delay.getTransitionNb()+":"+delay.getStartDate()+":"+delay.getEndDate()+":"+delay.getDelay());
				}
				writer.println("Net:");
				if (StartController.getSimu() != null && StartController.getClassModel().equals("Net")) {
					//if it is a token net
					Net n = (Net) StartController.getSimu().getN();
					for (Place p : n.getCurrentMarking().getListMarked())
						writer.println("place:" + p.getNumber());
					for (int t : n.blocked.keySet())
						writer.println("blocked:" + t);
					for (int t : n.enabled.keySet())
						writer.println("enabled:" + t + ":" + n.getTransitions().get(t).getClock());
					for (int t : n.firable.keySet())
						writer.println("fireable:" + t);
				}else if (StartController.getSimu() != null && StartController.getClassModel().equals("TrajectoryNet")) {
					//if it is a trajectory net
					TrajectoryNet n = (TrajectoryNet) StartController.getSimu().getN();
					//save content of a place as : 
					//trajectory:placeNb:segment1:segment2:etc
					//segments are describe as coordinates of points: p0.x,p0.y,p1.x,p1.y
					for (TrajPlace p : n.places.values()) {
						for (Trajectory traj : p.trajectories) {
							String textTraj = "trajectory:"+p.getNumber();
							for (Segment segment : traj.getSegments()) {
								textTraj += ":"+segment.getP0().getX()+","+segment.getP0().getY()+","+segment.getP1().getX()+","+segment.getP1().getY();
							}
							writer.println(textTraj);
						}
					}
					for (BooleanPlace bp : n.booleanPlaces.values()) {
						if (bp.isValue())
							writer.println("control:"+bp.getNumber());
					}
					for (int t : n.blocked.keySet())
						writer.println("blocked:" + t);
					for (int t : n.enabled.keySet())
						writer.println("enabled:" + t + ":" + n.getTransitions().get(t).getClock());
					for (int t : n.fireable.keySet())
						writer.println("fireable:" + t);
				}
				writer.println("TimeTable:");
				if (StartController.getTtConfig() != null) {
					TimeTable tt = StartController.getTtConfig().getTable();
					for (TableEvent te : tt.getEvents().values().stream().sorted(Comparator.comparing(TableEvent::getRecordedDate)).filter(o -> o.getRealized())
							.collect(Collectors.toList()))
						writer.println(te.getNumber() + ":" + te.getRecordedDate());
				}
				writer.close();
			} catch (IOException ex) {
				logger.log(Level.WARNING, "Warning message");
			}
		}
	}

	/**
	 * load the state of a project it is called from the StartController.loadState()
	 * method
	 */
	void loadState() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("States files (*.state)", "*.state");
		fileChooser.getExtensionFilters().add(extFilter);
		/** init the directory set by the preferences pane */
		initFolders();
		if (!netsFolder.equals("")) {
			File directory = new File(netsFolder);
			if (directory.exists())
				fileChooser.setInitialDirectory(new File(netsFolder));
		}
		/** the file of the correspondancies to be loaded for the simulation */
		File stateFile = fileChooser.showOpenDialog(Main.getPrimaryStage());
		/** the file url of the correspondance file to simulate */
		String stateFileUrl = null;
		if (stateFile != null) {
			stateFileUrl = stateFile.getAbsolutePath();
			BufferedReader br = null;
			HashMap<Integer, Float> events = new HashMap<>();
			String date = "0";
			ArrayList<Delay> delays = new ArrayList<>();
			try {
				br = new BufferedReader(new FileReader(stateFileUrl));
				while (br.ready()) { // until end of file
					String line = br.readLine();
					if (line.startsWith("Project")) {
						String[] segments = line.split(":");
						File projectFile = new File(netsFolder + "/" + segments[1]);
						loadProjectFile(projectFile);
						date = segments[2];
						
					}else if (line.startsWith("Delay")){
						String[] segments = line.split(":");
						Delay delay = new Delay (Float.parseFloat(segments[2]),Float.parseFloat(segments[3]),Integer.parseInt(segments[1]),Float.parseFloat(segments[4]));
						delays.add(delay);
					}else if (line.startsWith("Net") && StartController.getSimu() != null
							&& StartController.getClassModel().equals("Net")) {
						StartController.setDelays(delays, true);
						Net n = (Net) StartController.getSimu().getN();
						ArrayList<Place> marking = new ArrayList<>();
						HashMap<Integer, Transition> fireable = new HashMap<>();
						HashMap<Integer, Transition> blocked = new HashMap<>();
						HashMap<Integer, Transition> enabled = new HashMap<>();
						HashMap<Integer, Float> config = new HashMap<>();
						while (br.ready() && !line.startsWith("TimeTable")) {
							line = br.readLine();
							if (!line.startsWith("TimeTable")) {
								String[] segments = line.split(":");
								if (segments[0].equals("place")) {
									Place p = n.getPlace(Integer.parseInt(segments[1]));
									marking.add(p);
								}else if (segments[0].equals("enabled")) {
									Transition t = n.findTransition(Integer.parseInt(segments[1]));
									enabled.put(t.getNumber(), t);
									t.setClock(Float.parseFloat(segments[2]));
									config.put(t.getNumber(), Float.parseFloat(segments[2]));
								}else if (segments[0].equals("fireable")) {
									Transition t = n.findTransition(Integer.parseInt(segments[1]));
									t.setClock(0f);
									fireable.put(t.getNumber(), t);
								}else if (segments[0].equals("blocked")) {
									Transition t = n.findTransition(Integer.parseInt(segments[1]));
									t.setClock(0f);
									blocked.put(t.getNumber(), t);
								}
							}
						}
						Marking m = new Marking(marking);
						n.setCurrentMarking(m);
						n.reset(false);
						n.setBlocked(blocked);
						n.setEnabled(enabled);
						n.setFirable(fireable);
						n.setConfig(config);
					}else if (line.startsWith("Net") && StartController.getSimu() != null
							&& StartController.getClassModel().equals("TrajectoryNet")) {
						StartController.setDelays(delays, true);
						TrajectoryNet n = (TrajectoryNet) StartController.getSimu().getN();
						HashMap<Integer, TrajTransition> fireable = new HashMap<>();
						HashMap<Integer, TrajTransition> blocked = new HashMap<>();
						HashMap<Integer, TrajTransition> enabled = new HashMap<>();
						n.currentMarking.reset();
						n.reset(false);
						while (br.ready() && !line.startsWith("TimeTable")) {
							line = br.readLine();
							if (!line.startsWith("TimeTable")) {
								String[] segments = line.split(":");
								if (segments[0].equals("trajectory")) {//trajectory:placeNb:x1,y1,x2,y2:x2,y2,x3,y3
									TrajPlace p = (TrajPlace) n.findPlace(Integer.parseInt(segments[1]));
									Trajectory traj = new Trajectory();
									for (int i = 2; i < segments.length; i++) {
										String[] points = segments[i].split(",");
										float x0 = Float.parseFloat(points[0]);
										float y0 = Float.parseFloat(points[1]);
										float x1 = Float.parseFloat(points[2]);
										float y1 = Float.parseFloat(points[3]);
										Segment segment = new Segment(x0, y0, x1, y1);
										traj.addSegment(segment);
									}
									p.addTrajectory(traj);
								}else if (segments[0].equals("enabled")) {
									TrajTransition t = (TrajTransition) n.findTransition(Integer.parseInt(segments[1]));
									enabled.put(t.getNumber(), t);
									t.setClock(Float.parseFloat(segments[2]));
								}else if (segments[0].equals("fireable")) {
									TrajTransition t = (TrajTransition) n.findTransition(Integer.parseInt(segments[1]));
									t.setClock(0f);
									fireable.put(t.getNumber(), t);
								}else if (segments[0].equals("blocked")) {
									TrajTransition t = (TrajTransition) n.findTransition(Integer.parseInt(segments[1]));
									t.setClock(0f);
									blocked.put(t.getNumber(), t);
								}
							}
						}
						n.blocked = blocked;
						n.enabled = enabled;
						n.fireable = fireable;
					} else {// the line is in the timetable section
						TimeTable tt = StartController.getTtConfig().getTable();
						TTConfig ttConfig = StartController.getTtConfig();
						String[] segments = line.split(":");
						TableEvent te = tt.getEvent(Integer.parseInt(segments[0]));
						te.setRecordedDate(Float.parseFloat(segments[1]));
						ttConfig.timeMove(te.getRecordedDate());
						ttConfig.discreteMoveTTPN(te.getNumber());
						TableEvent prete = te;
						while (br.ready()) {
							line = br.readLine();
							segments = line.split(":");
							te = tt.getEvent(Integer.parseInt(segments[0]));
							te.setRecordedDate(Float.parseFloat(segments[1]));
							ttConfig.timeMove(te.getRecordedDate() - prete.getRecordedDate());
							ttConfig.discreteMoveTTPN(te.getNumber());
							prete = te;
						}
						ttConfig.setCurrentDate(Float.parseFloat(date));						
						StartController.getRpnc().setCurrentTime(Float.parseFloat(date));
						StartController.setDateTT(date);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						logger.log(Level.WARNING,
								"error while closing the BufferedReader in class LoadFiles.loadState");
					}
				}
			}
		}
	}

	/** init the folders path as defined in the preferences pane */
	void initFolders() {
		BufferedReader br = null;
		Logger logger = Logger.getLogger("logger");
		try {
			br = new BufferedReader(new FileReader("foldersConfiguration.txt"));

			while (br.ready()) { // Tant qu'on peut encore lire des choses
				String line = br.readLine();
				String[] elements = line.split("%");
				if (elements[0].equals("logs") && elements.length > 1)
					logsFolder = elements[1];
				else if (elements[0].equals("nets") && elements.length > 1)
					netsFolder = elements[1];
				else if (elements[0].equals("tt") && elements.length > 1)
					timeTablesFolder = elements[1];
				else if (elements[0].equals("tooltips") && elements.length > 1) {
					if (elements[1].equals("off")) {
						StartController.setTooltips(false);
						StartController.setTooltips(true);;
						//System.out.println("tooltips off");
					}
					else {
						StartController.setTooltips(true);
						StartController.setTooltips(false);
					}
				}
					
			}
		} catch (Exception e) {
			if (!this.console) 
				logger.log(Level.WARNING, "error while reading the BufferedReader in class LoadFiles");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class LoadFiles");
				}
			}
		}
	}
	
	/**load the content of the net file in the source tab*/
	public void loadSourceNetContent() {
		Sim simu = StartController.getSimu();
		String fname = "";
		if (simu != null)
			fname = simu.getN().fname;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fname));
			StringProperty source = StartController.getSourceNetText();
			source.set("");
			while (br.ready()) { // Tant qu'on peut encore lire des choses
				String line = br.readLine();
				source.set(source.get()+line+"\n");
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "error while reading the BufferedReader in class LoadFile");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class LoadFiles");
				}
			}
		}
	}
	
	/**load the content of the tt in the source events tab*/
	public void loadSourceEventsContent() {
		TTConfig ttConfig = StartController.getTtConfig();
		String fname = "";
		if (ttConfig != null)
			fname = ttConfig.getTable().getFilename();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fname));
			StringProperty source = StartController.getSourceEventsTextString();
			source.set("");
			while (br.ready()) { // Tant qu'on peut encore lire des choses
				String line = br.readLine();
				source.set(source.get()+line+"\n");
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "error while reading the BufferedReader in class LoadFile");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class LoadFiles");
				}
			}
		}
	}
	
	public void loadSourceCorContent() {
		String fname = StartController.getCorrespondancePath();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fname));
			StringProperty source = StartController.getSourceCorTextString();
			source.set("");
			while (br.ready()) { // Tant qu'on peut encore lire des choses
				String line = br.readLine();
				source.set(source.get()+line+"\n");
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "error while reading the BufferedReader in class LoadFile");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class LoadFiles");
				}
			}
		}
	}

	public String getLogsFolder() {
		return logsFolder;
	}

	public String getNetsFolder() {
		return netsFolder;
	}

	public String getTimeTablesFolder() {
		return timeTablesFolder;
	}
	
	public String loadFile(Stage stage) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		/** the file to be loaded*/
		File netFile = fileChooser.showOpenDialog(stage);
		/** the file url*/
		String netFileUrl = null;
		if (netFile != null)
			netFileUrl = netFile.getAbsolutePath();
		return netFileUrl;
	}
	
	void loadWindowPosition(SplitPane global, SplitPane tables, SplitPane logsAndStats) throws Exception {
		BufferedReader br = null;
		Logger logger = Logger.getLogger("logger");
		try {
			br = new BufferedReader(new FileReader("windowPosition.txt"));

			for (int i = 0; i < 3; i++) { // (une ligne par splitpane)
				String line = br.readLine();
				String[] elements = line.split("/");
				if (i == 1) {
					for (int j = 0; j < global.getDividerPositions().length; j++)
						global.setDividerPosition(j, Double.parseDouble(elements[j]));
				} else if (i == 2) {
					for (int j = 0; j < tables.getDividerPositions().length; j++)
						tables.setDividerPosition(j, Double.parseDouble(elements[j]));
				} else if (i == 3) {
					for (int j = 0; j < logsAndStats.getDividerPositions().length; j++)
						logsAndStats.setDividerPosition(j, Double.parseDouble(elements[j]));
				}
			}
		} catch (Exception e) {
			if (!this.console) 
				logger.log(Level.WARNING, "error while reading the BufferedReader in class LoadFiles file windowPosition.txt");
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing the BufferedReader in class LoadFiles");
					throw e;
				}
			}
		}
	}

}
