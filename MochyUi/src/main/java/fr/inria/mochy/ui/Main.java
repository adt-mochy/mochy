package fr.inria.mochy.ui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.ModuleLayer.Controller;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Entry point of the Application
 * <p>
 * <img src="Main.svg" alt="Main class diagram" style="float: right;">
 */
public class Main extends Application{

	static Stage primaryStage;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	/**load the start interface-prepare the components menu, tables, fiels and buttons*/
	@Override
	public void start(Stage stage) {
		
			primaryStage = stage;
			
			//load the xml file of the user interface
			/*Scene scene = new Scene((Parent) JfxUtils.loadFxml("start.fxml"));
			primaryStage.setScene(scene);
			primaryStage.setTitle("MOCHY");
			primaryStage.setMaximized(true);
			primaryStage.show();*/
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("start.fxml"));
			Parent root = null;
			try {
				root = loader.load();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			StartController controller = loader.getController();
			double[] windowSize = getWindowSize();
			Scene scene;
			if (windowSize[0] != 0 && windowSize[1] != 0)
				scene = new Scene(root, windowSize[0], windowSize[1]);
			else 
				scene = new Scene(root);
			primaryStage = new Stage();
			primaryStage.setScene(scene);
			primaryStage.setTitle("MOCHY");
			primaryStage.setOnCloseRequest(e -> {controller.saveWindowPosition(); Platform.exit();});
			primaryStage.show();
	}

	public static Stage getPrimaryStage () {return primaryStage;}
	
	/*@Override
	public void stop() {
		StartController.saveWindowPosition();
	}*/
	
	double[] getWindowSize() {
		BufferedReader br = null;
		Logger logger = Logger.getLogger("logger");
		double[] windowSize = new double[2];
		try {
			br = new BufferedReader(new FileReader("windowPosition.txt"));
			String line = br.readLine();
			String[] elements = line.split("/");
			windowSize[0] = Double.parseDouble(elements[0]);
			windowSize[1] = Double.parseDouble(elements[1]);
		} catch (Exception e) {
			System.out.println("initial window size : window preferences not loaded");
			windowSize[0] = 0;
			windowSize[1] = 0;
		}
		return windowSize;
	}
	
}
