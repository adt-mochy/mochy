package fr.inria.mochy.ui;

import java.lang.reflect.Method;

public class MethodType {
	Method method;
	
	MethodType(Method method) {
		this.method = method;
	}
	
	public Method getMethod() {
		return method;
	}
	
	public void setMethod(Method method) {
		this.method = method;
	}
	
	@Override
	public String toString() {
		String s = method.getName();
		if (method.getParameterCount()>0)
			s += ":"+method.getParameters()[0].getParameterizedType().getTypeName();
		return s;
	}
	
	public String getName() {
		return method.getName();
	}
}
