package fr.inria.mochy.ui;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Polygon;

import fr.inria.mochy.addons.MultipleAxesLineChart;
import fr.inria.mochy.core.RPN.RPNConfig;
import fr.inria.mochy.core.RPN.RegulNet;
import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.equalization.EquNetNeural;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.sampler.Sampler;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.Tag;
import fr.inria.mochy.core.timetable.TimeTable;
import fr.inria.mochy.core.trajectory.TrajPlace;
import fr.inria.mochy.statsAndTasks.AbstractStats;
import fr.inria.mochy.statsAndTasks.MultipleRunsTask;
import fr.inria.mochy.statsAndTasks.MultipleTokensNbTask;
import fr.inria.mochy.statsAndTasks.StatsTask;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Allow to launch a simulation of multiple runs on a timetable with or without
 * the connection to a net. It uses the stats and is available from the
 * Stats/Multiple Runs menu.
 * <p>
 * <img src="MultipleRunsController.svg" alt="MultipleRunsController class
 * diagram" style="float: right;">
 */
public class MultipleRunsController implements Initializable {

	Task<ArrayList[]> task;// = new StatsTask();

	@FXML
	TextField nbRuns;

	@FXML
	CheckBox logsItems;// true if the transition/event fired are displayed in the log pane

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		nbRuns.setText("1");

	}

	@FXML
	/**
	 * perform multiple runs of timetable, the number of runs is defined in
	 * stats/multiple runs
	 */
	public void run(ActionEvent evt) {
		//run();
		int runs = 1;
		if (nbRuns != null) {
			runs = Integer.parseInt(nbRuns.getText());
		}
		MultipleRunsTask task = new MultipleRunsTask(runs, logsItems);
		equalizationFullStats(false, false, true, task);
	}

	public void run() {
		StringProperty logs = StartController.getLogsTextString();
		StringProperty stats = StartController.getStatsTextString();
		Sampler sampler = new Sampler();
		HashMap<Tag, Boolean> listTags = StartController.getListTags();
		float delaySumTotal = 0;
		int realizedEvtsTotal = 0;
		int deadLocksNb = 0; // the number of locks during the simulation, eg there is no move available to
								// realize the next events
		float timeBetweenTwoPlacesSum = 0;// the sum of the time between start and end places to calculate the average
											// time
		
		//HashMap<Integer, ArrayList<Float>> startPlaceTime = new HashMap<>();// the list of the time at the start place
																			// for a specific token
		//HashMap<Integer, ArrayList<Float>> endPlaceTime = new HashMap<>();// the list of the time at the end place for a
																			// specific token
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		// used for the EqualizationNet steps
		float standardDeviationAvg = 0;
		float timeElapsedAvg = 0;
		int discreteStepsAvg = 0;
		int stepsAvg = 0;
		float minTime = Float.MAX_VALUE;
		float maxTime = 0;
		int minSteps = Integer.MAX_VALUE;
		int maxSteps = 0;
		float speedAvg = 0;
		int speedNb = 0;// the number of steps done when the speed sum is calculated
		int sumDifferenceTime = 0;

		stats.set("");
		String log = "logs/Joint" + timestamp.toString().replace(":", "") + ".txt";
		int runs = 1;
		if (nbRuns != null) {
			runs = Integer.parseInt(nbRuns.getText());
		}
		for (int i = 1; i <= runs; i++) {
			TTConfig ttConfig = StartController.getTtConfig();
			Sim simu = StartController.getSimu();
			RPNConfig rpnc = StartController.getRpnc();
			RegulNet rn = StartController.getRn();
			String correspPath = StartController.getCorrespondancePath();
			float delaySum = 0;
			int nbRealizedEvt = 0;
			String firstDelay = "";
			float timeStartPlace = -1;
			float timeEndPlace = -1;
			int nbJourneyBetweenTwoPlaces = 0;// the number of journey between the start and end places to calculate the
			// average time
			ArrayList<Float>[] startList = new ArrayList[StartController.simu.getN().nbTokens];
			for (int j = 0; j < startList.length; j++)
				startList[j] = new ArrayList<>();
			
			ArrayList<Float>[] endList = new ArrayList[StartController.simu.getN().nbTokens];
			for (int j = 0; j < endList.length; j++)
				endList[j] = new ArrayList<>();
			
			if (ttConfig != null && !StartController.getConnectTT()) {// if we make stats on the table only
				ttConfig.reset();
				while (!ttConfig.allRealized()) {// while all events are not realized
					float timedMove = sampler.invertTransform(1);
					timedMove = -1;
					for (TableEvent te : ttConfig.getMinevents()) {
						if ((te.getDate() - ttConfig.getCurrentDate()) <= 0) {
							timedMove = -1;
							break;
						} else if (timedMove == -1 && (te.getDate() - ttConfig.getCurrentDate()) > 0)
							timedMove = te.getDate() - ttConfig.getCurrentDate();
						else if ((te.getDate() - ttConfig.getCurrentDate()) > 0)
							timedMove = Math.min(timedMove, (te.getDate() - ttConfig.getCurrentDate()));
					}
					if (timedMove != -1)
						ttConfig.timeMove(timedMove);
					if (ttConfig.getMinevents().size() > 0) {
						TableEvent minEvt = ttConfig.getMinevents().get(0);
						for (TableEvent e : ttConfig.getMinevents()) {
							if (e.getDate() < minEvt.getDate())
								minEvt = e;
						}
						if (ttConfig.discreteMove(minEvt.getNumber())) {
							nbRealizedEvt++;
							delaySum += ttConfig.getCurrentDate() - minEvt.getInitialDate();
							realizedEvtsTotal++;
							for (Tag tag : listTags.keySet()) {
								if (tag.containEvent(minEvt))
									tag.addDelayOfAnEvent(ttConfig.getCurrentDate() - minEvt.getInitialDate());
							}
						}
					}
				}
				delaySumTotal += delaySum;
				if (nbRealizedEvt > 0)
					stats.set(stats.get() + "\nrun " + i + " - delay sum : " + delaySum + " - average delay : "
							+ (int) (delaySum / nbRealizedEvt));
				ttConfig.reset();
			}
			/** if the timetable is connected to the net */
			else if (ttConfig != null && simu != null && rpnc != null && StartController.isCorrespLoaded()
					&& StartController.getConnectTT()) {
				rpnc.reset();
				PhysicalModel n = rpnc.getNetAndTT().getNet();
				// n.reset();
				TimeTable tt = rpnc.getNetAndTT().getTable();

				rpnc = new RPNConfig(n, ttConfig, log, true);
				rpnc.setDelays(StartController.getDelays());

				rn.setNet(n);
				rn.setTable(ttConfig.getTable());
				StartController.setTtConfig(ttConfig);
				StartController.setRpnc(rpnc);

				boolean deadLock = false; // true when there is no firable transition to continue the realization of the
											// events

				HashMap<Integer, Integer> positionToken = new HashMap<>();// <placeNb,tokenNb>
				boolean net = false;//true if the type of physical model is Net
				if (StartController.isCalculTime()) {
					int j = 0;
					for (PlaceAbstract aP : StartController.simu.getN().getPlaces().values()) {
						if (aP instanceof Place) {
							net = true;
							Place p = (Place) aP;
							if (p.getContents() != null) {
								positionToken.put(p.getNumber(), j);
								j++;
							}
						} else if (aP instanceof TrajPlace) {
							TrajPlace p = (TrajPlace) aP;

						}
					}
				}

				while (!ttConfig.allRealized() && !deadLock) {// while all events are not realized
					StringProperty logsTextString = StartController.getLogsTextString();

					boolean dmoveFired = StartController.tryDiscreteMove();

					if (dmoveFired && StartController.isEventFired()) {// If firinf the table Event was successful
						TableEvent cte = StartController.getEvent();
						TransitionAbstract onet = StartController.getTransition();
						nbRealizedEvt++;
						if (ttConfig.getCurrentDate() - cte.getInitialDate() > StartController.getDelayEscape()) {
							if (StartController.getMaxInterval() == null)
								delaySum += ttConfig.getCurrentDate() - cte.getInitialDate();
							else if (cte.getDate() > StartController.getMinInterval()
									&& cte.getDate() < StartController.getMaxInterval())
								delaySum += ttConfig.getCurrentDate() - cte.getInitialDate();
						}
						if (firstDelay.equals("") && ttConfig.getCurrentDate() - cte.getInitialDate() > 0) {
							firstDelay = "event " + cte.getLabel() + " - nb " + cte.getNumber() + " - value "
									+ (ttConfig.getCurrentDate() - cte.getInitialDate() + " - date : "
											+ ttConfig.getCurrentDate());
						}
						realizedEvtsTotal++;
						for (Tag tag : listTags.keySet()) {
							if (tag.containEvent(cte))
								tag.addDelayOfAnEvent(ttConfig.getCurrentDate() - cte.getInitialDate());
						}
						if (StartController.isCalculTime() && net == true) {
							positionToken.put(onet.getPost().get(0).getNumber(),
									positionToken.get(onet.getPre().get(0).getNumber()));
							int startPlaceNb = Integer.parseInt(StartController.getStartPlace().split("-")[0]);
							if (onet.getPre().contains(n.findPlace(startPlaceNb))) {
								timeStartPlace = rpnc.getCurrentTime();
								// startPlaceTime.put(positionToken.get(onet.getPre().get(0).getNumber()),timeStartPlace);
								startList[positionToken.get(onet.getPre().get(0).getNumber())].add(timeStartPlace);
							}
							int endPlaceNb = Integer.parseInt(StartController.getEndPlace().split("-")[0]);
							if (onet.getPre().contains(n.findPlace(endPlaceNb))
									&& startList[positionToken.get(onet.getPre().get(0).getNumber())]
											.size() > endList[positionToken.get(onet.getPre().get(0).getNumber())]
													.size()) {
								timeEndPlace = rpnc.getCurrentTime();
								endList[positionToken.get(onet.getPre().get(0).getNumber())].add(timeEndPlace);
								// endPlaceTime.put(positionToken.get(onet.getPre().get(0).getNumber()),timeEndPlace);
								// timeBetweenTwoPlacesSum += timeEndPlace - timeStartPlace;
								// nbJourneyBetweenTwoPlaces++;
								// timeStartPlace = -1;
							}
							positionToken.remove(onet.getPre().get(0).getNumber());
						}
						n.discreteMove(onet, log, rpnc.getCurrentTime(), true);
						if (logsItems.isSelected()) {
							logsTextString.setValue(logsTextString.getValue() + "\nFiring t" + onet.getNumber() + "x e"
									+ cte.getNumber() + " Successful");
						}

					} else if (!dmoveFired) { // No discrete move is urgent, so the move is a timed move
						// or a timed move followed by a place filling

						// No discrete move is urgent, so the move is a timed move
						// or a timed move followed by a place filling

						float minTableTimeElapse = ttConfig.getTableDelay();
						float MinNetElapse = StartController.getNetDelay();
						float delayControlFill = StartController.delayPFill();
						if (logsItems.isSelected()) {
							logsTextString.setValue(logsTextString.getValue() + "\nTTelapse=" + minTableTimeElapse
									+ "\nNetelapse=" + MinNetElapse + "\nPFillelapse=" + delayControlFill);
						}
						float delayTmove = -3; // set to 0 for now

						if (delayControlFill == 0) { // some control places need to be filled now
							StartController.FillControlNow();
						} else {

							if (delayControlFill > 0) {

								delayTmove = delayControlFill;
							}

							if (minTableTimeElapse > 0) {
								if (delayTmove < 0) {
									delayTmove = minTableTimeElapse;
								} else {
									delayTmove = Math.min(minTableTimeElapse, delayControlFill);
								}
							}

							if (MinNetElapse > 0) {
								if (delayTmove < 0) {
									delayTmove = MinNetElapse;
								} else {
									delayTmove = Math.min(delayTmove, MinNetElapse);
								}
							}

						}
						if (logsItems.isSelected()) {
							logsTextString.setValue(logsTextString.getValue() + "\nTimed Move: " + delayTmove);
						}
						// Ready to perform timed move
						// the timed move cannot be negative
						if (delayTmove <= 0) {
							delayTmove = 0;
							deadLock = true;
						}
						rpnc.advanceVerifiedTime(delayTmove);
						// ttConfig.timeMove((int) delayTmove);

						// It remains to fill the control places if new event reach their occurence date
						StartController.FillControlNow();

					}
				}
				if (deadLock)
					deadLocksNb++;
				delaySumTotal += delaySum;
				if (nbRealizedEvt > 0)
					stats.set(stats.get() + "\nrun " + i + " - delay sum : " + delaySum + " - average delay : "
							+ (delaySum / nbRealizedEvt) + "\nFirst Delay : " + firstDelay + "\n");
				rpnc.reset();
			} else if (!StartController.isCorrespLoaded() && StartController.getConnectTT()
					&& !StartController.classModel.startsWith("EqualizationNet")) {
				logs.set("the correspondancies file must be loaded");
			} else if (StartController.classModel.startsWith("EqualizationNet")) {
				EquNet n = (EquNet) StartController.simu.getN();
				n.reset(false);
				float standardDeviation = Float.POSITIVE_INFINITY;
				int steps = 0;
				int limit = 1;
				try {
					limit = Integer.parseInt(StartController.getTargetEqualization());
					StartController.getLogsTextString().setValue("");
				} catch (NumberFormatException e) {
					e.printStackTrace();
					StartController.getLogsTextString()
							.setValue("Warning : the field next to steps equalization is not filled with a number");
				}

				while (standardDeviation > limit && steps != 10000) {
					simu.oneStep();
					standardDeviation = n.getStandardDeviation(StartController.getSimu().getPathLogs());
					if (n.isDiscreteMove()) {
						speedAvg += n.getLastTokenSpeed();
						speedNb++;
					}
					steps++;
				}

				if (steps != 10000) {
					stepsAvg += steps;
					standardDeviationAvg += standardDeviation;
					timeElapsedAvg += n.getTimeElapsed();
					discreteStepsAvg += n.getNbDiscreteSteps();
					minTime = Math.min(minTime, n.getTimeElapsed());
					maxTime = Math.max(maxTime, n.getTimeElapsed());
					minSteps = Math.min(minSteps, steps);
					maxSteps = Math.max(maxSteps, steps);
				} else
					deadLocksNb++;

			}
			timeBetweenTwoPlacesSum = 0;
			float difference = 0;
			nbJourneyBetweenTwoPlaces = 0;
			int nbTokens = StartController.simu.getN().nbTokens;// the number of marked places
			for (int j = 0; j < nbTokens; j++) {
				ArrayList<Float> start = startList[j];
				ArrayList<Float> end = endList[j];
				while (start.size() > 0 && end.size() > 0) {
					nbJourneyBetweenTwoPlaces++;
					//difference = endPlaceTime.get(0) - startPlaceTime.get(0);
					difference = end.get(0) - start.get(0);
					timeBetweenTwoPlacesSum += difference;
					//startPlaceTime.remove(0);
					//endPlaceTime.remove(0);
					start.remove(0);
					end.remove(0);
				}
			}
			sumDifferenceTime += timeBetweenTwoPlacesSum/nbJourneyBetweenTwoPlaces;
		}
		for (Tag tag : listTags.keySet()) {
			if (listTags.get(tag)) {// si le tag est actif pour les stats
				tag.computeConfidenceInterval();
				stats.set(stats.get() + "\nAverage delay at tag " + tag.getName() + " : " + tag.getAverrageDelay()
						+ " / 95% confidence interval is between " + tag.getAlpha() + " and " + tag.getBeta());
			}
			tag.reset();
		}
		if (realizedEvtsTotal > 0)
			stats.set(stats.get() + "\nTotal average delay : " + delaySumTotal / realizedEvtsTotal);

		
		
		// if (startPlaceTime.size() > 0 && endPlaceTime.size() > 0)
		// difference = endPlaceTime.get(endPlaceTime.size() - 1) -
		// startPlaceTime.get(0);//
		// timeBetweenTwoPlacesSum += difference;//
		
		// System.out.println("start and end PlaceTime size: " + startPlaceTime.size() +
		// " - " + endPlaceTime.size());
		// System.out.println(startPlaceTime.get(0) + "-" +
		// endPlaceTime.get(endPlaceTime.size() - 1));
		// for (int i = 0; i < nbTokens; i++) {
		// ArrayList<Float> startTimes = new ArrayList<>();
		// startTimes = startPlaceTime.entrySet().stream().filter(e ->
		// e.getValue().equals(i)).collect(Collectors.toList());
		// }
		

		if (sumDifferenceTime > 0)
			stats.set(stats.get() + "\nAverage time between " + StartController.getStartPlace() + " and "
					+ StartController.getEndPlace() + " : " + (sumDifferenceTime / runs));

		stats.set(stats.get() + "\nNumber of lock : " + deadLocksNb);

		if (StartController.classModel.startsWith("EqualizationNet")) {
			stepsAvg = stepsAvg / (runs - deadLocksNb);
			standardDeviationAvg = standardDeviationAvg / (runs - deadLocksNb);
			timeElapsedAvg = timeElapsedAvg / (runs - deadLocksNb);
			if (speedNb != 0)
				speedAvg = speedAvg / speedNb;
			discreteStepsAvg = discreteStepsAvg / (runs - deadLocksNb);
			int limit = Integer.parseInt(StartController.getTargetEqualization());
			stats.set(stats.get() + "\n steps and time necessary to get to a standard deviation < " + limit);
			stats.set(stats.get() + "\n for " + runs + " runs : ");
			stats.set(stats.get() + "\n steps average : " + stepsAvg + " min steps : " + minSteps + " max steps : "
					+ maxSteps);
			stats.set(stats.get() + "\n discrete steps average : " + discreteStepsAvg);
			stats.set(stats.get() + "\n time elapsed average : " + timeElapsedAvg + " min time : " + minTime
					+ " max time : " + maxTime);
			EquNet n = (EquNet) StartController.simu.getN();
			stats.set(stats.get() + "\n speed average : " + n.getAvgSpeed());
		}
	}

	/**
	 * allow to launch the statsTask class to perform simulation runs and generate
	 * equalization statistics only one of the boolean stats1, stats2, stats3 must
	 * be true to launch the specific class if stats3 is true, the statsTask
	 * parameter will be used
	 */
	public void equalizationFullStats(boolean stats1, boolean stats2, boolean stats3, AbstractStats statsTask) {

		final Label label = new Label("Simu : ");
		final ProgressBar progressBar = new ProgressBar(0);
		final ProgressIndicator progressIndicator = new ProgressIndicator(0);

		final Button startButton = new Button("Start");
		final Button cancelButton = new Button("Cancel");

		final Label statusLabel = new Label();
		statusLabel.setMinWidth(250);
		statusLabel.setTextFill(Color.BLUE);

		// Start Button.
		startButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				startButton.setDisable(true);
				progressBar.setProgress(0);
				progressIndicator.setProgress(0);
				cancelButton.setDisable(false);

				// Create a Task.
				if (stats1)
					task = new StatsTask();
				if (stats2)
					task = new MultipleTokensNbTask(StartController.simu.getFname());
				if (stats3)
					task = statsTask;

				task.setOnFailed(evt -> {
					System.err.println("The task failed with the following exception:");
					task.getException().printStackTrace(System.err);
					StartController.getLogsTextString().set("Warning : "+task.getException().getMessage());
				});

				// Unbind progress property
				progressBar.progressProperty().unbind();

				// Bind progress property
				progressBar.progressProperty().bind(task.progressProperty());

				// Hủy bỏ kết nối thuộc tính progress
				progressIndicator.progressProperty().unbind();

				// Bind progress property.
				progressIndicator.progressProperty().bind(task.progressProperty());

				// Unbind text property for Label.
				statusLabel.textProperty().unbind();

				// Bind the text property of Label
				// with message property of Task
				statusLabel.textProperty().bind(task.messageProperty());

				// When completed tasks
				task.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, //
						new EventHandler<WorkerStateEvent>() {

							@Override
							public void handle(WorkerStateEvent t) {
								/*List<File> copied = task.getValue();
								statusLabel.textProperty().unbind();
								statusLabel.setText("Copied: " + copied.size());*/

								statusLabel.textProperty().unbind();
								statusLabel.setText("Runs Done");

								if (stats2) {
									try {
										ArrayList[] results;
										results = (ArrayList[]) task.getValue();
										displayChart(results[0], results[1], results[2], results[3]);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						});

				// Start the Task.
				new Thread(task).start();
			}
		});

		// Cancel
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				startButton.setDisable(false);
				cancelButton.setDisable(true);
				// task.cancel(true);
				progressBar.progressProperty().unbind();
				progressIndicator.progressProperty().unbind();
				statusLabel.textProperty().unbind();
				//
				progressBar.setProgress(0);
				progressIndicator.setProgress(0);
				task.cancel();
			}
		});

		FlowPane root = new FlowPane();
		root.setPadding(new Insets(10));
		root.setHgap(10);

		root.getChildren().addAll(label, progressBar, progressIndicator, //
				statusLabel, startButton, cancelButton);

		Scene scene = new Scene(root, 500, 120, Color.WHITE);
		Stage stage = new Stage();
		stage.setTitle("Statistics runs");
		stage.setScene(scene);
		stage.show();
	}

	void displayChart(ArrayList<Integer> x, ArrayList<Float> y, ArrayList<Integer> y2, ArrayList<Float> y3) {
		Stage stage = new Stage();
		// defining the axes
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel("Number of tokens");
		yAxis.setLabel("Time elapsed");
		// creating the chart
		final LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);

		lineChart.setTitle("Data up to equalization");
		// lineChart2.setTitle("Average speed by number of tokens");
		// defining a series
		XYChart.Series<Number, Number> series = new XYChart.Series<>();
		series.setName("Average time elapsed");
		XYChart.Series<Integer, Integer> series2 = new XYChart.Series<>();
		series2.setName("Number of over target runs");
		XYChart.Series<Integer, Float> series3 = new XYChart.Series<>();
		series3.setName("Average speed");
		// populating the series with data
		for (int i = 0; i < x.size(); i++) {
			series.getData().add(new XYChart.Data<Number, Number>(x.get(i), y.get(i)));
			series2.getData().add(new XYChart.Data<Integer, Integer>(x.get(i), y2.get(i)));
			series3.getData().add(new XYChart.Data<Integer, Float>(x.get(i), y3.get(i)));
		}

		lineChart.getData().add(series);
		MultipleAxesLineChart chart = new MultipleAxesLineChart(lineChart, Color.BLACK);
		chart.addSeries(series3, Color.BLUE, false);
		chart.addSeries(series2, Color.GREEN, false);

		Scene scene = new Scene(chart, 800, 600);
		stage.setScene(scene);
		stage.show();
	}
}
