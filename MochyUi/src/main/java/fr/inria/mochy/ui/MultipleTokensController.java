package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

/**it allows to parameterized the MultipleTokensNbTask which run a simulation with
 * different numbers of tokens*/
public class MultipleTokensController implements Initializable{

	@FXML
	TextField from;//the start number of tokens
	
	@FXML
	TextField to;//the end number of tokens
	
	@FXML
	TextField steps;//the max number of steps for a run
	
	@FXML
	TextField runs;//the number of runs for a specific number of tokens
	
	@FXML
	TextField sd;//standard deviation target
	
	@FXML
	CheckBox logs;//enable or disable the logs of the discrete steps in the logs folder
	
	public static int fromValue;
	public static int toValue;
	public static int stepsValue;
	public static int runsValue;
	public static int standardDeviation;
	public static boolean enableLogs;
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		enableLogs=false;
		logs.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				enableLogs = newValue;
				StartController.enableLogs.set(newValue);
			}
		});
	}

	@FXML
	void launch(ActionEvent evt) {
		fromValue = Integer.parseInt(from.getText());
		toValue = Integer.parseInt(to.getText());
		stepsValue = Integer.parseInt(steps.getText());
		runsValue = Integer.parseInt(runs.getText());
		standardDeviation = Integer.parseInt(sd.getText());
		
		MultipleRunsController controller = new MultipleRunsController();
		controller.equalizationFullStats(false, true, false, null);
	}
	
}
