package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.statsAndTasks.GenerateNeuralTask;
import fr.inria.mochy.statsAndTasks.OptimizeNeuralNetTask;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class OptimizeNeuralController implements Initializable {
	@FXML TextField sdtf;
	@FXML TextField msf;
	@FXML TextField r;
	
	public static int standardDeviationTarget;
	public static int maxSteps;
	public static int runs;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}
	
	@FXML
	void launch(ActionEvent evt) {
		standardDeviationTarget = Integer.parseInt(sdtf.getText());
		maxSteps = Integer.parseInt(msf.getText());
		runs = Integer.parseInt(r.getText());
		MultipleRunsController controller = new MultipleRunsController();
		controller.equalizationFullStats(false, false, true, new OptimizeNeuralNetTask());
	}

}
