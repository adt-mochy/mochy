package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.statsAndTasks.OptimizeNeuralNetTask;
import fr.inria.mochy.statsAndTasks.OptimizeNnetMultiTokensNbTask;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class OptimizeNeuralMultiTokensNbController implements Initializable{
	@FXML TextField sdtf;
	@FXML TextField msf;
	@FXML TextField r;
	@FXML TextField tokensMin;
	@FXML TextField tokensMax;
	@FXML TextField tokensInterval;
	
	public static int standardDeviationTarget;
	public static int maxSteps;
	public static int runs;
	public static int tokMin;
	public static int tokMax;
	public static int tokInterval;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}
	
	@FXML
	void launch(ActionEvent evt) {
		standardDeviationTarget = Integer.parseInt(sdtf.getText());
		maxSteps = Integer.parseInt(msf.getText());
		runs = Integer.parseInt(r.getText());
		tokMin = Integer.parseInt(tokensMin.getText());
		tokMax = Integer.parseInt(tokensMax.getText());
		tokInterval = Integer.parseInt(tokensInterval.getText());
		MultipleRunsController controller = new MultipleRunsController();
		controller.equalizationFullStats(false, false, true, new OptimizeNnetMultiTokensNbTask());
	}
}
