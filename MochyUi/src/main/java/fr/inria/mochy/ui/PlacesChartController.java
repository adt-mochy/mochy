package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.core.dom.PlaceDom;
import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.trajectory.Segment;
import fr.inria.mochy.core.trajectory.Trajectory;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class PlacesChartController implements Initializable {
	@FXML
	LineChart<Number, Number> lineChart;

	@FXML
	private NumberAxis xAxis;

	@FXML
	private NumberAxis yAxis;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// the place which we want to display the trajectories
		PlaceDom p = View.p;

		if (StartController.classModel.equals("TrajectoryNet")) {
			int nb = p.getTrajectories().size();

			xAxis.setLabel("Time");
			yAxis.setLabel("Distance");

			lineChart.setTitle("Evolution of the trajectories");
			lineChart.getYAxis().setAutoRanging(true);
			lineChart.getXAxis().setAutoRanging(true);

			for (int i = 0; i < nb; i++) {
				XYChart.Series<Number, Number> serie = new XYChart.Series<>();
				Trajectory trajectory = p.getTrajectories().get(i);
				for (Segment segment : trajectory.getSegments()) {
					segment.drop();
					serie.getData()
							.add(new XYChart.Data<Number, Number>(segment.getP0().getX(), segment.getP0().getY()));
					serie.getData()
							.add(new XYChart.Data<Number, Number>(segment.getP1().getX(), segment.getP1().getY()));
				}
				lineChart.getData().add(serie);
			}
		}else if (StartController.getClassModel().startsWith("EqualizationNet")) {
			int nb = p.getTokens().size();

			xAxis.setLabel("Distance To Browse");
			yAxis.setLabel("Time To Browse");

			lineChart.setTitle("Evolution of the tokens");
			lineChart.getYAxis().setAutoRanging(true);
			lineChart.getXAxis().setAutoRanging(true);

			for (int i = 0; i < nb; i++) {
				XYChart.Series<Number, Number> serie = new XYChart.Series<>();
				Token token = p.getTokens().get(i);
				System.out.println("("+token.getxPlace()+","+token.allowedTime()+")");
				serie.getData()
							.add(new XYChart.Data<Number, Number>(token.getxPlace(), token.allowedTime()));
				serie.getData()
							.add(new XYChart.Data<Number, Number>(token.getPlace().getDistance(), 0));
				lineChart.getData().add(serie);
			}
		}
	}
}
