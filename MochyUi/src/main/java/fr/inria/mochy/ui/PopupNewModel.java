package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.core.mochysim.Sim;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**a pop up displayed to reload the model or not from the edit/preferences pane by chosing a new model*/
public class PopupNewModel  implements Initializable {
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	@FXML
	void yes (ActionEvent evt) {
		Sim simu = StartController.getSimu();
		LoadFiles loadFiles = new LoadFiles();
		loadFiles.generateSimu(simu.getFname());
		StartController.refreshNet(true);
		View.popupStage.close();
	}
	
	@FXML
	void no (ActionEvent evt) {
		View.popupStage.close();
	}
}
