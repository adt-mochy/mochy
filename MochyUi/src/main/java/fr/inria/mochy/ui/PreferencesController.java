package fr.inria.mochy.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.css.CssMetaData;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 * Set the default folders for input and output files and manage settings for the use of the tool.
 * It is available from the Edit/Preferences menu.
 * <p>
 * <img src="PreferencesController.svg" alt="PreferencesController class diagram" style="float: right;">
 */
public class PreferencesController implements Initializable {

	@FXML
	TextField logsFolder;
	@FXML
	TextField netsFolder;
	@FXML
	TextField timeTablesFolder;
	@FXML
	TextField sampling;
	@FXML
	CheckBox resetDate;
	@FXML
	CheckBox trajectoryNet;
	@FXML
	CheckBox equalizationNet;
	@FXML
	CheckBox logs;
	@FXML
	CheckBox tooltips;

	LoadFiles loadFiles = new LoadFiles();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		tooltips.setSelected(!StartController.tooltipsProperty.get());
		tooltips.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (!newValue) {
					StartController.setTooltips(true);
				}else
					StartController.setTooltips(false);
			}
		});
		if (StartController.enableLogs.get())
			logs.setSelected(true);
		logs.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				StartController.enableLogs.set(newValue);
			}
		});
		loadFiles.initFolders();
		logsFolder.setText(loadFiles.getLogsFolder());
		netsFolder.setText(loadFiles.getNetsFolder());
		timeTablesFolder.setText(loadFiles.getTimeTablesFolder());
		sampling.setText(Double.toString(StartController.getSampling()));
		resetDate.setSelected(StartController.isResetDateTTLoad());
		resetDate.selectedProperty().addListener((ChangeListener<? super Boolean>) new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (resetDate.isSelected())
					StartController.setResetDateTTLoad(true);
				else
					StartController.setResetDateTTLoad(false);
			}
	    });
		if (StartController.getClassModel().equals("TrajectoryNet"))
			trajectoryNet.setSelected(true);
		trajectoryNet.selectedProperty().addListener((ChangeListener<? super Boolean>) new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (trajectoryNet.isSelected()) {
					StartController.setClassModel("TrajectoryNet");
					StartController.setDisplayEqualizationFields(false);	
				}
				else if (equalizationNet.isSelected()) {
					StartController.setClassModel("EqualizationNet");
					StartController.setDisplayEqualizationFields(true);
				}
				else {
					StartController.setClassModel("Net");
					StartController.setDisplayEqualizationFields(false);
				}
				StringProperty logs = StartController.getLogsTextString();
				if (StartController.getSimu() != null && !StartController.getClassModel().equals(StartController.getFileModel())) {
					logs.setValue("Warning: the file "+StartController.getFileModel()+" loaded does not match the model choosen : "+StartController.getClassModel()+".");
				}else
					logs.setValue("");
				View.loadPopUpNewModel();
			}
	    });
		if (StartController.getClassModel().startsWith("EqualizationNet"))
			equalizationNet.setSelected(true);
		equalizationNet.selectedProperty().addListener((ChangeListener<? super Boolean>) new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (trajectoryNet.isSelected()) {
					StartController.setClassModel("TrajectoryNet");
					StartController.setDisplayEqualizationFields(false);
				}
				else if (equalizationNet.isSelected()) {
					StartController.setClassModel("EqualizationNet");
					StartController.setDisplayEqualizationFields(true);
				}
				else {
					StartController.setClassModel("Net");
					StartController.setDisplayEqualizationFields(false);
				}
				View.loadPopUpNewModel();
				StringProperty logs = StartController.getLogsTextString();
				if (StartController.getSimu() != null && !StartController.getClassModel().equals(StartController.getFileModel())) {
					logs.setValue("Warning: the file "+StartController.getFileModel()+" loaded does not match the model choosen : "+StartController.getClassModel()+".");
				}else
					logs.setValue("");
			}
	    });
	}

	@FXML
	void logsFolderButton(ActionEvent evt) {
		Stage stage = new Stage();

		DirectoryChooser directoryChooser = new DirectoryChooser();
		if (!loadFiles.getLogsFolder().equals("")) {
			File folder = new File(loadFiles.getLogsFolder());
			if (folder.exists())
				directoryChooser.setInitialDirectory(folder);
		}
		File selectedDirectory = directoryChooser.showDialog(stage);

		if (selectedDirectory != null) {
			logsFolder.setText(selectedDirectory.getAbsolutePath());
		}
	}

	@FXML
	void netsFolderButton(ActionEvent evt) {
		Stage stage = new Stage();

		DirectoryChooser directoryChooser = new DirectoryChooser();
		if (!loadFiles.getNetsFolder().equals("")) {
			File folder = new File(loadFiles.getNetsFolder());
			if (folder.exists())
				directoryChooser.setInitialDirectory(folder);
		}
		File selectedDirectory = directoryChooser.showDialog(stage);

		if (selectedDirectory != null) {
			netsFolder.setText(selectedDirectory.getAbsolutePath());
		}
	}

	@FXML
	void timeTablesFolderButton(ActionEvent evt) {
		Stage stage = new Stage();

		DirectoryChooser directoryChooser = new DirectoryChooser();
		if (!loadFiles.getTimeTablesFolder().equals("")) {
			File folder = new File(loadFiles.getTimeTablesFolder());
			if (folder.exists())
				directoryChooser.setInitialDirectory(folder);
		}
		File selectedDirectory = directoryChooser.showDialog(stage);

		if (selectedDirectory != null) {
			timeTablesFolder.setText(selectedDirectory.getAbsolutePath());
		}
	}

	@FXML
	void saveFolders(ActionEvent evt) {
		FileWriter file = null;
		Logger logger = Logger.getLogger("logger");
		try {
			file = new FileWriter("foldersConfiguration.txt", false);
			if (!logsFolder.getText().equals(""))
				file.write("logs%" + logsFolder.getText() + "\n");
			if (!netsFolder.getText().equals(""))
				file.write("nets%" + netsFolder.getText() + "\n");
			if (!timeTablesFolder.getText().equals(""))
				file.write("tt%" + timeTablesFolder.getText() + "\n");
			if (tooltips.isSelected())
				file.write("tooltips%on");
			else
				file.write("tooltips%off");

		} catch (IOException e) {
			logger.log(Level.WARNING, "error of writing in the file of the folders configuration");
		} finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing in the file of the folders configuration");
				}
			}
		}
		StartController.setSampling(Double.parseDouble(sampling.getText()));
		View.stagePreferences.close();
	}
	
	public static void saveWindowPosition(SplitPane global, SplitPane tables, SplitPane logsAndStats) {
		FileWriter file = null;
		Logger logger = Logger.getLogger("logger");
		try {
			file = new FileWriter("windowPosition.txt", false);
			file.write(Main.getPrimaryStage().getScene().getWidth()+"/"+Main.getPrimaryStage().getScene().getHeight());
			file.write("\n");
			for (double value : global.getDividerPositions())
				file.write(Double.toString(value)+"/");
			file.write("\n");
			for (double value : tables.getDividerPositions())
				file.write(Double.toString(value)+"/");
			file.write("\n");
			for (double value : logsAndStats.getDividerPositions())
				file.write(Double.toString(value)+"/");
			
		} catch (IOException e) {
			logger.log(Level.WARNING, "error of writing in the file of the folders configuration");
		} finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException e) {
					logger.log(Level.WARNING, "error while closing in the file of the folders configuration");
				}
			}
		}
	}

}
