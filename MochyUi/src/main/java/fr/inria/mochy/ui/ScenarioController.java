package fr.inria.mochy.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.mochysim.Delay;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Transition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * perform scenario of delays at specific transition(s) for a specific time
 * <p>
 * <img src="ScenarioController.svg" alt="ScenarioController class diagram"
 * style="float: right;">
 */
public class ScenarioController implements Initializable {

	@FXML
	TextField scenarioInput;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}

	/***/
	@FXML
	void save(ActionEvent evt) {
		String[] inputs = scenarioInput.getText().split(";");
		ArrayList<Delay> delays = new ArrayList<>();
		for (int i = 0; i < inputs.length; i++) {
			String[] segments = inputs[i].split(":");
			float start = Float.parseFloat(segments[1].split(",")[0].substring(1));
			float end = Float
					.parseFloat(segments[1].split(",")[1].substring(0, segments[1].split(",")[1].length() - 1));
			float delay = Float.parseFloat(segments[2]);
			PhysicalModel n = StartController.getSimu().getN();
			if (segments[0].equals("all")) {
				for (Integer tnum : n.getTransitions().keySet()) {
					Delay d = new Delay(start, end, tnum, delay);
					delays.add(d);
				}
			} else {
				TransitionAbstract t = n.findTransition(segments[0]);
				Delay d = new Delay(start, end, t.getNumber(), delay);
				delays.add(d);
			}
		}
		StartController.setDelays(delays, true);
		View.stageScenario.close();
	}

	@FXML
	void resetDelays(ActionEvent evt) {
		StartController.resetDelays();
	}
	
	@FXML
	/**load the list all delays windows from stats/scenario menu and list all delays button
	 * the controller is fr.inria.mochy.ui.ListAllDelaysController
	 * the vue is src/main/resources/listAllDelays.fxml*/
	void listAllDelays(ActionEvent evt) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("listAllDelays.fxml"));
	    Parent root1 = null;
		try {
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
		    //stage.initModality(Modality.APPLICATION_MODAL);
		    //stage.initStyle(StageStyle.UNDECORATED);
		    stage.setTitle("List of all the delays set as scenarios");
		    stage.setScene(new Scene(root1));  
		    stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
