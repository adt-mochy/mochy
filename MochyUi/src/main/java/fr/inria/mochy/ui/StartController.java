package fr.inria.mochy.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.util.TransferFunctionType;

import fr.inria.mochy.core.RPN.RPNConfig;
import fr.inria.mochy.core.RPN.RegulNet;
import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.dom.Dom;
import fr.inria.mochy.core.dom.PlaceDom;
import fr.inria.mochy.core.dom.TransitionDom;
import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.equalization.EquNetNeural;
import fr.inria.mochy.core.equalization.EquNetStopsLoic;
import fr.inria.mochy.core.equalization.EquPlace;
import fr.inria.mochy.core.equalization.EquTransition;
import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.exceptions.NullGarageException;
import fr.inria.mochy.core.mochysim.Delay;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.sampler.Sampler;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.Tag;
import fr.inria.mochy.core.timetable.TimeTable;
import fr.inria.mochy.statsAndTasks.AbstractStats;
import fr.inria.mochy.statsAndTasks.GenerateNnetMultiTokensNbTask;
import fr.inria.mochy.statsAndTasks.OptimizeNnetMultiTokensNbTask;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.css.CssMetaData;
import javafx.css.StyleOrigin;
import javafx.css.Styleable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.SplitPane.Divider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 * Controls the main window, its tables, buttons, fields and menus.
 * <p>
 * <img src="StartController.svg" alt="StartController class diagram" style=
 * "float: right;">
 */
public class StartController implements Initializable {

	LoadFiles loadFiles = new LoadFiles();// initializes the directories according to set preferences
	static View view = new View();

	@FXML ScrollPane scrollPane;
	@FXML SplitPane splitPaneGlobal;
	@FXML SplitPane splitPaneTables;
	@FXML SplitPane splitPaneLogsAndStats;

	@FXML TableView<PlaceDom> placesTable = new TableView<>();
	TableColumn<PlaceDom, String> name = new TableColumn<>("name");
	TableColumn<PlaceDom, String> content = new TableColumn<>("content");

	@FXML TableView<TransitionDom> transitionsTable;
	TableColumn<TransitionDom, String> transitionName = new TableColumn<>("name");
	TableColumn<TransitionDom, String> distribution = new TableColumn<>("status");
	TableColumn<TransitionDom, String> pre = new TableColumn<>("pre");
	TableColumn<TransitionDom, String> post = new TableColumn<>("post");
	TableColumn<TransitionDom, Integer> lowerBound = new TableColumn<>("lowerBound");
	TableColumn<TransitionDom, Integer> upperBound = new TableColumn<>("upperBound");
	TableColumn<TransitionDom, String> clock = new TableColumn<>("clock");

	@FXML TableView<TableEvent> eventsTable = new TableView<>();
	TableColumn<TableEvent, Integer> number = new TableColumn<>("nb");
	TableColumn<TableEvent, Integer> initialDate = new TableColumn<>("in. Date");
	TableColumn<TableEvent, Integer> Date = new TableColumn<>("Date");
	TableColumn<TableEvent, Integer> recordedDate = new TableColumn<>("rec Date");
	TableColumn<TableEvent, String> label = new TableColumn<>("label");
	TableColumn<TableEvent, String> transitions = new TableColumn<>("transnames");
	TableColumn<TableEvent, String> realized = new TableColumn<>("realized");
	TableColumn<TableEvent, String> tags = new TableColumn<>("tags");

	@FXML TableView<TableDependency> dependenciesTable = new TableView<>();
	TableColumn<TableDependency, Integer> startEvent = new TableColumn<>("startEvent");
	TableColumn<TableDependency, Integer> endEvent = new TableColumn<>("endEvent");
	TableColumn<TableDependency, Integer> duration = new TableColumn<>("duration");

	@FXML TextField nbSteps;

	/** infos of the discrete move */
	StringProperty infosValue = new SimpleStringProperty();
	@FXML Label infos;

	/** time value of the timed move */
	@FXML TextField time;

	/** value of a place to fill */
	@FXML TextField fplace;

	@FXML Tooltip tooltip1;// transitions table
	@FXML Tooltip tooltip2;// places table
	@FXML Tooltip tooltip3;// events table
	@FXML Tooltip tooltip4;// dependencies table
	@FXML Tab tabNet;
	@FXML Tooltip tooltipNet;
	@FXML Tab tabTT;
	@FXML Tooltip tooltipTT;
	@FXML Tab tabJoint;
	@FXML Tooltip tooltipJoint;

	/** values for the logs at the bottom of the display */
	static StringProperty logsTextString = new SimpleStringProperty();
	@FXML TextArea logsText;

	/** values for the stats at the bottom right of the display */
	static StringProperty statsTextString = new SimpleStringProperty();
	@FXML TextArea statsText;

	/** the content of the .net file displayed in the source net pane */
	static StringProperty sourceNetTextString = new SimpleStringProperty();
	@FXML TextArea sourceNetText;

	/** the content of the .tt file display in the source event pane */
	static StringProperty sourceEventsTextString = new SimpleStringProperty();
	@FXML TextArea sourceEventsText;

	/** the content of the .cor file display in the source cor event tab */
	static StringProperty sourceCorTextString = new SimpleStringProperty();
	@FXML TextArea sourceCorText;

	/** value of the sampling to be done for the chart display */
	static double sampling = 1;

	/**
	 * true if the date of the timetable must be reset at the TT load it can be
	 * modified from the preferences pane
	 */
	static boolean resetDateTTLoad = true;
	// static boolean enableLogs = false;
	public static BooleanProperty enableLogs = new SimpleBooleanProperty(false);

	/** data used for the statistics linked to the event table */
	static int delaySum = 0;
	static int nbOfRealizedEvents = 0;
	static boolean delaySumEnabled = false;
	static boolean averageDelayEnabled = false;
	static int delayEscape = 0;
	static boolean interval = false;// calcul the stats in the interval
	static int minInterval = 0;
	static Integer maxInterval = null;
	static HashMap<Tag, Boolean> listTags = new HashMap<>();
	static ArrayList<Delay> delays = new ArrayList<>();
	static String startPlace = "";
	static String endPlace = "";
	static boolean calculTime = false;
	static TableEvent event = null;
	static boolean eventFired = false;
	static TransitionAbstract transition = null;

	/** the checkbox to connect the TT to the net for joint sim */
	@FXML CheckBox connectTT;
	static BooleanProperty TTConnected = new SimpleBooleanProperty(false);

	/**
	 * the property to display or not the tooltips can be disabled from
	 * preferencesController
	 */
	static BooleanProperty tooltipsProperty = new SimpleBooleanProperty(false);

	/** check if the polar diagram from the view class is closed or not */
	static boolean polarOpen = false;

	/**
	 * check if the histogeam headways diagram from the view class is closed or not
	 */
	static boolean histogramOpen = false;

	/** if set to true, display the chart after the call of launchsteps method */
	static boolean displayChart = true;

	/** display the current date of the timetable */
	static StringProperty dateTTString = new SimpleStringProperty();
	@FXML TextField dateTT;
	@FXML TextField dateTT1;

	/**
	 * label for the number of steps to get to the equalization / EqualizationNet
	 * feature
	 */
	@FXML Label stepsEqualizationInfo;
	StringProperty stepsEqualizationInfoString = new SimpleStringProperty();
	@FXML Button stepsEqualizationButton;
	static BooleanProperty displayEqualizationFields = new SimpleBooleanProperty();
	@FXML MenuItem multiRunsFullStats;
	@FXML MenuItem headwaysHistogramId;
	@FXML MenuItem multiRunsTokensNb;
	@FXML Button insertTokenButton;
	@FXML MenuItem genNeuralNets;
	@FXML MenuItem optimizeNeuralNetItem;
	@FXML MenuItem generateMultiTokensNb;
	@FXML MenuItem optimizeMultiTokensNb;
	@FXML MenuItem nnetPipelineMenu;
	static BooleanProperty refreshNet = new SimpleBooleanProperty();

	@FXML TextField targetEqualization;// the target standard deviation which we want to get
	static String targetEqualizationValue = "";

	@FXML
	/** the time to move for a timed move action */
	TextField timeTT;

	@FXML
	/** the nb of the event on which we want to apply a delay */
	TextField eventNbDelayTT;

	@FXML
	/** the delay to apply to a specific event */
	TextField timeDelayTT;

	/** The steps to perform for the joint simulation */
	@FXML TextField stepsJoint;

	/** the infos about the correspondancies file for the joint simulation */
	static StringProperty infosCorrespString = new SimpleStringProperty();
	@FXML Label infosCorresp;

	@FXML Tab sourceNetTab;
	@FXML Tab sourceEventsTab;
	@FXML Tab sourceCorTab;

	public static String classModel = "Net";// Net or TrajectoryNet or EqualizationNet.className
	static String fileModel = "Net";
	public static Sim simu;
	static TTConfig ttConfig;
	static RegulNet rn; // The regulated net currently simulated
	static RPNConfig rpnc; // the current configuration of the simulated RPN
	static String correspondancePath; // the path of the correspondance file
	static boolean correspLoaded = false;
	static String projectFileName = null;

	Logger logger = Logger.getLogger("log");

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		try {
			loadFiles.loadWindowPosition(splitPaneGlobal, splitPaneTables, splitPaneLogsAndStats);
		} catch (Exception e1) {
			System.out.println("initial window position (not loaded)");
		}

		/** prepare the columns of the transitions and places tables **/
		name.setCellValueFactory(new PropertyValueFactory("name"));
		name.prefWidthProperty().bind(placesTable.widthProperty().multiply(0.4));
		content.setCellValueFactory(new PropertyValueFactory("content"));
		content.prefWidthProperty().bind(placesTable.widthProperty().multiply(0.3));
		placesTable.getColumns().addAll(name, content);

		transitionName.setCellValueFactory(new PropertyValueFactory("name"));
		pre.setCellValueFactory(new PropertyValueFactory("pre"));
		post.setCellValueFactory(new PropertyValueFactory("post"));
		distribution.setCellValueFactory(new PropertyValueFactory("distribution"));
		lowerBound.setCellValueFactory(new PropertyValueFactory("lowerBound"));
		upperBound.setCellValueFactory(new PropertyValueFactory("upperBound"));
		clock.setCellValueFactory(new PropertyValueFactory("clock"));

		/**
		 * set the size of the rows and columns to implement the chart button in the
		 * transition table
		 */
		placesTable.setFixedCellSize(31);// 30.85
		transitionsTable.setFixedCellSize(31);
		clock.prefWidthProperty().bind(transitionsTable.widthProperty().multiply(0.08));
		transitionsTable.getColumns().addAll(transitionName, pre, post, distribution, lowerBound, upperBound, clock);
		JfxUtils.addButtonToTable(transitionsTable, Main.getPrimaryStage());
		JfxUtils.addButtonToTablePlace(placesTable, Main.getPrimaryStage());

		/** prepare the columns of the events and dependencies **/
		number.setCellValueFactory(new PropertyValueFactory("number"));
		number.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.08));
		initialDate.setCellValueFactory(new PropertyValueFactory("initialDate"));
		initialDate.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.15));
		Date.setCellValueFactory(new PropertyValueFactory("Date"));
		Date.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.15));
		recordedDate.setCellValueFactory(new PropertyValueFactory("recordedDate"));
		recordedDate.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.15));
		label.setCellValueFactory(new PropertyValueFactory("label"));
		transitions.setCellValueFactory(new PropertyValueFactory("transitions"));
		realized.setCellValueFactory(new PropertyValueFactory("realized"));
		realized.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.12));
		tags.setCellValueFactory(new PropertyValueFactory("tags"));
		tags.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.10));
		eventsTable.getColumns().addAll(number, initialDate, Date, recordedDate, label, transitions, realized, tags);

		startEvent.setCellValueFactory(new PropertyValueFactory("startEvent"));
		endEvent.setCellValueFactory(new PropertyValueFactory("endEvent"));
		duration.setCellValueFactory(new PropertyValueFactory("duration"));
		duration.prefWidthProperty().bind(eventsTable.widthProperty().multiply(0.15));
		dependenciesTable.getColumns().addAll(startEvent, endEvent, duration);

		/**
		 * set up a context menu for setting a scenario on a transition by a right click
		 */
		ContextMenu contextMenuTransition = new ContextMenu();
		MenuItem addAScenario = new MenuItem("Create a scenario");
		contextMenuTransition.getItems().add(addAScenario);

		/**
		 * launch a firable transition if there is a double click on it and set up a
		 * green background on fireable transitions
		 */
		transitionsTable.setRowFactory(tv -> new TableRow<TransitionDom>() {
			{
				this.setOnMouseClicked(event -> {
					if (event.getClickCount() == 2 && (!this.isEmpty())) {
						TransitionDom rowData = this.getItem();
						displayNet(simu.fireTransition(Integer.parseInt(rowData.getNumber())));
					}
				});
				this.setOnContextMenuRequested(e -> {
					addAScenario.setOnAction(e2 -> {
						if (simu != null) {
							TransitionAbstract t = simu.getN().getTransitions()
									.get(Integer.parseInt(this.getItem().getNumber()));
							// view.addAScenarioToATransition(t);
							view.addAScenarioToATransition(t, transitionsTable, placesTable);
						}
					});
					contextMenuTransition.show(this, e.getSceneX(), e.getSceneY());
				});
			}

			@Override
			public void updateItem(TransitionDom item, boolean empty) {
				super.updateItem(item, empty);
				if (item == null) {
					setStyle("");
				} else if (item.getDistribution().equals("fireable") && item.control()) {
					setStyle("-fx-background-color: lime;");
				} else {
					setStyle("");
				}
			}
		});

		placesTable.setRowFactory(tv -> new TableRow<>() {
			public void updateItem(PlaceDom item, boolean empty) {
				super.updateItem(item, empty);
				if (item == null || item.getContent() == "null")
					setStyle("");
				else
					setStyle("-fx-background-color: lime;");
			}
		});

		/** set up a context menu for setting a delay on an event by a right click */
		ContextMenu contextMenuEvents = new ContextMenu();
		MenuItem addADelay = new MenuItem("Add a delay");
		contextMenuEvents.getItems().add(addADelay);

		/**
		 * launch a fireable event if there is a double click on it and add colors
		 * depending of the status of the event : realized, late and fireable, late and
		 * not fireable or fireable at time
		 */
		eventsTable.setRowFactory(tv -> new TableRow<TableEvent>() {
			{
				// TableRow<TableEvent> row = new TableRow<>();
				this.setOnMouseClicked(evnt -> {
					if (evnt.getClickCount() == 2 && (!this.isEmpty())) {
						TableEvent rowData = this.getItem();
						// realizeEvent(rowData);
						realizeClickedEvent(rowData);
						view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
					}
				});

				this.setOnContextMenuRequested(e -> {
					addADelay.setOnAction(e2 -> {
						view.addADelayToAnEvent(this.getItem(), eventsTable, dependenciesTable);
					});
					contextMenuEvents.show(this, e.getSceneX(), e.getSceneY());
				});
			}

			@Override
			public void updateItem(TableEvent item, boolean empty) {
				super.updateItem(item, empty);
				boolean eventFirable = false;
				for (TableEvent event : ttConfig.getMinevents()) {
					if (event.equals(item))
						eventFirable = true;
				}
				if (item == null || dateTT == null) {
					setStyle("");
				} else if (!item.getRealized() && item.getDate() == Float.parseFloat(dateTT.getText())
						&& eventFirable) {
					setStyle("-fx-background-color: lime;");
				} else if (!item.getRealized() && item.getDate() < Float.parseFloat(dateTT.getText())
						&& !eventFirable) {
					setStyle("-fx-background-color: hotpink;");
				} else if (!item.getRealized() && item.getDate() < Float.parseFloat(dateTT.getText()) && eventFirable) {
					setStyle("-fx-background-color: red;");
				} else if (item.getRealized()) {
					setStyle("-fx-background-color: lightblue;");
				} else {
					setStyle("");
				}
			}
		});

		/*
		 * connectTT.selectedProperty().addListener((ChangeListener<? super Boolean>)
		 * new ChangeListener<Boolean>() {
		 * 
		 * @Override public void changed(ObservableValue<? extends Boolean> observable,
		 * Boolean oldValue, Boolean newValue) { if (connectTT.isSelected()) TTConnected
		 * = true; else TTConnected = false; } });
		 */
		connectTT.selectedProperty().bindBidirectional(TTConnected);

		/**
		 * bind the logs text area to the logs text property to update its content
		 * dynamically
		 */
		if (logsText != null)
			logsText.textProperty().bind(logsTextString);

		statsText.textProperty().bind(statsTextString);

		sourceNetText.textProperty().bind(sourceNetTextString);

		sourceEventsText.textProperty().bind(sourceEventsTextString);

		sourceCorText.textProperty().bind(sourceCorTextString);

		if (dateTT != null)
			dateTT.textProperty().bind(dateTTString);

		if (dateTT1 != null)
			dateTT1.textProperty().bind(dateTTString);

		/**
		 * bind the infos of the firable transitions to infosValue to update its content
		 * dynamically
		 */
		if (infos != null)
			infos.textProperty().bind(infosValue);

		/**
		 * bind the infos of the correspondancies file to infosCorrespString to update
		 * its content
		 */
		if (infosCorresp != null)
			infosCorresp.textProperty().bind(infosCorrespString);

		stepsEqualizationInfo.textProperty().bind(stepsEqualizationInfoString);

		/** set the date to 0 from start */
		dateTTString.set("0");

		/** Load the content of the net file in the source tab */
		sourceNetTab.setOnSelectionChanged(e -> {
			if (sourceNetTab.isSelected())
				loadFiles.loadSourceNetContent();// use setSourceNetText
		});

		/** load the content of the tt file in the source tab */
		sourceEventsTab.setOnSelectionChanged(e -> {
			if (sourceEventsTab.isSelected())
				loadFiles.loadSourceEventsContent();// use setSourceNetText
		});

		/** load the content of the cor file in the source cor tab */
		sourceCorTab.setOnSelectionChanged(e -> {
			if (sourceCorTab.isSelected())
				loadFiles.loadSourceCorContent();// use setSourceNetText
		});

		/** clear the logs pane with a right click/clear menu */
		ContextMenu contextMenu = new ContextMenu();
		MenuItem clear = new MenuItem("Clear");
		clear.setOnAction(e -> {
			logsTextString.set("");
		});
		contextMenu.getItems().add(clear);
		logsText.setContextMenu(contextMenu);

		/** clear the stats pane with a right click/clear menu */
		ContextMenu contextMenu2 = new ContextMenu();
		MenuItem clear2 = new MenuItem("Clear");
		clear2.setOnAction(e -> {
			statsTextString.set("");
		});
		contextMenu2.getItems().add(clear2);
		statsText.setContextMenu(contextMenu2);

		stepsEqualizationButton.setVisible(false);
		targetEqualization.setVisible(false);
		multiRunsFullStats.setVisible(false);
		headwaysHistogramId.setVisible(false);
		multiRunsTokensNb.setVisible(false);
		insertTokenButton.setVisible(false);
		genNeuralNets.setVisible(false);
		optimizeNeuralNetItem.setVisible(false);
		generateMultiTokensNb.setVisible(false);
		optimizeMultiTokensNb.setVisible(false);
		nnetPipelineMenu.setVisible(false);
		displayEqualizationFields.addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				stepsEqualizationButton.setVisible(true);
				targetEqualization.setVisible(true);
				multiRunsFullStats.setVisible(true);
				headwaysHistogramId.setVisible(true);
				multiRunsTokensNb.setVisible(true);
				insertTokenButton.setVisible(true);
				genNeuralNets.setVisible(true);
				optimizeNeuralNetItem.setVisible(true);
				generateMultiTokensNb.setVisible(true);
				optimizeMultiTokensNb.setVisible(true);
				nnetPipelineMenu.setVisible(true);
			} else {
				stepsEqualizationButton.setVisible(false);
				targetEqualization.setVisible(false);
				multiRunsFullStats.setVisible(false);
				headwaysHistogramId.setVisible(false);
				multiRunsTokensNb.setVisible(false);
				insertTokenButton.setVisible(false);
				genNeuralNets.setVisible(false);
				optimizeNeuralNetItem.setVisible(false);
				generateMultiTokensNb.setVisible(false);
				optimizeMultiTokensNb.setVisible(false);
				nnetPipelineMenu.setVisible(false);
			}
		});
		targetEqualization.textProperty().addListener((observable, oldValue, newValue) -> {
			targetEqualizationValue = newValue;
		});

		refreshNet.addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				displayNet(simu.displayFeedback());
			}
			refreshNet.setValue(false);
		});

		enableLogs.addListener((observable, oldValue, newValue) -> {
			if (newValue && simu != null)
				simu.setEnableLogs(true);
			else if (simu != null)
				simu.setEnableLogs(false);
		});

		logsTextString.addListener((observable, oldValue, newValue) -> {
			if (newValue.startsWith("Warning")) {
				logsText.setStyle("-fx-text-fill: red;");
			} else
				logsText.setStyle("");
		});

		// tooltipsProperty.set(false);set according to the preferences from the
		// LoadFiles call
		tooltipsProperty.addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				Tooltip.uninstall(placesTable, tooltip2);
				Tooltip.uninstall(transitionsTable, tooltip1);
				Tooltip.uninstall(eventsTable, tooltip3);
				Tooltip.uninstall(dependenciesTable, tooltip4);
				tabNet.setTooltip(null);
				tabTT.setTooltip(null);
				tabJoint.setTooltip(null);
			} else {
				Tooltip.install(placesTable, tooltip2);
				Tooltip.install(transitionsTable, tooltip1);
				Tooltip.install(eventsTable, tooltip3);
				Tooltip.install(dependenciesTable, tooltip4);
				tabNet.setTooltip(tooltipNet);
				tabTT.setTooltip(tooltipTT);
				tabJoint.setTooltip(tooltipJoint);
				setTooltipDuration(tooltip2);
				setTooltipDuration(tooltip1);
				setTooltipDuration(tooltip3);
				setTooltipDuration(tooltip4);
				setTooltipDuration(tooltipNet);
				setTooltipDuration(tooltipTT);
				setTooltipDuration(tooltipJoint);
			}
		});
		loadFiles.initFolders();// to set or not the tooltips
	}

	/***********************************************************************************/
	/****************************
	 * NET : places and transitions
	 *************************/
	/***********************************************************************************/
	@FXML
	/** load the net file and display its data */
	/** The net can be of different type as long as it extends the physical model */
	/**
	 * Specific methods for loading a particular net are encoded in the net class
	 */
	void loadNet(ActionEvent evt) {
		Stage primaryStage = Main.getPrimaryStage();
		simu = loadFiles.loadNetFile(primaryStage);
		if (simu != null) { // If a simu was created, i.e. a net was correctly loaded
			System.out.println("Loaded a " + classModel);
			PhysicalModel net = simu.getN();
			displayNet(simu.displayFeedback());
		}
	}

	@FXML
	/** perform one step of the simu and display the new data */
	void oneStep(ActionEvent evt) {
		if (simu != null)
			displayNet(simu.oneStep());
	}

	static int avgStandardDeviation;
	static int avgSpeed;
	static int discreteSteps;
	static int fullDiscreteSteps;
	static int overLimitStdDev;
	static int limit;

	public static float launchStepsEqu(int steps) {

		avgStandardDeviation = 0;
		avgSpeed = 0;
		discreteSteps = 0;
		overLimitStdDev = 0;
		fullDiscreteSteps = 0;
		ArrayList<Float> x = new ArrayList<>();
		ArrayList<Float> y = new ArrayList<>();
		ArrayList<Float> y2 = new ArrayList<>();
		EquNet n = (EquNet) simu.getN();
		n.resetSpeedData();
		float preAvgSpeed = 0;

		limit = 1;
		try {
			limit = Integer.parseInt(targetEqualizationValue);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			getLogsTextString().setValue(
					"Warning : the field next to steps equalization is not filled with a number (standard deviation limit)");
		}
		if (!polarOpen) {
			for (int i = 0; i < steps; i++) {
				simu.oneStep();
				avgStandardDeviation += n.getStandardDeviation("");
				if (n.isDiscreteMove() && n.getLastTokenSpeed() != 0) {
					avgSpeed += n.getLastTokenSpeed();
					discreteSteps++;
				}
				if (n.isDiscreteMove())
					fullDiscreteSteps++;
				if (n.getStandardDeviation("") > limit && !n.isDiscreteMove())
					overLimitStdDev++;
				x.add(n.getTimeElapsed());
				y.add(n.getStandardDeviation(""));
				// if (n.getCurrentAvgSpeed() != 0) {
				y2.add(n.getCurrentAvgSpeed());
				// preAvgSpeed = n.getCurrentAvgSpeed();
				// } else
			}
			avgStandardDeviation = avgStandardDeviation / steps;
			avgSpeed = avgSpeed / discreteSteps;
			statsTextString.setValue("average standard deviation : " + avgStandardDeviation + " - average speed : "
					+ n.getAvgSpeed() + " - number of times that the standard deviation " + " is over the limit " + limit
					+ " : " + overLimitStdDev + " / " + (steps - fullDiscreteSteps) + "timed moves");
			if (displayChart)
				View.chartEquStandardDeviation(x, y, y2);
		} else {
			View.stagePolar.show();
			AnimationTimer timer = new AnimationTimer() {
				private long lastUpdate = 0;
				int i = 0;

				@Override
				public void handle(long now) {
					if (now - lastUpdate >= 100_000_000) { // delay de 100 ms
						simu.oneStep();
						avgStandardDeviation += n.getStandardDeviation("");
						if (n.isDiscreteMove() && n.getLastTokenSpeed() != 0) {
							avgSpeed += n.getLastTokenSpeed();
							discreteSteps++;
						}
						if (n.isDiscreteMove())
							fullDiscreteSteps++;
						if (n.getStandardDeviation("") > limit && !n.isDiscreteMove())
							overLimitStdDev++;
						x.add(n.getTimeElapsed());
						y.add(n.getStandardDeviation(""));
						y2.add(n.getCurrentAvgSpeed());
						View.generatePolarDiagram();
						i++;
						lastUpdate = now;
					}
					if (i == steps) {
						avgStandardDeviation = avgStandardDeviation / steps;
						avgSpeed = avgSpeed / discreteSteps;
						statsTextString.setValue("average standard deviation : " + avgStandardDeviation + " - average speed : "
								+ n.getAvgSpeed() + " - number of times that the standard deviation " + " is over the limit " + limit
								+ " : " + overLimitStdDev + " / " + (steps - fullDiscreteSteps) + "timed moves");
						if (displayChart)
							View.chartEquStandardDeviation(x, y, y2);
						this.stop();
					}
				}
			};
			timer.start();
		}
		
		return avgStandardDeviation;
	}

	@FXML
	/** perform the number of steps defined in nbSteps */
	public void launchSteps(ActionEvent evt) {
		int steps = Integer.parseInt(nbSteps.getText());
		if (simu != null && classModel.startsWith("EqualizationNet")) {
			launchStepsEqu(steps);
		} else if (simu != null) {
			// simu.main(steps);
			if (polarOpen) {
				polarAnimation(steps);
			} else {
				for (int i = 0; i < steps; i++)
					simu.oneStep();
			}
		}
		displayNet();
	}

	public void polarAnimation(int steps) {
		View.stagePolar.show();
		AnimationTimer timer = new AnimationTimer() {
			private long lastUpdate = 0;
			int i = 0;

			@Override
			public void handle(long now) {
				if (now - lastUpdate >= 100_000_000) { // delay de 100 ms
					simu.oneStep();
					View.generatePolarDiagram();
					i++;
					lastUpdate = now;
				}
				if (i == steps)
					this.stop();
			}
		};
		timer.start();
	}

	public static void delay(long millis, Runnable continuation) {
		Task<Void> sleeper = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				try {
					Thread.sleep(millis);
				} catch (InterruptedException e) {
				}
				return null;
			}
		};
		sleeper.setOnSucceeded(event -> continuation.run());
		new Thread(sleeper).start();
	}

	@FXML
	/** perform multiple steps until there is an equalization of the headways */
	void stepsEqualization(ActionEvent evt) {
		if (this.classModel.startsWith("EqualizationNet")) {
			EquNet n = (EquNet) this.simu.getN();
			float standardDeviation = Float.POSITIVE_INFINITY;
			int steps = 0;
			int limit = 1;
			try {
				limit = Integer.parseInt(targetEqualization.getText());
				StartController.getLogsTextString().setValue("");
			} catch (NumberFormatException e) {
				e.printStackTrace();
				getLogsTextString()
						.setValue("Warning : the field next to steps equalization is not filled with a number");
			}
			FileWriter fileLogs = null;
			ArrayList<Float> x = new ArrayList<>();
			ArrayList<Float> y = new ArrayList<>();// the standard deviation values
			ArrayList<Float> y2 = new ArrayList<>();// the speed values

			try {
				fileLogs = new FileWriter(simu.getPathLogs().replace("txt", "csv"), false);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			while (standardDeviation > limit && steps != 10000) {
				simu.oneStep();
				standardDeviation = n.getStandardDeviation(this.simu.getPathLogs());
				steps++;
				/*
				 * try { fileLogs.write(String.valueOf(n.getTimeElapsed()).replace(".",
				 * ",")+";"+String.valueOf(standardDeviation).replace(".", ",")+"\n"); } catch
				 * (IOException e) { logger.log(Level.WARNING,
				 * "error of writing in the csv fileLogs in class StartController"); }
				 */
				x.add(n.getTimeElapsed());
				y.add(standardDeviation);
				y2.add(n.getCurrentAvgSpeed());
			}

			try {
				fileLogs.close();
			} catch (IOException e) {
				logger.log(Level.WARNING, "error while closing the csv fileLogs in class StartController");
			}

			float average = n.getAverage();
			displayNet();

			if (steps == 10000)
				stepsEqualizationInfoString.set("standard deviation is > " + limit + " after 10000 steps");
			else {
				stepsEqualizationInfoString.set(steps + "steps, standard deviation : " + standardDeviation
						+ ", average : " + average + ", time elapsed " + n.getTimeElapsed() + ", nb discrete steps : "
						+ n.getNbDiscreteSteps() + ",\n current average speed : " + n.getAvgSpeed());
				View.chartEquStandardDeviation(x, y, y2);

			}
		}
	}

	@FXML
	void generateMultiTokensNb(ActionEvent evt) {
		// MultipleRunsController controller = new MultipleRunsController();
		// controller.equalizationFullStats(false, false, true, new
		// GenerateNnetMultiTokensNbTask());
		View.displayGenerateNeuralMultiTokensNb();
	}

	@FXML
	void optimizeMultiTokensNb(ActionEvent evt) {
		// MultipleRunsController controller = new MultipleRunsController();
		// controller.equalizationFullStats(false, false, true, new
		// OptimizeNnetMultiTokensNbTask());
		View.optimizeNeuralMultiTokensNb();
	}

	@FXML
	/** display an histogram showing the headways before and after the tokens */
	void headwaysHistogram(ActionEvent evt) {
		if (this.classModel.startsWith("EqualizationNet")) {
			View.displayHeadwaysHistogram();
		}
	}

	@FXML
	/**
	 * perform 1000 runs for different values of noise and alpha controller calls
	 * StatsTask
	 */
	void multiRunsFullStatsClicked(ActionEvent evt) {
		/*
		 * MultipleRunsController controller = new MultipleRunsController();
		 * controller.equalizationFullStats(true, false);
		 */
		View.displayAlphaNoise();
	}

	@FXML
	/**
	 * perform 100 runs for different number of tokens controller calls
	 * MultipleTokensNbTask
	 */
	void multiRunsTokensNbClicked(ActionEvent evt) {
		// MultipleRunsController controller = new MultipleRunsController();
		// controller.equalizationFullStats(false, true);
		View.displayMultipleTokens();
	}

	@FXML
	/** analyse a nnet outputs depending of the inputs */
	/**
	 * assume that the second transition is a moving one and that all the
	 * transitions have the same normal time...
	 */
	void analyseNNet(ActionEvent evt) {
		EquNetNeural net = (EquNetNeural) simu.getN();
		for (int i = 0; i < (net.getTotalDistance()
				/ net.tokens.size()); i += (net.getTotalDistance() / net.tokens.size()) / 10) {
			for (int j = 0; j < (net.getTotalDistance()
					/ net.tokens.size()); j += (net.getTotalDistance() / net.tokens.size()) / 10) {
				net.getNnet().setInput(new double[] { (double) i, (double) j });
				net.getNnet().calculate();
				EquTransition t = (EquTransition) net.transitions.get(2);
				float k = (float) (t.getNormalTime() * (1 + net.getNnet().getOutput()[0]));
			}
		}
	}

	@FXML
	/**
	 * insert a token in the equalization net in the garage place if one is defined
	 */
	void insertToken(ActionEvent evt) {
		try {
			if (simu != null) {
				EquNet n = (EquNet) simu.getN();
				try {
					n.insertToken();
				} catch (NullGarageException e) {
					StartController.logsTextString.set(e.getMessage());
					e.printStackTrace();
				}
				displayNet();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	/** perform a step if possible from the firable transitions */
	void discreteStep(ActionEvent evt) {
		if (simu != null)
			displayNet(simu.discreteStep());
	}

	@FXML
	/** perform a timed move between 0 and the minimum clock of the transitions */
	void timedMove(ActionEvent evt) {
		if (simu != null) {
			if (Float.valueOf(time.getText()) > simu.getN().maxAllowedTimedMove() && simu.getN().numberFireable() == 0)
				// && simu.getN().getFirable().size() > 0)
				simu.getN().progressTime(simu.getN().maxAllowedTimedMove());
			else if (simu.getN().numberFireable() == 0)
				simu.getN().progressTime(Float.valueOf(time.getText()));

			displayNet(simu.displayFeedback());
		}
	}

	@FXML
	/** fills a place */
	void fillPlace(ActionEvent evt) {

		if (simu != null && this.classModel.equals("Net")) { // check if the simulator has been loaded

			Net simnet = (Net) simu.getN(); // finds the net
			Integer placenum = Integer.valueOf(fplace.getText());

			// check if placenum is a control place

			if (simnet.isAControlPlace(placenum.intValue())) {
				simnet.addToken(placenum.intValue()); // adds a token
				System.out.println("Control Place " + placenum + " marked");
			}

			displayNet(simu.displayFeedback()); // updates the table
		}
	}

	@FXML
	/** fills a place */
	void emptyPlace(ActionEvent evt) {

		if (simu != null && this.classModel.equals("Net")) { // check if the simulator has been loaded

			Net simnet = (Net) simu.getN(); // finds the net
			Integer placenum = Integer.valueOf(fplace.getText());

			// check if placenum is a control place

			if (simnet.isAControlPlace(placenum.intValue())) {
				simnet.getPlace(placenum.intValue()).resetPlace(); // adds a token
				System.out.println("Control Place " + placenum + " marked");
			}

			displayNet(simu.displayFeedback()); // updates the table
		}
	}

	/** display the table and informations on the ui */
	void displayNet(ArrayList<Dom> feedback) {
		// System.out.println("displayNet" +feedback.size());
		view.displayFeedback(feedback, placesTable, transitionsTable);
		view.displayInfoDiscretStep(simu, infosValue);
		if (time != null && simu.getN().maxAllowedTimedMove() > 0)
			time.setText(Float.toString(simu.getN().maxAllowedTimedMove()));
		else if (time != null)
			time.setText(Float.toString(0));
		if (polarOpen) {
			// View.stagePolar.close();
			// view.ShowPolarDiagram();
			View.generatePolarDiagram();
		}
		if (histogramOpen)
			View.generateHeadwaysHistogram();
	}

	/** display the table and informations on the ui */
	public void displayNet() {
		ArrayList<Dom> feedback = simu.displayFeedback();
		view.displayFeedback(feedback, placesTable, transitionsTable);
		view.displayInfoDiscretStep(simu, infosValue);
		if (time != null && simu.getN().maxAllowedTimedMove() > 0)
			time.setText(Float.toString(simu.getN().maxAllowedTimedMove()));
		else if (time != null)
			time.setText(Float.toString(0));
		if (polarOpen) {
			// View.stagePolar.close();
			// view.ShowPolarDiagram();
			View.generatePolarDiagram();
		}
		if (histogramOpen)
			View.generateHeadwaysHistogram();
	}

	@FXML
	public void resetNet(ActionEvent evt) {
		simu.reset();
		displayNet();
	}

	/***********************************************************************************/
	/**********************
	 * Timetable : events and dependencies
	 ************************/
	/***********************************************************************************/
	@FXML
	/** load the timetable and display its data */
	void loadTT(ActionEvent evt) {
		Stage primaryStage = Main.getPrimaryStage();
		ttConfig = loadFiles.loadTT(primaryStage, logsTextString);

		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);

	}

	@FXML
	/** perform one step of the timetable if an event is fireable */
	void oneStepTT(ActionEvent evt) {
		if (ttConfig != null && ttConfig.getMinevents().size() >= 1) {
			// System.out.println(ttConfig.getMinevents());
			TableEvent minEvent = ttConfig.getMinevents().get(0);
			for (TableEvent event : ttConfig.getMinevents()) {
				if (event.getDate() < minEvent.getDate())
					minEvent = event;
			}
			// System.out.println(minEvent.getLabel());
			realizeEvent(minEvent);
			view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
		}
	}

	@FXML
	/** perform a time move of the timetable */
	void timedMoveTT(ActionEvent evt) {
		if (ttConfig != null) {
			if (timeTT.getText().equals(""))
				StartController.getLogsTextString()
						.set("Warning : the value next to time move must be filled with a number");
			else {
				try {
					float value = Float.parseFloat(timeTT.getText());
					ttConfig.timeMove(value);
					dateTTString.set(String.valueOf(ttConfig.getCurrentDate()));
					view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
					StartController.getLogsTextString().set("Time advance of " + value);
				} catch (NumberFormatException e) {
					StartController.getLogsTextString()
							.set("Warning : the value next to time move must be filled with a number (0.5 as example)");
				}
			}
		}
	}

	@FXML
	/**
	 * perform a random timed move based on invert transform from the class Sampler
	 * in the core package
	 */
	void randomTimedMove(ActionEvent evt) {
		if (ttConfig != null) {
			Sampler sampler = new Sampler();
			int timedMove = sampler.invertTransform(10);
			timedMove = (int) sampler.invertTransformWeibull(30, 50);
			timedMove = (int) sampler.gaussSampler(100, 20);
			ttConfig.timeMove(timedMove);
			dateTTString.set(String.valueOf(ttConfig.getCurrentDate()));
			view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
		}
	}

	@FXML
	/** increase the date of a specific event for a defined amount of time */
	void delayTT(ActionEvent evt) {
		if (ttConfig != null) {
			int evtid = Integer.valueOf(eventNbDelayTT.getText());
			float delay = Float.valueOf(timeDelayTT.getText());
			float date = ttConfig.getTable().getEvent(evtid).getDate();
			TableEvent te = ttConfig.getTable().getEvent(evtid);
			if (!te.getRealized()) {
				te.setDate(date + delay);
				// ttConfig.getTable().getEvent(evtid).setInitialDate(date + delay);
				ttConfig.getTable().PropagateDelay(evtid, delay);
			}
		}
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
	}

	/**
	 * Realize a clicked event from the timetable iff it is a minimal event
	 */
	void realizeClickedEvent(TableEvent evt) {

		// Test : dispay minimal events before realization
		// System.out.println("Minimal events before realization (On click)");
		// System.out.println(evt.drop());

		ArrayList<TableEvent> ml = ttConfig.getMinevents();
		/*
		 * for (TableEvent te : ml) { System.out.println(te.drop()); }
		 */

		// if the event is not already realized
		if (!evt.getRealized()) {

			// test if the event is minimal
			ml = ttConfig.getMinevents();
			boolean ismin = false;
			for (TableEvent me : ml) {
				if (me.getNumber().equals(evt.getNumber())) {
					ismin = true;
				}
			}
			if (ismin) {
				realizeEvent(evt);
			} else {
				// System.out.println("Event not minimal");
			}
		} else {
			// System.out.println("Event already realized");
		}

	}

	/** the possible realization of an event */
	void realizeEvent(TableEvent event) {
		// computes the delay between current date and event date
		// int currentDate = Integer.parseInt(dateTT.getText());

		// Test : dispay minimal events before realization
		// System.out.println("Minimal events before realization");
		ArrayList<TableEvent> ml = ttConfig.getMinevents();
		/*
		 * for (TableEvent te : ml) { System.out.println(te.drop()); }
		 */

		float currentDate = ttConfig.getCurrentDate();
		float delay = currentDate - event.getInitialDate();
		// System.out.println("*** DELAY : " + delay);

		// control that the event date is in the stats interval
		boolean inInterval = false;
		if (event.getDate() >= minInterval && maxInterval == null && isInterval())
			inInterval = true;
		else if (isInterval())
			inInterval = event.getDate() >= minInterval && event.getDate() <= maxInterval;
		if (!isInterval())
			inInterval = true;

		// control that an event tag is in the list of the enabled stats'tags
		boolean inTag = true;
		for (Tag tag : listTags.keySet()) {
			if (listTags.get(tag))
				inTag = false;
		}
		for (Tag tag : listTags.keySet()) {
			for (String tagEvent : event.getTags()) {
				if (tag.getName().equals(tagEvent) && listTags.get(tag))
					inTag = true;
			}
		}

		ttConfig.getTable().PropagateDelay(event.getNumber(), delay);

		// if the discreteMove is realized
		if (ttConfig.discreteMove(event.getNumber())) {
			if (delay >= delayEscape && inInterval && inTag) {
				delaySum += delay;
				nbOfRealizedEvents++;
			}
		}

		// display the statistics if enabled
		String logs = "";
		if (delaySumEnabled)
			logs = "The sum delay of the events in the interval is " + delaySum + "\n";
		if (averageDelayEnabled && nbOfRealizedEvents > 0) {
			logs += "The average delay of the events in the interval is : " + (delaySum / nbOfRealizedEvents);
		}

		// Test : dispay minimal events before realization
		// System.out.println("Minimal events after realization");
		ml = ttConfig.getMinevents();
		/*
		 * for (TableEvent te : ml) { System.out.println(te.drop()); }
		 */

		// Sends the logged information to the log textfield of the simulator window.
		statsTextString.setValue(logs);
	}

	@FXML
	/** save the state of the TT with the recorded dates as initial dates */
	void saveStateTT(ActionEvent evt) {
		FileChooser fileChooser = new FileChooser();
		LoadFiles loadFiles = new LoadFiles();
		if (!loadFiles.getTimeTablesFolder().equals("")) {
			fileChooser.setInitialDirectory(new File(loadFiles.getTimeTablesFolder()));
		}

		// Set extension filter for text files
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TimeTable state files", "*.statett");
		fileChooser.getExtensionFilters().add(extFilter);

		// Show save file dialog
		File file = fileChooser.showSaveDialog(Main.getPrimaryStage());

		// Write the data of the table in the file
		if (file != null) {
			try {
				PrintWriter writer;
				writer = new PrintWriter(file);
				writer.print("realized");
				for (Object o : eventsTable.getItems().stream().filter(te -> te.getRealized())
						.collect(Collectors.toList())) {
					TableEvent event = (TableEvent) o;
					writer.print(":" + event.getNumber() + ":" + event.getInitialDate());
				}
				writer.println();
				writer.print("minEvents");
				for (TableEvent te : ttConfig.getMinList()) {
					writer.print(":" + te.getNumber());
				}
				writer.println();
				writer.println("date:" + ttConfig.getCurrentDate());
				for (Object o : eventsTable.getItems()) {
					TableEvent event = (TableEvent) o;
					if (event.getRecordedDate() != 0)
						writer.println("event:" + event.getNumber() + ":tag={" + event.getTagsString() + "}:"
								+ event.getRecordedDate() + ":" + event.getLabel());
					else
						writer.println("event:" + event.getNumber() + ":tag={" + event.getTagsString() + "}:"
								+ event.getInitialDate() + ":" + event.getLabel());
				}
				for (Object o : dependenciesTable.getItems()) {
					TableDependency dep = (TableDependency) o;
					writer.println("dependency:" + dep.getOrigin() + ":" + dep.getGoal() + ":" + dep.getDuration());
				}
				writer.close();
			} catch (IOException ex) {
				logger.log(Level.WARNING, "Warning message");
			}
		}
	}

	@FXML
	void loadStateTT(ActionEvent evt) {
		FileChooser fileChooser = new FileChooser();
		LoadFiles loadFiles = new LoadFiles();
		if (!loadFiles.getTimeTablesFolder().equals("")) {
			fileChooser.setInitialDirectory(new File(loadFiles.getTimeTablesFolder()));
		}

		// Set extension filter for text files
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TimeTable state files", "*.statett");
		fileChooser.getExtensionFilters().add(extFilter);

		// Show save file dialog
		File file = fileChooser.showOpenDialog(Main.getPrimaryStage());

		if (file != null) {
			loadFiles.generateTTConfig(file.getAbsolutePath());

			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(file));
				String line = br.readLine();
				String[] segments = line.split(":");// realized:event1 number:event1 initial date:event number2...
				int lastIndex = segments.length - 2;
				if (line.startsWith("realized")) {
					for (int i = 1; i < segments.length; i++) {
						TableEvent te = ttConfig.getTable().events.get(Integer.parseInt(segments[i]));
						te.setRealized(true);
						if (i == lastIndex) {
							int eventNb = te.getNumber();
							float delay = te.getDate() - te.getInitialDate();
							ttConfig.getTable().PropagateDelay(eventNb, delay);
						}
						i++;
						te.setInitialDate(Float.parseFloat(segments[i]));
						te.setRecordedDate(te.getDate());
					}
				}
				line = br.readLine();
				if (line.startsWith("minEvents")) {
					segments = line.split(":");
					ArrayList<TableEvent> minList = new ArrayList<>();
					for (int i = 1; i < segments.length; i++) {
						minList.add(ttConfig.getTable().getEvent(Integer.parseInt(segments[i])));
					}
					ttConfig.setMinevents(minList);
				}
				line = br.readLine();
				if (line.startsWith("date")) {
					float date = Float.parseFloat(line.split(":")[1]);
					ttConfig.setCurrentDate(date);
					StartController.dateTTString.set(String.valueOf(date));
				}
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.WARNING,
						"error while reading the BufferedReader in class StartController/loadStateTT");
				StartController.logsTextString
						.set("Warning : error while loading the state of tt : " + file.getAbsolutePath());
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						logger.log(Level.WARNING,
								"error while closing the BufferedReader in class startController/loadStateTT");
					}
				}
			}
		}
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
	}

	@FXML
	void resetTT(ActionEvent evt) {
		ttConfig.reset();
		dateTTString.set(String.valueOf(ttConfig.getCurrentDate()));
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
	}

	/** ---------------------------------------------------------------------- */
	/** ---------------------------------------------------------------------- */

	@FXML
	/** load the correspondance file */
	void loadCorrespondance(ActionEvent evt) {
		Stage primaryStage = Main.getPrimaryStage();
		loadFiles.loadCorrespondance(primaryStage, infosCorrespString);
		// update timetable info
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
	}

	@FXML
	/** Advances time wrt to the join simulation semantics */
	void testJointTmove(ActionEvent evt) {

		float remTime = rpnc.maxTimedMove();
		logsTextString.setValue(logsTextString.getValue() + "Remaining Time  : \n");
		logsTextString.setValue(logsTextString.getValue() + remTime + "\n");

		rpnc.advanceTime(remTime);

		// update timetable info
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);

		// displays the net
		displayNet();
		dateTTString.set(String.valueOf(ttConfig.getCurrentDate()));

	}

	@FXML
	/**
	 * test for the join simulation between the timetable and the net
	 * 
	 * This test is connected to button Joint Sim, and loads a net, a timetable, and
	 * a correspondance table
	 * 
	 */

	void loadJointSim(ActionEvent evt) {

		String path = loadFiles.getNetsFolder() + "\\";

		Net n = null;
		if (simu != null && this.classModel.equals("Net")) {
			n = (Net) simu.getN();
		} else if (this.classModel.equals("Net")) {
			try {
				simu = new Sim(0, path + "NetWithControlPlaces.net", path, true, this.classModel);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			n = (Net) simu.getN();
			logsTextString.setValue(n.loadFile());
			displayNet();
		}

		// Simulate with the loaded Timetable, or a default table
		TimeTable tt;// the timetable simulated
		if (ttConfig != null) {
			tt = ttConfig.getTable();
		} else {
			tt = new TimeTable(path + "timetableControl.tt");
			tt.fileLoading();
			// logsTextString.setValue(logsTextString.getValue() + "Table created: " +
			// tt.size() + " events \n");
			// logsTextString.setValue(logsTextString.getValue() + "===================\n");

			ttConfig = new TTConfig(tt);

		}

		rn = new RegulNet(tt, n);
		// BUG HERE : Correspondance does not work....
		// rn.LoadCorrespondance(path + "Corr1.cor");
		rn.LoadCorrespondance(path + "CorrControl.cor");
		rn.buildCorrespondance();

		// logsTextString.setValue(logsTextString.getValue() + "Regulated net built\n");

		// logsTextString.setValue(logsTextString.getValue() + "===================\n");

		/*
		 * TTConfig ttc = new TTConfig(tt); ttc.init();
		 * logsTextString.setValue(logsTextString.getValue() + ttc.dropConfig()); String
		 * pathlog = new String(); boolean enableLogs = true; rpnc = new RPNConfig(n,
		 * tt, pathlog, enableLogs); view.displayTTFeedback(ttc, eventsTable,
		 * dependenciesTable);
		 */

		ttConfig.init();
		// logsTextString.setValue(logsTextString.getValue() + ttConfig.dropConfig());
		String pathlog = new String();
		boolean enableLogs = true;
		rpnc = new RPNConfig(n, ttConfig, pathlog, enableLogs);

		// update timetable info
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);

	}

	@FXML
	/** test the join simulation between the timetable and the net */
	void showConfig(ActionEvent evt) {

		/*
		 * logsTextString.setValue(logsTextString.getValue() + "RPN Config:\n"); String
		 * s = rpnc.DropConfig(); logsTextString.setValue(logsTextString.getValue() +
		 * s); s = rpnc.showMaxTimedMove();
		 * logsTextString.setValue(logsTextString.getValue() + "\n" + s);
		 * logsTextString.setValue(logsTextString.getValue() + "===================\n");
		 */

	}

	@FXML
	/**
	 * Method to force control place filling if a transition needs some control
	 * place to be filled.
	 * 
	 * @param evt
	 */
	void testJoinPlaceFill(ActionEvent evt) {

		testJoinPlaceFill();

		// displays the net
		displayNet();

	}

	static void testJoinPlaceFill() {
		System.out.println("Filling Control Places");
		// find the minimal events of TT
		ArrayList<TableEvent> mlist = ttConfig.getMinevents();

		// find the event with the minimal date
		TableEvent minevt = new TableEvent();
		ArrayList<TableEvent> minevts = new ArrayList<>();
		float mindate = -1; // dummy value for initialization
		boolean lockedEvent;

		// for every event that is minimal wrt the ordering in TT
		for (TableEvent one_evt : mlist) { // search every minimal event who has at least one not blocked linked
											// transition
			float evtdate = one_evt.getDate();
			lockedEvent = false;

			// list all transitions implementing this event
			for (int tNb : one_evt.getTransnames()) {
				if (simu.getN().isBlocked(tNb))
					lockedEvent = true;
			}

			// if a minimal date was not found and the event is not blocked
			if (mindate == -1 && !lockedEvent) {
				minevt = one_evt;
				mindate = evtdate;
			}

			// if a greater minimal date was formerly found
			// and the event is not blocked
			if (mindate > evtdate && !lockedEvent) {
				minevt = one_evt;
				mindate = evtdate;
			}
		}
		minevts.add(minevt);
		// System.out.println("Minimal Event" + minevt.drop());
		for (TableEvent one_evt : mlist) {
			float evtdate = one_evt.getDate();
			if (evtdate == minevt.getDate()) {
				minevts.add(one_evt);
				// System.out.println("Minimal Event" + one_evt.drop());
			}
		}

		// Now we have found the minimal event, and stored it in minevt
		// if the event has not yet filled its control places, fill them
		for (TableEvent min : minevts) {
			if (!min.getPlacesMarked()) {
				// say that control places have been marked
				min.setPlacesMarked(true);
				ArrayList<Integer> pnames = min.getPlacesNames();
				PhysicalModel n = rn.getNet();

				for (Integer aplace : pnames) { //
					System.out.println("Place :" + aplace + "Event : " + min.getNumber());
					PlaceAbstract p = n.getControlPlace(aplace);
					p.addToken();
				}
			} else {
				System.out.println("minimal event marked");
			}
		}
	}

	/*
	 * @FXML void delayPFill(ActionEvent evt) { FillControlNow(); }
	 */

	/***
	 * calcule le delai necessaire pour atteindre un événement qui doit remplir une
	 * place de controle
	 */
	public static float delayPFill() {
		float currentTime = rpnc.getCurrentTime();
		float delay = -1;
		// boucle sur les événements
		for (TableEvent te : ttConfig.getMinevents()) {
			// logsTextString.set(logsTextString.get() + "\nev :" + te.getNumber());
			ArrayList<Integer> pnames = te.getPlacesNames();
			// récupere les événements liés à des places de controle non marquées
			if (!te.getPlacesMarked() && !pnames.isEmpty() && te.getDate() >= currentTime) {
				// récupére le plus petit de ces délais
				if (delay == -1) {
					delay = te.getDate() - currentTime;
				} else {
					delay = Math.min(delay, te.getDate() - currentTime);
				}
			}
		}
		// logsTextString.set(logsTextString.get() + "\n===========\ndelayPFill : " +
		// delay);
		return delay;
	}

	/**
	 * This method fills the control places connected to minimal events of the
	 * timetable which execution date is the current date
	 * 
	 * @param evt
	 */
	public static void FillControlNow() {
		float currentTime = rpnc.getCurrentTime();
		PhysicalModel simnet = rpnc.getNetAndTT().getNet();

		// for every minimal event in the timetable
		for (TableEvent te : ttConfig.getMinevents()) {

			// get the list of control places of event te
			ArrayList<Integer> pnames = te.getPlacesNames();

			// If one did not already mark the places for this event
			// and the list of places is not empty
			// and the event's date is the current date
			if (!te.getPlacesMarked() && !pnames.isEmpty() && te.getDate() == currentTime) {

				for (int pn : pnames) {// for every control place attached to te
					// adds a token
					PlaceAbstract p = simnet.getControlPlace(pn);
					p.addToken();
					te.setMarked(true);// say that te gave its control token to ctrl places
					// System.out.println("Control Place " + pn + " marked");
				}
			}
		}
		// logsTextString.set(logsTextString.get() + "\nControl Places filled");
	}

	/**
	 * Compute the duration that the net can let elapse it is the smallest time to
	 * fire
	 * 
	 * @return
	 */
	public static float getNetDelay() {

		// get earliest TTF to 0 in the net
		// We reuse tlist, the list of firable transitions
		/*
		 * float MinNetElapse = -1;// A dummy value for start
		 * 
		 * // get the Petri net of the regulated net PhysicalModel n = rn.getNet();
		 * 
		 * for (Transition t : n.getEnabled().values()) { // consider every firable
		 * transition
		 * 
		 * Float tf = t.getClock(); System.out.println(tf); if (tf > 0) { if
		 * (MinNetElapse < 0) { MinNetElapse = tf; } else { MinNetElapse =
		 * Math.min(MinNetElapse, tf); } } // From here we have the delay allowed by the
		 * net. }
		 */
		PhysicalModel n = rn.getNet();
		float minNetElapse = n.minimumClock();
		return minNetElapse;

	}

	/**
	 * the method called upon click of the global move button of the interface
	 * 
	 * @param evt
	 */
	@FXML
	void globalMove(ActionEvent evt) {

		globalMove(); // performs a discrete or timed move

		// displays the net
		displayNet();
		// update timetable info
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
// set the date in the widgets
		dateTTString.set(String.valueOf(ttConfig.getCurrentDate()));
	}

	/**
	 * Selects which rule applies from the current configuration of the net and the
	 * timetable
	 */
	public void globalMove() {

		boolean dmoveFired = tryDiscreteMove();// try to perform a discrete step at current date

		if (!dmoveFired) {
			// No discrete move is urgent, so the move is a timed move
			// or a timed move followed by a place filling

			float minTableTimeElapse = ttConfig.getTableDelay();
			float MinNetElapse = getNetDelay();
			float delayControlFill = delayPFill();

			// logsTextString.setValue(logsTextString.getValue() + "\nTTelapse=" +
			// minTableTimeElapse + "\nNetelapse="
			// + MinNetElapse + "\nPFillelapse=" + delayControlFill);

			float delayTmove = -3; // set to 0 for now

			if (delayControlFill == 0) { // some control places need to be filled now
				FillControlNow();
			} else {

				if (delayControlFill > 0) {

					delayTmove = delayControlFill;
				}

				if (minTableTimeElapse > 0) {
					if (delayTmove < 0) {
						delayTmove = minTableTimeElapse;
					} else {
						delayTmove = Math.min(minTableTimeElapse, delayControlFill);
					}
				}

				if (MinNetElapse > 0) {
					if (delayTmove < 0) {
						delayTmove = MinNetElapse;
					} else {
						delayTmove = Math.min(delayTmove, MinNetElapse);
					}
				}

			}

			// logsTextString.setValue(logsTextString.getValue() + "\nTimed Move: " +
			// delayTmove);

			// Ready to perform timed move
			// the timed move cannot be negative
			if (delayTmove < 0) {
				delayTmove = 0;
			}
			rpnc.advanceVerifiedTime(delayTmove);
			// ttConfig.timeMove((int) delayTmove);

			// It remains to fill the control places if new events reach their occurence
			// date
			FillControlNow();

		}
	}

	/**
	 * A method that tries to fire a discrete move. If this trial was a success, it
	 * returns true, otherwise it returns false
	 * 
	 * This method is used in a global move to decide if a discrete step can be
	 * taken take it if feasible, or conversely switch to a timed step.
	 * 
	 * @return
	 */
	public static boolean tryDiscreteMove() {

		// If no transition of the net has a time to fire of 0, then return false

		// if one transition t has a ttf of 0
		// 1) if this transition has all its control places filled (or has no control
		// place)
		// 1.a) if this transition does not realize a table event fire it (discrete
		// move)
		// 1.b) if this transition realizes a table event AND and there is an event e
		// that is minimal
		// and whose date is smaller than the current date fire t and e jointly (joint
		// discrete move)
		//

		// Find transitions with 0 TTF

		// Net n = rn.getNet();
		PhysicalModel n = rn.getNet();

		// select the first firable transition
		ArrayList<TransitionAbstract> firecontr = new ArrayList<TransitionAbstract>();
		/*
		 * HashMap<Integer, Transition> tlistfirable = n.getFirable(); for (Transition t
		 * : tlistfirable.values()) { if (t.controlAllowsFiring()) { firecontr.add(t); }
		 * }
		 */
		firecontr = n.fireableTransition();

		// System.out.println("firecontr.size=" + firecontr.size());
		for (TransitionAbstract t : firecontr) {
			// System.out.println("fireable " + t.getName());
		}
		// at this point firecontr contains a list of transitions that can fire
		// (according to the net)

		if (firecontr.size() > 0) { // if one discrete move is allowed in the net

			// take the fist one
			// TODO: In the future : choose randomly which one fires
			TransitionAbstract onet = firecontr.get(0);

			// Check if one event needs to fire with transition onet
			ArrayList<TableEvent> mlist = ttConfig.getMinevents();
			/*
			 * for (TableEvent te : mlist) { System.out.println("min event : " +
			 * te.getLabel()); }
			 */
			TableEvent cte = null;

			for (TransitionAbstract t : firecontr) {
				for (TableEvent te : mlist) {
					if (te.realizedBy(t.getNumber())) {
						// System.out.println("linked event found : " + te.getLabel());
						if (cte == null) {
							cte = te;
							onet = t;
						} else if (te.getDate() < cte.getDate()) {
							cte = te;
							onet = t;
						}
					}
				}
			}

			StartController.setTransition(onet);
			Boolean tcefired = false;
			if (cte != null) { // if we have found a correponding event in the timetable

				if (cte.getDate() <= rpnc.getCurrentTime()) { // if the date ot the table event is before current time
					// logsTextString.setValue(logsTextString.getValue() + "Firing Event " +
					// cte.getNumber() + "\n");

					tcefired = ttConfig.discreteMoveTTPN(cte.getNumber());
					// logsTextString.setValue(logsTextString.getValue() + "TTCdiscrete");

					if (!tcefired) { // log error if event firing failed
						// logsTextString.setValue(logsTextString.getValue() + "Failed\n");
					} else {
						// System.out.println("event " + cte.getNumber() + " fired");
					}
				} else {
					// there is an event in the table that is not yet ready to fire
					// so we need to let time elapse to be able to fire it

					return false;
				}

			} else {// check if a corresponding event exist
				for (TableEvent te : onet.getLinkedEvents()) {
					if (!te.getRealized()) { // event realized by transition t
						// remember this event
						// System.out.println("linked unrealized event found : " + te.getLabel());
						cte = te;
					}
				}
				if (cte == null) {// no linked event exist, so the transition can be fired
					//logsTextString
							//.setValue(logsTextString.getValue() + "no linked event to " + onet.getName() + " found\n");
					//logsTextString.setValue(logsTextString.getValue() + "fire " + onet.getName() + "\n");
					String infos = n.discreteMove(onet, rpnc.getPathLogs(), rpnc.getCurrentTime(), enableLogs.get());
					//logsTextString.setValue(logsTextString.getValue() + infos);
				} else {
					// there is an event in the table that is not yet ready to fire
					// so we need to let time elapse to be able to fire it
					return false;
				}
			}
			if (tcefired) {// If firinf the table Event was successful
				String infos = n.discreteMove(onet, rpnc.getPathLogs(), rpnc.getCurrentTime(), enableLogs.get());
				// logsTextString.setValue(logsTextString.getValue() + "Firing t" +
				// onet.getNumber() + "x e"
				// + cte.getNumber() + " Successful\n");
				StartController.setEventFired(true);
				StartController.setEvent(cte);
				// logsTextString.setValue(logsTextString.getValue() + infos);
			} else
				StartController.setEventFired(false);

		} else { // no transition is firable in the net
			return false;
		}

		return true;
	}

	@FXML
	/** test the join simulation between the timetable and the net */
	void testJointDmove(ActionEvent evt) {

		PhysicalModel n = rn.getNet();

		// select the first firable transition
		// HashMap<Integer, Transition> tlistfireable = n.getFirable();
		ArrayList<TransitionAbstract> tlistfireable = n.fireableTransition();

		if (tlistfireable.size() == 0) { // if no transition is firable
			logsTextString.setValue("No  Transition firable");

			System.out.println("No firable Transition");
		}

		if (tlistfireable.size() > 0) { // if at least one transition is firable
			TransitionAbstract t = tlistfireable.get(0);
			logsTextString.setValue(
					logsTextString.getValue() + "Firing Transition" + t + " at date " + rpnc.getCurrentTime() + "\n");

			System.out.println("Firing Transition " + t);

			// for every minimal event of TT, check if is is realized by t

			ArrayList<TableEvent> mlist = ttConfig.getMinevents();
			TableEvent cte = null;
			for (TableEvent te : mlist) {
				if (te.realizedBy(t.getNumber())) { // event realized by transition t
					// remember this event
					cte = te;
					break;
				}

			}
			Boolean tcefired = false;
			if (cte != null) { // if we have found a correponding event in the timetable

				if (cte.getDate() <= rpnc.getCurrentTime()) { // if the date ot the table event is before current time
					logsTextString.setValue(logsTextString.getValue() + "Firing Event " + cte.getNumber() + "\n");

					tcefired = ttConfig.discreteMoveTTPN(cte.getNumber());
					logsTextString.setValue(logsTextString.getValue() + "TTCdiscrete");

					if (!tcefired) { // log error if event firing failed
						logsTextString.setValue(logsTextString.getValue() + "Failed\n");
					}
				}

			}
			if (tcefired) {// If firinf the table Event was successful
				n.discreteMove(t, "toto", rpnc.getCurrentTime(), false);
				// logsTextString.setValue(logsTextString.getValue() + "Firing t" +
				// t.getNumber() + "x e" + cte.getNumber()
				// + " Successful\n");

			}
			// displays the net
			displayNet();
			// update timetable info
			view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
			dateTTString.set(String.valueOf(ttConfig.getCurrentDate()));

		}
	}

	@FXML
	/** test the join simulation between the timetable and the net */
	void testJointSim(ActionEvent evt) {

		// ********************************************
		// TESTS
		// ********************************************************
		// Useless buttons and functions to test joint simulation
		// ********************************************************

		String path = loadFiles.getNetsFolder() + "\\";

		Net n = null;
		if (simu != null && this.classModel.equals("Net")) {
			n = (Net) simu.getN();
		} else if (this.classModel.equals("Net")) {
			try {
				simu = new Sim(0, path + "JointNet1.net", path, true, this.classModel);
			} catch (FileNotFoundException | ClassNotFoundException | NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			n = (Net) simu.getN();
			logsTextString.setValue(n.loadFile());
			displayNet();
		}

		// Simulate with the loaded Timetable, or a default table
		TimeTable tt;// the timetable simulated
		if (ttConfig != null) {
			tt = ttConfig.getTable();
		} else {
			tt = new TimeTable(path + "JointTable2.tt");
			tt.fileLoading();
			// logsTextString.setValue(logsTextString.getValue() + "Table created: " +
			// tt.size() + " events \n");
			// logsTextString.setValue(logsTextString.getValue() + "===================\n");

			ttConfig = new TTConfig(tt);
		}

		rn = new RegulNet(tt, n);
		// CAUTION : Correspondance block progression if places are filled when events
		// reach their execution date
		rn.LoadCorrespondance(path + "Corr1.cor");
		rn.buildCorrespondance();

		// logsTextString.setValue(logsTextString.getValue() + "Regulated net built\n");

		// logsTextString.setValue(logsTextString.getValue() + "===================\n");

		ttConfig = new TTConfig(tt);
		ttConfig.init();
		// logsTextString.setValue(logsTextString.getValue() + ttConfig.dropConfig());
		String pathlog = new String();
		boolean enableLogs = true;

		// Buil a new RpN Config
		rpnc = new RPNConfig(n, ttConfig, pathlog, enableLogs);
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);

		/// logsTextString.setValue(logsTextString.getValue() + "RPN Config:\n");
		// String s = rpnc.DropConfig();
		// logsTextString.setValue(logsTextString.getValue() + s);
		// logsTextString.setValue(logsTextString.getValue() + "===================\n");

		rpnc.advanceTime(15);
		ttConfig.timeMove(15);

		// logsTextString.setValue(logsTextString.getValue() + "RPN Config:\n");
		// s = rpnc.DropConfig();
		// logsTextString.setValue(logsTextString.getValue() + s);
		// logsTextString.setValue(logsTextString.getValue() + "===================\n");

		float remTime = rpnc.maxTimedMove();
		// logsTextString.setValue(logsTextString.getValue() + "Remainng Time : \n");
		// logsTextString.setValue(logsTextString.getValue() + remTime + "\n");

		rpnc.advanceTime(remTime);

		// logsTextString.setValue(logsTextString.getValue() + "RPN Config:\n");
		// s = rpnc.DropConfig();
		// logsTextString.setValue(logsTextString.getValue() + s);
		// logsTextString.setValue(logsTextString.getValue() + "===================\n");

		remTime = rpnc.maxTimedMove();
		// logsTextString.setValue(logsTextString.getValue() + "Remainng Time : \n");
		// logsTextString.setValue(logsTextString.getValue() + remTime + "\n");

		// update timetable info
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);

		// displays the net
		displayNet();

		// test to fire a transition corresponding to minimal event

		// select the first firable trnasition
		HashMap<Integer, Transition> tlistfireable = n.getFirable();

		if (tlistfireable.size() == 0) { // if no transition is firable
			// logsTextString.setValue("No Transition firable");
		}

		if (tlistfireable.size() > 0) { // if at least one transition is firable
			Transition t = tlistfireable.get(0);
			// logsTextString.setValue(
			// logsTextString.getValue() + "Firing Transition" + t + " at date " +
			// rpnc.getCurrentTime() + "\n");

			// for every minimal event of TT, check if is is realized by t

			ArrayList<TableEvent> mlist = tt.minList();
			TableEvent cte = null;
			for (TableEvent te : mlist) {
				if (te.realizedBy(t.getNumber())) { // event realized by transition t
					// remember this event
					cte = te;
					break;
				}

			}
			Boolean tcefired = false;
			if (cte != null) { // if we have found a correponding event in the timetable

				if (cte.getDate() <= rpnc.getCurrentTime()) { // if the date ot the table event is before current time
					// logsTextString.setValue(logsTextString.getValue() + "Firing Event " +
					// cte.getNumber() + "\n");

					tcefired = ttConfig.discreteMoveTTPN(cte.getNumber());
					// logsTextString.setValue(logsTextString.getValue() + "TTCdiscrete");

					if (!tcefired) { // log error if event firing failed
						// logsTextString.setValue(logsTextString.getValue() + "Failed\n");
					}
				}

			}
			if (tcefired) {// If firinf the table Event was successful
				n.discreteMove(t, "toto", rpnc.getCurrentTime(), false);
				// logsTextString.setValue(logsTextString.getValue() + "Firing t" +
				// t.getNumber() + "x e" + cte.getNumber()
				// + " Successful\n");

			}
			// displays the net
			displayNet();
			dateTTString.set(String.valueOf(ttConfig.getCurrentDate()));
		}
	}

	/**
	 * perform the steps as indicated in the stepsJoint text field for the joint
	 * simulation
	 */
	@FXML
	void goStepsJoint(ActionEvent evt) {
		int steps = Integer.parseInt(stepsJoint.getText());
		for (int i = 1; i <= steps; i++) {
			globalMove(evt);
		}
	}

	/** save the state of the project : current project, net state, tt state */
	@FXML
	void saveState(ActionEvent evt) {
		loadFiles.saveState();
	}

	/** load the state of the project */
	@FXML
	void loadState(ActionEvent evt) {
		loadFiles.loadState();
		displayNet();
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
		dateTTString.set(String.valueOf(ttConfig.getCurrentDate()));
	}

	/** reset the joint simulation with the reset button in the joint tab */
	@FXML
	void resetJoint(ActionEvent evt) {
		rpnc.reset();
		displayNet();
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
		dateTTString.set(String.valueOf(ttConfig.getCurrentDate()));
	}

	/** ------------------------------------------------- */
	/** ----------------------manage project------------- */
	@FXML
	/** save the project (net, tt, correspondance) in a file */
	void saveProject(ActionEvent evt) {
		FileChooser fileChooser = new FileChooser();
		LoadFiles loadFiles = new LoadFiles();
		if (!loadFiles.getNetsFolder().equals("")) {
			fileChooser.setInitialDirectory(new File(loadFiles.getNetsFolder()));
		}
		File file = fileChooser.showSaveDialog(Main.getPrimaryStage());
		if (file != null) {
			try {
				PrintWriter writer;
				writer = new PrintWriter(file);
				String[] segments;
				if (simu != null && simu.getN() != null) {
					segments = simu.getN().fname.replaceAll(Pattern.quote("\\"), "\\\\").split("\\\\");
					writer.println("net:" + segments[segments.length - 1]);
				}
				if (ttConfig != null && ttConfig.getTable() != null) {
					segments = ttConfig.getTable().getFilename().replaceAll(Pattern.quote("\\"), "\\\\").split("\\\\");
					writer.println("tt:" + segments[segments.length - 1]);
				}
				if (getCorrespondancePath() != null) {
					segments = getCorrespondancePath().replaceAll(Pattern.quote("\\"), "\\\\").split("\\\\");
					writer.println("corresp:" + segments[segments.length - 1]);
				}
				writer.close();
			} catch (IOException ex) {
				logger.log(Level.WARNING, "issue while writing in the project file");
			}
		}

	}

	@FXML
	/** load a project (net, tt, correspondance) from a file */
	void loadProject(ActionEvent evt) {
		loadFiles.loadProject();
		if (simu != null) {
			displayNet(simu.displayFeedback());
		}
		// update timetable info
		view.displayTTFeedback(ttConfig, eventsTable, dependenciesTable);
		// displayNet();
	}

	@FXML
	/**
	 * generate neural nets for EqualizationNet.EquNetNeural nets they are saved in
	 * the neural folder there is a csv file with the average speed and the average
	 * timeelapsed of each neural network runs. calls stats.GenerateNeuralTask from
	 * GenerateNeuralController
	 */
	void generateNeuralNets(ActionEvent evt) {
		if (classModel.startsWith("EqualizationNet.EquNetNeural"))
			View.displayGenerateNeural();
	}

	@FXML
	/** seek to optimize the current neural net */
	void optimizeNeuralNet(ActionEvent evt) {
		if (classModel.startsWith("EqualizationNet.EquNetNeural"))
			View.optimizeNeuralNet();
	}

	@FXML
	/** generate and optimize random neural nets and keep the best */
	void fullNeuralNetPipeline(ActionEvent evt) {
		view.fullNeuralNetPipeline();
	}

	@FXML
	/** Display the net graphviz scheme */
	void displayNet(ActionEvent evt) {
		view.displayGraphvizScheme("net.png");
	}

	@FXML
	/** Display the net graphviz scheme */
	void displayTT(ActionEvent evt) {
		view.displayGraphvizScheme("tt.png");
	}

	@FXML
	/** Display the net graphviz scheme */
	void displayNetTT(ActionEvent evt) {
		view.displayGraphvizScheme("total.png");
	}

	@FXML
	/** Display the net graphviz scheme */
	void displayPolar(ActionEvent evt) {
		// Create a polar diagram with remaining times to fire
		view.ShowPolarDiagram();
	}

	@FXML
	/** the display of the stats panel */
	void statsMenuItem(ActionEvent evt) {
		view.displayWindowStats();
	}

	@FXML
	/** launch a class to perform statistics */
	void launchStatsClass(ActionEvent evt) {
		View.displayGenericStats();

	}

	@FXML
	/** perform multiple runs of a timetable and add stats in the log panel */
	void multipleRuns(ActionEvent evt) {
		view.multipleRuns();
	}

	@FXML
	/** the display of the preferences pan */
	void preferencesMenuItem(ActionEvent evt) {
		view.displayPreferences();
	}

	@FXML
	/**
	 * display the configuration interface to create or edit net or timettable files
	 */
	void config(ActionEvent evt) {
		view.displayConfig();
	}

	/** tool to generate timetable and correspondancies files from a net file */
	@FXML
	void generateTTCorFromNet(ActionEvent evt) {
		view.generateTTCor();
	}

	/** tool to generate dependencies from a tt file with multiple lines */
	@FXML
	void generateDependencies(ActionEvent evt) {
		view.generateDependencies();
	}

	/** perform scenario of delays at specific transition(s) for a specific time */
	@FXML
	void scenario(ActionEvent evt) {
		view.scenario();
	}
	
	/**display the processing of LTL signals window*/
	@FXML
	void ltlSignals(ActionEvent evt) {
		view.displayLTLSignals();
	}

	@FXML
	/** quit */
	void quit(ActionEvent evt) {
		// System.exit(-1);
		saveWindowPosition();
		Platform.exit();
	}

	void saveWindowPosition() {
		/*
		 * int i = 0; for (Divider divider : splitPaneGlobal.getDividers()) {
		 * dividersPosGlobal[i].set(divider.getPosition()); i++; }
		 * 
		 * i = 0; for (Divider divider : splitPaneTables.getDividers()) {
		 * dividersPosTables[i].set(divider.getPosition()); i++; }
		 * 
		 * i = 0; for (Divider divider : splitPaneLogsAndStats.getDividers()) {
		 * dividersPosLogsAndStats[i].set(divider.getPosition()); i++; }
		 */
		PreferencesController.saveWindowPosition(splitPaneGlobal, splitPaneTables, splitPaneLogsAndStats);
	}

	/**
	 * Getters and Setters for the statistic window linked to the StatsController
	 * class
	 */
	public static void setDelaySumEnabled(boolean value) {
		delaySumEnabled = value;
	}

	public static boolean getDelaySumEnabled() {
		return delaySumEnabled;
	}

	public static void SetAverageDelayEnabled(boolean value) {
		averageDelayEnabled = value;
	}

	public static boolean getAverageDelayEnabled() {
		return averageDelayEnabled;
	}

	public static void SetDelayEscape(int value) {
		delayEscape = value;
	}

	public static int getDelayEscape() {
		return delayEscape;
	}

	public static int getMinInterval() {
		return minInterval;
	}

	public static void setMinInterval(int minInterval) {
		StartController.minInterval = minInterval;
	}

	public static Integer getMaxInterval() {
		return maxInterval;
	}

	public static void setMaxInterval(Integer maxInterval) {
		StartController.maxInterval = maxInterval;
	}

	public static HashMap<Tag, Boolean> getListTags() {
		return listTags;
	}

	public static void setListTags(HashMap<Tag, Boolean> value) {
		listTags = value;
	}

	public static double getSampling() {
		return sampling;
	}

	public static void setSampling(double value) {
		sampling = value;
	}

	public static boolean isResetDateTTLoad() {
		return resetDateTTLoad;
	}

	public static void setResetDateTTLoad(boolean value) {
		resetDateTTLoad = value;
	}

	public static StringProperty getLogsTextString() {
		return logsTextString;
	}

	public static StringProperty getStatsTextString() {
		return statsTextString;
	}

	public static StringProperty getSourceNetText() {
		return sourceNetTextString;
	}

	public static void setSourceNetText(String value) {
		sourceNetTextString.set(value);
	}

	public static StringProperty getSourceEventsTextString() {
		return sourceEventsTextString;
	}

	public static void setSourceEventsTextString(String value) {
		sourceEventsTextString.set(value);
		;
	}

	public static StringProperty getSourceCorTextString() {
		return sourceCorTextString;
	}

	public static void setSourceCorTextString(String value) {
		sourceCorTextString.set(value);
	}

	public static TTConfig getTtConfig() {
		return ttConfig;
	}

	public static Sim getSimu() {
		return simu;
	}

	public static void setRn(RegulNet rn) {
		StartController.rn = rn;
	}

	public static void setRpnc(RPNConfig rpnc) {
		StartController.rpnc = rpnc;
	}

	public static boolean getConnectTT() {
		return TTConnected.get();
	}

	public static void setConnectTT(boolean value) {
		TTConnected.set(value);
	}

	public static RPNConfig getRpnc() {
		return rpnc;
	}

	public static RegulNet getRn() {
		return rn;
	}

	public static boolean isCorrespLoaded() {
		return correspLoaded;
	}

	public static void setCorrespLoaded(boolean value) {
		correspLoaded = value;
	}

	public static String getCorrespondancePath() {
		return correspondancePath;
	}

	public static void setCorrespondancePath(String value) {
		correspondancePath = value;
	}

	public static StringProperty getInfosCorrespString() {
		return infosCorrespString;
	}

	public static void setSimu(Sim simu) {
		StartController.simu = simu;
	}

	public static void setTtConfig(TTConfig ttConfig) {
		StartController.ttConfig = ttConfig;
	}

	public static void setDelaySum(int value) {
		delaySum = value;
	}

	public static void setNbOfRealizedEvents(int value) {
		nbOfRealizedEvents = value;
	}

	public static boolean getResetDateTTLoad() {
		return resetDateTTLoad;
	}

	public static String getDateTT() {
		return dateTTString.getValue();
	}

	public static void setDateTT(String value) {
		dateTTString.set(value);
	}

	/** The delays are used from the stats/scenario window */
	public static ArrayList<Delay> getDelays() {
		return delays;
	}

	public static void setDelays(ArrayList<Delay> delays, boolean reset) {
		StartController.delays = delays;
		if (rpnc != null) {
			rpnc.setDelays(delays);
			rpnc.manageDelays();
			if (reset)
				rpnc.reset();
		}
	}

	public static void resetDelays() {
		if (rpnc != null) {
			rpnc.resetDelays();
		}
	}

	public static String getStartPlace() {
		return startPlace;
	}

	public static void setStartPlace(String startPlace) {
		StartController.startPlace = startPlace;
	}

	public static String getEndPlace() {
		return endPlace;
	}

	public static void setEndPlace(String endPlace) {
		StartController.endPlace = endPlace;
	}

	public static boolean isCalculTime() {
		return calculTime;
	}

	public static void setCalculTime(boolean calculTime) {
		StartController.calculTime = calculTime;
	}

	public static String getClassModel() {
		return classModel;
	}

	public static void setClassModel(String value) {
		classModel = value;
	}

	public static String getFileModel() {
		return fileModel;
	}

	public static void setFileModel(String value) {
		fileModel = value;
	}

	public static boolean isInterval() {
		return interval;
	}

	public static void setInterval(boolean interval) {
		StartController.interval = interval;
	}

	public static String getProjectFileName() {
		return projectFileName;
	}

	public static void setProjectFileName(String projectFileName) {
		StartController.projectFileName = projectFileName;
	}

	public static TableEvent getEvent() {
		return event;
	}

	public static void setEvent(TableEvent event) {
		StartController.event = event;
	}

	public static boolean isEventFired() {
		return eventFired;
	}

	public static void setEventFired(boolean eventFired) {
		StartController.eventFired = eventFired;
	}

	public static TransitionAbstract getTransition() {
		return transition;
	}

	public static void setTransition(TransitionAbstract onet) {
		StartController.transition = onet;
	}

	public boolean isPolarOpen() {
		return polarOpen;
	}

	public static void setPolarOpen(boolean value) {
		StartController.polarOpen = value;
	}

	public StringProperty getStepsEqualizationInfoString() {
		return stepsEqualizationInfoString;
	}

	public void setStepsEqualizationInfoString(String value) {
		this.stepsEqualizationInfoString.set(value);
		;
	}

	public static boolean isHistogramOpen() {
		return histogramOpen;
	}

	public static void setHistogramOpen(boolean histogramOpen) {
		StartController.histogramOpen = histogramOpen;
	}

	public static String getTargetEqualization() {
		return targetEqualizationValue;
	}

	public static void setDisplayEqualizationFields(boolean value) {
		displayEqualizationFields.setValue(value);
	}

	public static void refreshNet(boolean value) {
		refreshNet.setValue(value);
	}

	public static void setDisplayChart(boolean displayChart) {
		StartController.displayChart = displayChart;
	}

	public static void setTooltips(boolean value) {
		tooltipsProperty.set(value);
	}

	public void setTooltipDuration(Tooltip tooltip) {
		tooltip.setStyle("-fx-show-duration: 10s ;");
	}
	
	public static void setMethods(ArrayList<Pair<Method, String>> methods) {
		simu.setMethods(methods);
	}
	
}
