package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.controlsfx.control.CheckComboBox;

import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.dom.Dom;
import fr.inria.mochy.core.dom.PlaceDom;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.timetable.Tag;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 * The controller for the window of statistics available from the menu stats
 * <p>
 * <img src="StatsController.svg" alt="StatsController class diagram" style=
 * "float: right;">
 */
public class StatsController implements Initializable {

	@FXML
	CheckBox delaySum;
	@FXML
	CheckBox averageDelay;
	@FXML
	TextField delayEscape;
	@FXML
	Button escapeDelayOk;
	@FXML
	TextField min;// the min of the interval in which the calculus must be done
	@FXML
	TextField max;// the max of the interval in which the calculus must be done
	@FXML
	CheckComboBox<String> tags;// the tags in which the calculus must be done
	@FXML
	CheckBox validTimeCalcul;
	@FXML
	ComboBox<String> startPlaces; // the start places to calculate the average time
	@FXML
	ComboBox<String> endPlaces; // the end places to calculate the average time

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// calculate the sum of the delay for the events of the timetable
		if (StartController.getDelaySumEnabled())
			delaySum.setSelected(true);
		delaySum.selectedProperty().addListener((ChangeListener<? super Boolean>) new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (delaySum.isSelected())
					StartController.setDelaySumEnabled(true);
				else
					StartController.setDelaySumEnabled(false);
			}
		});

		// calculate the average delay for the events of the timetable
		if (StartController.getAverageDelayEnabled())
			averageDelay.setSelected(true);
		averageDelay.selectedProperty().addListener((ChangeListener<? super Boolean>) new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (averageDelay.isSelected())
					StartController.SetAverageDelayEnabled(true);
				else
					StartController.SetAverageDelayEnabled(false);
			}
		});

		// escape the delay for the statistics if it is less or equal to delayEscape
		delayEscape.setText(Integer.toString(StartController.getDelayEscape()));
		escapeDelayOk.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				StartController.SetDelayEscape(Integer.parseInt(delayEscape.getText()));
			}
		});

		// define the min and the max of the interval in which the calculation must be
		// done
		min.setText((Integer.toString(StartController.getMinInterval())));
		if (StartController.getMaxInterval() != null)
			max.setText((Integer.toString(StartController.getMaxInterval())));

		// set the list of available tags
		HashMap<Tag, Boolean> listTags = StartController.getListTags();
		/*
		 * for (String tag : listTags.keySet()) { tags.getItems().add(tag); if
		 * (listTags.get(tag) == true) { tags.getCheckModel().check(tag); } }
		 */
		for (Tag tag : listTags.keySet()) {
			tags.getItems().add(tag.getName());
			if (listTags.get(tag))
				tags.getCheckModel().check(tag.getName());
		}

		if (StartController.getSimu() != null) {
			PhysicalModel n = StartController.getSimu().getN();
			for (PlaceAbstract place : n.getPlaces().values()) {
				startPlaces.getItems().addAll(place.getNumber() + "-" + place.getName());
				endPlaces.getItems().addAll(place.getNumber() + "-" + place.getName());
			}
		}

		if (StartController.isCalculTime()) {
			startPlaces.setValue(StartController.getStartPlace());
			endPlaces.setValue(StartController.getEndPlace());
			validTimeCalcul.setSelected(true);
		}

		validTimeCalcul.selectedProperty()
				.addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {
					if (new_val) {
						StartController.setCalculTime(true);
						StartController.setStartPlace(startPlaces.getValue());
						StartController.setEndPlace(endPlaces.getValue());
					} else
						StartController.setCalculTime(false);
				});
	}

	@FXML
	/** set the min and max values of the interval to perform the calculus */
	void intervalOk(ActionEvent evt) {
		StartController.setInterval(true);
		StartController.setMinInterval(Integer.parseInt(min.getText()));
		StartController.setMaxInterval(Integer.parseInt(max.getText()));
	}

	@FXML
	/**
	 * save the checkcombobox items as other values have an Ok button or real time
	 * save configuration
	 */
	void save(ActionEvent evt) {
		HashMap<Tag, Boolean> listTags = StartController.getListTags();
		for (Tag tag : listTags.keySet()) {
			listTags.put(tag, false);
		}
		for (int i : tags.getCheckModel().getCheckedIndices()) {
			String s = tags.getCheckModel().getItem(i);
			if (tags.getCheckModel().isChecked(s)) {
				for (Tag tag : listTags.keySet()) {
					if (tag.getName().equals(s))
						listTags.put(tag, true);
				}
			}
		}

		View.stageStats.close();
	}
}
