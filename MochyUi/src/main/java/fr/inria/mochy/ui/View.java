package fr.inria.mochy.ui;

import static guru.nidi.graphviz.model.Factory.graph;
import static guru.nidi.graphviz.model.Factory.node;
import static guru.nidi.graphviz.model.Factory.to;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.controlsfx.control.CheckComboBox;
import org.jzy3d.chart.AWTChart;
import org.jzy3d.chart.Chart;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Polygon;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;

import fr.inria.mochy.addons.MultipleAxesLineChart;
import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.abstractClass.PlaceAbstract;
import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.dom.Dom;
import fr.inria.mochy.core.dom.PlaceDom;
import fr.inria.mochy.core.dom.TransitionDom;
import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.equalization.EquPlace;
import fr.inria.mochy.core.equalization.Token;
import fr.inria.mochy.core.mochysim.Net;
import fr.inria.mochy.core.mochysim.Place;
import fr.inria.mochy.core.mochysim.Sim;
import fr.inria.mochy.core.mochysim.Transition;
import fr.inria.mochy.core.timetable.TTConfig;
import fr.inria.mochy.core.timetable.TableDependency;
import fr.inria.mochy.core.timetable.TableEvent;
import fr.inria.mochy.core.trajectory.TrajPlace;
import fr.inria.mochy.core.trajectory.TrajTransition;
import fr.inria.mochy.core.trajectory.TrajectoryNet;
import guru.nidi.graphviz.attribute.Label;
import guru.nidi.graphviz.attribute.Records;
import guru.nidi.graphviz.attribute.Style;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Graph;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
//import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * View is used to display content.
 * <p>
 * <img src="View.svg" alt="View class diagram" style="float: right;">
 */
public class View {

	Logger logger = Logger.getLogger("logger");
	private static String OS = System.getProperty("os.name").toLowerCase();
	public static boolean IS_WINDOWS = (OS.indexOf("win") >= 0);
	public static boolean IS_MAC = (OS.indexOf("mac") >= 0);
	public static boolean IS_UNIX = (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	public static boolean IS_SOLARIS = (OS.indexOf("sunos") >= 0);

	/**
	 * the stage to display the preferences pane, used with displayPreferences and
	 * PreferencesController class
	 */
	static Stage stagePreferences;

	/**
	 * the stage to display the stats pane, used with displayWindowStats and
	 * StatsController class
	 */
	static Stage stageStats;

	/**
	 * the stage to display the tool to create scenario with delays at specific
	 * transition for a specific time
	 */
	static Stage stageScenario;

	/**
	 * stage to display the tool to add a delay to a defined transition for a
	 * specific time.
	 */
	static Stage stageScenarioToTransition;
	/** the transition which a delay will be added */
	static TransitionAbstract transition;
	static TableView transitionsTable;
	static TableView placesTable;

	/**
	 * the stage to display the graphviz schemes of net, timetable or net+timetable
	 */
	Stage stageGraphviz;

	/**
	 * the transitionDom used to display the chart with function displayChart and
	 * BarChartController class
	 */
	static TransitionDom t;
	static PlaceDom p;
	static PieChart chart;
	static Stage stagePolar;
	static Stage stageHeadwaysHistogram;
	static Scene scenePolar;
	static Chart chartPolar;
	static float polarTotalTime;

	/**
	 * used in loadPopUpNewModel to allow to reload the input file when the model is
	 * changed
	 */
	static Stage popupStage;

	/** used in addADelayToAnEvent for addADelayToAnEventController */
	static Stage addADelayToAnEventStage;
	static TableEvent tableEvent;
	static TableView eventsTable;
	static TableView dependenciesTable;

	/**
	 * @param feedback : a list of Dom (transitions and places) to display the
	 *                 places have the type of "p" and transitions have "t" a
	 *                 PlaceDom or TransitionDom is then created then the addPlace
	 *                 or addTransition is called to had the element on the user
	 *                 interface
	 */
	public void displayFeedback(ArrayList<Dom> feedback, TableView<PlaceDom> placesTable,
			TableView<TransitionDom> transitionTable) {
		ObservableList<PlaceDom> placesList = FXCollections.observableArrayList();
		ObservableList<TransitionDom> transitionsList = FXCollections.observableArrayList();
		for (Dom d : feedback) {
			if (d.getType().equals("p")) {
				PlaceDom p = new PlaceDom(d.getName(), d.getContent());
				p.setNumber(d.getNumber());
				p.setControl(d.getControl());
				p.setTrajectories(d.getTrajectories());
				p.setTokens(d.getTokens());
				placesList.add(p);
			} else if (d.getType().equals("t")) {
				TransitionDom t = new TransitionDom(d.getName(), d.getPre(), d.getPost(), d.getContent(),
						d.getLowerBound(), d.getUpperBound(), d.getClock());
				t.setNumber(d.getNumber());
				t.setSampler(d.getSampler());
				t.setControl(d.getControl());
				if (d.getSampler() != null && d.getSampler().equals("Gaussian")) {
					t.setGaussian(true);
					t.setWeibull(false);
					t.setWeibullCoef(0);
				} else if (d.getSampler() != null && d.getSampler().contains("Weibull")) {
					t.setWeibull(true);
					t.setGaussian(false);
					String[] segment = d.getSampler().split(":");
					t.setWeibullCoef(Integer.parseInt(segment[1]));
				} else {
					t.setGaussian(false);
					t.setWeibull(false);
					t.setWeibullCoef(0);
				}
				transitionsList.add(t);
			}
		}
		placesTable.setItems(placesList);
		transitionTable.setItems(transitionsList);
		placesTable.refresh();
		transitionTable.refresh();
		// grid.add(transitionsPane, 0, 1);
		// displayInfoDiscretStep();
		// displayTimeMove();
		// interactTT(ttConfig);
	}

	/**
	 * display the timetable on the ui ttConfig : the configuration of the timetable
	 * to retrieve its data grid : the GridPane of the timetable on the ui
	 * eventTable, dependencyTable : the tables displayed on the ui eventPane,
	 * dependencyPane : the panes which contents a title and a table
	 */
	void displayTTFeedback(TTConfig ttConfig, TableView<TableEvent> eventTable,
			TableView<TableDependency> dependencyTable) {
		ObservableList<TableEvent> eventsList = FXCollections.observableArrayList();
		ObservableList<TableDependency> dependenciesList = FXCollections.observableArrayList();
		if (ttConfig != null) {
			for (TableEvent e : ttConfig.getTable().getEvents().values()) {
				eventsList.add(e);
			}
			for (TableDependency d : ttConfig.getTable().getDependencies())
				dependenciesList.add(d);

			eventTable.setItems(eventsList);
			dependencyTable.setItems(dependenciesList);
			eventTable.refresh();
			dependencyTable.refresh();
			/*
			 * grid.getChildren().remove(eventPane);
			 * grid.getChildren().remove(dependencyPane); grid.add(eventPane, 3, 1);
			 * grid.add(dependencyPane, 4, 1);
			 */
		}
	}

	/** display the firable transitions near to the discrete step button */
	public void displayInfoDiscretStep(Sim simu, StringProperty infosValue) {

		if (simu.getN().numberFireable() == 0) {
			infosValue.set("no firable transitions");
		} else {
			PhysicalModel n = StartController.getSimu().getN();
			String s = "firable transitions : ";
			for (TransitionAbstract t : n.getFirable().values())
				s += t.getName() + " ";
			infosValue.set(s);
		}

	}

	/** display the stats window */
	public void displayWindowStats() {
		// load the xml file of the user interface
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("stats.fxml"));
		stageStats = new Stage();
		stageStats.setScene(scene);
		stageStats.setTitle("Stats");
		stageStats.show();
	}

	/** display the preferences window */
	public void displayPreferences() {
		// load the xml file of the preferences panel
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("preferences.fxml"));
		stagePreferences = new Stage();
		stagePreferences.setScene(scene);
		stagePreferences.setTitle("Preferences");
		stagePreferences.show();
	}

	/** display the chart associated with the clock probability of a transition */
	public void displayChart(Stage primaryStage, TransitionDom t) {
		this.t = t;

		Scene scene = new Scene((Parent) JfxUtils.loadFxml("chart.fxml"));

		// New window (Stage)
		Stage newWindow = new Stage();
		newWindow.setTitle("Chart");
		newWindow.setScene(scene);

		// Set position of second window, related to primary window.
		if (primaryStage != null) {
			newWindow.setX(primaryStage.getX() + 200);
			newWindow.setY(primaryStage.getY() + 100);
		}

		newWindow.show();
	}

	public void displayChart(Stage primaryStage, PlaceDom data) {
		this.p = data;

		Scene scene = new Scene((Parent) JfxUtils.loadFxml("chartPlaces.fxml"));

		// New window (Stage)
		Stage newWindow = new Stage();
		newWindow.setTitle("Chart");
		newWindow.setScene(scene);

		// Set position of second window, related to primary window.
		if (primaryStage != null) {
			newWindow.setX(primaryStage.getX() + 200);
			newWindow.setY(primaryStage.getY() + 100);
		}

		newWindow.show();
	}

	/**
	 * display the multiple runs window available from the Stats/Multiple Runs menu.
	 */
	public void multipleRuns() {
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("multipleRuns.fxml"));
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle("Multiple runs");
		stage.show();
	}

	/**
	 * display the window which allow to create/edit net or timetable files.
	 */
	public void displayConfig() {
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("config.fxml"));
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle("Configuration");
		stage.show();
	}

	/**
	 * display the window which allow to generate timetable and correspondancies
	 * files from a net file
	 */
	public void generateTTCor() {
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("generateTimeTable.fxml"));
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle("Generate TT and Cor from Net");
		stage.show();
	}

	/**
	 * display the window which allows to generate dependencies from a timetable to
	 * link multiple lines of events
	 */
	public void generateDependencies() {
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("generateDependencies.fxml"));
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle("Generate dependencies from a timetable");
		stage.show();
	}

	/**
	 * display the window which perform scenario of delays at specific transition(s)
	 * for a specific time
	 */
	public void scenario() {
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("scenario.fxml"));
		stageScenario = new Stage();
		stageScenario.setScene(scene);
		stageScenario.setTitle("Scenario");
		stageScenario.show();
	}
	
	/**display the window which process the LTL signals*/
	public void displayLTLSignals() {
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("ltlSignals.fxml"));
		Stage stageLTL = new Stage();
		stageLTL.setScene(scene);
		stageLTL.setTitle("LTL Signals");
		stageLTL.show();
	}

	public void ShowPolarDiagram() {

		Sim simu = StartController.getSimu(); // find the simulator for the current simulation
		// HashMap<Integer,Transition> TrMap=simu.getN().transitions; // get the list of
		// transitions of the physical model
		HashMap<Integer, ? extends TransitionAbstract> TrMap = simu.getN().getTransitions();

		Group root = new Group();
		scenePolar = new Scene(root, 600, 400);
		stagePolar = new Stage();
		stagePolar.setTitle("Remaining TTFs");

		// Create a list of observable data
		ObservableList<PieChart.Data> PieChartLabelsData = FXCollections
				.observableArrayList(new PieChart.Data("Dummy ", 100));

		// create a PieChartLabels from this list
		chart = new PieChart(PieChartLabelsData);
		chart.setTitle("Remaining Time to Fire");
		chart.getData().clear(); // empty data contents for PieChartLabels

		// add all transitions : an empty grey interval if the transition is not
		// enabled, and a grey interval cut by an data sector in gree representing
		// remainning time to fire otherwise
		polarTotalTime = 0;
		for (TransitionAbstract tr : TrMap.values()) {
			if (tr.getUpperBound() != null)
				polarTotalTime += tr.getUpperBound();
		}

		for (TransitionAbstract tr : TrMap.values()) {// visit the whole list of transitions

			// test if the transition has a TTF
			Float ttf = tr.getClock(); // get the clock of the transition
			if (ttf == null && !(tr.getUpperBound() == null)) { // null clock means transition is not enabled

				PieChart.Data data = new PieChart.Data(tr.getName(), Float.parseFloat(tr.getMax()));

				chart.getData().add(data);
				data.getNode().setStyle("-fx-pie-color: #efefef;");

			} else if (!(ttf == null)) { // the transition is enabled and has a TTF
				if (!(tr.getUpperBound() == null)) {
					Float value = Float.parseFloat(tr.getMax()) - ttf;

					// draw the interval upper bound
					PieChart.Data data = new PieChart.Data(tr.getName(), value);
					chart.getData().add(data);
					data.getNode().setStyle("-fx-pie-color: #efefef;"); // Grey color
				}
				// draw the TTF
				PieChart.Data data2 = new PieChart.Data(tr.getName() + "-TTF", ttf);
				chart.getData().add(data2);
				data2.getNode().setStyle("-fx-pie-color: #5eef5e;");
			}

		}

		// chart.getData().add(new PieChartLabels.Data("t5", 100));

		chart.setLegendVisible(false);
		/*
		 * ObservableList<PieChart.Data> datas2 = FXCollections .observableArrayList();
		 * double total = 0; for (Data data : chart.getData()) total +=
		 * data.getPieValue(); for (Data data : chart.getData()) { if
		 * (data.getPieValue()/total < 0.03) data.setPieValue(total*0.03); }
		 */

		((Group) scenePolar.getRoot()).getChildren().add(chart);
		stagePolar.setScene(scenePolar);
		stagePolar.setX(700);
		stagePolar.show();
		StartController.setPolarOpen(true);
		stagePolar.setOnCloseRequest(event -> {
			StartController.setPolarOpen(false);
		});
	}

	public static void generatePolarDiagram() {
		Sim simu = StartController.getSimu(); // find the simulator for the current simulation
		// HashMap<Integer,Transition> TrMap=simu.getN().transitions; // get the list of
		// transitions of the physical model
		HashMap<Integer, ? extends TransitionAbstract> TrMap = simu.getN().getTransitions();

		// Create a list of observable data
		ObservableList<PieChart.Data> datas = FXCollections.observableArrayList();

		// create a PieChartLabels from this list
		chart = new PieChart(datas);
		chart.setTitle("Remaining Time to Fire");
		chart.getData().clear(); // empty data contents for PieChartLabels

		// add all transitions : an empty grey interval if the transition is not
		// enabled, and a grey interval cut by an data sector in gree representing
		// remainning time to fire otherwise

		for (TransitionAbstract tr : TrMap.values()) {// visit the whole list of transitions

			// test if the transition has a TTF
			Float ttf = tr.getClock(); // get the clock of the transition
			System.out.println(tr.getName()+" "+ttf+" "+tr.getUpperBound());
			if (ttf == null && !(tr.getUpperBound() == null)) { // null clock means transition is not enabled
				//System.out.println("1");
				PieChart.Data data = new PieChart.Data(tr.getName(), Float.parseFloat(tr.getMax()));

				chart.getData().add(data);
				data.getNode().setStyle("-fx-pie-color: #efefef;");
				data.getNode().setVisible(true);

			} else if (!(ttf == null)) { // the transition is enabled and has a TTF
				if (!(tr.getUpperBound() == null)) {
					Float value = Float.parseFloat(tr.getMax()) - ttf;

					// draw the interval upper bound
					PieChart.Data data = new PieChart.Data(tr.getName(), value);
					chart.getData().add(data);
					data.getNode().setStyle("-fx-pie-color: #efefef;"); // Grey color
					data.getNode().setVisible(true);
				}

				for (Float timeToFire : tr.tokensTimeToFire()) {
					// draw the TTF
					//System.out.println("2 : "+timeToFire);
					if (timeToFire != null) {
						if (timeToFire < polarTotalTime * 0.05)
							timeToFire = (float) (polarTotalTime * 0.05);
						//System.out.println("3 : "+timeToFire);
						PieChart.Data data2 = new PieChart.Data(tr.getName() + "-TTF", timeToFire);
						chart.getData().add(data2);
						data2.getNode().setStyle("-fx-pie-color: #5eef5e;");
						data2.getNode().setVisible(true);
						// data2.getNode().minWidth(5);
					}
				}
			}

		}

		// chart.getData().add(new PieChartLabels.Data("t5", 100));

		chart.setLegendVisible(false);
		chart.setLabelsVisible(true);
		// chart.setStyle("-fx-start-angle:5;");
		/*
		 * double total = 0; for (Data data : chart.getData()) total +=
		 * data.getPieValue();
		 */
		// for (Data data : chart.getData()) {
		// if (data.getPieValue()/total < 0.05)
		// data.setPieValue(total*0.05);
		// data.getNode().minWidth(20);
		// }
		// chart.setMinWidth(700);
		// chart.setMinHeight(500);
		// chart.setLabelLineLength(50);
		Group root = new Group();
		Scene scene = new Scene(root, 600, 400);
		((Group) scene.getRoot()).getChildren().add(chart);
		stagePolar.setScene(scene);
		stagePolar.show();
	}

	public static void displayHeadwaysHistogram() {
		Group root = new Group();
		Scene scene = new Scene(root, 600, 400);
		stageHeadwaysHistogram = new Stage();
		stageHeadwaysHistogram.setTitle("Headways histogram");

		generateHeadwaysHistogram();

		// ((Group) scene.getRoot()).getChildren().add(histogram);
		// stageHeadwaysHistogram.setScene(scene);
		// stageHeadwaysHistogram.setX(700);
		// stageHeadwaysHistogram.show();
		StartController.setHistogramOpen(true);
		stageHeadwaysHistogram.setOnCloseRequest(event -> {
			StartController.setHistogramOpen(false);
		});
	}

	public static void generateHeadwaysHistogram() {

		final CategoryAxis xAxis = new CategoryAxis();
		final NumberAxis yAxis = new NumberAxis();
		final BarChart<String, Number> histogram = new BarChart<>(xAxis, yAxis);
		histogram.setCategoryGap(0);
		histogram.setBarGap(5);

		xAxis.setLabel("Bef(ore) and Aft(er) Tokens");
		yAxis.setLabel("Headways distance");

		/**
		 * browse the tokens of the equalization net and get its distance from the pre
		 * and to the next tokens add it then to the histogram
		 */
		if (StartController.classModel.startsWith("EqualizationNet")) {
			EquNet n = (EquNet) StartController.getSimu().getN();
			/** V1 */
			/*
			 * for (EquPlace p : n.getPlaces().values()) { int number = 0;//used if there is
			 * multiple tokens in the same place for (Token token : p.getTokens()) {
			 * XYChart.Series<String, Number> serie = new XYChart.Series(); if (number == 0)
			 * serie.setName(p.getName()); else serie.setName(p.getName()+" "+number); float
			 * DnPlusUn = token.postToken.getXTotal() - token.getXTotal(); if (DnPlusUn < 0)
			 * DnPlusUn = token.postToken.getXTotal() + n.getTotalDistance() -
			 * token.getXTotal(); float DnMoinsUn = token.getXTotal() -
			 * token.previousToken.getXTotal(); if (DnMoinsUn < 0) DnMoinsUn =
			 * token.getXTotal() + n.getTotalDistance() - token.previousToken.getXTotal();
			 * if (number ==0) { serie.getData().add(new XYChart.Data<String,
			 * Number>("bef "+p.getName(), DnMoinsUn)); serie.getData().add(new
			 * XYChart.Data<String, Number>("aft "+p.getName(), DnPlusUn)); }else {
			 * serie.getData().add(new XYChart.Data<String,
			 * Number>("bef "+p.getName()+" "+number, DnMoinsUn)); serie.getData().add(new
			 * XYChart.Data<String, Number>("aft "+p.getName()+" "+number, DnPlusUn)); }
			 * 
			 * histogram.getData().addAll(serie); number++; } }
			 */
			/** V2 */
			xAxis.setLabel("Tokens headways from indicated places");
			XYChart.Series<String, Number> serie = new XYChart.Series<>();
			serie.setName("");
			for (EquPlace p : n.getPlaces().values()) {
				if (!p.equals(n.garage)) {
					int number = 0;// used if there is multiple tokens in the same place
					for (Token token : p.getTokens()) {
						float DnPlusUn = token.postToken.getXTotal() - token.roundXTotal();
						if (DnPlusUn < 0)
							DnPlusUn = token.postToken.getXTotal() + n.getTotalDistance() - token.roundXTotal();
						if (number == 0)
							serie.getData().add(new XYChart.Data<String, Number>(
									p.getName() + "-" + token.postToken.getPlace().getName(), DnPlusUn));
						else
							serie.getData().add(new XYChart.Data<String, Number>(
									p.getName() + "-" + token.postToken.getPlace().getName() + " " + number, DnPlusUn));
						number++;
					}
				}
			}
			histogram.getData().addAll(serie);
		}

		Group root = new Group();
		Scene scene = new Scene(root, 600, 400);
		((Group) scene.getRoot()).getChildren().add(histogram);
		stageHeadwaysHistogram.setScene(scene);
		stageHeadwaysHistogram.show();
	}

	/** display the graphviz scheme for the net, tt or net+tt */
	public void displayGraphvizScheme(String imageName) {
		TTConfig ttConfig = StartController.getTtConfig();
		Sim simu = StartController.getSimu();
		/*
		 * if ((imageName == "net.png" || imageName == "total.png") && simu!=null) { for
		 * (Transition t : simu.getN().getTransitions()) { Node node0 =
		 * node("trans"+t.getNumber()).with(Records.of("<"+t.getNumber()+">transition "
		 * +t.getName())); for (Place pcontrol : t.getControlPre()) { graph =
		 * graph.with(node(pcontrol.getName()).link(to(node0).with(Style.DASHED))); }
		 * for (Place pin : t.getPre()) { graph =
		 * graph.with(node(pin.getName()).link(node0)); } for (Place pout : t.getPost())
		 * { graph = graph.with(node0.link(pout.getName())); } } } if ((imageName ==
		 * "tt.png" || imageName == "total.png") && ttConfig != null) { for
		 * (TableDependency d : ttConfig.getTable().getDependencies()) { TableEvent
		 * start = ttConfig.getTable().getEvent(d.getStartEvent()); TableEvent end =
		 * ttConfig.getTable().getEvent(d.getEndEvent()); Node startNode =
		 * node("evt"+start.getNumber()).with(Label.of("evt "+start.getNumber()+" "
		 * +start.getLabel())); Node endNode =
		 * node("evt"+end.getNumber()).with(Label.of("evt "+start.getNumber()+" "+end.
		 * getLabel())); graph = graph.with(startNode.link(endNode)); } } if (imageName
		 * == "total.png" && simu != null && ttConfig != null) { for (TableEvent te :
		 * ttConfig.getTable().getEvents().values()) { Node node1 =
		 * node("evt"+te.getNumber());//.with(Label.of("evt "+te.getLabel())); for(int
		 * tnumber : te.getTransNames()) { Transition t =
		 * simu.getN().findTransition(tnumber); Node node0 =
		 * node("trans"+t.getNumber()).with(Records.of("<"+t.getNumber()+">transition "
		 * +t.getName())); graph = graph.with(node1.link(node0)); } } } try {
		 * Graphviz.fromGraph(graph).render(Format.PNG).toFile(new
		 * File("graphviz/image.png")); } catch (IOException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 */
		FileWriter file = null;
		try {
			File dir = new File("graphviz");
			dir.mkdir();
			file = new FileWriter("graphviz/file.dot", false);
			file.write("digraph {\n");
			if (simu != null) {
				PhysicalModel n = simu.getN();
				if ((imageName == "net.png" || imageName == "total.png") && simu != null) {
					file.write("subgraph cluster_0 {\n");
					for (PlaceAbstract place : n.getPlaces().values()) {
						file.write("\"" + place.getName() + "\"\n");
					}
					HashMap<Integer, ? extends PlaceAbstract> controlPlaces = simu.getN().getControlPlaces();
					for (PlaceAbstract place : controlPlaces.values()) {
						file.write("\"" + place.getName() + "\"\n");
					}
					for (TransitionAbstract t : n.getTransitions().values()) {
						file.write("\"trans" + t.getNumber() + "\" " + "[\"shape\"=\"record\",\"label\"=\"transition "
								+ t.getName() + "\"]\n");
						for (PlaceAbstract pcontrol : t.getControlPre()) {
							file.write("\"" + pcontrol.getName() + "\" -> \"trans" + t.getNumber()
									+ "\" [\"style\"=\"dashed\"]\n");
						}
						for (PlaceAbstract pin : t.getPre()) {
							file.write("\"" + pin.getName() + "\" -> \"trans" + t.getNumber() + "\"\n");
						}
						for (PlaceAbstract pout : t.getPost()) {
							file.write("\"trans" + t.getNumber() + "\" -> \"" + pout.getName() + "\"\n");
						}
					}
					file.write("}\n");
				}
				if ((imageName == "tt.png" || imageName == "total.png") && ttConfig != null) {
					file.write("subgraph cluster_1 {\n");
					for (TableEvent start : ttConfig.getTable().getEvents().values()) {
						boolean fireable = false;
						if (ttConfig.getMinevents().contains(start))
							fireable = true;
						if (start.getRealized())
							file.write("\"evt" + start.getNumber() + "\" [\"label\"=\"evt " + start.getNumber() + " "
									+ start.getLabel() + "\",color=\"blue\"]\n");
						else if (fireable && ttConfig.getCurrentDate() == start.getDate())
							file.write("\"evt" + start.getNumber() + "\" [\"label\"=\"evt " + start.getNumber() + " "
									+ start.getLabel() + "\",color=\"green\"]\n");
						else if (fireable && ttConfig.getCurrentDate() > start.getDate())
							file.write("\"evt" + start.getNumber() + "\" [\"label\"=\"evt " + start.getNumber() + " "
									+ start.getLabel() + "\",color=\"red\"]\n");
						else if (!fireable && ttConfig.getCurrentDate() > start.getDate())
							file.write("\"evt" + start.getNumber() + "\" [\"label\"=\"evt " + start.getNumber() + " "
									+ start.getLabel() + "\",color=\"pink\"]\n");
						else
							file.write("\"evt" + start.getNumber() + "\" [\"label\"=\"evt " + start.getNumber() + " "
									+ start.getLabel() + "\"]\n");
					}
					for (TableDependency d : ttConfig.getTable().getDependencies()) {
						TableEvent start = ttConfig.getTable().getEvent(d.getStartEvent());
						TableEvent end = ttConfig.getTable().getEvent(d.getEndEvent());
						file.write("\"evt" + start.getNumber() + "\" -> \"evt" + end.getNumber() + "\"\n");
					}
					file.write("}\n");
				}
				if (imageName == "total.png" && simu != null && ttConfig != null) {
					for (TableEvent te : ttConfig.getTable().getEvents().values()) {
						for (int tnumber : te.getTransNames()) {
							TransitionAbstract t = n.findTransition(tnumber);
							file.write("\"evt" + te.getNumber() + "\" -> \"trans" + t.getNumber()
									+ "\" [\"style\"=\"dashed\", dir=\"both\"]\n");
						}
						for (int placeNb : te.getPlacesNames()) {
							PlaceAbstract p = n.findPlace(placeNb);
							file.write("\"evt" + te.getNumber() + "\" -> \"" + p.getName()
									+ "\" [dir=none,color = \"black:invis:black\"]\n");
						}
					}
				}
			}
			file.write("}");
		} catch (IOException e) {
			logger.log(Level.WARNING, "error of writing in file dot in class View");
		} finally {
			try {
				file.close();
			} catch (IOException e) {
				logger.log(Level.WARNING, "error while closing the file dot in class View");
			}
		}
		ProcessBuilder processBuilder = new ProcessBuilder();
		if (IS_WINDOWS)
			processBuilder.command("cmd.exe", "/c",
					"dot -Tpng graphviz/file.dot -o graphviz/image.png ; dot -Tpdf graphviz/file.dot -o graphviz/image.pdf");
		else if (IS_UNIX || IS_SOLARIS)
			processBuilder.command("/bin/bash", "-c",
					"dot -Tpng graphviz/file.dot -o graphviz/image.png ; dot -Tpdf graphviz/file.dot -o graphviz/image.pdf");
		else if (IS_MAC)
			processBuilder.command(
					"dot -Tpng graphviz/file.dot -o graphviz/image.png ; dot -Tpdf graphviz/file.dot -o graphviz/image.pdf");
		try {
			Process process = processBuilder.start();
			// StringBuilder output = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				// output.append(line + "\n");
			}
			int exitVal = process.waitFor();
			if (exitVal == 0) {
				System.out.println("Success!");
			}
			// System.out.println(output);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		File fileImage = new File("graphviz/image.png");
		String localUrl = null;
		try {
			localUrl = fileImage.toURI().toURL().toString();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (localUrl != null) {
			Image image = new Image(localUrl);
			ImageView imageView = new ImageView(image);
			ScrollPane root = new ScrollPane();
			root.setPadding(new Insets(20));
			root.setContent(imageView);
			MenuBar menuBar = new MenuBar();
			Menu fileMenu = new Menu("File");
			MenuItem savePng = new MenuItem("save as png");
			MenuItem savePdf = new MenuItem("save as pdf");
			fileMenu.getItems().addAll(savePng, savePdf);
			savePng.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent t) {
					save("png");
				}
			});
			savePdf.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent t) {
					save("pdf");
				}
			});
			menuBar.getMenus().add(fileMenu);
			BorderPane theBorderPane = new BorderPane();
			theBorderPane.setTop(menuBar);
			theBorderPane.setCenter(root);

			Scene scene = new Scene(theBorderPane, 600, 400);
			stageGraphviz = new Stage();
			stageGraphviz.setScene(scene);
			stageGraphviz.setTitle(imageName);
			stageGraphviz.show();
		}
	}

	private void save(String format) {
		FileChooser fileChooser = new FileChooser();
		LoadFiles loadFiles = new LoadFiles();
		if (!loadFiles.getNetsFolder().equals("")) {
			fileChooser.setInitialDirectory(new File(loadFiles.getNetsFolder()));
		}

		// Set extension filter for text files
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(format + " files (*." + format + ")",
				"*." + format);
		fileChooser.getExtensionFilters().add(extFilter);

		// Show save file dialog
		File file = fileChooser.showSaveDialog(stageGraphviz);

		// Write the data of the table in the file
		if (file != null) {
			File origin = new File("graphviz/image." + format);
			try {
				FileUtils.copyFile(origin, file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * display on a chart the standard deviation as y, the speed as y2 depending of
	 * the time elapsed x
	 */
	static void chartEquStandardDeviation(ArrayList<Float> x, ArrayList<Float> y, ArrayList<Float> y2) {
		Group root = new Group();
		Scene scene = new Scene(root, 600, 400);
		Stage stage = new Stage();

		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel("Time");
		xAxis.setAutoRanging(true);
		yAxis.setLabel("standard deviation");
		// creating the chart
		final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);
		lineChart.setTitle("equalization-standard deviation");
		// defining a series
		XYChart.Series series = new XYChart.Series();// the standard deviation over time elapsed
		series.setName("standard deviation");
		// populating the series with data
		for (int i = 0; i < x.size() && i < y.size(); i++) {
			series.getData().add(new XYChart.Data<Float, Float>(x.get(i), y.get(i)));
		}
		lineChart.getData().add(series);

		XYChart.Series series2 = new XYChart.Series();// the speed over time elapsed
		series2.setName("average speed");
		for (int i = 0; i < x.size() && i < y2.size(); i++) {
			series2.getData().add(new XYChart.Data<Float, Float>(x.get(i), y2.get(i)));
		}

		MultipleAxesLineChart chart = new MultipleAxesLineChart(lineChart, Color.BLACK, 1.5);
		chart.addSeries(series2, Color.BLUE, false);

		scene = new Scene(chart, 800, 600);
		stage.setScene(scene);
		stage.show();
	}

	public static void display3DChart(float[][] distDataProp, float[] noise, float[] alpha) {
		List<Polygon> polygons = new ArrayList<Polygon>();
		for (int i = 0; i < distDataProp.length - 1; i++) {
			for (int j = 0; j < distDataProp[i].length - 1; j++) {
				Polygon polygon = new Polygon();
				polygon.add(new Point(new Coord3d(noise[i], alpha[j], distDataProp[i][j])));
				polygon.add(new Point(new Coord3d(noise[i], alpha[j + 1], distDataProp[i][j + 1])));
				polygon.add(new Point(new Coord3d(noise[i + 1], alpha[j + 1], distDataProp[i + 1][j + 1])));
				polygon.add(new Point(new Coord3d(noise[i + 1], alpha[j], distDataProp[i + 1][j])));
				polygons.add(polygon);
			}
		}

		// Creates the 3d object
		Shape surface = new Shape(polygons);
		surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(),
				surface.getBounds().getZmax(), new org.jzy3d.colors.Color(1, 1, 1, .5f)));
		surface.setWireframeDisplayed(true);
		surface.setWireframeColor(org.jzy3d.colors.Color.BLACK);

		// Chart chart = new Chart();
		// chart.getScene().getGraph().add(surface);
		Chart chart = new AWTChart(Quality.Advanced);
		chart.getAxeLayout().setXAxeLabel("noise");
		chart.getAxeLayout().setYAxeLabel("alpha");
		chart.getAxeLayout().setZAxeLabel("time elapsed");
		chart.add(surface);
		chart.open("Jzy3d Demo", 600, 600);

	}

	static void loadPopUpNewModel() {
		if (StartController.getSimu() != null) {
			popupStage = new Stage();
			Scene scene = new Scene((Parent) JfxUtils.loadFxml("popupNewModel.fxml"));
			popupStage.setScene(scene);
			popupStage.setTitle("New Model");
			popupStage.show();
		}
	}

	/** the constoller is AlphasNoisesController */
	static void displayAlphaNoise() {
		Stage stage = new Stage();
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("alphaNoise.fxml"));
		stage.setScene(scene);
		stage.setTitle("Different values of alpha and noise simulation");
		stage.show();
	}

	/** the controller is MultipleTokensController */
	static void displayMultipleTokens() {
		Stage stage = new Stage();
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("multipleTokens.fxml"));
		stage.setScene(scene);
		stage.setTitle("Different values of the number of tokens simulation");
		stage.show();
	}

	/** calls GenericStatsController */
	static void displayGenericStats() {
		Stage stage = new Stage();
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("genericStats.fxml"));
		stage.setScene(scene);
		stage.setTitle("Generic stats");
		stage.show();
	}

	/** calls GenerateNeuralController */
	static void displayGenerateNeural() {
		Stage stage = new Stage();
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("generateNeural.fxml"));
		stage.setScene(scene);
		stage.setTitle("Generate neural networks");
		stage.show();
	}

	/** calls OptimizeNeuralController which calls OptimizeNeuralNetTask */
	static void optimizeNeuralNet() {
		Stage stage = new Stage();
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("optimizeNeural.fxml"));
		stage.setScene(scene);
		stage.setTitle("Optimize neural networks");
		stage.show();
	}

	/** calls GenerateNeuralMultiTokensNbController */
	static void displayGenerateNeuralMultiTokensNb() {
		Stage stage = new Stage();
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("generateNeuralMultiTokensNb.fxml"));
		stage.setScene(scene);
		stage.setTitle("Generate neural networks-Multi tokens Nb");
		stage.show();
	}

	/**
	 * calls OptimizeNeuralMultiTokensNbController which calls
	 * OptimizeNnetMultiTokensNbTask
	 */
	static void optimizeNeuralMultiTokensNb() {
		Stage stage = new Stage();
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("optimizeNeuralMultiTokensNb.fxml"));
		stage.setScene(scene);
		stage.setTitle("Optimize neural networks-Multi tokens Nb");
		stage.show();
	}

	public void fullNeuralNetPipeline() {
		Stage stage = new Stage();
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("fullNeuralNetPipeline.fxml"));
		stage.setScene(scene);
		stage.setTitle("Full neural net generation pipeline");
		stage.show();
	}

	public void addADelayToAnEvent(TableEvent item, TableView eventsTable, TableView dependenciesTable) {
		tableEvent = item;
		this.eventsTable = eventsTable;
		this.dependenciesTable = dependenciesTable;
		addADelayToAnEventStage = new Stage();
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("addADelayToEvent.fxml"));
		addADelayToAnEventStage.setScene(scene);
		addADelayToAnEventStage.setTitle("Add a delay to the event " + item.getLabel());
		addADelayToAnEventStage.getProperties().put(0, item);
		addADelayToAnEventStage.show();
	}

	public void addAScenarioToATransition(TransitionAbstract t, TableView transitions, TableView places) {
		transition = t;
		transitionsTable = transitions;
		placesTable = places;
		Scene scene = new Scene((Parent) JfxUtils.loadFxml("addScenarioToATransition.fxml"));
		stageScenarioToTransition = new Stage();
		stageScenarioToTransition.setScene(scene);
		stageScenarioToTransition.setTitle("Scenario");
		stageScenarioToTransition.show();
	}

}
