package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.core.timetable.TableEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Window;

public class addADelayToAnEventController implements Initializable {
	@FXML TextField textField;
	@FXML Text text;
	TableEvent te;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		te = StartController.view.tableEvent;
		text.setText("to the event " + te.getLabel());
	}

	/**add the delay to the event*/
	@FXML
	void enter (ActionEvent evt) {
		if (textField.getText().equals(""))
			StartController.logsTextString.set("Warning : the text field is empty");
		else {
			try {
				int delay = Integer.parseInt(textField.getText());
				int evtid = te.getNumber();
				float date = te.getDate();
				if (!te.getRealized()) {
					te.setDate(date + delay);
					StartController.ttConfig.getTable().PropagateDelay(evtid, delay);
					StartController.view.displayTTFeedback(StartController.ttConfig, StartController.view.eventsTable, StartController.view.dependenciesTable);
				} else 
					StartController.logsTextString.set("Warning : the event " + te.getLabel() + "has already been realized.");
				StartController.view.addADelayToAnEventStage.close();
			} catch (NumberFormatException e) {
				StartController.logsTextString.set("Warning : the text field must contain a number");
			}
		}
	}
	
}
