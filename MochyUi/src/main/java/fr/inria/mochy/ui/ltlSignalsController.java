package fr.inria.mochy.ui;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;
import java.util.stream.Stream;

import fr.inria.mochy.MochyVerif.Grammar.Eg1;
import fr.inria.mochy.MochyVerif.Grammar.ParseException;
import fr.inria.mochy.MochyVerif.SLTLVerif.Signal;
import fr.inria.mochy.core.abstractClass.LTL;
import fr.inria.mochy.core.abstractClass.PhysicalModel;
import fr.inria.mochy.core.equalization.EquNet;
import fr.inria.mochy.core.equalization.EquPlace;
import fr.inria.mochy.core.mochysim.Sim;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.util.Pair;

public class ltlSignalsController implements Initializable {
	/** the choice of the method to analyse */
	@FXML ChoiceBox<Method> choiceBox1;
	@FXML ChoiceBox<Method> choiceBox2;
	@FXML ChoiceBox<Method> choiceBox3;

	@FXML TextField checkTextField;
	SimpleStringProperty checkTextValue = new SimpleStringProperty();

	@FXML TextField param1;
	@FXML TextField param2;
	@FXML TextField param3;

	@FXML ChoiceBox<String> operator1;
	@FXML ChoiceBox<String> operator2;
	@FXML ChoiceBox<String> operator3;

	@FXML TextField treshold1;
	@FXML TextField treshold2;
	@FXML TextField treshold3;
	@FXML TextField ltlFormula;
	@FXML TextField maxDate;
	float initialDate = 0;
	@FXML TextField runsNb;
	@FXML TextField resultProbPhi;

	@FXML Button check1;
	@FXML Button check2;
	@FXML Button check3;
	@FXML Button checkLTL;
	@FXML Button launch;

	ArrayList<Pair<Method, String>> methods = new ArrayList<>();

	/**the list of the activated signals (p, q, r) from the user interface*/
	ArrayList<String> indexOfSignalsActivated = new ArrayList<>();

	static Eg1 ltlParser = null;

	String filename;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		filename = new File(StartController.getSimu().getPathLogs()).getParent() + "/out.txt";
		checkTextField.textProperty().bind(checkTextValue);
		checkTextValue.addListener((observable, oldValue, newValue) -> {
			if (newValue.startsWith("Warning")) {
				checkTextField.setStyle("-fx-text-fill: red;");
			} else
				checkTextField.setStyle("-fx-text-fill: green");
		});

		/** get the methods of the loaded class of the physical model */
		PhysicalModel net = StartController.getSimu().getN();
		Method[] methods = net.getClass().getMethods();

		/** convert the methods name to be readable */
		ArrayList<Method> methodsOnly = new ArrayList<>();
		for (int i = 0; i < methods.length; i++) {
			LTL annotation = methods[i].getAnnotation(LTL.class);
			if (annotation != null)
				methodsOnly.add(methods[i]);
		}

		/**
		 * set the behavior of the methods choiceBox, of the methods param textfield and
		 * the operators
		 */
		setSignalBehavior(choiceBox1, methodsOnly, param1, operator1, treshold1, check1);
		setSignalBehavior(choiceBox2, methodsOnly, param2, operator2, treshold2, check2);
		setSignalBehavior(choiceBox3, methodsOnly, param3, operator3, treshold3, check3);
		
		checkLTL.setOnAction(e -> {//launch a run and get the result of the intervals where the formula is true
			checkTextValue.set((String) oneRun(false));
		});
		
		launch.setOnAction(e -> {//launch multiple runs to get the probability where the formula is true
			launchMultipleRuns();
		});
	}
	
	/**perform one run of init/load methods/simu/analyse logs
	 * @param length : to get the length of the interval where 
	 * the LTL formula is true instead of the full interval description
	 * @return the intervals where the LTL formula is true*/
	private Object oneRun(boolean length) {
		Sim simu = StartController.getSimu();
		float maxDate = Float.parseFloat(this.maxDate.getText());
		boolean enableLogsStart = simu.isEnableLogs();
		simu.setEnableLogs(true);
		simu.reset();
		FileWriter outputFile;
		try {
			outputFile = new FileWriter(filename);
			outputFile.write("");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		//load the methods which we want to analyse in the simu
		loadMethods();
		
		// simu :
		while (simu.getN().getTimeElapsed() <= maxDate) {
			simu.oneStep();
		}
		
		//analyze : 
		String command = getLTLFormula();//get the signals syntax and the LTL formula
		InputStream targetStream = new ByteArrayInputStream(command.getBytes());
		if (ltlParser == null)
			ltlParser = new Eg1(targetStream);
		else
			Eg1.ReInit(targetStream);
		simu.setEnableLogs(enableLogsStart);
		return ltlParser.getOutput(length);
	}

	/**load the methods which we want to analyse*/
	private void loadMethods() {
		methods = new ArrayList<Pair<Method, String>>();
		indexOfSignalsActivated = new ArrayList<>();
		getSignalInput("p", choiceBox1, param1);
		getSignalInput("q", choiceBox2, param2);
		getSignalInput("s", choiceBox3, param3);
		StartController.setMethods(methods);
	}

	/**get a declared signal in the user interface and add it to the methods list*/
	private void getSignalInput(String name, ChoiceBox<Method> choiceBox, TextField param) {
		//String result = "";
		if (choiceBox.getSelectionModel().getSelectedItem() != null) {
			Method m = choiceBox.getSelectionModel().getSelectedItem();
			String p = param.getText();
			if (param.isEditable())
				p = "param." + p;
			methods.add(new Pair<Method, String>(m, p));
			//result = name + "=" + m.getDeclaringClass().toString().substring(6) + "." + m.getName() + "=" + p + ";";
			indexOfSignalsActivated.add(name);
		}
		//return result;
	}

	/**get the signals syntax and get the prefix of the LTL formula*/
	private String getLTLFormula() {
		String result = "dec\n";
		int fieldNb = 1;
		for (String field : indexOfSignalsActivated) {
			System.out.println("indexOf : " + field);
			if (field.equals("p"))
				result += getSignalSyntax("p", fieldNb, choiceBox1, param1, operator1, treshold1);
			else if (field.equals("q"))
				result += getSignalSyntax("q", fieldNb, choiceBox2, param2, operator2, treshold2);
			else
				result += getSignalSyntax("s", fieldNb, choiceBox3, param3, operator3, treshold3);
			fieldNb++;
		}
		result += "endec\n" + ltlFormula.getText();
		return result;
	}

	/**get a signal syntax for the LTL formula to analyse the log file*/
	private String getSignalSyntax(String name, int field, ChoiceBox<Method> choiceBox, TextField param,
			ChoiceBox<String> operator, TextField treshold) {
		if (choiceBox.getSelectionModel().getSelectedItem() != null) {
			float finalDate = Float.parseFloat(maxDate.getText());
			String o = operator.getSelectionModel().getSelectedItem();
			float t = Float.parseFloat(treshold.getText());
			return "Signal " + name + " = " + filename.replace("\\", "/") + " " + o + " " + t + " " + field + " "
					+ finalDate + "\n";
		} else
			return "";
	}

	/**
	 * set the behavior of the methods choiceBox, of the methods param textfield,
	 */
	void setSignalBehavior(ChoiceBox<Method> choiceBox, ArrayList<Method> methods, TextField param,
			ChoiceBox<String> operator, TextField treshold, Button check) {
		ObservableList<Method> methodsList = FXCollections.observableArrayList(methods);

		param.setEditable(false);

		/** load the list of the converted methods names */
		choiceBox.setItems(methodsList);

		choiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
				Method m = methods.get((Integer) newValue);
				param.clear();
				if (m.getParameterCount() > 0)
					param.setEditable(true);
				else
					param.setEditable(false);
			}
		});

		ObservableList<String> operators = addOperators();// add <, <=, >, >= and =
		operator.setItems(operators);

		check.setOnAction(evt -> {
			try {
				Method m = choiceBox.getSelectionModel().getSelectedItem();
				String p = "";
				if (m.getParameterCount() > 0)
					p = param.getText();
				String o = operator.getSelectionModel().getSelectedItem();
				String t = treshold.getText();
				callMethod(m, p, o, t);
			} catch (NullPointerException e) {
				checkTextValue.setValue("Warning : no method selected");
			}
		});
	}
	
	/**launch multiple runs to check the probability that the LTL formula is true*/
	private void launchMultipleRuns() {
		int nbRuns = Integer.parseInt(runsNb.getText());
		float probability = 0;
		float initialDate = 0;
		float maxDate = Float.parseFloat(this.maxDate.getText());
		for (int i = 0; i < nbRuns; i++) {
			boolean trueAtDate0 = (boolean) oneRun(true);
			if (trueAtDate0)
				probability += 1;
				//probability += length / (maxDate - initialDate);
		}
		probability /= nbRuns;
		resultProbPhi.setText(Float.toString(probability));
	}

	/**
	 * call a method with the specified parameter and compare the result with the
	 * operator and the treshold <method(<param>)><operator><treshold>
	 * 
	 * @param method
	 * @param param
	 * @param operator
	 * @param treshold
	 */
	String callMethod(Method method, String param, String operator, String treshold) {
		PhysicalModel n = StartController.getSimu().getN();
		String result = "";
		boolean invoked = false;
		checkTextField.setStyle("");
		try {
			if (param.equals(""))
				result = method.invoke(n).toString();
			else {
				if (method.getParameterTypes()[0] == Integer.class)
					result = method.invoke(n, Integer.parseInt(param)).toString();
				else if (method.getParameterTypes()[0] == String.class)
					result = method.invoke(n, param).toString();
			}
			invoked = true;
		} catch (IllegalArgumentException e) {
			result = "Warning : wrong argument Net object or parameter";
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			result = "Warning : can't invoke " + method.getName();
			e.printStackTrace();
		} catch (SecurityException e) {
			result = "Warning : security exception";
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			result = "Warning : illegal access exception";
			e.printStackTrace();
		}
		if (invoked) {
			float t = 0;
			float r = 0;
			try {
				t = Float.parseFloat(treshold);
				r = Float.parseFloat(result);
			} catch (Exception e) {
				result = "Warning : one of the values are not numbers";
			}
			boolean c = false;// comparison
			try {
				switch (operator) {
				case "<":
					c = r < t;
					break;
				case "<=":
					c = r <= t;
					break;
				case ">":
					c = r > t;
					break;
				case ">=":
					c = r >= t;
					break;
				case "==":
					c = r == t;
					break;
				}
				result += " -" + c;
			} catch (NullPointerException e) {
				result = "Warning : No operator is selected <, <=, >, >= or =";
			}
		}
		checkTextValue.setValue(result);
		return result;
	}
	
	/** add the operators to the signals */
	ObservableList<String> addOperators() {
		ArrayList<String> operators = new ArrayList<>();
		operators.add("<");
		operators.add("<=");
		operators.add(">");
		operators.add(">=");
		operators.add("=");
		return FXCollections.observableArrayList(operators);
	}
}
