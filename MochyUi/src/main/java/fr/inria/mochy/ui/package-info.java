/**
 * Manage the windows of the user interface and control the models from MochyCore for simulation.
 * <p>
 *
 * <img src="package.svg" alt="Package class diagram" style="float: right;">
 */
package fr.inria.mochy.ui;
