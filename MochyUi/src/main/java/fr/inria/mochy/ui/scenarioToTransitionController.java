package fr.inria.mochy.ui;

import java.net.URL;
import java.util.ResourceBundle;

import fr.inria.mochy.core.abstractClass.TransitionAbstract;
import fr.inria.mochy.core.mochysim.Delay;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class scenarioToTransitionController implements Initializable{
	
	@FXML TextField delayTF;
	@FXML TextField fromDateTF;
	@FXML TextField toDateTF;	
	@FXML Text text;
	TransitionAbstract t;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		t = StartController.view.transition;
		text.setText("to transition " + t.getName());
	}
	
	@FXML void submit (ActionEvent e) {
		try {
			int transitionNb = t.getNumber();
			float delay = Float.parseFloat(delayTF.getText());
			float fromDate = Float.parseFloat(fromDateTF.getText());
			float toDate = Float.parseFloat(toDateTF.getText());
			Delay d = new Delay(fromDate, toDate, transitionNb, delay);
			StartController.getDelays().add(d);
			StartController.getRpnc().manageDelays();
			View view = StartController.view;
			view.displayFeedback(StartController.simu.displayFeedback(), view.placesTable, view.transitionsTable);
			//StartController.view.stageScenarioToTransition.close();
			Stage stage = (Stage) text.getScene().getWindow();
			stage.close();
		}catch (NumberFormatException exception) {
			StartController.logsTextString.set("Warning : the delay and the dates text fields must be filled by a number.");
		}
	}
	
}
