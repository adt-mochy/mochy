/**
 * Mochy User Interface Module. It implements a JavaFx based User interface for fr.inria.MochyCore
 * <p> 
 * <img src="../package-dependencies.svg" alt="Package dependencies diagram" style="float: right;">
 */
module fr.inria.MochyUi {
    requires javafx.controls;
    requires javafx.fxml;
	requires java.sql;
	requires fr.inria.MochyCore;
	requires javafx.graphics;
	requires javafx.base;
	requires org.controlsfx.controls;
	requires commons.math3;
	requires guru.nidi.graphviz;
	requires org.apache.commons.io;
	requires jzy3d.api;
	requires neuroph;
	requires fr.inria.MochyVerif;

    opens fr.inria.mochy.ui to javafx.fxml;
    exports fr.inria.mochy.ui;
    opens fr.inria.mochy.addons to javafx.fxml;
    exports fr.inria.mochy.addons;
    
} 