package fr.inria.mochy.MochyVerif.Grammar;

/*import java.io.ByteArrayInputStream;
import java.io.InputStream;

import fr.inria.mochy.MochyVerif.Grammar.*;
import fr.inria.mochy.MochyVerif.SLTLVerif.TreeFormula;

public class Formula {
	
	// class for Signal LTL formulas
	// Core Formulas are of the form 
	// \phi ::= true | signal(p) | \phi_1 V \phi_2 |  \not \phi | \phi_1 U_[a,b] \phi_2 
	
	// signal(p) is a boolean signal, given as input, and should appear on a leaf
	// true is the boolean signal which is always true in any interval
	
	// Syntactic sugar : 
	// With easy operator : \phi_1 ^ \phi_2 defined as intersection
	// implication : \phi_1 -> \phi_2
	
	//time-constrained eventually operator
	// <>_[a,b] \phi ::= true U_[a,b] \phi
	// \phi olds at some instant in time interval [a,b]
	
	
	//time-constrained eventually and always operators
	// []_[a,b] \phi ::= \not <>_[a,b] \not \phi
	// \phi always holds in time interval [a,b]
	
	// We assume that formulas are well parenthesized, i.e. 
	
	//(\phi_1) -> ( (\phi_2)  U_[2,3] (\phi_3) ) is correct
	// \phi_1 -> \phi_2  U_[2,3] \phi_3 is not
	
	String name; // the name of the formula 
	String Expression; // the formula itself
	String value; // the truth value computed for the formula

	
	Formula(){
		name=new String("MITL Formula");

		Expression=new String("true");
		value=new String("true");
	}
	
	Formula(String s){
		name=new String("MITL Formula");

		Expression=new String(s);
		value=new String("true");
	}
	
	
	Formula(String n, String s){
		name=new String(n);

		Expression=new String(s);
		value=new String("true");
	}
	
	
/**
 * Creates a parsing tree from a plain text formula 
 * @return
 */
/*	public  void parseExpression() {
		
	    System.out.println("Reading MITL formula from standard input...");
	    Eg1 t = new Eg1(System.in);
	    try {
	    TreeFormula ParsedFormula;
	    
	      t.Start();
	      //n.dump("");
	      System.out.println("Formula correctly parsed");
	      
	      
	      
	    } catch (Exception e) {
	      System.out.println("Oops.");
	      System.out.println(e.getMessage());
	      e.printStackTrace();
	    }
	  }

		
		
		
	
	public static void main(String[] args) {
		// Main method to test Formula parsing
		
		//String initialString = "Signal(\"p\")";
		//String initialString = "NOT( Signal(\"p\") ) ";
		String initialString = "dec\n"
				+ "Signal p = out.txt\n"
				+ "Signal q = out.txt\n"
				+ "endec\n"
				+ "Signal(\"p\") U_[ 30 , 40 ] Signal( \" q \" )";
		//String initialString = "200";
		
	    InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
	
	    System.out.println("Parsing Formula "+ initialString);
	    Eg1 t = new Eg1(targetStream);
	    try {
	    TreeFormula ParsedFormula;
	    
	      t.Start();
	      //n.dump("");
	      System.out.println("Formula correctly parsed");
	      t.drop();
	      System.out.println("Last Integer"+t.LastInteger);
	      
	      
	    } catch (Exception e) {
	      System.out.println("Oops.");
	      System.out.println(e.getMessage());
	      e.printStackTrace();
	    }

	    
	    
}
	
	
}*/
