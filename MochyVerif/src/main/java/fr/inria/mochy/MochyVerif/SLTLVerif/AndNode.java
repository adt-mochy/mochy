package fr.inria.mochy.MochyVerif.SLTLVerif;

public class AndNode extends TreeNode{

	
	// Inherits subformula from Treenode
	// Inherits attachedSignal from TreeNode
	
		
TreeNode left;
TreeNode right;



public AndNode(TreeNode l, TreeNode r) {
	left = l; 
	right = r;
	subformula =new String("And ( " + l + " , "+ r + ")");
	buildSignal();
}

public String toString() {
	return subformula;
}



/**
 * Computed a boolean signal for the current node out of the signals of the children
 * The children's signals must have been evaluated before
 */
void buildSignal() {

	attachedSignal = left.getSignal().intersection(right.getSignal());
	
}



	
	
	
}
