package fr.inria.mochy.MochyVerif.SLTLVerif;

public class Compare {
	float threshold;
	String operator;
	
	Compare(float threshold, String operator){
		this.threshold = threshold;
		this.operator = operator;
	}
	
	public boolean compare(float value) {
		boolean c = false;//comparison
		float v = value;
		float t = threshold;
		switch (operator) {
			case "<":
				c = v < t;
				break;
			case "<=":
				c = v <= t;
				break;
			case ">":
				c = v > t;
				break;
			case ">=":
				c = v >= t;
				break;
			case "=":
				c = v == t;
				break;
		}
		return c;
	}
}
