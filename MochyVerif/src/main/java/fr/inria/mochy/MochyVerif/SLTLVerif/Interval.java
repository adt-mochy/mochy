package fr.inria.mochy.MochyVerif.SLTLVerif;

// a class to define time intervals

public class Interval {
	
	
	float begin; // starting date of the interval 
	float end; // end date of the interval

	
	Interval (float b, float e){
		
		this.begin = b;
		this.end =e;
	}
	
	
	float getBegin() {return begin;}// returns lowed bound of interval
	float getEnd() {return end;} // returns upper bound of interval
	
	
	
	/**
	 * 
	 * @param I2
	 * @return true if the two intervals overlap
	 */
	boolean overlap(Interval I2) {
		
		// I2 strating after current interval but before end of current interval
		if ( ( I2.getBegin() <= this.end) &&(I2.getEnd() >= this.end)) { return true;}
		
		// curren tinterval ending 
		if ( (   this.begin <= I2.getEnd()) &&(  this.end >= I2.getEnd() )) { return true;}

		// I2 contained in current interval
		if ( (this.begin <= I2.getBegin()) && ( this.end >= I2.getEnd() ) ) { return true;}
		
		// current interval contained in I2
		if ( (  I2.getBegin() <= this.begin) && (   I2.getEnd() >= this.end) ) { return true;}

		return false;
		
	}
	
	/**
	 * 
	 * @param a
	 * @return true is a is contained in the current interval
	 */
	boolean contains(float a) {
		return ( (a >= this.begin) && (a<= this.end));
	}
	
	/**
	 * 
	 * @param itv : an interval
	 * @return true if current interval starts before interval itv
	 */
	boolean StartsBefore(Interval itv) {
		return (this.begin < itv.begin);
	}
	
	
	

	/**
	 * 
	 * @param I2 An interval
	 * @return a new inteval, union of the current interval and of the parameter
	 */
	Interval Union(Interval I2) {
		// we assume intervals are not disjoint
		float nb = Math.min(this.begin, I2.getBegin());
		float ne = Math.max(this.end, I2.getEnd());
		return new Interval(nb,ne);
	}
	/**
	 * 
	 * @param I2
	 * @return
	 */
	Interval Intersection(Interval I2) {
		float nb= Math.max(this.begin,I2.getBegin());
		float ne = Math.min(this.end,I2.getEnd());
		
		return new Interval(nb,ne);
	}
	
	
	
	
	
	/**
	 * Performs a left shift of the interval
	 * @param a : lower bound for left shift
	 * @param b : upper bound for left shift
	 * @return The left shift of the interval 
	 * Warning : all bounds of left shift must be greater than 0
	 */
	Interval backShift(float a, float b) {
		
		float nb = Math.max(this.begin - b,0); 
		float ne = Math.max(this.end - a,0); 
		
		return new Interval(nb,ne);
		
	}
	
	
	public String toString() {
		
		return new String("["+this.begin+","+this.end+"]");
	}
	
}
