package fr.inria.mochy.MochyVerif.SLTLVerif;

import java.util.Comparator;

public class IntervalComparator implements Comparator<Interval>{

	@Override
	public int compare(Interval i1, Interval i2) {
		
		if (i1.getBegin() < i2.getBegin()) { return -1; } 
		if (i2.getBegin() < i1.getBegin()) { return 1; } 
		
		return 0;
	}

}
