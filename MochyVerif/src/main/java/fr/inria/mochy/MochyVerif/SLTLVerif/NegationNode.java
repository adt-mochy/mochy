package fr.inria.mochy.MochyVerif.SLTLVerif;

public class NegationNode extends TreeNode {
	
	// Inherits subformula from Treenode
	// Inherits attachedSignal from TreeNode
	
	TreeNode children;// negation nodes have a single children node
	
	/**
	 * Constructor for a negation node without subformula nor children
	 */
	public NegationNode(){
		this.subformula=new String("Negation");
	}
	
	/**
	 * 
	 * @param s : a string representing a subformula to be attached with this node
	 */
	public NegationNode(String s){
		this.subformula=s;
	}
	
	
	public NegationNode(TreeNode tn){
		this.subformula = new String("NOT("+tn.getFormula()+")");
		children = tn;
		attachedSignal = children.getSignal().Negation();
		System.out.println("Negation done"+ attachedSignal);
		
	}
	
	
	
	public String toString() {
		String s = new String(subformula);
		s += "\n"+attachedSignal;
		return s;
	}
	
}
