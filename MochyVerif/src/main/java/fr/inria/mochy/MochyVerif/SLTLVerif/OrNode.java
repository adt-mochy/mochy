package fr.inria.mochy.MochyVerif.SLTLVerif;

public class OrNode extends TreeNode{

	// Inherits subformula from Treenode
		// Inherits attachedSignal from TreeNode
		
			
	TreeNode left;
	TreeNode right;
	
	
	
	public String toString() {
		return subformula;
	}
	
	
	
	/**
	 * Computed a boolean signal for the current node out of the signals of the children
	 * The children's signals must have been evaluated before
	 */
	void buildSignal() {
	
		attachedSignal = left.getSignal().union(right.getSignal());
		
	}
	
	
	
}
