package fr.inria.mochy.MochyVerif.SLTLVerif;

public class Signal {
	String name;
	float threshold;
	float maxdate;
	int fieldnumber;
	String filename;
	String operator;
	
	public Signal(String name, String operator, float threshold, int fieldnumber, float maxdate, String filename){
		this.name = name;
		this.threshold = threshold;
		this.fieldnumber = fieldnumber;
		this.maxdate = maxdate;
		this.filename = filename;
		this.operator = operator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getThreshold() {
		return threshold;
	}

	public void setThreshold(float threshold) {
		this.threshold = threshold;
	}

	public float getMaxdate() {
		return maxdate;
	}

	public void setMaxdate(float maxdate) {
		this.maxdate = maxdate;
	}

	public int getFieldnumber() {
		return fieldnumber;
	}

	public void setFieldnumber(int fieldnumber) {
		this.fieldnumber = fieldnumber;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	
}
