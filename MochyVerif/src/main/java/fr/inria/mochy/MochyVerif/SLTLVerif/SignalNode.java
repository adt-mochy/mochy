package fr.inria.mochy.MochyVerif.SLTLVerif;

public class SignalNode extends TreeNode {

	// Inherits subformula from Treenode
	// Inherits attachedSignal from TreeNode

	
	String SignalName; // The name of the signal --- !!! logical name, not the filename
	String filename; // The file containing the signal (null if the signal does not come from a file
	public float threshold;
	public int fieldnumber;
	public float maxdate;
	public String operator;
	
	public SignalNode(){
		subformula=new String("Signal()");
	}
	

	public SignalNode(String filename){
		subformula=new String("Signal("+ filename+")");
		SignalName=new String(filename);
	}

	public SignalNode(String logName, String fi){
		subformula=new String("Signal("+ logName+")");
		SignalName=new String(logName);
		filename= new String(fi);
	}

	
	
	public SignalNode(float begin,float end,float maxdate){
		subformula= new String("Signal(true)");
		attachedSignal = new booleanSignal(begin,end,maxdate);
	}

	
	/**
	 * Builds a signal from a text file 
	 */
	public void parseSignal() {
		
		System.out.println(this);
		/* bs2.parseFloatField("out.txt", 400, 2, 25); */
System.out.println("Parsing signal " + SignalName + " from file "+filename);
        attachedSignal = new booleanSignal();
		//attachedSignal.parseFloatField(filename, 400, 2, 25);
		attachedSignal.parseFloatField(filename, operator, threshold, fieldnumber, maxdate);
	}
	
	
	
	/*
	 * returns the boolean signal attached to this final node
	 */
	public booleanSignal getSignal(){
		return this.attachedSignal;
		
	}
	
	
	public String toString() {
		String s= new String ("Signal ");
		s = s+ SignalName;
		s = s+ " : "+ filename;
		return s;
		
	}


	public float getThreshold() {
		return threshold;
	}


	public void setThreshold(float threshold) {
		this.threshold = threshold;
	}


	public int getFieldnumber() {
		return fieldnumber;
	}


	public String getSignalName() {
		return SignalName;
	}


	public void setSignalName(String signalName) {
		SignalName = signalName;
	}


	public String getFilename() {
		return filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}


	public void setFieldnumber(int fieldnumber) {
		this.fieldnumber = fieldnumber;
	}


	public float getMaxdate() {
		return maxdate;
	}


	public void setMaxdate(float maxdate) {
		this.maxdate = maxdate;
	}


	public String getOperator() {
		return operator;
	}


	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	
}
