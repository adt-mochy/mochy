package fr.inria.mochy.MochyVerif.SLTLVerif;

import java.util.ArrayList;

// The parsing tree of a Signal LTL formula
// Signal LTL formula are of the form 
// p | phi_1 AND phi_2 | phi_1 OR phi_2 | NOT phi | phi_1 UNTIL_[a,b] phi_2 
// where p is a boolean signal and a,b rational values (or integers)


// The class tree formula is the parsing tree obtained after reading a particular formula

public class TreeFormula {
	
	TreeNode root; // the root of the tree
	String subformula; // the subformula attached to that tree (which parsing is that tree)
	float maximalDuration; // the maximal duration of a boolean signal in the formula. This is needed for negation
	
	
	
	/**
	 * A dummy tree formula to test parser. No arguments
	 */
	public TreeFormula() {
	root= new RootNode();		
		subformula=new String("Test Formula");
	}

	public TreeFormula(String s) {
		root= new RootNode();		
			subformula=new String(s);
		}

	
	/**
	 * A dummy tree formula to test parser. No arguments
	 */
	public TreeFormula(float maxDate) {
			
		subformula=new String("Test Formula");
		maximalDuration=maxDate;
	}

	
	public TreeFormula(TreeNode n, String s) {
	
		root=n;
		subformula=new String(s);
		
	}
	
	
	/**
	 * Computed a boolean signal for the current root out of the signals of the children
	 * The children's signals must have been evaluated before
	 * @return
	 */
	booleanSignal buildSignal() {
		
		booleanSignal newSignal= new booleanSignal();
		
		// TODO : use the functions of booleanSignal to compute this new signal 
		// depending on the type of node 
		
		
		return newSignal;
	}
	
	

}
