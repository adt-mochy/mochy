package fr.inria.mochy.MochyVerif.SLTLVerif;

public class TreeNode {

	public String subformula; // the subformula attached to the subtree starting at that node
	
	public booleanSignal attachedSignal;// the boolean signal computed from the children of the node
	
	
	
	public TreeNode (){
		subformula=new String("Empty Tree Node");
	}
	
	
	
	
	public TreeNode (String sf){
		subformula=new String(sf);
	}
	
	
	/*
	 * getSignal
	 * returns the boolean signal for that node
	 */
	public booleanSignal getSignal() {
		return attachedSignal;
		
	}
	
	public String getFormula() {
		return subformula;
	}
	
}
