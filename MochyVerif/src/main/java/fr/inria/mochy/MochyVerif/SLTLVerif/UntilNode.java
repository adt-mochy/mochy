package fr.inria.mochy.MochyVerif.SLTLVerif;

public class UntilNode extends TreeNode{
	
	
	// The node for a formula of the form 
	// \phi U_[a,b] \psi
	
	TreeNode left; // formula \phi
	TreeNode right; // formula \psi
	
	int a; // start of the until interval
	int b; // end of the until interval

	booleanSignal nodeSignal; // the boolean signal evaluated from operands
	
	
	public UntilNode(Integer f, Integer l, TreeNode lft, TreeNode rgh) {
		
		left = lft;
		right=rgh;
		a=f;
		b = l;
		this.subformula = new String(lft.getFormula() + " U_["+a+","+b+"] "+ rgh.getFormula());
	}
	
	
	
	/*
	 * Fills the nodesignal varaible, computing is from operands
	 */
	public void computeNodeSignal() {
		/* two formulas : phi and psi */
		/* an interval [a,b] */ 
		
		booleanSignal psi = right.getSignal();
		booleanSignal phi=left.getSignal();

		/* phi is cut into unit signals phi_1,.....phi_k	 */
		//Interval[] units = new Interval[phi.size()]; 
		
		/* creations of a table for gamma i's */
		booleanSignal[] gamma = new booleanSignal[phi.size()];
		
		/* compute, for each i in 1..k the formula */
		/* gamma_i = shift(a,b)(phi_i ^ \psi) ^ \phi_i */
		/* nb : we do not need to make an intersection as phi_i is */
		/* a unit signal, it is just a simple filtering */
		
		for (int i=0;i<phi.size();i++) {
			Interval currentphi = phi.getInterval(i);
			booleanSignal pregamma = new booleanSignal();
			
			/* Filtering with phi_i */
			for (Interval it:psi.positiveParts) {
				if (it.overlap(currentphi)) {
					float maxa = Math.max(it.getBegin(),currentphi.getBegin());
					float minb = Math.min(it.getEnd(),currentphi.getEnd());
					pregamma.add(new Interval(maxa,minb));
				}
			}
			/* now, shift pregamma */
			booleanSignal shiftedgamma=pregamma.backshift(this.a, this.b);
			
			booleanSignal filteredgamma=new booleanSignal();
			/* Filtering again with phi_i */
			for (Interval it:psi.positiveParts) {
				if (it.overlap(currentphi)) {
					float maxa = Math.max(it.getBegin(),currentphi.getBegin());
					float minb = Math.min(it.getEnd(),currentphi.getEnd());
					filteredgamma.add(new Interval(maxa,minb));
				}
			}	
			gamma[i]=filteredgamma;
		}
		
		
		/* The result is th union of all \gamma_i's */
		/* Union keeps the sorted characterisitcs of interval, but not */
		/* does not check overlapping */
		booleanSignal resultGamma= gamma[0];
		for (int i=1;i<phi.size();i++) {
			resultGamma.union(gamma[i]);
			resultGamma=resultGamma.groupOverlaping();		
		}
		
		/* keep result as signal for the node */
		nodeSignal=resultGamma;
		
	}
	
	public booleanSignal getSignal() { return nodeSignal;}
	
}
