package fr.inria.mochy.MochyVerif.SLTLVerif;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;


// A boolean signal is a signal that recall all time intervals in which some 
// property (e.g. speed > 50) holds. 
// It is represented by a list of positive intervals 

public class booleanSignal {

	ArrayList<Interval> positiveParts;  // list of positive intervals in signal
	float maxDate; // maximal date on which intervals are defined
	
	
	// ******************************
	// *         Constructors
	// ******************************
	
	
	booleanSignal(){
		positiveParts = new ArrayList<Interval>();
		
	}

	/**
	 * Constructor to create a boolean signal with a single positive interval [0,maxdate]
	 * @param maxdate
	 */
	booleanSignal(float maxdate){
		positiveParts = new ArrayList<Interval>();
		positiveParts.add(new Interval(0,maxdate));
	}

	/**
	 * Constructor to create a boolean signal with a single positive interval [0,maxdate]
	 * @param maxdate
	 */
	booleanSignal(float max, ArrayList<Interval> li){
		positiveParts = li;
		maxDate=max;
	}

	
	
	/**
	 * 
	 * @param mindate : start of the positive interval
	 * @param maxdate : end of the positive interval
	 */
	booleanSignal(float begin, float end, float maxdate){
		this.maxDate=maxdate;
		positiveParts = new ArrayList<Interval>();
		positiveParts.add(new Interval(begin,end));
			
	}
	
	
	/*
	 * Creates a boolean signal from a signal stored in a file 
	 * filename : the name of the file where the signal is stored
	 * threshold :  a value thta must be exceeded to start a posiitve interval. 
	 */
	booleanSignal(String filename, String operator, float threshold, float maxdate){
		// We assume that the file contains one field , 
		// and set as boolean property value(t) > threshold 
		this.parseFloatField(filename, operator, threshold, 1, maxdate);
		
	}
	
	
	/**
	 * Gives the size of a boolean signal (i.e. the number of positive intervals)
	 * @return
	 */
	int size() {
		return positiveParts.size();
	}
	
	/**
	 * 
	 * @return the first interval of the boolean signal
	 */
	Interval getFirstInterval() {
		
		return this.positiveParts.get(0);
		
	};
	
	/**
	 * 
	 * @return the i^th interval of the boolean signal
	 */
	Interval getInterval(int i) {
		
		return this.positiveParts.get(i);
		
	};
	
	// return a new signal that 
	// cuts the current signal ad reduces it to (sub) intervals occurring before 
	// the given date maxdate
	public booleanSignal crop(float maxdate){
		
		booleanSignal bs = new booleanSignal();
		for (Interval itv: positiveParts) {
			if (itv.getBegin()<maxdate) {
				if (itv.getEnd()>maxdate) {
					bs.add(new Interval (itv.getBegin(),maxdate));
				} else {
					bs.add(new Interval (itv.getBegin(),itv.getEnd()));
				}
			}
			
		}
	return bs;	
	}
	
	
	
	
	/**
	 * true if the signal has no positive parts
	 * @return
	 */
	boolean isAllNegative() {
		return (this.positiveParts.size()==0);
	}
	
	/**
	 * 
	 * @param date : a given date
	 * @return trud is a positive interval contains the date
	 */
	public boolean trueAtDate(float date) {
		
		for (Interval itv:positiveParts) {
			if (itv.contains(date)) { return true;}
			
		}
		return false; // no interval containing date was found
	}
	
	
	
	/**
	 * 
	 * @param bs2 : another boolean signal
	 * @return
	 */
	booleanSignal union(booleanSignal bs2) {
		
		booleanSignal result = new booleanSignal();
			// add current intervals to new signal
				for (Interval itv:this.positiveParts) {
					result.add(itv);
				}

				// add current intervals to new signal
				for (Interval itv:bs2.positiveParts) {
					result.add(itv);
				}
				result.sortParts();

				return result;
	}
	
	
	
	booleanSignal intersection(booleanSignal bs2) {
		
		booleanSignal newSignal = new booleanSignal();
		
		for (Interval it2:bs2.positiveParts) { // visit every positive interval in bs2
			
			for (Interval it1:this.positiveParts) { // visit every positive interval in bs2
					if (it1.overlap(it2)) {
						Interval itv = it1.Intersection(it2);
						newSignal.add(itv); 
					}
				
			}
		}
		
		return newSignal;
		
	}
	
	
	
	/**
	 * Sorts positive parts according to their starting date
	 * Note : we use Java's sorting mechanism which is quite efficient
	 */
	private void sortParts(){
		
		Collections.sort(positiveParts, new IntervalComparator() );
		
	}
	
	/**
	 * merges ajdacent overlapping intervals in signals 
	 * to obtain a canonical and reduced representation
	 */
	booleanSignal mergeAdjacent() {
		
		booleanSignal newSignal = new booleanSignal();
		
		if (this.positiveParts.size() == 0 ) { // if current signal is empty
			return newSignal; // return an empty signal
		}
		
		
		// for every interval
		// if it overlaps with the next one 
		// merge both and continue with current interval 
		// otherwise add 
		Interval current= this.getFirstInterval();
		Interval otherInterval; // the next interval in the list of positive signals
		
		for (int i=1;i<this.positiveParts.size();i++) {
		
			otherInterval=this.getInterval(i); // get i^th interval
			if (current.overlap(otherInterval)) { // if the current interval and the ith overlap
				// current interval becomes the union of both
				current=current.Union(otherInterval);
			
			} else { // intervals do not overlap, this is a hole in signal
		
				newSignal.add(current); // add last interval 
				current=otherInterval; // and resume from next one
			}
			
		}
		
		newSignal.add(current); // add the last interval open during exploration
		
		return newSignal;
		
	}
	
	// adds an interval to the list of positive intervals
	// we assume that 
	// 1) Intervals do not verlap
	// 2) the new interval starts at a date greater than the last interval memorized so far
	//
	void add(Interval itv) {
		
		if (positiveParts.size()==0) {
			this.positiveParts.add(itv);
			
		} else {//TODO : add exceptions
		
			Interval LastInt = positiveParts.get(positiveParts.size()-1);
			if (itv.StartsBefore(LastInt)) { 
				System.out.println("Warning, new interval starts before last recorded one");
			}
			
			if (itv.overlap(LastInt)) { 
				System.out.println("Warning, intervals overlap");
			}
	
			this.positiveParts.add(itv);
		}
	}

	
	

	/**
	 * 
	 * @param negLastDate The maximal date of the negated signal
	 * @return a signal that is the negation of the current positive signal in [0,negMaxDate]
	 */
	public booleanSignal Negation(float negMaxDate) {
		
		booleanSignal bs = new booleanSignal();
		
		if (this.isAllNegative()) {// the signal has no positive interval
			
			Interval itv = new Interval(0,maxDate); 
			bs.add(itv);
			return bs;
			
		}
		
		// From this point, we can assume that the current signa is not empty, i.e. it has at least 
		// one positive interval
		
		// an iterator to visit all positive intervals
		Iterator it=positiveParts.iterator();
		
		// get the first interval
		Interval fi= (Interval) it.next();
		
		float firstdate=fi.getBegin();
		float lastdate=fi.getEnd(); // read the last date of the interval
		
		if (!this.trueAtDate(0)) { // If the signal is negative , then the negation starts 
			// at date 0 
			bs.add(new Interval(0,firstdate) );
			
		}
		// The negation resumes after the end of the last positive interval
		
		while (it.hasNext()) {
			fi= (Interval) it.next();
			firstdate=fi.getBegin();
			bs.add(new Interval(lastdate,firstdate) );
			
			lastdate=fi.getEnd();
		}
		
		// if the las positive interval end before the maximal date for the negation, create 
		// a final interval
		if (lastdate < negMaxDate) {
			bs.add(new Interval(lastdate,negMaxDate) );
		}
		return bs;
		
	}
	
	
	/**
	 * removes all intervals of the form [a,a]
	 */
	public void RemoveSingletons() {
		
		ArrayList<Interval> newpp = new ArrayList<Interval>();
		
		for (Interval it : positiveParts) { /* for every positive interval in the signal */
			
			if ( it.getBegin() != it.getEnd()) {
				newpp.add(it);
			}
		}
		
		positiveParts=newpp;
	}
	
	
	
	public booleanSignal Negation() {
		return this.Negation(this.maxDate);// take the maximal date of the boolean signal as upper bound
	}
	
	/**
	 * performs a backshift of a boolean signal
	 * a : value of the shift (for the lower bounds)
	 * b : the other value of the shift (for the upper bound)
	 * Waring : we assume that intervals in the signal are ordered accoring to 
	 * their lower bound
	 */
	public booleanSignal backshift(float a, float b){
		
		ArrayList<Interval> newpp= new ArrayList<Interval>();
		
		/* first shift all intervals in Signal */
		for (Interval it:this.positiveParts) {
		
			float m= it.getBegin();
			float n = it.getEnd();
			if ( n-a > 0) { /* if not the shift occurs before date 0 and must be ignored */
			
				if (m-b<0) { 
					newpp.add(new Interval (0,n-a));
					} else {
						newpp.add(new Interval (m-b,n-a));						
					}
			}
		}
		
		/* At this point, all unit signals have been shifted, but some of them may overlap */
		
		/* group unit signals into overlapping unit signals */
		/* for each group take the min and max dates of each */
		/* !!!!!! We assume that intervals are ordered according to their lower bound */
		/* !!! we assume th signa has at least one interval */
		ArrayList<Interval> mergedUnits = new ArrayList<Interval>();
		Interval lastit = newpp.get(0); /* get the first interval */
		float lasta = lastit.getBegin();
		float lastb = lastit.getEnd();
		mergedUnits .add(lastit);
		
		for (int i=1;i<newpp.size();i++) {
			Interval currentInterval = newpp.get(i); /* read the next unit */
			
			/* if the current Interval and the last interval overlap, merge them */
			if (currentInterval.getBegin() < lastb) {
				lastb = Math.max(lastb,currentInterval.getEnd());
			} else {
				/* the current interval does not overlap, we start a new group */
				/* of possibly ovelapping intervals */
				lastit = currentInterval;
				lasta = lastit.getBegin();
				lastb = lastit.getEnd();
				mergedUnits .add(lastit);
			}	
		}//endfor : all shifted units have been considered 
		
		return new booleanSignal(this.maxDate,mergedUnits);
	}
	
	/**
	 * A method to group cliques of overlapping intervals into a single one 
	 * @return
	 */
	public booleanSignal groupOverlaping() {
	
		ArrayList<Interval> mergedUnits = new ArrayList<Interval>();
		Interval lastit = positiveParts.get(0); /* get the first interval */
		float lasta = lastit.getBegin();
		float lastb = lastit.getEnd();
		mergedUnits.add(lastit);
		
		for (int i=1;i<positiveParts.size();i++) {
			Interval currentInterval = positiveParts.get(i); /* read the next unit */
			
			/* if the current Interval and the last interval overlap, merge them */
			if (currentInterval.getBegin() < lastb) {
				lastb = Math.max(lastb,currentInterval.getEnd());
			} else {
				/* the current interval does not overlap, we start a new group */
				/* of possibly ovelapping intervals */
				lastit = currentInterval;
				lasta = lastit.getBegin();
				lastb = lastit.getEnd();
				mergedUnits .add(lastit);
			}	
		}//endfor : all shifted units have been considered 
		
		return new booleanSignal(this.maxDate,mergedUnits);
		
	}
	
	void parseFloatField(String filename, String operator, float threshold, int fieldNumber, float maxdate) {
		
		String line; // a line of the file
		String[] splitline; // The same line split in different fields
		BufferedReader br; // Buffer to read file 
		float date; // a date read in the file 
		float value; //a value read in the file in field number fieldNumber 

		float newdate; // a new date read in the file 
		float newvalue; //a new value read in the file in field number fieldNumber 
		
		boolean inPositive; // true if we are currently parsing bounds of a positive interval
		float begin=0;// starting date of the currently read positive interval
		int nblines=0; 
		
		Compare compare = new Compare(threshold, operator);
		
		try {
			
			br = new BufferedReader(new FileReader(filename)); 		// Open File
			System.out.println("File "+filename+" open");
			
			// read the first line 
			line = br.readLine();
			splitline = line.split(";"); // Split the line into an array of fields
			
			// Get the date 
			date = Float.parseFloat(splitline[0]); // We assume that the date is always field 0 
			if (date >0 ) { System.out.println("Error, initial value missing");}
			value = Float.parseFloat(splitline[fieldNumber]);
			nblines=1;

			// if the first value is already greater (or equal) than the threshold
			// we start a positive integer at 0 
			if (compare.compare(value)) {
				inPositive=true;
				begin=date; // the interval begin at this date 
			} else { inPositive=false;} //Otherwise we are not yet parsing a positive interval 
			
			while (br.ready()) {// as long as one data value is unread 

				// read each line 
				line = br.readLine();
				splitline = line.split(";"); // Split the line into an array of fields
				
				// Get the date 
				newdate = Float.parseFloat(splitline[0]); // We assume that the date is always field 0 
				newvalue = Float.parseFloat(splitline[fieldNumber]); // the value is on the designated field
				
				
				// If the value has changed, we may have to open or close the interval 
				if (value != newvalue) {
					
					// The interval continues 
					if ( inPositive && compare.compare(newvalue) ) {
						// do nothing 
					}
					
					// A new interval starts 
					if ( !inPositive && compare.compare(newvalue) ) {
						begin = newdate;
						inPositive=true;
					}
					
					// The interval stops. Close it and add it to the list of positive intervals
					if ( inPositive && !compare.compare(newvalue) ) {
						positiveParts.add(new Interval(begin,newdate));
						inPositive=false;
					}

					// still is a negative interval : nothing to do 
					if ( !inPositive && !compare.compare(newvalue) ) {
						// do nothing
					}

					
					// remember the last read value
					date=newdate;
					value=newvalue;
					
				}
			
				
				nblines++;
			}	// end of buffered reading 
			
			if (inPositive) { // if an interval was open, close it at maximal date
				positiveParts.add(new Interval(begin,maxdate));
			}
			
		} catch (IOException e){ 
			System.out.println("Error when opening file filename");
			
		}
		
		System.out.println("Nb lines read : "+nblines);
		
	} // end of parsing
	
	
	public String toString() {// converts signal to string to display positive intervals 
		
		String s= new String(" ");
		for (Interval itv:positiveParts) {
			s=s+itv;
		}
		
		return s;
	}
	
	public String getLength() {//get the length of the interval parts
		Float length = 0f;
		
		for (Interval itv:positiveParts) {
			length += itv.getEnd() - itv.getBegin();
		}
		
		return length.toString();
	}
	
	public static void main(String[] args) {
		// Main method to test Signals construction
		
		booleanSignal bs = new booleanSignal();
		bs.add(new Interval(0.23f,123.34f));
		bs.add(new Interval(142.0f,155.30f));
		
		System.out.println(bs);
		
		booleanSignal bs2 = new booleanSignal();
		
		bs2.parseFloatField("out.txt", ">=", 400, 2, 25);
		System.out.println(bs2);

		
		booleanSignal bs3= new booleanSignal();
		bs3.parseFloatField("adjacent.txt", ">=", 400, 2, 25);
		System.out.println("bs3="+ bs3);
		
		booleanSignal bs4 = bs3.union(bs);
		System.out.println("bs4= bs U bs3"+ bs4);
		
		System.out.println("bs4 merged ="+ bs4.mergeAdjacent());
		
		booleanSignal bs5 = bs3.intersection(bs2);
		System.out.println("bs5= bs2 ^ bs3"+ bs5);
		
		booleanSignal bs6 = bs5.Negation(400);
		System.out.println("bs6= NOT (bs5)" + bs6);
		
	}
	
	
}// end of Class
