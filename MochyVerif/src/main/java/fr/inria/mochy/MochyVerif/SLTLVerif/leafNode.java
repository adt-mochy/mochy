package fr.inria.mochy.MochyVerif.SLTLVerif;

public class leafNode extends TreeNode{

	// Inherits subformula from Treenode
	// Inherits attachedSignal from TreeNode

// leaves of the parsing tree are the nodes of the tree that carry either the 
	// true signal, or a signal read from a file
	
	String filename;// the name of a file containing a signal

	
	leafNode(String filename){
		// creates a leaf node out of a filename
		
	}
	
	leafNode(float maxdate){
		// a node with the true value
		subformula = new String("true");
		Interval itv=new Interval(0,maxdate);
		attachedSignal=new booleanSignal();
		attachedSignal.add(itv);
	}
	
}
