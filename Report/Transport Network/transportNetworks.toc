\babel@toc {USenglish}{}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Transport networks and regulation}{2}{section.2}%
\contentsline {section}{\numberline {3}Models : Petri-nets}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}Semantics of Transport Petri nets with regulation}{5}{subsection.3.1}%
\contentsline {section}{\numberline {4}Traffic Management with a quadratic criterion}{7}{section.4}%
\contentsline {subsection}{\numberline {4.1}Moving blocks and equalization regulation}{7}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Headway Equalization}{7}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Headway optimization as a regulation rule}{8}{subsection.4.3}%
\contentsline {section}{\numberline {5}Traffic management with Neural networks}{8}{section.5}%
\contentsline {subsection}{\numberline {5.1}Neural Networks}{8}{subsection.5.1}%
\contentsline {section}{\numberline {6}Experimentation}{12}{section.6}%
\contentsline {subsection}{\numberline {6.1}Convergence time}{12}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Speed}{13}{subsection.6.2}%
\contentsline {subsubsection}{\numberline {6.2.1}Speed and alpha = 1}{13}{subsubsection.6.2.1}%
\contentsline {subsubsection}{\numberline {6.2.2}Speed and alpha = 10}{13}{subsubsection.6.2.2}%
\contentsline {subsubsection}{\numberline {6.2.3}Speed with a min of 260}{13}{subsubsection.6.2.3}%
\vspace *{\baselineskip }
\contentsline {section}{\numberline {7}References}{14}{section.7}%
