# CI Dockerfile for the building jars


It includes maven java 11 and graphviz 

## Building the image

- `docker build -t registry.gitlab.inria.fr/adt-mochy/mochy/ci-build:latest .` to build the image

## Pushing the image to the docker registry for use by the CI

- `docker login registry.gitlab.inria.fr`  (enter your gitlab login and password
- `docker push registry.gitlab.inria.fr/adt-mochy/mochy/ci-build:latest`
