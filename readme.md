# Mochy

## project structure

* *MochyCore* core library of the simulator
* *MochyCore.test* test project for MochyCore
* *MochyUi* JavaFX based GUI for running the simulator

## Configuration

run : 
	java version : 14.0.2
	javafx version : 11.0.2
	graphviz version : 2.50.0

development : 
	apache maven version : 3.6.3

## Importing the project

git clone https://gitlab.inria.fr/adt-mochy/mochy

## Building the project

most commands can be done either from EClipse IDE or directly on the command line.



### Building from the command line


Build all jars and install them in `$HOME/.m2/repository`

```sh
mvn clean install
```

Some results:

- test reports: in _MochyCore.test/target/surefire-reports_
- final jar for distribution: in _MochyUi/target/MochyUi-x.x.x-SNAPSHOT.jar_


note: using `-Dmaven.repo.local=.m2/repository` makes sure to download and install 
the artefacts locally to the project instead of the $HOME folder 

In order to launch the commands independently you can have a look how
the commands are called in the `.gitlab-ci.yml` as they are used by the continuous integration pipelines.

Result of the code coverage analyzis + checkstyle + findbug is available ins sonarqube: https://sonarqube.inria.fr/sonarqube/dashboard?id=fr.inria.mochy%3Afr.inria.mochy.root 



### Running from the command line

#### from the root of the repository using maven

```sh
mvn -pl MochyUi clean javafx:run
```


#### Run using the resulting jar 
This method can be used by end user having only the jar file _MochyUi-x.x.x-SNAPSHOT.jar_

Linux : 
```sh
java --module-path $JAVAFX_HOME/lib --add-modules javafx.controls,javafx.fxml -jar MochyUi-0.0.1-SNAPSHOT.jar
```
Windows : 
```sh
java --module-path %JAVAFX_HOME%/lib --add-modules javafx.controls,javafx.fxml -jar MochyUi-0.0.1-SNAPSHOT.jar
```

where JAVAFX_HOME environment variable points to the java fx installation on your computer

the use of the display menu to show schemes needs graphviz to be installed and graphviz/bin needs to be set in the environment variable path.


## Website

The address of the website is https://adt-mochy.gitlabpages.inria.fr/mochy/

The distribution website is automatically built from a website desciption in ACSIIDOC. (cf. https://asciidoctor.org/docs/asciidoc-writers-guide/)

To edit asciidoc, you can use the Eclipse plugin  https://marketplace.eclipse.org/content/asciidoctor-editor  (basic editor with WYSIWYG) or the external editor https://asciidocfx.com/ 

A tutorial on ACSIIDOC can be found here: https://www.vogella.com/tutorials/AsciiDoc/article.html

### Build informations

The site is built and deployed using gitlab ci (cf. section `pages:` in https://gitlab.inria.fr/adt-mochy/mochy/-/blob/master/.gitlab-ci.yml 
in order to see how to compile the html pages using asciidoctor)

Note that all the files that must be published (css, images, javascript, jar, resulting html) must be copied in the `public` folder in this job.


## JAVADOC

The javadoc should include all classes as we are provinding a guire for developper



