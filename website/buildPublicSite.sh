#!/bin/bash
set -x #echo on

# copy apidocs
mkdir -p ../public/apidocs/MochyCore
cp -r ../MochyCore/target/apidocs/* ../public/apidocs/MochyCore
mkdir -p ../public/apidocs/MochyUi
cp -r ../MochyUi/target/apidocs/* ../public/apidocs/MochyUi

# copy jars in download folder
mkdir -p ../public/download/latest_build
cp -r download/* ../public/download
cp -r download/.content_desc.html ../public/download
cp  ../MochyUi/target/MochyUi*.jar ../public/download/latest_build
cp  ../Mochy.example.rennes/target/*.zip ../public/download/latest_build
cd ..

# build index pages for download folder
cd public/download
source make_index.sh
cd ../..

# main page
cd website
cp -r image ../public
cp -r css ../css
asciidoctor -r asciidoctor-diagram -D ../public *.asciidoc
cd ..

# user guide page
mkdir -p public/user_guide
cd Documentation/user_guide
cp -r image ../../public/user_guide
asciidoctor -r asciidoctor-diagram -D ../../public/user_guide *.asciidoc
cd ../..

# specification page
mkdir -p public/specification
cd Documentation/specification
cp -r image ../../public/specification
asciidoctor -r asciidoctor-diagram -D ../../public/specification *.asciidoc
cd ../..